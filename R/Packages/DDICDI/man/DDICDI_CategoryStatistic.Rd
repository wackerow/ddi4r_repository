% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_CategoryStatistic}
\alias{DDICDI_CategoryStatistic}
\title{DDICDI_CategoryStatistic  Statistics related to a specific category of an InstanceVariable within a data set.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/CategoryStatistic}{CategoryStatistic}
  Definition
  ============
  Statistics related to a specific category of an InstanceVariable within a data set.
  Examples
  ==========
  The percentage of females from a demographic data set.
  Explanatory notes
  ===================
  Statistics at the data set are used as indicators as to the appropriateness of using a some data.
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_CategoryStatistic$DdiUrn} returns the DDI URN of myDDICDI_CategoryStatistic.} 
  }
}
\subsection{Active Bindings for DDICDI Properties:}{
  \describe{
    \item{\code{categoryStatistic_categoryValue}}{ A read/write active binding.
  Value of the category to which the statistics apply.
  

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_categoryValue <- mycharacter} sets the categoryStatistic_categoryValue property of 'myDDICDI_CategoryStatistic' to the vector of datatype:character

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_categoryValue} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{categoryStatistic_filterValue}}{ A read/write active binding.
  The value of the filter variable to which the category statistic is restricted.
  

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_filterValue <- mycharacter} sets the categoryStatistic_filterValue property of 'myDDICDI_CategoryStatistic' to the vector of datatype:character

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_filterValue} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{categoryStatistic_statistic}}{ A read/write active binding.
  The value of the identified type of statistic for the category. May be repeated to provide unweighted or weighted values and different computation bases.

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_statistic <- list(myDDICDI_Statistic)} sets the 'categoryStatistic_statistic' property of 'myDDICDI_CategoryStatistic' to a list of R6 DDICDI_Statistic class instances

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_statistic} returns a list of of R6 : DDICDI_Statistic class instances}
  }
  \describe{
    \item{\code{categoryStatistic_typeOfCategoryStatistic}}{ A read/write active binding.
  Type of category statistic. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_typeOfCategoryStatistic <- list(myDDICDI_ExternalControlledVocabularyEntry)} sets the 'categoryStatistic_typeOfCategoryStatistic' property of 'myDDICDI_CategoryStatistic' to a list of R6 DDICDI_ExternalControlledVocabularyEntry class instances

                             \code{myDDICDI_CategoryStatistic$categoryStatistic_typeOfCategoryStatistic} returns a list of of R6 : DDICDI_ExternalControlledVocabularyEntry class instances}
  }
}
\subsection{Active Bindings for DDICDI Relationships:}{
  \describe{
    \item{\code{CategoryStatistic_for_Datum}}{ A read/write active binding.
   

                             \code{myDDICDI_CategoryStatistic$CategoryStatistic_for_Datum <- c(myDatumURN)} sets the association 'CategoryStatistic_for_Datum' from 'myDDICDI_CategoryStatistic' to a character vector of DdiUrns for R6 Datum class instances

                             \code{myDDICDI_CategoryStatistic$CategoryStatistic_for_Datum} returns a character vector of of DdiUrns for R6  Datum class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_CategoryStatistic$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, categoryStatistic_categoryValue=NA, categoryStatistic_filterValue=NA, categoryStatistic_statistic=NA, categoryStatistic_typeOfCategoryStatistic=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_CategoryStatistic$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_CategoryStatistic$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_CategoryStatistic$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_CategoryStatistic$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_CategoryStatistic$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_CategoryStatistic$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_CategoryStatistic$new()}}
\item \href{#method-clone}{\code{DDICDI_CategoryStatistic$clone()}}
}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  categoryStatistic_categoryValue = NA,
  categoryStatistic_filterValue = NA,
  categoryStatistic_statistic = NA,
  categoryStatistic_typeOfCategoryStatistic = NA,
  CategoryStatistic_for_Datum = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_CategoryStatistic$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
