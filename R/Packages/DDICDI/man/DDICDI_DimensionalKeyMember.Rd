% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_DimensionalKeyMember}
\alias{DDICDI_DimensionalKeyMember}
\title{DDICDI_DimensionalKeyMember  Single data instance that is part of a dimensional key.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/DimensionalKeyMember}{DimensionalKeyMember}
  Definition
  ============
  Single data instance that is part of a dimensional key. 
  Examples
  ==========
  The "Ontario" string in a dimensional dataset where data points are identified by Province and other dimensions.
  Explanatory notes
  ===================
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDICDI_InstanceValue}}}
   \item{\code{\link{DDICDI_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_DimensionalKeyMember$DdiUrn} returns the DDI URN of myDDICDI_DimensionalKeyMember.} 
  }
}
\subsection{Active Bindings for DDICDI Relationships:}{
  \describe{
    \item{\code{DimensionalKeyMember_hasValueFrom_CodeList}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalKeyMember$DimensionalKeyMember_hasValueFrom_CodeList <- c(myCodeListURN)} sets the association 'DimensionalKeyMember_hasValueFrom_CodeList' from 'myDDICDI_DimensionalKeyMember' to a character vector of DdiUrns for R6 CodeList class instances

                             \code{myDDICDI_DimensionalKeyMember$DimensionalKeyMember_hasValueFrom_CodeList} returns a character vector of of DdiUrns for R6  CodeList class instances}
  }
  \describe{
    \item{\code{DimensionalKeyMember_isBasedOn_DimensionComponent}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalKeyMember$DimensionalKeyMember_isBasedOn_DimensionComponent <- c(myDimensionComponentURN)} sets the association 'DimensionalKeyMember_isBasedOn_DimensionComponent' from 'myDDICDI_DimensionalKeyMember' to a character vector of DdiUrns for R6 DimensionComponent class instances

                             \code{myDDICDI_DimensionalKeyMember$DimensionalKeyMember_isBasedOn_DimensionComponent} returns a character vector of of DdiUrns for R6  DimensionComponent class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_DimensionalKeyMember$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, , ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Super classes}{
\code{\link[DDICDI:DDICDI_Identifiable]{DDICDI::DDICDI_Identifiable}} -> \code{\link[DDICDI:DDICDI_InstanceValue]{DDICDI::DDICDI_InstanceValue}} -> \code{DDICDI_DimensionalKeyMember}
}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_DimensionalKeyMember$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_DimensionalKeyMember$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_DimensionalKeyMember$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_DimensionalKeyMember$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_DimensionalKeyMember$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_DimensionalKeyMember$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_DimensionalKeyMember$new()}}
\item \href{#method-clone}{\code{DDICDI_DimensionalKeyMember$clone()}}
}
}
\if{html}{
\out{<details open ><summary>Inherited methods</summary>}
\itemize{
}
\out{</details>}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  DimensionalKeyMember_hasValueFrom_CodeList = NA,
  DimensionalKeyMember_isBasedOn_DimensionComponent = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyMember$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
