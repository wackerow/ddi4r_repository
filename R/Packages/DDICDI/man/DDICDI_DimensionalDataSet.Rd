% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_DimensionalDataSet}
\alias{DDICDI_DimensionalDataSet}
\title{DDICDI_DimensionalDataSet  Organized collection of multidimensional data.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/DimensionalDataSet}{DimensionalDataSet}
  Definition
  ============
  Organized collection of multidimensional data.
  Examples
  ==========
  A dimensional dataset with Income values in each data point, where the dimensions organizing the data points are Sex and Marital Status.
  Explanatory notes
  ===================
  Similar to Structural N-Cube.
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDICDI_InformationObject}}}
   \item{\code{\link{DDICDI_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_DimensionalDataSet$DdiUrn} returns the DDI URN of myDDICDI_DimensionalDataSet.} 
  }
}
\subsection{Active Bindings for DDICDI Properties:}{
  \describe{
    \item{\code{dimensionalDataSet_name}}{ A read/write active binding.
  A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

                             \code{myDDICDI_DimensionalDataSet$dimensionalDataSet_name <- list(myDDICDI_ObjectName)} sets the 'dimensionalDataSet_name' property of 'myDDICDI_DimensionalDataSet' to a list of R6 DDICDI_ObjectName class instances

                             \code{myDDICDI_DimensionalDataSet$dimensionalDataSet_name} returns a list of of R6 : DDICDI_ObjectName class instances}
  }
}
\subsection{Active Bindings for DDICDI Relationships:}{
  \describe{
    \item{\code{DimensionalDataSet_isStructuredBy_DimensionalDataStructure}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_isStructuredBy_DimensionalDataStructure <- c(myDimensionalDataStructureURN)} sets the association 'DimensionalDataSet_isStructuredBy_DimensionalDataStructure' from 'myDDICDI_DimensionalDataSet' to a character vector of DdiUrns for R6 DimensionalDataStructure class instances

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_isStructuredBy_DimensionalDataStructure} returns a character vector of of DdiUrns for R6  DimensionalDataStructure class instances}
  }
  \describe{
    \item{\code{DimensionalDataSet_represents_ScopedMeasure}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_represents_ScopedMeasure <- c(myScopedMeasureURN)} sets the association 'DimensionalDataSet_represents_ScopedMeasure' from 'myDDICDI_DimensionalDataSet' to a character vector of DdiUrns for R6 ScopedMeasure class instances

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_represents_ScopedMeasure} returns a character vector of of DdiUrns for R6  ScopedMeasure class instances}
  }
  \describe{
    \item{\code{DimensionalDataSet_has_DimensionalKey}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_has_DimensionalKey <- c(myDimensionalKeyURN)} sets the association 'DimensionalDataSet_has_DimensionalKey' from 'myDDICDI_DimensionalDataSet' to a character vector of DdiUrns for R6 DimensionalKey class instances

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_has_DimensionalKey} returns a character vector of of DdiUrns for R6  DimensionalKey class instances}
  }
  \describe{
    \item{\code{DimensionalDataSet_has_DataPoint}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_has_DataPoint <- c(myDataPointURN)} sets the association 'DimensionalDataSet_has_DataPoint' from 'myDDICDI_DimensionalDataSet' to a character vector of DdiUrns for R6 DataPoint class instances

                             \code{myDDICDI_DimensionalDataSet$DimensionalDataSet_has_DataPoint} returns a character vector of of DdiUrns for R6  DataPoint class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_DimensionalDataSet$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, dimensionalDataSet_name=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Super classes}{
\code{\link[DDICDI:DDICDI_Identifiable]{DDICDI::DDICDI_Identifiable}} -> \code{\link[DDICDI:DDICDI_InformationObject]{DDICDI::DDICDI_InformationObject}} -> \code{DDICDI_DimensionalDataSet}
}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_DimensionalDataSet$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_DimensionalDataSet$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_DimensionalDataSet$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_DimensionalDataSet$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_DimensionalDataSet$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_DimensionalDataSet$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_DimensionalDataSet$new()}}
\item \href{#method-clone}{\code{DDICDI_DimensionalDataSet$clone()}}
}
}
\if{html}{
\out{<details open ><summary>Inherited methods</summary>}
\itemize{
}
\out{</details>}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  dimensionalDataSet_name = NA,
  DimensionalDataSet_isStructuredBy_DimensionalDataStructure = NA,
  DimensionalDataSet_represents_ScopedMeasure = NA,
  DimensionalDataSet_has_DimensionalKey = NA,
  DimensionalDataSet_has_DataPoint = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalDataSet$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
