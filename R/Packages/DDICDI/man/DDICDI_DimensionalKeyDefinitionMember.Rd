% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_DimensionalKeyDefinitionMember}
\alias{DDICDI_DimensionalKeyDefinitionMember}
\title{DDICDI_DimensionalKeyDefinitionMember  Single concept that is part of a dimensional key definition.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/DimensionalKeyDefinitionMember}{DimensionalKeyDefinitionMember}
  Definition
  ============
  Single concept that is part of a dimensional key definition.
  Examples
  ==========
  The [Ontario] category in a dimensional dataset where data points are defined by Province and other dimensions.
  Explanatory notes
  ===================
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDICDI_ConceptualValue}}}
   \item{\code{\link{DDICDI_Concept}}}
   \item{\code{\link{DDICDI_AnnotatedIdentifiable}}}
   \item{\code{\link{DDICDI_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_DimensionalKeyDefinitionMember$DdiUrn} returns the DDI URN of myDDICDI_DimensionalKeyDefinitionMember.} 
  }
}
\subsection{Active Bindings for DDICDI Relationships:}{
  \describe{
    \item{\code{DimensionalKeyDefinitionMember_isRepresentedBy_DimensionalKeyMember}}{ A read/write active binding.
   

                             \code{myDDICDI_DimensionalKeyDefinitionMember$DimensionalKeyDefinitionMember_isRepresentedBy_DimensionalKeyMember <- c(myDimensionalKeyMemberURN)} sets the association 'DimensionalKeyDefinitionMember_isRepresentedBy_DimensionalKeyMember' from 'myDDICDI_DimensionalKeyDefinitionMember' to a character vector of DdiUrns for R6 DimensionalKeyMember class instances

                             \code{myDDICDI_DimensionalKeyDefinitionMember$DimensionalKeyDefinitionMember_isRepresentedBy_DimensionalKeyMember} returns a character vector of of DdiUrns for R6  DimensionalKeyMember class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_DimensionalKeyDefinitionMember$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, , ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Super classes}{
\code{\link[DDICDI:DDICDI_Identifiable]{DDICDI::DDICDI_Identifiable}} -> \code{\link[DDICDI:DDICDI_AnnotatedIdentifiable]{DDICDI::DDICDI_AnnotatedIdentifiable}} -> \code{\link[DDICDI:DDICDI_Concept]{DDICDI::DDICDI_Concept}} -> \code{\link[DDICDI:DDICDI_ConceptualValue]{DDICDI::DDICDI_ConceptualValue}} -> \code{DDICDI_DimensionalKeyDefinitionMember}
}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_DimensionalKeyDefinitionMember$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_DimensionalKeyDefinitionMember$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_DimensionalKeyDefinitionMember$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_DimensionalKeyDefinitionMember$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_DimensionalKeyDefinitionMember$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_DimensionalKeyDefinitionMember$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_DimensionalKeyDefinitionMember$new()}}
\item \href{#method-clone}{\code{DDICDI_DimensionalKeyDefinitionMember$clone()}}
}
}
\if{html}{
\out{<details open ><summary>Inherited methods</summary>}
\itemize{
}
\out{</details>}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  DimensionalKeyDefinitionMember_isRepresentedBy_DimensionalKeyMember = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_DimensionalKeyDefinitionMember$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
