% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_IndividualName}
\alias{DDICDI_IndividualName}
\title{DDICDI_IndividualName  The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix. The preferred compilation of the name parts may also be provided. The legal or formal name of the individual should have the isFormal attribute set to true. The preferred name should be noted with the isPreferred attribute. The attribute sex provides information to assist in the appropriate use of pronouns.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/IndividualName}{IndividualName}
  Definition
  ============
  The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix. The preferred compilation of the name parts may also be provided. The legal or formal name of the individual should have the isFormal attribute set to true. The preferred name should be noted with the isPreferred attribute. The attribute sex provides information to assist in the appropriate use of pronouns.
  Examples
  ==========
  Explanatory notes
  ===================
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_IndividualName$DdiUrn} returns the DDI URN of myDDICDI_IndividualName.} 
  }
}
\subsection{Active Bindings for DDICDI Properties:}{
  \describe{
    \item{\code{individualName_abbreviation}}{ A read/write active binding.
  An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.

                             \code{myDDICDI_IndividualName$individualName_abbreviation <- list(myDDICDI_InternationalString)} sets the 'individualName_abbreviation' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_InternationalString class instances

                             \code{myDDICDI_IndividualName$individualName_abbreviation} returns a list of of R6 : DDICDI_InternationalString class instances}
  }
  \describe{
    \item{\code{individualName_context}}{ A read/write active binding.
  A name may be specific to a particular context, i.e. common usage, business, social, etc.. Identify the context related to the specified name. Supports the use of an external controlled vocabulary.

                             \code{myDDICDI_IndividualName$individualName_context <- list(myDDICDI_ExternalControlledVocabularyEntry)} sets the 'individualName_context' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_ExternalControlledVocabularyEntry class instances

                             \code{myDDICDI_IndividualName$individualName_context} returns a list of of R6 : DDICDI_ExternalControlledVocabularyEntry class instances}
  }
  \describe{
    \item{\code{individualName_effectiveDates}}{ A read/write active binding.
  Clarifies when the name information is accurate.

                             \code{myDDICDI_IndividualName$individualName_effectiveDates <- list(myDDICDI_DateRange)} sets the 'individualName_effectiveDates' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_DateRange class instances

                             \code{myDDICDI_IndividualName$individualName_effectiveDates} returns a list of of R6 : DDICDI_DateRange class instances}
  }
  \describe{
    \item{\code{individualName_firstGiven}}{ A read/write active binding.
  First (given) name of the individual.

                             \code{myDDICDI_IndividualName$individualName_firstGiven <- mycharacter} sets the individualName_firstGiven property of 'myDDICDI_IndividualName' to the vector of datatype:character

                             \code{myDDICDI_IndividualName$individualName_firstGiven} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{individualName_fullName}}{ A read/write active binding.
  This provides a means of providing a full name as a single object for display or print such as identification badges etc. For example a person with the name of William Grace for official use may prefer a display name of Bill Grace on a name tag or other informal publication.

                             \code{myDDICDI_IndividualName$individualName_fullName <- list(myDDICDI_InternationalString)} sets the 'individualName_fullName' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_InternationalString class instances

                             \code{myDDICDI_IndividualName$individualName_fullName} returns a list of of R6 : DDICDI_InternationalString class instances}
  }
  \describe{
    \item{\code{individualName_isFormal}}{ A read/write active binding.
  The legal or formal name of the individual should have the isFormal attribute set to true. To avoid confusion only one individual name should have the isFormal attribute set to true. Use the TypeOfIndividualName to further differentiate the type and applied usage when multiple names are provided.

                             \code{myDDICDI_IndividualName$individualName_isFormal <- mylogical} sets the individualName_isFormal property of 'myDDICDI_IndividualName' to the vector of datatype:logical

                             \code{myDDICDI_IndividualName$individualName_isFormal} returns a vector of datatype: logical}
  }
  \describe{
    \item{\code{individualName_isPreferred}}{ A read/write active binding.
  If more than one name for the object is provided, use the isPreferred attribute to indicate which is the preferred name content. All other names should be set to isPreferred="false".

                             \code{myDDICDI_IndividualName$individualName_isPreferred <- mylogical} sets the individualName_isPreferred property of 'myDDICDI_IndividualName' to the vector of datatype:logical

                             \code{myDDICDI_IndividualName$individualName_isPreferred} returns a vector of datatype: logical}
  }
  \describe{
    \item{\code{individualName_lastFamily}}{ A read/write active binding.
  Last (family) name /surname of the individual.

                             \code{myDDICDI_IndividualName$individualName_lastFamily <- mycharacter} sets the individualName_lastFamily property of 'myDDICDI_IndividualName' to the vector of datatype:character

                             \code{myDDICDI_IndividualName$individualName_lastFamily} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{individualName_middle}}{ A read/write active binding.
  Middle name or initial of the individual.

                             \code{myDDICDI_IndividualName$individualName_middle <- mycharacter} sets the individualName_middle property of 'myDDICDI_IndividualName' to the vector of datatype:character

                             \code{myDDICDI_IndividualName$individualName_middle} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{individualName_prefix}}{ A read/write active binding.
  Title that precedes the name of the individual, such as Ms., or Dr.

                             \code{myDDICDI_IndividualName$individualName_prefix <- mycharacter} sets the individualName_prefix property of 'myDDICDI_IndividualName' to the vector of datatype:character

                             \code{myDDICDI_IndividualName$individualName_prefix} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{individualName_sex}}{ A read/write active binding.
  Sex allows for the specification of male, female or neutral. The purpose of providing this information is to assist others in the appropriate use of pronouns when addressing the individual. Note that many countries/languages may offer a neutral pronoun form.

                             \code{myDDICDI_IndividualName$individualName_sex <- list(myDDICDI_SexSpecificationType)} sets the 'individualName_sex' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_SexSpecificationType class instances

                             \code{myDDICDI_IndividualName$individualName_sex} returns a list of of R6 : DDICDI_SexSpecificationType class instances}
  }
  \describe{
    \item{\code{individualName_suffix}}{ A read/write active binding.
  Title that follows the name of the individual, such as Esq. (abbreviation for <font color="#222222">Esquire. This is usually a </font>courtesy title)<font color="#222222">.</font>
  

                             \code{myDDICDI_IndividualName$individualName_suffix <- mycharacter} sets the individualName_suffix property of 'myDDICDI_IndividualName' to the vector of datatype:character

                             \code{myDDICDI_IndividualName$individualName_suffix} returns a vector of datatype: character}
  }
  \describe{
    \item{\code{individualName_typeOfIndividualName}}{ A read/write active binding.
  The type of individual name provided. the use of a controlled vocabulary is strongly recommended. At minimum his should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.

                             \code{myDDICDI_IndividualName$individualName_typeOfIndividualName <- list(myDDICDI_ExternalControlledVocabularyEntry)} sets the 'individualName_typeOfIndividualName' property of 'myDDICDI_IndividualName' to a list of R6 DDICDI_ExternalControlledVocabularyEntry class instances

                             \code{myDDICDI_IndividualName$individualName_typeOfIndividualName} returns a list of of R6 : DDICDI_ExternalControlledVocabularyEntry class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_IndividualName$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, individualName_abbreviation=NA, individualName_context=NA, individualName_effectiveDates=NA, individualName_firstGiven=NA, individualName_fullName=NA, individualName_isFormal=NA, individualName_isPreferred=NA, individualName_lastFamily=NA, individualName_middle=NA, individualName_prefix=NA, individualName_sex=NA, individualName_suffix=NA, individualName_typeOfIndividualName=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_IndividualName$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_IndividualName$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_IndividualName$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_IndividualName$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_IndividualName$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_IndividualName$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_IndividualName$new()}}
\item \href{#method-clone}{\code{DDICDI_IndividualName$clone()}}
}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  individualName_abbreviation = NA,
  individualName_context = NA,
  individualName_effectiveDates = NA,
  individualName_firstGiven = NA,
  individualName_fullName = NA,
  individualName_isFormal = NA,
  individualName_isPreferred = NA,
  individualName_lastFamily = NA,
  individualName_middle = NA,
  individualName_prefix = NA,
  individualName_sex = NA,
  individualName_suffix = NA,
  individualName_typeOfIndividualName = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_IndividualName$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
