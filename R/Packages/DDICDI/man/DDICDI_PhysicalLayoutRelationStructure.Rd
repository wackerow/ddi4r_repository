% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDICDIClasses.R
\docType{class}
\name{DDICDI_PhysicalLayoutRelationStructure}
\alias{DDICDI_PhysicalLayoutRelationStructure}
\title{DDICDI_PhysicalLayoutRelationStructure  Realization of relation structure that is used to describe the sequence of value mappings in a physical layout.}
\format{
\code{\link{R6Class}} object.
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDICDI class \href{https://ddi-alliance.bitbucket.io/DDI-CDI/DDI-CDI_Public_Review_1/2_Model/Field-Level_Documentation/PhysicalLayoutRelationStructure}{PhysicalLayoutRelationStructure}
  Definition
  ============
  Realization of relation structure that is used to describe the sequence of value mappings in a physical layout.
  Examples
  ==========
  The W3C Tabular Data on the Web specification allows for a list datatype. 
  In the example below there are three top level InstanceVariables:
      PersonID – the person identifier;
      AgeYr – age in year;
      BpSys_Dia – blood pressure (a list containing Systolic and Diastolic values).
  There are two variables at a secondary level of the hierarchy:
      Systolic – the systolic pressure;
      Diastolic – the diastolic pressure.
  The delimited file below uses the comma to separate "columns" and forward slash to separate elements of a blood pressure list.
  PersonID, AgeYr, BpSys_Dia
  1,22,119/67
  2,68,122/70
  The PhysicalRelationStructure in this case would describe a BpSys_Dia  list variable as containing an ordered sequence of the Systolic and Diastolic InstanceVariables.
  Explanatory notes
  ===================
  This can be more complex than a simple sequence.
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDICDI_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDICDI_PhysicalLayoutRelationStructure$DdiUrn} returns the DDI URN of myDDICDI_PhysicalLayoutRelationStructure.} 
  }
}
\subsection{Active Bindings for DDICDI Properties:}{
  \describe{
    \item{\code{physicalLayoutRelationStructure_criteria}}{ A read/write active binding.
  Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.).

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_criteria <- list(myDDICDI_InternationalStructuredString)} sets the 'physicalLayoutRelationStructure_criteria' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_InternationalStructuredString class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_criteria} returns a list of of R6 : DDICDI_InternationalStructuredString class instances}
  }
  \describe{
    \item{\code{physicalLayoutRelationStructure_name}}{ A read/write active binding.
  A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided then a context to differentiate usage must be provided as well.

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_name <- list(myDDICDI_ObjectName)} sets the 'physicalLayoutRelationStructure_name' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_ObjectName class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_name} returns a list of of R6 : DDICDI_ObjectName class instances}
  }
  \describe{
    \item{\code{physicalLayoutRelationStructure_purpose}}{ A read/write active binding.
  An explanation of the purpose of the layout relation structure.
  

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_purpose <- list(myDDICDI_InternationalStructuredString)} sets the 'physicalLayoutRelationStructure_purpose' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_InternationalStructuredString class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_purpose} returns a list of of R6 : DDICDI_InternationalStructuredString class instances}
  }
  \describe{
    \item{\code{physicalLayoutRelationStructure_semantics}}{ A read/write active binding.
  Provides semantic context for the relationship.

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_semantics <- list(myDDICDI_ExternalControlledVocabularyEntry)} sets the 'physicalLayoutRelationStructure_semantics' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_ExternalControlledVocabularyEntry class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_semantics} returns a list of of R6 : DDICDI_ExternalControlledVocabularyEntry class instances}
  }
  \describe{
    \item{\code{physicalLayoutRelationStructure_specification}}{ A read/write active binding.
  

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_specification <- list(myDDICDI_SpecificationType)} sets the 'physicalLayoutRelationStructure_specification' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_SpecificationType class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_specification} returns a list of of R6 : DDICDI_SpecificationType class instances}
  }
  \describe{
    \item{\code{physicalLayoutRelationStructure_totality}}{ A read/write active binding.
  Type of relation in terms of totality with respect to an associated collection.

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_totality <- list(myDDICDI_TotalityType)} sets the 'physicalLayoutRelationStructure_totality' property of 'myDDICDI_PhysicalLayoutRelationStructure' to a list of R6 DDICDI_TotalityType class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$physicalLayoutRelationStructure_totality} returns a list of of R6 : DDICDI_TotalityType class instances}
  }
}
\subsection{Active Bindings for DDICDI Relationships:}{
  \describe{
    \item{\code{PhysicalLayoutRelationStructure_structures_PhysicalSegmentLayout}}{ A read/write active binding.
   

                             \code{myDDICDI_PhysicalLayoutRelationStructure$PhysicalLayoutRelationStructure_structures_PhysicalSegmentLayout <- c(myPhysicalSegmentLayoutURN)} sets the association 'PhysicalLayoutRelationStructure_structures_PhysicalSegmentLayout' from 'myDDICDI_PhysicalLayoutRelationStructure' to a character vector of DdiUrns for R6 PhysicalSegmentLayout class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$PhysicalLayoutRelationStructure_structures_PhysicalSegmentLayout} returns a character vector of of DdiUrns for R6  PhysicalSegmentLayout class instances}
  }
  \describe{
    \item{\code{PhysicalLayoutRelationStructure_has_ValueMappingRelationship}}{ A read/write active binding.
   

                             \code{myDDICDI_PhysicalLayoutRelationStructure$PhysicalLayoutRelationStructure_has_ValueMappingRelationship <- c(myValueMappingRelationshipURN)} sets the association 'PhysicalLayoutRelationStructure_has_ValueMappingRelationship' from 'myDDICDI_PhysicalLayoutRelationStructure' to a character vector of DdiUrns for R6 ValueMappingRelationship class instances

                             \code{myDDICDI_PhysicalLayoutRelationStructure$PhysicalLayoutRelationStructure_has_ValueMappingRelationship} returns a character vector of of DdiUrns for R6  ValueMappingRelationship class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDICDI_PhysicalLayoutRelationStructure$new()). A DDI-CDI property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI-CDI R6 object. }
      \item{\code{new(silentState=FALSE, physicalLayoutRelationStructure_criteria=NA, physicalLayoutRelationStructure_name=NA, physicalLayoutRelationStructure_purpose=NA, physicalLayoutRelationStructure_semantics=NA, physicalLayoutRelationStructure_specification=NA, physicalLayoutRelationStructure_totality=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
\section{Super class}{
\code{\link[DDICDI:DDICDI_Identifiable]{DDICDI::DDICDI_Identifiable}} -> \code{DDICDI_PhysicalLayoutRelationStructure}
}
\section{Methods}{
\subsection{Public methods}{
\itemize{
\item \href{#method-print}{\code{DDICDI_PhysicalLayoutRelationStructure$print()}}
\item \href{#method-describe_initializeArguments}{\code{DDICDI_PhysicalLayoutRelationStructure$describe_initializeArguments()}}
\item \href{#method-setSilent}{\code{DDICDI_PhysicalLayoutRelationStructure$setSilent()}}
\item \href{#method-setNotSilent}{\code{DDICDI_PhysicalLayoutRelationStructure$setNotSilent()}}
\item \href{#method-getIsSilent}{\code{DDICDI_PhysicalLayoutRelationStructure$getIsSilent()}}
\item \href{#method-get_ClassGroup}{\code{DDICDI_PhysicalLayoutRelationStructure$get_ClassGroup()}}
\item \href{#method-new}{\code{DDICDI_PhysicalLayoutRelationStructure$new()}}
\item \href{#method-clone}{\code{DDICDI_PhysicalLayoutRelationStructure$clone()}}
}
}
\if{html}{
\out{<details open ><summary>Inherited methods</summary>}
\itemize{
}
\out{</details>}
}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-print"></a>}}
\if{latex}{\out{\hypertarget{method-print}{}}}
\subsection{Method \code{print()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$print(...)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-describe_initializeArguments"></a>}}
\if{latex}{\out{\hypertarget{method-describe_initializeArguments}{}}}
\subsection{Method \code{describe_initializeArguments()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$describe_initializeArguments()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setSilent}{}}}
\subsection{Method \code{setSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$setSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-setNotSilent"></a>}}
\if{latex}{\out{\hypertarget{method-setNotSilent}{}}}
\subsection{Method \code{setNotSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$setNotSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-getIsSilent"></a>}}
\if{latex}{\out{\hypertarget{method-getIsSilent}{}}}
\subsection{Method \code{getIsSilent()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$getIsSilent()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-get_ClassGroup"></a>}}
\if{latex}{\out{\hypertarget{method-get_ClassGroup}{}}}
\subsection{Method \code{get_ClassGroup()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$get_ClassGroup()}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-new"></a>}}
\if{latex}{\out{\hypertarget{method-new}{}}}
\subsection{Method \code{new()}}{
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$new(
  registerThisObject = TRUE,
  silentState = FALSE,
  physicalLayoutRelationStructure_criteria = NA,
  physicalLayoutRelationStructure_name = NA,
  physicalLayoutRelationStructure_purpose = NA,
  physicalLayoutRelationStructure_semantics = NA,
  physicalLayoutRelationStructure_specification = NA,
  physicalLayoutRelationStructure_totality = NA,
  PhysicalLayoutRelationStructure_structures_PhysicalSegmentLayout = NA,
  PhysicalLayoutRelationStructure_has_ValueMappingRelationship = NA,
  ...
)}\if{html}{\out{</div>}}
}

}
\if{html}{\out{<hr>}}
\if{html}{\out{<a id="method-clone"></a>}}
\if{latex}{\out{\hypertarget{method-clone}{}}}
\subsection{Method \code{clone()}}{
The objects of this class are cloneable with this method.
\subsection{Usage}{
\if{html}{\out{<div class="r">}}\preformatted{DDICDI_PhysicalLayoutRelationStructure$clone(deep = FALSE)}\if{html}{\out{</div>}}
}

\subsection{Arguments}{
\if{html}{\out{<div class="arguments">}}
\describe{
\item{\code{deep}}{Whether to make a deep clone.}
}
\if{html}{\out{</div>}}
}
}
}
