% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\docType{class}
\name{DDI4_GeographicExtent}
\alias{DDI4_GeographicExtent}
\title{DDI4_GeographicExtent  Defines the extent of a geographic unit for a specified period of time using Bounding Box, Inclusive and Exclusive Polygons, and an area coverage that notes a type of coverage (land, water, etc}
\format{\code{\link{R6Class}} object.}
\usage{
DDI4_GeographicExtent
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDI4 class \href{https://lion.ddialliance.org/ddiobjects/GeographicExtent}{GeographicExtent} 

  Definition

  ============

  Defines the extent of a geographic unit for a specified period of time using Bounding Box, Inclusive and Exclusive Polygons, and an area coverage that notes a type of coverage (land, water, etc.) and measurement for the coverage. The same geographic extent may be used by multiple versions of a single geographic version or by different geographic units occupying the same spatial area.

  

  Examples

  ==========

  Bounding box for Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837.  Minnesota Land area: 206207.099 sq K, Water area: 18974.589 sq K 

  

  Explanatory notes

  ===================

  Clarifies the source of a change in terms of footprint of an area as opposed to a name or coding change. 

  

  Synonyms

  ==========

  

  

  DDI 3.2 mapping

  =================

  r:GeographicBoundaryType

  

  RDF mapping

  =============

  

  

  GSIM mapping

  ==============
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDI4_AnnotatedIdentifiable}}}
   \item{\code{\link{DDI4_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDI4_GeographicExtent$DdiUrn} returns the DDI URN of myDDI4_GeographicExtent.} 
  }
}
\subsection{Active Bindings for DDI4 Properties:}{
  \describe{
    \item{\code{boundingPolygon}}{ A read/write active binding.
       A description of the boundaries of the polygon either in-line or by a reference to an external file containing the boundaries. Repeatable to describe non-contiguous areas such as islands or Native American Reservations in some parts of the United States.

                             \code{myDDI4_GeographicExtent$boundingPolygon <- list(myPolygon)} sets the 'boundingPolygon' property of 'myDDI4_GeographicExtent' to a list of R6 Polygon class instances

                             \code{myDDI4_GeographicExtent$boundingPolygon} returns a list of of R6 : Polygon class instances}
  }
  \describe{
    \item{\code{excludingPolygon}}{ A read/write active binding.
       A description of a the boundaries of a polygon internal to the bounding polygon which should be excluded. For example, for the bounding polygon describing the State of Brandenburg in Germany, the Excluding Polygon would describe the boundary of Berlin, creating hole within Brandenburg which is occupied by Berlin.

                             \code{myDDI4_GeographicExtent$excludingPolygon <- list(myPolygon)} sets the 'excludingPolygon' property of 'myDDI4_GeographicExtent' to a list of R6 Polygon class instances

                             \code{myDDI4_GeographicExtent$excludingPolygon} returns a list of of R6 : Polygon class instances}
  }
  \describe{
    \item{\code{geographicTime}}{ A read/write active binding.
       A time for which the polygon is an accurate description of the area. This may be a range (without an end date if currently still valid) or a single date when the shape was know to be valid if a range is not available.

                             \code{myDDI4_GeographicExtent$geographicTime <- list(myDateRange)} sets the 'geographicTime' property of 'myDDI4_GeographicExtent' to a list of R6 DateRange class instances

                             \code{myDDI4_GeographicExtent$geographicTime} returns a list of of R6 : DateRange class instances}
  }
  \describe{
    \item{\code{hasAreaCoverage}}{ A read/write active binding.
       Means of describing the area covered by the geographic extent either in its entirety (total area) or specific subsets (land, water, urban, rural, etc.) providing a definition and measurement.

                             \code{myDDI4_GeographicExtent$hasAreaCoverage <- list(myAreaCoverage)} sets the 'hasAreaCoverage' property of 'myDDI4_GeographicExtent' to a list of R6 AreaCoverage class instances

                             \code{myDDI4_GeographicExtent$hasAreaCoverage} returns a list of of R6 : AreaCoverage class instances}
  }
  \describe{
    \item{\code{hasCentroid}}{ A read/write active binding.
       Identifies the centroid of a polygon as a specific point

                             \code{myDDI4_GeographicExtent$hasCentroid <- list(mySpatialPoint)} sets the 'hasCentroid' property of 'myDDI4_GeographicExtent' to a list of R6 SpatialPoint class instances

                             \code{myDDI4_GeographicExtent$hasCentroid} returns a list of of R6 : SpatialPoint class instances}
  }
  \describe{
    \item{\code{locationPoint}}{ A read/write active binding.
       The geographic extent is a single point.

                             \code{myDDI4_GeographicExtent$locationPoint <- list(mySpatialPoint)} sets the 'locationPoint' property of 'myDDI4_GeographicExtent' to a list of R6 SpatialPoint class instances

                             \code{myDDI4_GeographicExtent$locationPoint} returns a list of of R6 : SpatialPoint class instances}
  }
  \describe{
    \item{\code{isSpatialLine}}{ A read/write active binding.
       The geographic extent is a single line

                             \code{myDDI4_GeographicExtent$isSpatialLine <- list(mySpatialLine)} sets the 'isSpatialLine' property of 'myDDI4_GeographicExtent' to a list of R6 SpatialLine class instances

                             \code{myDDI4_GeographicExtent$isSpatialLine} returns a list of of R6 : SpatialLine class instances}
  }
}
\subsection{Active Bindings for DDI4 Relationships:}{
  \describe{
    \item{\code{hasBoundingBox}}{ A read/write active binding.
         A Bounding Box (North, South Latitude and East, West Longitude) for the geographic extent.

                             \code{myDDI4_GeographicExtent$hasBoundingBox <- c(myBoundingBoxURN)} sets the association 'hasBoundingBox' from 'myDDI4_GeographicExtent' to a character vector of DdiUrns for R6 BoundingBox class instances

                             \code{myDDI4_GeographicExtent$hasBoundingBox} returns a character vector of of DdiUrns for R6  BoundingBox class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDI4_GeographicExtent$new()). A DDI 4 property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI 4 R6 object. }
      \item{\code{new(silentState=FALSE, boundingPolygon=NA, excludingPolygon=NA, geographicTime=NA, hasAreaCoverage=NA, hasCentroid=NA, locationPoint=NA, isSpatialLine=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
