% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\docType{class}
\name{DDI4_ClassificationItem}
\alias{DDI4_ClassificationItem}
\title{DDI4_ClassificationItem  A Classification Item represents a Category at a certain Level within a Statistical Classification}
\format{\code{\link{R6Class}} object.}
\usage{
DDI4_ClassificationItem
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDI4 class \href{https://lion.ddialliance.org/ddiobjects/ClassificationItem}{ClassificationItem} 

  Definition

  ============

  A Classification Item represents a Category at a certain Level within a Statistical Classification.

  

  

  Examples

  ==========

  In the  2012 NAICS (North American Industry Classification System) one Classification Item has the Code 23 and the Category construction.

  

  Explanatory notes

  ===================

  A Classification Item defines the content and the borders of the Category. A Unit can be classified to one and only one item at each Level of a Statistical Classification. As such a Classification Item is a placeholder for a position in a StatisitcalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. 

  

  This differentiates it from Code which is a kind of Designation, in particular it is an alphanumeric string assigned to stand in place of a category. For example, the letter M might stand for the category Male in the CodeList called Gender.

  

  Synonyms

  ==========

  

  

  DDI 3.2 mapping

  =================

  

  

  RDF mapping

  =============

  

  

  GSIM mapping

  ==============

  Classification Item
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDI4_Designation}}}
   \item{\code{\link{DDI4_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDI4_ClassificationItem$DdiUrn} returns the DDI URN of myDDI4_ClassificationItem.} 
  }
}
\subsection{Active Bindings for DDI4 Properties:}{
  \describe{
    \item{\code{isValid}}{ A read/write active binding.
       Indicates whether or not the item is currently valid. If updates are allowed in the Statistical Classification, an item may be restricted in its validity, i.e. it may become valid or invalid after the Statistical Classification has been released. 

                             \code{myDDI4_ClassificationItem$isValid <- mylogical} sets the isValid property of 'myDDI4_ClassificationItem' to the vector of datatype:logical

                             \code{myDDI4_ClassificationItem$isValid} returns a vector of datatype: logical}
  }
  \describe{
    \item{\code{isGenerated}}{ A read/write active binding.
       Indicates whether or not the item has been generated to make the level to which it belongs complete

                             \code{myDDI4_ClassificationItem$isGenerated <- mylogical} sets the isGenerated property of 'myDDI4_ClassificationItem' to the vector of datatype:logical

                             \code{myDDI4_ClassificationItem$isGenerated} returns a vector of datatype: logical}
  }
  \describe{
    \item{\code{explanatoryNotes}}{ A read/write active binding.
       A Classification Item may be associated with explanatory notes, which further describe and clarify the contents of the Category. Explanatory notes consist of: General note: Contains either additional information about the Category, or a general description of the Category, which is not structured according to the "includes", "includes also", "excludes" pattern. Includes: Specifies the contents of the Category. Includes also: A list of borderline cases, which belong to the described Category. Excludes: A list of borderline cases, which do not belong to the described Category. Excluded cases may contain a reference to the Classification Items to which the excluded cases belong.

                             \code{myDDI4_ClassificationItem$explanatoryNotes <- list(myInternationalStructuredString)} sets the 'explanatoryNotes' property of 'myDDI4_ClassificationItem' to a list of R6 InternationalStructuredString class instances

                             \code{myDDI4_ClassificationItem$explanatoryNotes} returns a list of of R6 : InternationalStructuredString class instances}
  }
  \describe{
    \item{\code{futureNotes}}{ A read/write active binding.
       The future events describe a change (or a number of changes) related to an invalid item. These changes may e.g. have turned the now invalid item into one or several successor items. This allows the possibility to follow successors of the item in the future.

                             \code{myDDI4_ClassificationItem$futureNotes <- list(myInternationalString)} sets the 'futureNotes' property of 'myDDI4_ClassificationItem' to a list of R6 InternationalString class instances

                             \code{myDDI4_ClassificationItem$futureNotes} returns a list of of R6 : InternationalString class instances}
  }
  \describe{
    \item{\code{changeLog}}{ A read/write active binding.
       Describes the changes, which the item has been subject to during the life time of the actual Statistical Classification.

                             \code{myDDI4_ClassificationItem$changeLog <- list(myInternationalString)} sets the 'changeLog' property of 'myDDI4_ClassificationItem' to a list of R6 InternationalString class instances

                             \code{myDDI4_ClassificationItem$changeLog} returns a list of of R6 : InternationalString class instances}
  }
  \describe{
    \item{\code{changeFromPreviousVersion}}{ A read/write active binding.
       Describes the changes, which the item has been subject to from the previous version to the actual Statistical Classification

                             \code{myDDI4_ClassificationItem$changeFromPreviousVersion <- list(myInternationalString)} sets the 'changeFromPreviousVersion' property of 'myDDI4_ClassificationItem' to a list of R6 InternationalString class instances

                             \code{myDDI4_ClassificationItem$changeFromPreviousVersion} returns a list of of R6 : InternationalString class instances}
  }
  \describe{
    \item{\code{validDates}}{ A read/write active binding.
       Dates for which the classification is valid. Date from which the item became valid. The date must be defined if the item belongs to a floating Statistical classification. Date at which the item became invalid. The date must be defined if the item belongs to a floating Statistical classification and is no longer valid

                             \code{myDDI4_ClassificationItem$validDates <- list(myDateRange)} sets the 'validDates' property of 'myDDI4_ClassificationItem' to a list of R6 DateRange class instances

                             \code{myDDI4_ClassificationItem$validDates} returns a list of of R6 : DateRange class instances}
  }
  \describe{
    \item{\code{name}}{ A read/write active binding.
       A Classification Item has a name as provided by the owner or maintenance unit. The name describes the content of the category. The name is unique within the Statistical Classification to which the item belongs, except for categories that are identical at more than one level in a hierarchical classification. Use the context attribute to identify Official Name or alternate names for the Classification Item.

                             \code{myDDI4_ClassificationItem$name <- list(myObjectName)} sets the 'name' property of 'myDDI4_ClassificationItem' to a list of R6 ObjectName class instances

                             \code{myDDI4_ClassificationItem$name} returns a list of of R6 : ObjectName class instances}
  }
  \describe{
    \item{\code{representation}}{ A read/write active binding.
       The value as expressed in the data file. 

                             \code{myDDI4_ClassificationItem$representation <- list(myValueString)} sets the 'representation' property of 'myDDI4_ClassificationItem' to a list of R6 ValueString class instances

                             \code{myDDI4_ClassificationItem$representation} returns a list of of R6 : ValueString class instances}
  }
}
\subsection{Active Bindings for DDI4 Relationships:}{
  \describe{
    \item{\code{caseLaw}}{ A read/write active binding.
         Case law rulings related to the Classification Item.

                             \code{myDDI4_ClassificationItem$caseLaw <- c(myAuthorizationSourceURN)} sets the association 'caseLaw' from 'myDDI4_ClassificationItem' to a character vector of DdiUrns for R6 AuthorizationSource class instances

                             \code{myDDI4_ClassificationItem$caseLaw} returns a character vector of of DdiUrns for R6  AuthorizationSource class instances}
  }
  \describe{
    \item{\code{exclude}}{ A read/write active binding.
         Classification Items to which the excluded cases belong (as described in explanatoryNotes).

                             \code{myDDI4_ClassificationItem$exclude <- c(myClassificationItemURN)} sets the association 'exclude' from 'myDDI4_ClassificationItem' to a character vector of DdiUrns for R6 ClassificationItem class instances

                             \code{myDDI4_ClassificationItem$exclude} returns a character vector of of DdiUrns for R6  ClassificationItem class instances}
  }
  \describe{
    \item{\code{denotes}}{ A read/write active binding.
         A definition for the code. Specialization of denotes for Categories.

                             \code{myDDI4_ClassificationItem$denotes <- c(myCategoryURN)} sets the association 'denotes' from 'myDDI4_ClassificationItem' to a character vector of DdiUrns for R6 Category class instances

                             \code{myDDI4_ClassificationItem$denotes} returns a character vector of of DdiUrns for R6  Category class instances}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDI4_ClassificationItem$new()). A DDI 4 property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI 4 R6 object. }
      \item{\code{new(silentState=FALSE, isValid=NA, isGenerated=NA, explanatoryNotes=NA, futureNotes=NA, changeLog=NA, changeFromPreviousVersion=NA, validDates=NA, name=NA, representation=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
