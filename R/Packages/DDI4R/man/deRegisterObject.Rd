% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\name{deRegisterObject}
\alias{deRegisterObject}
\title{deRegisterObject - Deregisters a list of DDI R6 objects by DDI URN}
\usage{
deRegisterObject(DdiUrns)
}
\arguments{
\item{DdiUrns}{a vector of DDI URNs or UUIDs (required)
An object of class character of length 1.}
}
\description{
This removes a list of DDI URNs from the hash table in the environment ddi.env.urn is used for managing DDI URNs.
}
\note{
to do: only one DDI URN should reference to one object
}
\examples{
 deRegisterObject("urn:ddi:example.org:myID12:1")

}
