% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\docType{class}
\name{DDI4_BoundingBox}
\alias{DDI4_BoundingBox}
\title{DDI4_BoundingBox  A type of Spatial coverage describing a rectangular area within which the actual range of location fits}
\format{\code{\link{R6Class}} object.}
\usage{
DDI4_BoundingBox
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDI4 class \href{https://lion.ddialliance.org/ddiobjects/BoundingBox}{BoundingBox} 

  Definition

  ============

  A type of Spatial coverage describing a rectangular area within which the actual range of location fits. A BoundingBox is described by 4 numbers - the maxima of the north, south, east, and west coordinates found in the area.

  

  Examples

  ==========

  Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837

  

  Explanatory notes

  ===================

  A BoundingBox is often described by two x,y coordinates where the x coordinates are used for the North and South Latitudes and y coordinates for the West and East Longitudes

  

  Synonyms

  ==========

  r:BoundingBox

  

  DDI 3.2 mapping

  =================

  

  

  RDF mapping

  =============

  

  

  GSIM mapping

  ==============
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
   \item{\code{\link{DDI4_AnnotatedIdentifiable}}}
   \item{\code{\link{DDI4_Identifiable}}}
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDI4_BoundingBox$DdiUrn} returns the DDI URN of myDDI4_BoundingBox.} 
  }
}
\subsection{Active Bindings for DDI4 Properties:}{
  \describe{
    \item{\code{eastLongitude}}{ A read/write active binding.
       The easternmost coordinate expressed as a decimal between the values of -180 and 180 degrees 

                             \code{myDDI4_BoundingBox$eastLongitude <- mynumeric} sets the eastLongitude property of 'myDDI4_BoundingBox' to the vector of datatype:numeric

                             \code{myDDI4_BoundingBox$eastLongitude} returns a vector of datatype: numeric}
  }
  \describe{
    \item{\code{westLongitude}}{ A read/write active binding.
       The westernmost coordinate expressed as a decimal between the values of -180 and 180 degrees 

                             \code{myDDI4_BoundingBox$westLongitude <- mynumeric} sets the westLongitude property of 'myDDI4_BoundingBox' to the vector of datatype:numeric

                             \code{myDDI4_BoundingBox$westLongitude} returns a vector of datatype: numeric}
  }
  \describe{
    \item{\code{northLatitude}}{ A read/write active binding.
       The northernmost coordinate expressed as a decimal between the values of -90 and 90 degrees.

                             \code{myDDI4_BoundingBox$northLatitude <- mynumeric} sets the northLatitude property of 'myDDI4_BoundingBox' to the vector of datatype:numeric

                             \code{myDDI4_BoundingBox$northLatitude} returns a vector of datatype: numeric}
  }
  \describe{
    \item{\code{southLatitude}}{ A read/write active binding.
       The southermost latitude expressed as a decimal between the values of -90 and 90 degrees

                             \code{myDDI4_BoundingBox$southLatitude <- mynumeric} sets the southLatitude property of 'myDDI4_BoundingBox' to the vector of datatype:numeric

                             \code{myDDI4_BoundingBox$southLatitude} returns a vector of datatype: numeric}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDI4_BoundingBox$new()). A DDI 4 property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI 4 R6 object. }
      \item{\code{new(silentState=FALSE, eastLongitude=NA, westLongitude=NA, northLatitude=NA, southLatitude=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
