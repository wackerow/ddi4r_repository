% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\docType{class}
\name{DDI4_CommandFile}
\alias{DDI4_CommandFile}
\title{DDI4_CommandFile  Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script}
\format{\code{\link{R6Class}} object.}
\usage{
DDI4_CommandFile
}
\value{
Content carrying object of class \code{\link{R6Class}}.
}
\description{
Implementation of the DDI4 class \href{https://lion.ddialliance.org/ddiobjects/CommandFile}{CommandFile} 

  Definition

  ============

  Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file, a description of the location of the file , and a URN or URL for the command file.

  

  Examples

  ==========

  

  

  Explanatory notes

  ===================

  

  

  Synonyms

  ==========

  

  

  DDI 3.2 mapping

  =================

  r:CommandFileType

  

  RDF mapping

  =============

  

  

  GSIM mapping

  ==============
}
\section{Ancestry}{

Inherits from (direct parent listed first):
 \itemize{
 }
}

\section{Active Bindings}{

  Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.
\subsection{Common Active Bindings:}{
  \describe{
    \item{\code{DdiUrn}}{ read only - \code{myDDI4_CommandFile$DdiUrn} returns the DDI URN of myDDI4_CommandFile.} 
  }
}
\subsection{Active Bindings for DDI4 Properties:}{
  \describe{
    \item{\code{programLanguage}}{ A read/write active binding.
       Designates the programming language used for the command. Supports the use of a controlled vocabulary.

                             \code{myDDI4_CommandFile$programLanguage <- list(myExternalControlledVocabularyEntry)} sets the 'programLanguage' property of 'myDDI4_CommandFile' to a list of R6 ExternalControlledVocabularyEntry class instances

                             \code{myDDI4_CommandFile$programLanguage} returns a list of of R6 : ExternalControlledVocabularyEntry class instances}
  }
  \describe{
    \item{\code{location}}{ A read/write active binding.
       A description of the location of the file. This may not be machine actionable. It supports a description expressed in multiple languages.

                             \code{myDDI4_CommandFile$location <- list(myInternationalString)} sets the 'location' property of 'myDDI4_CommandFile' to a list of R6 InternationalString class instances

                             \code{myDDI4_CommandFile$location} returns a list of of R6 : InternationalString class instances}
  }
  \describe{
    \item{\code{uri}}{ A read/write active binding.
       The URL or URN of the command file.

                             \code{myDDI4_CommandFile$uri <- mycharacter} sets the uri property of 'myDDI4_CommandFile' to the vector of datatype:character

                             \code{myDDI4_CommandFile$uri} returns a vector of datatype: character}
  }
}
\subsection{Other methods:}{
  \describe{
      \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of DDI4_CommandFile$new()). A DDI 4 property or association is the name of a description vector. The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument. An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }
      \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed These include messages about the object not being registered, or not found through the XML Catalog. Silent mode is useful when importing a set of objects that may not be in any particular order }
      \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed These include messages about the object not being registered, or not found through the XML Catalog. Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }
      \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}
      \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}
      \item{\code{print()}}{prints the object with a layout specific to a DDI 4 R6 object. }
      \item{\code{new(silentState=FALSE, programLanguage=NA, location=NA, uri=NA, ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the "Identifiable" class also have arguments of "agency", "id", and "version", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}
   }
}
}

\keyword{content}
