% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DDI4RClasses.R
\name{getObject}
\alias{getObject}
\title{getObject - Gets DDI object searching the registry for its DDI URN}
\usage{
getObject(DdiUrn = NA)
}
\arguments{
\item{DdiUrn}{DDI URN (required)
An object of class character of length 1.}
}
\value{
Reference to DDI R6 object
}
\description{
Returns the reference to a DDI R6 object
Note that this is called a LOT, so to speed it up it does not normalize the argument.
}
\examples{
 getObject("urn:ddi:example.org:myID12:1"), getObject(normalizeDdiUrn(" URN:ddi:example.org:myID12:1  "))

}
