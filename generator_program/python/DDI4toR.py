# -*- coding: utf-8 -*-
"""
Created on Mon Nov 14 08:59:27 2016
Parts from DDI4ModelViewer.py

Revised 2018_04_24 to produce R classes
Revised 2018_07_02 to use active bindings and a functional approach
Other revisions through July 2018
August 2018: relationships stored internally as DDI URNs, functions allow setting via R6 object 
September 2018: added describeObject and ddiXmlNodeToObject, added checks oon isPersistent
September20, 2018 added validateObjectReferences and importDdi4XmlFile
November 12, 2018 - renamed all R6 classes with the prefix "DDI4_"
November 20, 2018  added some exception handling to XML import and export
May, 2019  modified to read xmi file from C:\ddrive\projects\various\Python\DDI4AndR with Lion development in abeyence
June, 2019  changed UUIDgenerate to TRUE to use time based generation. This should avoid the problem of duplicate UUIDs in Windows.
June, 2019 cleaned up functioni names and all functions to lowerCamel case
July1, 2019 vectorized nameToDdiUrn

This work is licensed under a Creative Commons Attribution 4.0 International License.
https://creativecommons.org/licenses/by/4.0/

@author: Larry Hoyle
"""

import os
import sys
import re
import xml.etree.ElementTree as ET
import datetime


# get the most recent xmi.xml file of the PlatformIndependent DDI4 model
#     from the DDI development website

#import requests
#res = requests.get('http://lion.ddialliance.org/xmi.xml')
#res.status_code == requests.codes.ok
#print('Processing the following version of the DDI4 xmi.xml Platform Independent Model:')
#print(res.text[:167])

# parse the root element of the XML file into an XML tree
#rootDDI4XMI = ET.fromstring(res.text)
# ---------------------------------------------------------
# read the xmi from a file - Lion is no longer the official source

xmiFile = open("C:\\ddrive\\projects\\various\\Python\\DDI4AndR\\xmi.xml", mode="r", encoding="UTF-8") 
xmiText = xmiFile.read()
print('Processing the following version of the DDI4 xmi.xml Platform Independent Model:')
print(xmiText[:167])
xmiFile.close()
# parse the root element of the XML file into an XML tree
rootDDI4XMI = ET.fromstring(xmiText)



# get the default folder from the command line 
#otherwise default to this
includeComments=False
DataFolder = "C:\\Ddrive\\projects\\various\\Python\\DDI4AndR\\2019_07\\"
#  was hard coded, now is retieved from package
# CatalogFilePath = "file:///C:/Ddrive/projects/various/Python/DDI4AndR/2018_09/ddiurn_catalog.xml"



if len(sys.argv)> 1:
    if os.path.isdir(sys.argv[1]):
        DataFolder = sys.argv[1]

if not os.path.isdir(DataFolder):
    print('WARNING: output directory does not exist\n')
    sys.exit()    
else: 
    print('\nNOTE: Data files will be output to:\n    ' + DataFolder + '\n' )

# ----------for testing read a local static copy of the xmi 
#with open(DataFolder + "xmi.xml", 'r', encoding="utf-8") as f:
#    XMItext = f.read()
#rootDDI4XMI = ET.fromstring(XMItext)
# ---------------------------------------------------------





print(rootDDI4XMI.tag)


DebugFile = open(DataFolder + "Debug.txt", 'w', encoding="utf-8")



# primitive datatypes will be treated differently than R6 classes. As values of properties
# even if the cardinality of a property or relationship is 0..1 or 1..1 the value of the property will be a compound object
# primitive datatypes  will be R vectors
# R6 classes will be in lists
# this will simplify the logic of software processing DDI R6 objects. 
# Sofftware will not need to know if the value is a special case.

# list all of the possible primitive datatypes
primitiveDictionary = {"http://schema.omg.org/spec/UML/2.1/uml.xml#Boolean":"logical",
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#UnlimitedNatural":"character",
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#Real":"numeric", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#Date":"Date", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#Decimal":"numeric", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#Integer":"integer", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#String":"character", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#anyURI":"character", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#numeric":"numeric", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#Float":"numeric", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#NMTOKEN":"character", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#NMTOKENS":"character", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#DateTime":"POSIXlt", 
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#language":"character", 
                       "xhtml:BlkNoForm.mix":"character", 
                       "LanguageSpecification":"character", 
                       "xs:language":"character",
                       "http://schema.omg.org/spec/UML/2.1/uml.xml#NonNegativeInteger":"integer",
                       "http://www.w3.org/2001/XMLSchema#anyURI":"character"}

# define a function to rename classes
#  an example is Date, which R just can't handle as a user defined class
					   
def classRename(className):
   newName = {}
    #  newName = {"Date": "DDI4Date"}   with prefix of DDI4_ this is no longer needed   
   if className in newName.keys():
     return newName[className]   
   else:	 
     return className

# for each view get a list of its classes
#  make a dictionary of those lists keyed on the view name
# the use of diagrams may change with the canonical XMI                       

diagrams = {}

for diagram in rootDDI4XMI.iter('diagram'): 
    view = diagram.find('model').attrib.get('package')
    if view != 'ComplexDataTypes':
        #print(view)
        classList = []
        for element in diagram.find('elements'):
            classList.append(element.attrib.get(classRename('subject')))
        #print('   '+str(classList))
        diagrams[view] = classList
#print(str(diagrams))



# define a function to return a list of the Views in which a class appears

def viewList(className, diagrams ):
# given a class name return a list of the Views in which it appears     
    viewList = []
    for viewName, viewClassList in diagrams.items():        
        if className in viewClassList:
            viewList.append(viewName)
    if len(viewList)>0:
        return viewList
    else:
        return    

# define a function to build a human-readable cardinality string
    
def cardinalityString(lowerValue, upperValue):
# Given the lower and upper values from an xmi file, return a string e.g."0..n"
    if lowerValue == None:
        lowerValue = "0"
    elif lowerValue == "-1":
        lowerValue = "n"
    if upperValue == None:
        upperValue = "0"
    elif upperValue == "-1":
        upperValue = "n" 
    return lowerValue + ".." + upperValue  

	
# add a list of attributes to the field entry in allFields for this lookup class (the class or its ancestor)
def getClassFields(className, lookupClass, allFields):
  if lookupClass in classProperties.keys():
    for objectProperty in classProperties[lookupClass]: 
      cardinality =   cardinalityString(classProperties[lookupClass][objectProperty]['minimumCardinality'],
                                                        classProperties[lookupClass][objectProperty]['maximumCardinality'])
      if classProperties[lookupClass][objectProperty]["dataType"] in primitiveDictionary.keys():
        propertyDatatype =  primitiveDictionary[classProperties[lookupClass][objectProperty]["dataType"]] 
      else:
        propertyDatatype =  classProperties[lookupClass][objectProperty]["dataType"]      
      
      if className in  allFields.keys():      # add to the sub dictionary only if this property is not there
        if objectProperty not in allFields[className].keys() :  
          allFields[className][objectProperty] =  [cardinality,
                   propertyDatatype,
                   classProperties[lookupClass][objectProperty]['refersTo']] 
      else:                                            # create a new subdictionary
        allFields[className] = {objectProperty:  [cardinality,
                 propertyDatatype,
                 classProperties[lookupClass][objectProperty]['refersTo']] }         
          
  if lookupClass in classRelations.keys():  
    for objectRelationKey in classRelations[lookupClass].keys(): 
      #print('objectRelationKey  ' + className + ' ' + lookupClass + '.' + objectRelationKey + '\n' )
      if  objectRelationKey !=  "realizes":  
        if className in  allFields.keys(): # add to the sub dictionary only if this relation is not there
          if objectRelationKey not in allFields[className].keys() :   
            #print('    and class  ' +  objectRelationKey + '\n' )  
            allFields[className][objectRelationKey] =  [classRelations[lookupClass][objectRelationKey]['targetCardinality'],
                     classRelations[lookupClass][objectRelationKey]['relationTarget'],
                     'DdiClass'] 
        else:
          #print('    first class' +  objectRelationKey + '\n' )    
          allFields[className] = {objectRelationKey: [classRelations[lookupClass][objectRelationKey]['targetCardinality'],
                   classRelations[lookupClass][objectRelationKey]['relationTarget'],
                   'DdiClass'] }
  return allFields



# the classProperties dictionary will contain a dictionary of properties 
# for each class with properties
#     in that dictionary there will be details for the property 
#     e.g. minimumCardinality, maximumCardinality, dataType, refersTo
# example: 
#    ['InstanceVariable']['variableRole']
# is the dictionary 
#   {'dataType': 'InternationalStructuredString',
#    'maximumCardinality': '1',
#    'minimumCardinality': None,
#    'refersTo': 'DdiClass'}
# classProperties['InstanceVariable']['variableRole']['dataType'] 
#   is an 'InternationalStructuredString'
classProperties = {}





# the classRelations dictionary will contain a dictionary for each class with relations
#    with the relation name as the key
#    and a dictionary (relationTarget,sourceCardinality, targetCardinality) as the subkeys
# example:
#  classRelations['InstanceVariable']['basedOnConceptualVariable']
# is a dictionary   
#    {'relationTarget': 'ConceptualVariable',
#     'sourceCardinality': '0..n',
#    'targetCardinality': '0..1'}  
# so: classRelations['InstanceVariable']['basedOnConceptualVariable']['relationTarget']
# returns:  'ConceptualVariable'   
classRelations = {}




# this dictionary will contain true (is abstract) or false (is not abstract)
#  for all classes. It can be used to return all classes in the model.
# example:
# classIsAbstract['InstanceVariable']
# returns False
classIsAbstract = {}



#this dictionary will contain the parent class name for any class that 
# extends its parent
# example:
# classParentName['InstanceVariable']
# returns: 'RepresentedVariable'
classParentName = {}

# this dictionary contains the ordered list of all ancestors of each class
# example:
# classAncestorList['InstanceVariable']
# returns:  ['RepresentedVariable', 'ConceptualVariable', 'Concept', 'AnnotatedIdentifiable', 'Identifiable']
classAncestorList = {}


# this dictionary will contain the target cardinality keyed on the xmi relation 
# (association) name
# example: 
#  associationTargetCardinality['InstanceVariable_measures_association']
# returns: '1..1'
associationTargetCardinality = {}


# this dictionary will contain the enumerators. the value for each wnumerator is a list of defined values.
# example:
# classEnumerators['TotalityType']
# returns: ['Total', 'Partial', 'Unknown']
classEnumerators = {}


# this dictionary will contain the class documentation
# example:
# classComments['Budget']
# returns:
# 3'\n#Definition\n#============\n#A description of the budget for any of the main publication types that can contain a reference to an external budget document.\n#\n#Examples\n#==========\n#\n#\n#Explanatory notes\n#===================\n#\n#\n#Synonyms\n#==========\n#\n#\n#DDI 3.2 mapping\n#=================\n#r:BudgetType\n#\n#RDF mapping\n#=============\n#\n#\n#GSIM mapping\n#==============\n#\n#\t\t\t\t\t\t\t\t\t'
classComments = {}


#this dictionary will contain the description of each association
# example
# associationDescriptions['WorkflowStep']['isPerformedBy']
# returns:
# Identifies the Service Implementation which performs the Workflow Step. Specialization of isPerformedBy in Process Step for Service Implementations.
associationDescriptions = {}

# this dictionary will contain the descriptions of each class property
# example
# propertyDescriptions['Machine']['typeOfMachine']
#returns
# The kind of machine used - software, web service, physical ...
propertyDescriptions = {}


# this dictionary contains names of the "uml:DataType" elements as keys, Values are an associated regular expression.
# as of May 2018 there are no regexes in the XMI, so initially all values will be None
# These are in two DDI4 packages EnumerationsRegExp and Primitives
# in the Drupal XMI they are just listed with no properties or relationships, like this:
# <packagedElement xmi:type="uml:DataType" xmi:id="IsoDateType" name="IsoDateType"/>
# so this is just a list

umlDataTypeRegexes = {}


# this list contains the names of classes that ure descendents of Identifiable

IdentifiableClassList = []

# This is a list of the non-identifiables. 
# These include: StructuredDataTypes, RegularExpressions, and Enumerations  
# these will be R6 classes without URNs
NonIdentifiableClassList = []

# populate the classEnumerators dictionary
for packagedElement in rootDDI4XMI.iter('packagedElement'):
    # only want xmi:type="uml:Enumeration"
    if packagedElement.attrib.get('{http://www.omg.org/spec/XMI/20110701}type') == "uml:Enumeration":
        literalNameList = []
        name = packagedElement.attrib.get('name')
        for ownedLiteral in packagedElement.iter('ownedLiteral'):
          literalNameList.append(ownedLiteral.attrib.get('name'))
        classEnumerators[classRename(name)]=  literalNameList
        
# populate the umlDataTypeRegexes dictionary
for packagedElement in rootDDI4XMI.iter('packagedElement'):
    # only want xmi:type="uml:Enumeration"
    if packagedElement.attrib.get('{http://www.omg.org/spec/XMI/20110701}type') == "uml:DataType": 
# for now leave out the references to external standards here
        if  packagedElement.attrib.get('name')[0] !='x':
         umlDataTypeRegexes[packagedElement.attrib.get(classRename('name'))] = None
        
        

# populate the classIsAbstract dictionary 
# also populatet the class comments dictionary        
for packagedElement in rootDDI4XMI.iter('packagedElement'):
    # only want xmi:type="uml:Class"
    if packagedElement.attrib.get('{http://www.omg.org/spec/XMI/20110701}type') == "uml:Class":
        name = packagedElement.attrib.get('name')
        if packagedElement.attrib.get('isAbstract') == 'true':
            classIsAbstract[classRename(name)] = True
        else:
            classIsAbstract[classRename(name)] = False
            
        # capture any comment in the classComments dictionary
        # delete the pipe characters and add #s at the beginning of lines
        commentValue = packagedElement.find('ownedComment/body').text
        commentValue = re.sub('[\|]+','',commentValue)
        commentValue = re.sub('\n','\n#',commentValue)
        classComments[classRename(name)] = commentValue
    
  




  
# populate classParentName with the classes that extend another
for generalization in rootDDI4XMI.iter('generalization'):
    xmiId = generalization.attrib.get('{http://www.omg.org/spec/XMI/20110701}id')
    xmiIdRegex = re.compile(r'''(
           ^([^_]+)_   # child class
           ([^_]+)_    # "extends"
           ([^_]*)$    # parent class
           )''', re.VERBOSE)
    xmiIdSearch = xmiIdRegex.search(xmiId)
    child=xmiIdSearch.group(2)
    parent=xmiIdSearch.group(4)
    classParentName[classRename(child)] = classRename(parent)
    
# populate classAncestorList   
for child in classParentName.keys():
    ancestors = [classParentName[child]]
    while ancestors[-1] in classParentName:
       ancestors.append(classParentName[ancestors[-1]])        
    classAncestorList[child] = ancestors



   
#populate the associationTargetCardinality dictionary
for ownedEnd in rootDDI4XMI.iter('ownedEnd'):
    association = ownedEnd.attrib.get('association')
    
    if ownedEnd.find('lowerValue') == None or ownedEnd.find('upperValue') == None:
        targetCardinality = "Missing"
        print("NOTE: missing target cardinality for " + association)
    else:    
        targetCardinality = cardinalityString(ownedEnd.find('lowerValue').attrib.get('value'),ownedEnd.find('upperValue').attrib.get('value'))
    
        
    associationTargetCardinality[association] = targetCardinality
    
	
# populate the associationDescriptions dictionary
for packagedElement in rootDDI4XMI.iter('packagedElement'):
    # only want xmi:type="uml:Enumeration"
    if packagedElement.attrib.get('{http://www.omg.org/spec/XMI/20110701}type') == "uml:Association":
        associationName = packagedElement.attrib.get('name')
        associationClass = packagedElement.attrib.get('{http://www.omg.org/spec/XMI/20110701}id').split('_')[0]
      
        commentValue = packagedElement.find('ownedComment/body').text
        if commentValue != None:
            commentValue = commentValue.replace('\n', ' ')
        if classRename(associationClass) in associationDescriptions.keys():
            associationDescriptions[classRename(associationClass)][associationName] = commentValue
        else:
            associationDescriptions[classRename(associationClass)] = {}
            associationDescriptions[classRename(associationClass)][associationName] = commentValue
	
# populate the propertyDescriptions dictionary
for element in rootDDI4XMI.iter('element'):
    # only want xmi:type="uml:Enumeration"
    if element.attrib.get('{http://www.omg.org/spec/XMI/20110701}type') == "uml:Class":
        propertyClass = element.attrib.get('name')
        propertySet = element.find('attributes')
        if propertySet != None:  #  properties for this class    
          for property in propertySet.iter('attribute'):
            
            propertyName = property.attrib.get('name')
        #****************************************************
        #NOTE: Some property names may be reserved words in R
           # make any property names that are R keywords valid by appending a period. This list is from Reserved()
            if propertyName in ["if", "else", "repeat", "while", "function", "for", "in", "next", "break",  "TRUE", "FALSE", "NULL", "Inf", "NaN", "NA", "NA_integer_", "NA_real_", "NA_complex_", "NA_character_" ]:
              propertyName = propertyName + "."
        #****************************************************                            
            commentValue = property.find('documentation').attrib.get('value')        
        
            if classRename(propertyClass) in propertyDescriptions.keys():
               propertyDescriptions[classRename(propertyClass)][propertyName] = commentValue
            else:
               propertyDescriptions[classRename(propertyClass)] = {}
               propertyDescriptions[classRename(propertyClass)][propertyName] = commentValue

	
	
# find the Source, Target and relationship name for each relationship in the DDI4 model

for ownedAttribute in rootDDI4XMI.iter('ownedAttribute'):
    association = ownedAttribute.attrib.get('association')
    # NOTE: assumption - Relations all have an association attribute
    if association !=  None:
        associationRegex = re.compile(r'''(
           ^([^_]+)_   # source class
           ([^_]+)_    # association name
           ([^_]*)$    # "association"
           )''', re.VERBOSE)
        oASearch = associationRegex.search(association)
        relationName = oASearch.group(3)
        relationSource = ownedAttribute.attrib.get('name')
        
        relationTarget = ownedAttribute.find('type').attrib.get('{http://www.omg.org/spec/XMI/20110701}idref')
        
        #  extract and edit cardinalities into one string
        
        if ownedAttribute.find('lowerValue') == None:
            lowerCardinalityValue = "Missing"
            print("NOTE: for" + relationSource + "relation " + relationName + "cardinality is missing")                 
        else:
            lowerCardinalityValue = ownedAttribute.find('lowerValue').attrib.get('value')
        
        if ownedAttribute.find('upperValue') == None:
            upperCardinalityValue = "Missing"
            print("NOTE: for" + relationSource + "relation " + relationName + "cardinality is missing")                 
        else:
            upperCardinalityValue = ownedAttribute.find('upperValue').attrib.get('value')   
            
        sourceCardinality = cardinalityString(lowerCardinalityValue, upperCardinalityValue)
        targetCardinality = associationTargetCardinality[association]
        relationCardinality = sourceCardinality + "->" + targetCardinality

        # print(relationSource, relationName, relationTarget)
        
        # put relationNames in a list for each class
        if classRename(relationSource) in classRelations.keys():
            # add a relation to this class's list
            classRelations[classRename(relationSource)][relationName] =  {'relationTarget': relationTarget, 'sourceCardinality': sourceCardinality,'targetCardinality': targetCardinality}
        else:
            classRelations[classRename(relationSource)] = {relationName: {'relationTarget': relationTarget, 'sourceCardinality': sourceCardinality,'targetCardinality': targetCardinality}}
            
        
        #print(relationSource, relationName, relationCardinality)
    else:
        # capture properties and their details for each class in a dictionary
   
    
        xmiId = ownedAttribute.attrib.get('{http://www.omg.org/spec/XMI/20110701}id')
        idRegex = re.compile(r'''(
             ^([^_]+)_   # class name
             ([^_]*)$    # property
        )''', re.VERBOSE)
        idSearch = idRegex.search(xmiId)
        className = idSearch.group(2)
        propertyName = idSearch.group(3)               
        
        #****************************************************
        #NOTE: Some property names may be reserved words in R
           # make any property names that are R keywords valid by appending a period. This list is from Reserved()
        if propertyName in ["if", "else", "repeat", "while", "function", "for", "in", "next", "break",  "TRUE", "FALSE", "NULL", "Inf", "NaN", "NA", "NA_integer_", "NA_real_", "NA_complex_", "NA_character_" ]:
            propertyName = propertyName + "."
        #****************************************************       
                
        
        #DebugFile.write('xmiId: ' + xmiId + '  className: ' + className + '  propertyName: ' + propertyName+ '\n')
        #  extract and edit cardinalities into one string
        minimumCardinality = ownedAttribute.find('lowerValue').attrib.get('value')
        maximumCardinality = ownedAttribute.find('upperValue').attrib.get('value')
        propertyCardinality = cardinalityString(minimumCardinality,maximumCardinality)     
                
        # the datatype will be in the <type> element
        #   if a DDI4 class in the attribute xmi:idref 
        #   if a uml primitive in the attribute xmi:type with a value of "uml:PrimitiveType" or  "xsd:anyURI"
        #        and POSSIBLY an href attribute
        
        typeXmiType = ownedAttribute.find('type').attrib.get('{http://www.omg.org/spec/XMI/20110701}type')
        typeXmiIdref = ownedAttribute.find('type').attrib.get('{http://www.omg.org/spec/XMI/20110701}idref')
        typeHref = ownedAttribute.find('type').attrib.get('href')
        
        if typeXmiIdref != None:    # this is a reference to a DDI class
            refersTo = 'DdiClass'
            dataType = typeXmiIdref    
        elif typeXmiType != None:
            refersTo = typeXmiType
            dataType = typeHref   # was typeHref
        else:
            refersTo = 'unknownReference'
            dataType = 'unspecified'
            print ('unknown property type: ' + className)
            DebugFile.write('unknown property type: ' + className + "  typeXmiIdref= " + typeXmiIdref + " typeXmiType= " +typeXmiType )
         
        if typeXmiType == 'xs:language':
            refersTo = typeXmiType
            dataType = typeXmiType
        
        
        
        
        # enter or add to the dictionary of properties        
        
        if classRename(className) in classProperties.keys():
            # add a property to this class's list
            classProperties[classRename(className)][propertyName] = {'minimumCardinality': minimumCardinality, 'maximumCardinality': maximumCardinality, 'refersTo': refersTo, 'dataType': dataType}
            #DebugFile.write('    |' + className +  '| added property: ' + propertyName + '\n')
        else:
            classProperties[classRename(className)] = {propertyName: {'minimumCardinality': minimumCardinality, 'maximumCardinality': maximumCardinality, 'refersTo': refersTo, 'dataType': dataType}}
            #DebugFile.write('    |' + className +  '| created with property: ' + propertyName  + '\n')

     
# populate the dictionary of all fields (include all ancestral fields)
allFields = {}
for className in sorted(classIsAbstract.keys()):  
  allFields = getClassFields(className, className, allFields)
    
  if className in classAncestorList:
    for ancestorClass in classAncestorList[className]:    
      allFields = getClassFields(className, ancestorClass, allFields)


# populate the lists of the two types of DDI4 classes
for className in classIsAbstract.keys():
    if className in classAncestorList:
      if 'Identifiable' in classAncestorList[className]:
          IdentifiableClassList.append(className)
    else:
          NonIdentifiableClassList.append(className)

#  Identifiable is also "identifiable" add it too
IdentifiableClassList.append("Identifiable")          


#         -----------------------------------------------
#          write a file containing the R6 classes for DDI4
#         -----------------------------------------------
            
RFile = open(DataFolder + "DDI4RClasses.R", 'w+', encoding="utf-8")

# the R code should begin with a comment including when the code was generated from the xmi model

RFile.write("# R classes generated from the DDI4 xmi.xml file, " + datetime.datetime.now().isoformat() + "\n\n")
RFile.write("#This work is licensed under a Creative Commons Attribution 4.0 International License.\n")
RFile.write("#https://creativecommons.org/licenses/by/4.0/\n\n")
            
            
RFile.write("library(R6)\n")            
RFile.write("library(uuid)\n")  
RFile.write("library(XML)\n")
RFile.write("library(RCurl)\n")
RFile.write("#  not needed, was part of initial exploration library(data.table)\n")
RFile.write("library(stringr)\n")

RFile.write("\n")
RFile.write("\n")

RFile.write("#' @title  DDI4 Expressed as R6 Classes\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Contains the DDI4 model generated from the XMI file: \\url{https://lion.ddialliance.org/xmi.xml}.\n")
RFile.write("#' Each class in the DDI4 model is implemented as an R6 class.\n")
RFile.write("#' @author Larry Hoyle \\email{larryhoyle@ku.edu}, Joachim Wackerow \\email{joachim.wackerow@gesis.org}\n")
RFile.write("#' @references \\url{http://www.ddialliance.org/}\n")
RFile.write("#' @import uuid \n#                   UUIDgenerate \n")


RFile.write("# - - - - - - - - - -\n")
RFile.write("# preparation\n\n\n")



# RFile.write("ddi.env$folderPath <- \"file:///C:/ddrive/projects/various/DDI/DDI4/DDI4AndR/URNdictionary/\"\n\n")



# - - - - - - - - --  -- 
#  In the future the following section could be included from a file


RFile.write("# - - - - - - - - - -\n")
RFile.write("# start included from ddiUtil.R\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("\n")



RFile.write("#' @section Utility package for DDI\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' currently contains only functions for managing DDI URNs\n")
RFile.write("#' \n")
RFile.write("#' @name DDIUtil\n")
RFile.write("#' @docType package\n")
RFile.write("#' @author Larry Hoyle \\email{larryhoyle@ku.edu}, Joachim Wackerow \\email{joachim.wackerow@gesis.org}\n")
RFile.write("#' @references \\url{http://www.ddialliance.org/}\n")
RFile.write("#'\n")
RFile.write("#' @import RCurl \n#                    getURL \n")
RFile.write("#' @import stringr \n#                    str_locate_all str_trim\n")
RFile.write("#' @import XML \n#                      catalogResolve \n")

RFile.write("#'\n")
RFile.write("#'\n")
RFile.write("#\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("# Configuration for resolution of DDI URNs by XML Catalog file.\n")
RFile.write("# \n")
RFile.write("#' @export\n")
#    RFile.write("#      #' DefaultXmlCatalogFile <- \"file:///D:/DDI/R/DDI4R/ddiurn_catalog.xml\"\n")
#    RFile.write("DefaultXmlCatalogFile <- \"file:///D:/DDI/R/Lawrence/DDI4R/ddiurn_catalog.xml\"\n")
RFile.write("DefaultXmlCatalogFile <- system.file(\"extdata\",\"ddiurn_catalog.xml\",package=\"DDI4R\")\n")
            
RFile.write("\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("# load required libraries\n")
RFile.write("library( RCurl )    # for HTTP GET\n")
RFile.write("library( stringr )  # for string handling\n")
RFile.write("library( XML )      # for resolving URNs to URLs by XML Catalog \n")
RFile.write("\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("# preparation\n")
RFile.write("\n")
RFile.write("if ( !file.exists(DefaultXmlCatalogFile) ) {\n")
RFile.write("  stop( \"XML Catalog file \\\"\", DefaultXmlCatalogFile, \"\\\" doesn't exist!\")\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("DefaultXmlCatalogFile <-  paste(\"file:///\",DefaultXmlCatalogFile, sep=\"\",  collapse=NULL)\n")


RFile.write("# set OS environment variable for XML Catalog\n")
RFile.write("if( !exists( \"Sys.setenv\" ) ) Sys.setenv = Sys.putenv\n")

RFile.write("Sys.setenv( \"XML_CATALOG_FILES\" = DefaultXmlCatalogFile )\n")
RFile.write("Sys.getenv()\n")
RFile.write("\n")
RFile.write("# initialize parameter for id pattern\n")
RFile.write("idPrefix <- \"urn:ddi\"\n")
RFile.write("\n")

RFile.write("# make the master environment for DDI4R \n")
RFile.write("ddi.env <- new.env()\n")

RFile.write("# make the environment that allows direct lookup of DDI4 objects from DDIURNs \n")
RFile.write("#ddi.env.urn <- new.env( parent=ddi.env, hash=TRUE )\n")
RFile.write("ddi.env.urn <- new.env(hash=TRUE)\n")

RFile.write("# make the environment that allows reverse lookup of DDIURNs from objects that reference them DDIURNs \n")
RFile.write("#ddi.env.references <- new.env( parent=ddi.env, hash=TRUE )\n")
RFile.write("ddi.env.references <- new.env(hash=TRUE)\n")

RFile.write("\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("# functions\n")
RFile.write("\n")

RFile.write("#' @title is.anySentinel - Returns TRUE if any value in a vector is NA or NaN. Also returns TRUE if the length of the vector is 0 or the vector is NULL\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Returns true when a vector has length 0 or is NULL. Also returns true if any value in  a vector is  NA or NaN. These are sentinel values. \n")
RFile.write("#' Useful where checking for NA, but the vector might be NULL which would throw an error.\n")
RFile.write("#' \n")
RFile.write("#' @param testValue The value to check (required)\n")
RFile.write("#' \n")
RFile.write("#' \n")
RFile.write("#' @return TRUE if there is a sentinel value in the vector, FALSE otherwise.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples is.anySentinel(c(NULL,1,2))\n")
RFile.write("#' \n")
RFile.write("\n")
RFile.write("is.anySentinel <- function(testValue){\n")
RFile.write("  (length(testValue) == 0 || any(is.na(testValue))  )\n")
RFile.write("}\n")
RFile.write("\n")





RFile.write("#' @title normalizeDdiUrn - Normalize DDI URN\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Puts DDI URN prefix and DDI agency id into lower case.\n")
RFile.write("#' Removes leading and trailing spaces.\n")
RFile.write("#' \n")
RFile.write("#' @note \n")
RFile.write("#' To do: check for valid DDI URN syntax.\n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrn DDI URN (required).\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return An object of class character of length 1 containing the normalized DDI URN, or NULL if the URN is invalid.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' normalizeDdiUrn( \"urn:ddi:de.GESIS:gesis_ZA4265:1.0.0\" )\n")
RFile.write("#' \n")
RFile.write("normalizeDdiUrn <- function( DdiUrn=NA ) {\n")
RFile.write("  if(! is.character(DdiUrn)){\n")
RFile.write("     message(\"A DDI URN must be a character vector\", DdiUrn)\n")
RFile.write("     return(NULL)\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  normedDdiUrn <- trimws(DdiUrn)\n")
RFile.write("    # get the lower case prefix  \n")
RFile.write("  prefix <- tolower(substr(normedDdiUrn,1,7))\n")
RFile.write("  if( (!is.na(prefix)) && (prefix != idPrefix) ){\n")
RFile.write("    # This is not a URN, it could be a UUID, return NULL \n")
RFile.write("    return(NULL)\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("    # make sure the returned prefix is lower case \n")
RFile.write("  substr(normedDdiUrn,1,7) <- prefix\n")
RFile.write("\n")
RFile.write("  normedDdiUrn\n")
RFile.write("}\n")
RFile.write("\n")


RFile.write("#' @title resolveDdiUrn - Resolve DDI URN to URL\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Uses the XML Catalog mechanism for resolving DDI URNs to URLs\n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrn DDI URN (required)\n")
RFile.write("#' A character vector of length 1 containing a DDI URN.\n")
RFile.write("#' \n")
RFile.write("#' @param xmlCatalogFile reference to the XML Catalog File (optional)\n")
RFile.write("#' A character vector of length 1 containing a path or URI to the XML Catalog file to be used. Defaults to the package file.\n")
RFile.write("#' \n")
RFile.write("#' @return An object of class character of length 1 containing a URL, or NULL if the URN cannot be resolved.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples resolveDdiUrn( \"urn:ddi:de.GESIS:gesis_ZA4265:1.0.0\" )\n")
RFile.write("#' \n")
RFile.write("resolveDdiUrn <- function( DdiUrn, xmlCatalogFile=DefaultXmlCatalogFile ) {\n")
RFile.write("  if (is.null(normalizeDdiUrn( DdiUrn ))){\n")
RFile.write("###    message(\" Invalid DDI URN\")\n")
RFile.write("    return(NULL)\n")
RFile.write("  }\n")
RFile.write("  Sys.setenv( \"XML_CATALOG_FILES\" = xmlCatalogFile ) \n")
RFile.write("  ddiUrl <- XML::catalogResolve( normalizeDdiUrn( DdiUrn ), type=\"uri\", asIs=FALSE )\n")
RFile.write("  if ( is.na( ddiUrl ) ) {\n")
RFile.write("###    message( \"DDI URN \\\"\", DdiUrn, \"\\\" can't be resolved\" )\n")
RFile.write("    NULL\n")
RFile.write("  } else {\n")
RFile.write("    ddiUrl\n")
RFile.write("  }\n")
RFile.write("}\n")
RFile.write("\n")


RFile.write("#' @title getObjectFromURL - Get DDI object by URL\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Gets a DDI object identified by an URL.\n")
RFile.write("#' This can be a web resource or a local file.\n")
RFile.write("#' \n")
RFile.write("#' @note\n")
RFile.write("#' to do: objectClass, representationType\n")
RFile.write("#' \n")
RFile.write("#' @param ddiUrl URL of DDI resource (required).\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' @param ddiClass DDI class of URL resource (optional).\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' @param representationType Representation type (XML or RDF) of URL resource (optional).\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return The DDI object of type ddiClass.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples  getObjectFromURL(ddiUrl=\" https://snd.gu.se/catalogue/study/snd0741/export/ddi-3.2\")\n")
RFile.write("#' \n")
            
RFile.write("getObjectFromURL <- function( ddiUrl=NA, ddiClass=NA, representationType=NA ) {\n")
RFile.write("  Rcurl::getURL( stringr::str_trim( ddiUrl ) )\n")
RFile.write("}\n")
RFile.write("\n")







RFile.write("#' @title ddiUrnToName - Returns a value to be used as a key to register or look up DDI objects \n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Returns a key to use for the  ddi.env.urn registry. Currently this should be a valid URN, either a DDI URN or a UUID URN  \n")
RFile.write("#' @param DDiUrn  The URN to convert. \n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  character string which is a valid R name\n")
RFile.write("#' \n")
RFile.write("#' @export\n")

RFile.write("#' @examples   ddiUrnToName <- ddiUrnToName(\"URN:DDI:example.org:cd31b1a9-5617-4517-9c60-e9e8461c4e9b:1\" )\n")
RFile.write("#' \n")
RFile.write("\n")
RFile.write("\n")

RFile.write("ddiUrnToName <- function(DdiUrn){\n")
RFile.write("   DdiUrn\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")




RFile.write("#' @title nameToDdiUrn - Returns a vector with a  DDIURN or a UUID from a nameVector used as an objectKey in ddi.env.urn \n")
RFile.write("#' \n")
RFile.write("#' @description  \n")
RFile.write("#'  Checks to see if this is either a urn:ddi: or a urn:uuid: returns null if not \n")
RFile.write("#' @param nameVector  The nameVector to convert. \n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  a vector of valid Urns  or NA for invalid names \n")
RFile.write("#' \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples   DDIname <- nameToDdiUrn(c(\"urn:ddi:example.org:myId3:1\", \"a bad name\", \"urn:uuid:cd31b1a9-5617-4517-9c6-e9e8461c4e9b\") )\n")
RFile.write("#' \n")
RFile.write("\n")
RFile.write("\n")

RFile.write("nameToDdiUrn <- function(nameVector){\n")
RFile.write("  if(length(nameVector) == 0 ) return(NULL)\n")


RFile.write("  returnVector <- sapply(nameVector, function(name){  \n")
RFile.write("    if(substring(name, first=1, last=8) == \"urn:ddi:\"   ){\n")
RFile.write("      returnValue <- name\n")
RFile.write("    } else if (substring(name, first=1, last=9) == \"urn:uuid:\"){ \n")
RFile.write("      returnValue <- name\n")          
RFile.write("    } else {\n")
RFile.write("      returnValue <- NA\n")
RFile.write("    }\n")
RFile.write("  })\n")

RFile.write("  returnVector\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
            
RFile.write("####################################################################\n")
RFile.write("\n")
RFile.write("#' @title addToReferencedUrn - Adds to the dictionary entry of the URNs referencing a referenced URN\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' addToReferencedUrn adds an entry to a list of references to each referenced URN\n")
RFile.write("#' Each entry in the list is a length 2 vector, having the  referencing URN and the related association name.\n")
RFile.write("#' Both referencedUrnVector and referencingUrnVector may have a length greater than 1.\n")
RFile.write("#' The length of referencingAssociationName must be 1.\n")
RFile.write("#' These are stored in the environment ddi.env.references which is set up as a hash.\n")
RFile.write("#' The key is generated by a call to ddiUrnToName.\n")
RFile.write("#' This dictionary allows reverse lookup of object associations. It is maintained as objects are assigned associations.\n")
RFile.write("#'\n")
RFile.write("#' @param referencedUrnVector The URN vector of the objects being referenced \n")
RFile.write("#' \n")
RFile.write("#' @param referencingAssociationName The association name for the reference \n")
RFile.write("#' \n")
RFile.write("#' @param referencingUrnVector The URN vector of the objects doing the referencing \n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  the vector added\n")
RFile.write("#' \n")
RFile.write("#' @examples addToReferencedUrn(referencedUrnVector=\"urn:ddi:example.org:idCat:1\", referencingAssociationName=\"denotes\", referencingUrnVector=\"urn:ddi:example.org:idCode:1\")    \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#'\n")
RFile.write("\n")
RFile.write("addToReferencedUrn <- function(referencedUrnVector, referencingAssociationName, referencingUrnVector){\n")
RFile.write("  if(length(referencingAssociationName) == 0){\n")
RFile.write("    message(\"Warning: referencingAssociationName must be a character vector of length 1, not 0\")\n")
RFile.write("  }\n")
RFile.write("    if(length(referencingAssociationName) >1){\n")
RFile.write("    message(\"Warning: referencingAssociationName must be a character vector of length 1\")\n")
RFile.write("  }\n")
RFile.write("  \n")
RFile.write("  for (referencedUrn in referencedUrnVector){\n")
RFile.write("    referencedKey <- ddiUrnToName(referencedUrn)\n")
RFile.write("    # the new value to be added to this key is a list of vectors (referencingAssociationName,referencingUrn )\n")
RFile.write("    newValue <- lapply(referencingUrnVector, function(urn){c(referencingAssociationName,urn)})\n")
RFile.write("  \n")
RFile.write("     if (exists(referencedKey, inherits = FALSE, envir=ddi.env.references)){\n")
RFile.write("       # append the new value to the end of the existing value, but do not allow duplicates\n")
RFile.write("       existingValue <- get(referencedKey, inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("       assign(referencedKey, unique(c(existingValue, newValue)), inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("     } else {\n")
RFile.write("       assign(referencedKey, unique(newValue), inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("     }\n")
RFile.write("  \n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # return the complete value\n")
RFile.write("  invisible(get(referencedKey, inherits = FALSE, envir=ddi.env.references))\n")
RFile.write("}\n")
RFile.write("\n")

RFile.write("####################################################################\n")
RFile.write("\n")
RFile.write("#' @title getReferences - Retrives a list of the association name/URN of the objects referencing the referenced URN\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' getReferences returns a list of vectors, each of which contains the association name and URN of an object referencing the given URN.\n")
RFile.write("#' This is a reverse lookup of object associations.\n")
RFile.write("#'\n")
RFile.write("#' @param referencedUrn The URN of an object for which a list of referencing objects is desired \n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  a list of vectors c(associationName, referencingUrn) or NULL if no entry found\n")
RFile.write("#' \n")
RFile.write("#' @examples getReferences(referencedUrn=\"urn:ddi:example.org:idCat:1\")    \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#'\n")
RFile.write("\n")
RFile.write("getReferences <- function(referencedUrn){\n")
RFile.write("  referencedKey <- ddiUrnToName(referencedUrn)\n")
RFile.write("  # the mget embeds the list in a list. Return just the original list\n")
RFile.write("  mget(referencedKey, inherits = FALSE, envir=ddi.env.references, ifnotfound=list(NULL))[[1]]  \n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")

RFile.write("####################################################################\n")
RFile.write("\n")
RFile.write("#' @title removeReferencedUrn - Removes the entry for a referenced URN \n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' removeReferencedUrn removes the entry for the given URN. Thil removes ALL referencing association/URN vectors. \n")
RFile.write("#'\n")
RFile.write("#' @param referencedUrn The URN to remove from the referencing lookup table \n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  a list of vectors c(associationName, referencingUrn) or NULL if no entry found\n")
RFile.write("#' \n")
RFile.write("#' @examples removeReferencedUrn(referencedUrn=\"urn:ddi:example.org:idCat:1\")    \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#'\n")
RFile.write("\n")
RFile.write("removeReferencedUrn <- function(referencedUrn){\n")
RFile.write("  for (urn in referencedUrn){\n")
RFile.write("    referencedKey <- ddiUrnToName(urn)\n")
RFile.write("    if(exists(referencedKey, inherits = FALSE, envir=ddi.env.references)){\n")
RFile.write("      rm(list=referencedKey, inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")

RFile.write("####################################################################\n")
RFile.write("\n")
RFile.write("#' @title removeReferencingUrn - Removes one referencing vector from the entry for the referenced URN \n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' removeReferencingUrn removes one referencing vector from the entry for the referenced URN.\n")
RFile.write("#' This leaves an entry in the dictionary for the referenced URN if there are any other references to it.\n")
RFile.write("#'\n")
RFile.write("#' @param referencedUrn  The URN being referenced.  \n")
RFile.write("#' \n")
RFile.write("#' @param referencingUrn  The URN doing the referencing. THe vector for this URN will be removed from the list indexed by referencedUrn.\n")
RFile.write("#' \n")
RFile.write("#'\n")
RFile.write("#' @return  a list of vectors c(associationName, referencingUrn) or NULL if no entry found\n")
RFile.write("#' \n")
RFile.write("#' @examples removeReferencingUrn(referencedUrn=\"urn:ddi:example.org:idCategory:1\", referencingUrn=\"urn:ddi:example.org:idCode:1\" )    \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#'\n")
RFile.write("\n")
RFile.write("removeReferencingUrn <- function(referencedUrn, referencingUrn){\n")

RFile.write("  for (urn in referencedUrn){\n")
RFile.write("    referencedKey <- ddiUrnToName(urn)\n")
RFile.write("    # the mget will return a list of the list we want\n")
RFile.write("    entryList <- mget(referencedKey, inherits = FALSE, envir=ddi.env.references, ifnotfound=list(NULL))[[1]]\n")
RFile.write("    if(is.null(entryList)) next\n")
RFile.write("    # find the vector that has the referencingUrn\n")
RFile.write("    vectorMatches <- sapply(entryList, function(v){v[2]==referencingUrn} )\n")
RFile.write("    # eliminate that vector from the list, keep the others\n")
RFile.write("    newList <- entryList[! vectorMatches]\n")
RFile.write("    \n")
RFile.write("    if( length(newList)>0){\n")
RFile.write("      # remove the old entry from the environment\n")
RFile.write("      rm(list=referencedKey, inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("      # enter the new one\n")
RFile.write("      assign(referencedKey, newList, inherits = FALSE, envir=ddi.env.references)\n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")            
            
            
RFile.write("##################################################################################\n")
RFile.write("#' @title listReferencedDdiUrns - Lists the DdiUrns of all of the referenced objects\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' returns the list of DDI URNs \n")
RFile.write("#\n")
RFile.write("#' \n")
RFile.write("#' @return a list of DDI URNs\n")
RFile.write("#' \n")
RFile.write("#' @examples   listReferencedDdiUrns()\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
            
RFile.write("\n")
RFile.write("listReferencedDdiUrns <- function(){\n")
RFile.write("  nameToDdiUrn(ls(envir=ddi.env.references))\n")

RFile.write("}\n")
RFile.write("\n")



RFile.write("#' @title deRegisterObject - Deregisters a list of DDI R6 objects by DDI URN\n")
RFile.write("#' \n")
RFile.write("#' @description \n")
RFile.write("#' This removes a list of DDI URNs from the hash table in the environment ddi.env.urn is used for managing DDI URNs.\n")
RFile.write("#'\n")
RFile.write("#' @note\n")
RFile.write("#' to do: only one DDI URN should reference to one object\n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrns  a vector of DDI URNs or UUIDs (required)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples  deRegisterObject(\"urn:ddi:example.org:myID12:1\")\n")
RFile.write("#' \n")
            

            
RFile.write("deRegisterObject <- function( DdiUrns ) {\n")
RFile.write("  returnVals <- vapply(DdiUrns,\n")
RFile.write("    function(DdiUrn){\n")
RFile.write("      if (is.null(normalizeDdiUrn( DdiUrn ))){\n")
RFile.write("       # assume this is a UUID\n")
RFile.write("       objectKey <- paste0(\"urn:uuid:\", DdiUrn) \n")
RFile.write("      } else {\n")
RFile.write("        DdiUrn <- normalizeDdiUrn( DdiUrn )      \n")
RFile.write("        objectKey <- ddiUrnToName(DdiUrn)\n")  
RFile.write("      }\n")


 
RFile.write("      if ( exists( objectKey, inherits = FALSE, envir=ddi.env.urn ) ) {\n")
RFile.write("        rm( list=objectKey, inherits = FALSE, envir=ddi.env.urn )\n")
RFile.write("        return(objectKey)\n")
RFile.write("      } else {\n")
RFile.write("        return(NULL)        \n")
RFile.write("      }\n")
RFile.write("    },\n")
RFile.write("    character(1)      \n")
RFile.write("  )\n")
RFile.write("  invisible(returnVals)\n")

RFile.write("}\n")
RFile.write("\n\n")




RFile.write("#' @title registerObject - Registers a DDI R6 object by DDI URN\n")
RFile.write("#' \n")
RFile.write("#' @description \n")
RFile.write("#' A hash table in the environment ddi.env.urn is used for managing DDI URNs.\n")
RFile.write("#'\n")
RFile.write("#' @note\n")
RFile.write("#' Only one DDI URN should reference one object. If an object is already registered the old one will be deregistered first and a message will be printed to the error log.\n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrn a DDI URN  or a UUID for objects like struturedDatatypes with no DDI URN  (required)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' @param object DDI R6 object (required)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return invisibly returns the DdiUrn, or NULL if the URN is bad or already registered.\n")
RFile.write("#' \n")
RFile.write("#' @export\n\n")
RFile.write("#' @examples registerObject(\"urn:ddi:example.org:myID12:1\", DDI4_InstanceVariable$new()) \n")
RFile.write("#' \n")
            
            
            
RFile.write("registerObject <- function( DdiUrn, object ) {\n")

RFile.write("  if(class(object)[length(class(object))] != \"R6\"){\n")
RFile.write("    stop(\"object is wrong class to register: \", paste0(class(object), sep=\" \") )\n")
RFile.write("  } \n")

RFile.write("  DdiUrnArgument <- DdiUrn\n")
RFile.write("  DdiUrn <- normalizeDdiUrn(DdiUrn)\n")
RFile.write("  if (is.null(DdiUrn)){\n")
           
RFile.write("    if(grepl(\"[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}\", DdiUrnArgument )){ \n")
RFile.write("      # this is a UUID, register it with that\n")
        
RFile.write("      objectKey <- paste0(\"urn:uuid:\", DdiUrnArgument)\n")            
RFile.write("    } else {\n")
RFile.write("      stop(\"Cannot register an invalid DDI URN that is not a UUID: \", DdiUrnArgument)\n")
RFile.write("    }\n")

RFile.write("  } else {\n")
RFile.write("    # this is a DDI URN, register it with that\n")
RFile.write("    objectKey <-  ddiUrnToName(DdiUrn)\n")
RFile.write("  }\n")

RFile.write("    # check if the object isPersistent and it is already registered, if so it can't be reregistered\n")
      
RFile.write("  if (exists( objectKey, inherits = FALSE, envir=ddi.env.urn ) ) {\n")

RFile.write("    if ( !is.null(object$isPersistent)){\n")
RFile.write("      if ( object$isPersistent) stop( \"DDI URN \\\"\", DdiUrn, \"\\\" is persistent and is already registered! It can't be reregistered\" )\n")
RFile.write("    }\n")


RFile.write("    deRegisterObject(DdiUrnArgument)\n")
RFile.write("  } \n")
RFile.write("  assign( objectKey, object, inherits = FALSE, envir=ddi.env.urn )\n")
RFile.write("  invisible(objectKey)\n")

RFile.write("}\n")
RFile.write("\n")



RFile.write("#' @title isDdiUrnRegistered - Checks if DDI URN is registered\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Checks if the given DDI URN is registered in the DDI URN table\n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrn DDI URN (required)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return Logical value, or NULL if the URN is invalid.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples  isDdiUrnRegistered(\"urn:ddi:example.org:myID12:1\")\n")
RFile.write("#' \n")
             
RFile.write("isDdiUrnRegistered <- function( DdiUrn=NA ) {\n")
RFile.write("  if (is.null(normalizeDdiUrn( DdiUrn ))){\n")
RFile.write("    # assume this is a UUID \n")
RFile.write("    objectKey <- paste0(\"urn:uuid:\", DdiUrn)\n")
RFile.write("  } else {\n")
RFile.write("    objectKey <-  ddiUrnToName(normalizeDdiUrn(DdiUrn))\n")
RFile.write("  }\n")
RFile.write("  exists( objectKey, inherits = FALSE, envir=ddi.env.urn )\n")
RFile.write("}\n")
RFile.write("\n")




RFile.write("\n")
RFile.write("#' @title registerAllDdiObjects - Register all DDI R6 objects in the listed environment\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{registerAllDdiObjects} registers all unregistered DdiUrns in the specified environment\n")
RFile.write("#'   This might be used if loading a workspace containing Ddi R6 objects\n")
RFile.write("#'\n")
RFile.write("#' @return invisibly returns vector of DdiUrns newly registered\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' registerAllDdiObjects(environment())\n")
RFile.write("\n")
RFile.write("registerAllDdiObjects <- function(environ){\n")
RFile.write("\n")
RFile.write("  objectList <- lapply(ls(envir=environ), get, envir=environ)\n")
RFile.write("  \n")
RFile.write("  isR6 <- sapply(objectList, is.DDIObject)\n")
RFile.write("\n")
RFile.write("  names(isR6) <- ls(envir=environ)\n")
RFile.write("\n")
RFile.write("  R6Classes <- lapply(ls(envir=environ), get)[isR6]\n")
RFile.write("\n")
RFile.write("  names(R6Classes) <- names(ls(envir=environ)[isR6])\n")
RFile.write("\n")
RFile.write("  foundUrns <- c()\n")
RFile.write("  \n")
RFile.write("  for (object in R6Classes){\n")
RFile.write("    if (!is.null(object$DdiUrn)){\n")
RFile.write("      if (!isDdiUrnRegistered(object$DdiUrn)){\n")
RFile.write("        registerObject(object$DdiUrn,object)\n")
RFile.write("        foundUrns <- c(foundUrns,object$DdiUrn)\n")
RFile.write("      } \n")
RFile.write("    } \n")
RFile.write("  }\n")
RFile.write("  invisible(foundUrns)\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")


RFile.write("\n\n\n")

RFile.write("#' @title isSingleString - Check if a vector is a single string (Not Exported)\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' TRUE or FALSE. Is the argument a character vector of length 1? This function is not exported. It is only available to other functions within th package. \n")
RFile.write("#' \n")
RFile.write("#' @param input An R object)\n")
RFile.write("#' An R object of any class.\n")
RFile.write("#' @return TRUE if the object is character and length 1, otherwise FALSE.\n")
RFile.write("#' \n")
RFile.write("#' @examples\n")
RFile.write("#' isSingleString(c(\"a\",\"b\")) returns FALSE\n")
RFile.write("#' isSingleString(\"a\") returns TRUE\n")
RFile.write("\n")
RFile.write("isSingleString <- function( input ) {\n")
RFile.write("  is.character( input ) & length( input ) == 1\n")
RFile.write("}\n")
RFile.write("\n\n")


RFile.write("#' @title listDdiUrn - Lists registered DDI URNs\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Lists by default all registered DDI URNs.\n")
RFile.write("#' Lists optionally DDI URNs for a specific DDI agency (or sub-agency).\n")
RFile.write("#' List optionally all versions for a specific DDI id for a given agency id.\n")
RFile.write("#' \n")
RFile.write("#' @param agencyId DDI agency id (optional)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' @param id DDI object id (optional). An agency id is required if this is used.\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return An object of class character, or NULL if the URN is invalid.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples listDdiUrn() \n")
RFile.write("#' \n")
            
            
RFile.write("listDdiUrn <- function( agencyId=NA, id=NA ) {\n")
RFile.write("  idStartPattern <- paste( \"^\", idPrefix, sep=\"\" )\n")
RFile.write("  if ( is.na( agencyId ) & is.na( id ) ) {                           # default no parameters\n")
RFile.write("    idStartPattern <- paste( idStartPattern, \":\", \".+\", sep=\"\" )\n")
RFile.write("  } else if ( isSingleString( agencyId ) & is.na( id ) ) {           # only agency id\n")
RFile.write("    idStartPattern <- paste( idStartPattern, \":\", tolower( agencyId ), \"[^:]*:\", \".+\", sep=\"\" )\n")
RFile.write("  } else if ( isSingleString( agencyId ) & isSingleString( id ) ) {  # agency id and id\n")
RFile.write("    idStartPattern <- paste( idStartPattern, \":\", tolower( agencyId ), \":\", id, \":\", \".+\", sep=\"\" )\n")
RFile.write("  } else if ( is.na( agencyId ) & isSingleString( id ) ) {           # id but no agency id\n")
RFile.write("    stop( \"Id \\\"\", id, \"\\\" given without parameter for agency id!\" )\n")
RFile.write("  }\n")

RFile.write("  nameVector <- ls(envir=ddi.env.urn )\n")

RFile.write("  unname(sort(grep(pattern=idStartPattern, nameToDdiUrn(nameVector), value=TRUE ))) \n")

RFile.write("}\n")
RFile.write("\n")

RFile.write("#' @title saveDdi - Save the registries of DDI objects\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Saves the registries of DDI objects to external files. Passes arguments to the built-in save function.\n")
RFile.write("#' \n")
RFile.write("#' @param registryFile The file argument passed to save for the base DDI object registry. \n")
RFile.write("#'  A (writable binary-mode) connection or the name of the file where the data will be saved (when tilde expansion is done). \n")
RFile.write("#' @param referencesFile The file argument passed to save for the registry of referenced objects. \n")
RFile.write("#'  A (writable binary-mode) connection or the name of the file where the data will be saved (when tilde expansion is done). \n")
RFile.write("#'  	\n")
RFile.write("#' @param  ascii  If TRUE, an ASCII representation of the data is written. \n")
RFile.write("#' The default value of ascii is FALSE which leads to a binary file being written.\n")
RFile.write("#'  \n")
RFile.write("#' @param compress logical or character string specifying whether saving to a named file is to use compression. \n")
RFile.write("#'  TRUE corresponds to gzip compression, and character strings \"gzip\", \"bzip2\" or \"xz\" specify the type of compression. \n")
RFile.write("#'  Ignored when file is a connection\n")
RFile.write("#'  \n")
RFile.write("#' @export\n")
RFile.write("#' @examples saveDdi(registryFile = \"DDI4.RData\", referencesFile=\"DDI4references.RData\") \n")
RFile.write("#' \n")
RFile.write("\n")

RFile.write("saveDdi <- function(registryFile, referencesFile, ascii=FALSE, compress = isTRUE(!ascii)){\n")
RFile.write("  registryNameVector <- ls(envir=ddi.env.urn )\n")
RFile.write("  save(list=registryNameVector, file=registryFile, ascii=ascii, envir=ddi.env.urn)\n")
RFile.write("\n")
RFile.write("  referencesNameVector <- ls(envir=ddi.env.references )\n")
RFile.write("  save(list=referencesNameVector, file=referencesFile, ascii=ascii, envir=ddi.env.references)\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")



RFile.write("#' @title loadDdi - Loads saved copies of DDI objects into the DDI4 object registries\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' loads the registry of DDI objects from an external file. passes arguments to the built-in load function.\n")
RFile.write("#' \n")
RFile.write("#' @param registryFile The file argument passed to save for the base DDI object registry. \n")
RFile.write("#'  A (writable binary-mode) connection or the name of the file where the data will be saved (when tilde expansion is done). \n")
RFile.write("#' @param referencesFile The file argument passed to save for the registry of referenced objects. \n")
RFile.write("#'  A (writable binary-mode) connection or the name of the file where the data will be saved (when tilde expansion is done). \n")
RFile.write("#'\n")
RFile.write("#' @param verbose If true returns a vector of the DDI URNs loaded\n")
RFile.write("#'  \n")
RFile.write("#' @export\n")
RFile.write("#' @examples loadDdi(registryFile = \"DDI4.RData\", referencesFile=\"DDI4references.RData\", verbose=FALSE) \n")
RFile.write("#' \n")
RFile.write("\n")

RFile.write("loadDdi <- function(registryFile, referencesFile,  verbose = FALSE){\n")
RFile.write("  registryKeys <- load(file=registryFile, envir=ddi.env.urn, verbose=verbose)\n")
RFile.write("  if(length(registryKeys>0)){\n")
RFile.write("   ddiUrns <- sort( sapply(registryKeys, nameToDdiUrn)  )  \n")
RFile.write("  } else {\n")
RFile.write("   ddiUrns <- NULL\n")
RFile.write("  }\n")
RFile.write("  referencesKeys <- load(file=referencesFile, envir=ddi.env.references, verbose=verbose)\n")

RFile.write("  ddiUrns\n")
RFile.write("}\n\n")





RFile.write("#' @title listDdiUuid - Lists registered DDI UUIDs\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Lists all registered DDI UUIDs.\n")
RFile.write("#' @return An object of class character, or NULL if no UUIDS are registered.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples  listDdiUuid()\n")
RFile.write("#' \n")
            
            
RFile.write("listDdiUuid <- function( agencyId=NA, id=NA ) {\n")
RFile.write("  UuidPattern <- \"^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}\"\n")
RFile.write("  nameVector <- ls(envir=ddi.env.urn )\n")
RFile.write("  sort(grep(pattern=UuidPattern, nameToDdiUrn(nameVector), value=TRUE )) \n")
RFile.write("}\n")
RFile.write("\n")




RFile.write("#' @title getObject - Gets DDI object searching the registry for its DDI URN\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Returns the reference to a DDI R6 object\n")
RFile.write("#' Note that this is called a LOT, so to speed it up it does not normalize the argument. \n")            
RFile.write("#' \n")
RFile.write("#' @param DdiUrn DDI URN (required)\n")
RFile.write("#' An object of class character of length 1.\n")
RFile.write("#' \n")
RFile.write("#' @return Reference to DDI R6 object\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples  getObject(\"urn:ddi:example.org:myID12:1\"), getObject(normalizeDdiUrn(\" URN:ddi:example.org:myID12:1  \"))\n")
RFile.write("#' \n")
            
            
            
RFile.write("getObject <- function( DdiUrn=NA ) {\n")
RFile.write("                 # normedDdiUrn <- normalizeDdiUrn( DdiUrn )\n")
RFile.write("  if (grepl(\"^urn:ddi\", DdiUrn, ignore.case=TRUE)){\n")
RFile.write("    objectKey <- ddiUrnToName(DdiUrn)\n")
RFile.write("  } else {\n")
RFile.write("    # assume this is a UUID \n")
RFile.write("    objectKey <- paste0(\"urn:uuid:\", DdiUrn)\n")
RFile.write("  }\n")
RFile.write("  mget( objectKey, envir=ddi.env.urn , ifnotfound=list(NULL),	inherits = FALSE)[[1]] # response is reference to object\n")

RFile.write("}\n")
RFile.write("\n")




RFile.write("\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("# end included from ddiUtil.R\n")
RFile.write("# - - - - - - - - - -\n")
RFile.write("\n")
RFile.write("\n")


#---------------------
# R Function definitions 
#---------------------



RFile.write("#-------------------------------------------\n")
RFile.write("# R Function Definitions \n")
RFile.write("#-------------------------------------------\n")
RFile.write("\n")
RFile.write("\n")


RFile.write("#' @section Other R Function Definitions:\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Functions used by the R6 classes\n")
RFile.write("#' \n")

          
            
            

RFile.write("\n")

RFile.write("#' @title returnR6ObjectIfValid - Return a list of objects of the specified R6 class, otherwise stops with an error message\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{returnR6ObjectIfValid} is used to check that elements of a list of objects all are of the specified R6 class. \n")
RFile.write("#' It returns the list if so, otherwise it terminates with an error message.\n")
RFile.write("#' \n")
            
RFile.write("#' @param validClassName The valid R6 class name (required).\n")
RFile.write("#' A character value containing the allowed name of the classes in the list. All objects in the list must be of this class. (required).\n\n")

RFile.write("#' @param proposedObjectList A list of the proposed classes.\n")
RFile.write("#' A list of the proposed R6 classes of a length within the minimum and maximum cardinalities (required).\n\n")
            
RFile.write("#' @param minimumCardinality A number specifying the minimum number of elements in the list (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @param maximumCardinality A number specifying the maximum number of elements in the list (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @return Returns proposedObjectList if all list members are of the specified class\n")
RFile.write("#' \n")
            
RFile.write("#' @export\n")
            
            
RFile.write("#' @examples\n")
RFile.write("#' a <- Machine$new()\n")
RFile.write("#' b <- Machine$new()\n")
RFile.write("#' returnR6ObjectIfValid('Machine',list(a,b),1,2)\n")



RFile.write("\n")
RFile.write("\n")
RFile.write("# returnR6ObjectIfValid:  function returns a list of objects of the specified R6 class, otherwise stops with an error message.\n")
RFile.write("#                        Note that R6 objects cannot be in a vector, only a list\n")
RFile.write("#      Arguments\n")
RFile.write("#        validClassName - the allowed name of the classes in the list (all must be the same)\n")
RFile.write("#        proposedObjectList - a list of the proposed classes \n")
RFile.write("#        minimumCardinality - the minimum length of the list (minimum cardinality)\n")
RFile.write("#        maximumCardinality - the maximum length of the list (maximum cardinality)\n")
RFile.write("\n")
RFile.write("returnR6ObjectIfValid <- function(validClassName, proposedObjectList, minimumCardinality, maximumCardinality) {\n")
RFile.write("  # check for constraints on the proposed class \n")
RFile.write("\n")
RFile.write("  if(class(proposedObjectList)[1] != \"list\"){\n")
RFile.write("    stop(paste(\"the argument to proposedObjectList must be a list, not a \", class(proposedObjectList), \"\\n\"))\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  for (proposedObject in proposedObjectList) {\n")
RFile.write("    if( class(proposedObject)[length(class(proposedObject))] != \"R6\") {\n")
RFile.write("      stop(paste(\"entered class,\",class(proposedObject)[1],\", is not an R6 class. It must be either:\",validClassName, \" or an ancestor of that class.  \")) \n")
RFile.write("    }\n")

RFile.write("    if( !(validClassName %in% class(proposedObject) )) {\n")
RFile.write("      stop(paste(\"The valid class name, '\",   validClassName  ,\"', does not appear in the class hierarchy of the proposed object: \", paste(class(proposedObject),collapse=' '))) \n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  if (length(proposedObjectList) < minimumCardinality) {\n")
RFile.write("      stop(length(proposedObject), \" values of \",validClassName, \" were entered. At least \", minimumCardinality, \" are required.\" )\n")
RFile.write("  } else if (length(proposedObjectList) > maximumCardinality) {\n")
RFile.write("      stop(length(proposedObject), \" values of \",validClassName, \" were entered, No more than \", maximumCardinality, \" are allowed.\" )\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # proposed Class list follows constraints\n")
RFile.write("  proposedObjectList\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")



RFile.write("\n")
RFile.write("#' @title returnUrnVectorIfValid - Return a vector of URNs for objects of the specified R6 class, otherwise stops with an error message\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{returnUrnVectorIfValid} is used to check that elements of a vector of URNs all are of the specified R6 class, if possible. \n")
RFile.write("#' It returns the vector if so, otherwise it prints error messages and returns any valid URNs.\n")
RFile.write("#' In silent mode it does not print warnings, but will stop if cardinality or datatype rules are violated. \n")
RFile.write("#' \n")
RFile.write("#' @param validClassName The valid R6 class name (required).\n")
RFile.write("#' A character value containing the allowed name of the classes in the vector. All objects in the vector must be of this class. (required).\n")
RFile.write("\n")
RFile.write("#' @param proposedUrnVector A vector of URNS for the proposed classes.\n")
RFile.write("#' A character vector of the URNs to proposed R6 objects of a length within the minimum and maximum cardinalities (required).\n")
RFile.write("\n")
RFile.write("#' @param minimumCardinality A number specifying the minimum number of elements in the vector (required).\n")
RFile.write("\n")
RFile.write("#' An object of class numeric of length 1.\n")
RFile.write("#' @param maximumCardinality A number specifying the maximum number of elements in the vector (required).\n")
RFile.write("\n")
RFile.write("#' @param silent Prints warning messages if false.\n")
RFile.write("\n")
RFile.write("#' An object of class numeric of length 1.\n")
RFile.write("#' @return Returns proposedUrnVector if all vector members are of the specified class\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' returnUrnVectorIfValid('Concept',c(\"urn:ddi:example.org:IDConcept1:1\", \"urn:ddi:example.org:IDConcept2:1\" ),0,Inf)\n")
RFile.write("\n")

RFile.write("returnUrnVectorIfValid <- function(validClassName, proposedUrnVector, minimumCardinality, maximumCardinality, silent=FALSE) {\n")
RFile.write("  # check for constraints on the proposed URN vector and associated objects if registered \n")
RFile.write("\n")
RFile.write("  # first this must be a character vector\n")
RFile.write("  if(class(proposedUrnVector)[1] != \"character\"){\n")
RFile.write("    stop(paste(\"the argument to proposedUrnVector must be a character vector, not a \", class(proposedUrnVector), \"\\n\"))\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # check cardinality\n")
RFile.write("  if (length(proposedUrnVector) < minimumCardinality) {\n")
RFile.write("      stop(as.character(length(proposedUrnVector)), \" values of DDI URNs were entered. At least \", as.character(minimumCardinality), \" are required.\" )\n")
RFile.write("  } else if (length(proposedUrnVector) > maximumCardinality) {\n")
RFile.write("      stop(as.character(length(proposedUrnVector)), \" values of DDI URNs were entered, No more than \", as.character(maximumCardinality), \" are allowed.\" )\n")
RFile.write("  }\n")
RFile.write("  \n")
RFile.write("  for (proposedURN in proposedUrnVector) {    \n")
RFile.write("  \n")
RFile.write("  # Find the object if the URN is registered and check its class    \n")
RFile.write("    proposedObject <- getObject(DdiUrn=c(proposedURN))\n")
RFile.write("    if ( !is.null(proposedObject)){\n")
RFile.write("      #found an object in the registry\n")
RFile.write("      if( class(proposedObject)[length(class(proposedObject))] != \"R6\")  {\n")
RFile.write("        stop(paste(\"entered class,\",class(proposedObject)[1],\", is not an R6 class. It must be either:\",validClassName, \" or an ancestor of that class.  \")) \n")
RFile.write("      }\n")
RFile.write("      if( !(validClassName %in% class(proposedObject) ) ) {\n")
RFile.write("        stop(paste(\"In the DdiUrn \",  proposedURN,    \"\\n             the valid class name, '\",   validClassName  ,\"', does not appear in the class hierarchy of the proposed object: \", paste(class(proposedObject),collapse=' '))) \n")
RFile.write("      }           \n")
RFile.write("    } else {\n")
RFile.write("      \n")
RFile.write("      # check if the URN can be resolved via the XML catalog, for now print a message if so\n")
RFile.write("      URI <- resolveDdiUrn(proposedURN, DefaultXmlCatalogFile)\n")
RFile.write("      if (!is.null(URI)  ){\n")
RFile.write("        if(!silent) message(\"The URN \", proposedURN, \" was found externally at: \", URI )\n")
RFile.write("        # ######## eventually import and register if possible #########\n")
RFile.write("      } else {\n")
RFile.write("        if(!silent) message(\"The URN \", proposedURN, \" could not be found. It must be validated manually\")\n")
RFile.write("      }    \n")
RFile.write("    }\n")
RFile.write("  }\n")            
RFile.write("    \n")
RFile.write("  # proposed Class vector follows constraints\n")
RFile.write("  proposedUrnVector\n")
RFile.write("}\n")
RFile.write("\n")










RFile.write("#' @title returnVectorIfValid - Return a vector of primitive values of the specified type, otherwise stops with an error message.\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{returnVectorIfValid} is used to check that elements of a vector all are of the specified type. \n")
RFile.write("#' It returns the vector if so, otherwise it terminates with an error message.\n")
RFile.write("#' \n")
            
RFile.write("#' @param validVectorClass The valid  class name of the vector (required).\n")
RFile.write("#' A character value containing the name of the allowed class of the vector. As a vector all elements must be the same type. (required).\n\n")

RFile.write("#' @param proposedVector A list of the proposed classes.\n")
RFile.write("#' The vector of primitive values. Note that all objects in the vector must be of the same type (required).\n\n")
            
RFile.write("#' @param minimumCardinality A number specifying the minimum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @param maximumCardinality A number specifying the maximum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @return Returns proposedVector if all list members are of the specified class\n")
RFile.write("#' \n")
            
RFile.write("#' @export\n")
            
RFile.write("#' @examples\n")
RFile.write("#' n <- 1 \n")
RFile.write("#' m <- 2 \n")
RFile.write("#' v <- c(n,m) \n")
RFile.write("#' returnVectorIfValid(\"numeric\",v,1,2) \n")


RFile.write("\n")
RFile.write("\n")



RFile.write("#returnVectorIfValid : function returns a vector of primitive values of the specified type, otherwise stops with an error message\n")
RFile.write("#                      Primitive values (numeric, character, boolean) can be in a vector all of one type\n")
RFile.write("#      Arguments\n")
RFile.write("#        validVectorClass - the allowed class of the objects in the list (all must be the same)\n")
RFile.write("#        proposedVector - the vector of primitive values. Note that all objects in a vector must be of the same type\n")
RFile.write("#        minimumCardinality - the minimum length of the vector\n")
RFile.write("#        maximumCardinality = the maximum length of the vector\n")
RFile.write("\n")
RFile.write("returnVectorIfValid <- function(validVectorClass, proposedVector, minimumCardinality, maximumCardinality) {\n")
RFile.write("  # check for constraints on the proposed dataType \n")
RFile.write("\n")
RFile.write("  \n")
RFile.write("  if( class(proposedVector) != validVectorClass) {\n")
RFile.write("    stop(paste(\"entered dataType,\",class(proposedVector), \", is not of the data type:\",validVectorClass)) \n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  if (length(proposedVector) < minimumCardinality) {\n")
RFile.write("      stop(length(proposedVector), \" values of \",validVectorClass, \" were entered. At least \", minimumCardinality, \" are required.\" )\n")
RFile.write("  } else if (length(proposedVector) > maximumCardinality) {\n")
RFile.write("      stop(length(proposedVector), \" values of \",validVectorClass, \" were entered, No more than \", maximumCardinality, \" are allowed.\" )\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # proposed dataType list follows constraints\n")
RFile.write("  proposedVector\n")
RFile.write("}\n")
RFile.write("\n")




RFile.write("\n")

RFile.write("#' @title returnEnumeratedValues - Return a character vector if all of the elements validate against an Enumeration Object\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{returnEnumeratedValues} is used to check that elements of a vector are all valid for an enumeration. \n")
RFile.write("#' It returns the vector if so, otherwise it terminates with an error message.\n")
RFile.write("#' \n")
            
RFile.write("#' @param enumerationObject The R6 object containing the enumeration  (required).\n")
RFile.write("#' An enumeration object (not a class). All objects in the vector must have values within this enumeration. (required).\n\n")

RFile.write("#' @param proposedCharacterVector A vector of the proposed values.\n")
RFile.write("#' A vector of the proposed character values of a length within the minimum and maximum cardinalities (required).\n\n")
            
RFile.write("#' @param minimumCardinality A number specifying the minimum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @param maximumCardinality A number specifying the maximum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @return Returns proposedCharacterVector if all  members are in the enumeration\n")
RFile.write("#' \n")
            
RFile.write("#' @export\n")
            
            
RFile.write("#' @examples\n")
RFile.write("#' tot<-TotalityType$new()")
RFile.write("#' returnEnumeratedValues(enumerationObject=tot,c(\"Total\"),0,1)\n")



RFile.write("\n")
RFile.write("\n")
RFile.write("# returnEnumeratedValues:  function Return a character vector if all of the elements validate against an Enumeration Object.\n")
RFile.write("#                        \n")
RFile.write("#      Arguments\n")
RFile.write("#        enumerationObject - the allowed name of the classes in the list (all must be the same)\n")
RFile.write("#        proposedCharacterVector - a character vector containing the proposed values \n")
RFile.write("#        minimumCardinality - the minimum length of the vector (minimum cardinality)\n")
RFile.write("#        maximumCardinality - the maximum length of the vector (maximum cardinality)\n")
RFile.write("\n")
RFile.write("returnEnumeratedValues <- function(enumerationObject, proposedCharacterVector, minimumCardinality, maximumCardinality) {\n")
RFile.write("  # check for constraints on the proposed vector \n")
RFile.write("\n")
RFile.write("  if(class(proposedCharacterVector)[[1]] != \"character\"){\n")
RFile.write("    stop(paste(\"the argument to proposedCharacterVector must be a character vector, not a \", paste(class(proposedCharacterVector),collapse=' '), \"\\n\"))\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  for (proposedString in proposedCharacterVector) {\n")
RFile.write("    if( !enumerationObject$isValidValue(proposedString)) {\n")
RFile.write("      stop(paste(\"The value,\",proposedString,\", is not in the required enumeration. It must be one of: [\", paste(enumerationObject$get_ValidValues(),collapse=',') , \"]\"))\n" )
RFile.write("    }\n")


RFile.write("    if (length(proposedCharacterVector) < minimumCardinality) {\n")
RFile.write("      stop(paste(length(proposedCharacterVector), \" values were entered. At least \", minimumCardinality, \" are required.\" ))\n")
RFile.write("    }\n")
RFile.write("    else if (length(proposedCharacterVector) > maximumCardinality) {\n")
RFile.write("      stop(paste(length(proposedCharacterVector), \" values were entered, No more than \", maximumCardinality, \" are allowed.\" ))\n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # proposed vector follows constraints\n")
RFile.write("  proposedCharacterVector\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")




RFile.write("\n")

RFile.write("#' @title returnRegularExpressionValues - Return a character vector if all of the elements validate against a regular expression Object\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' \\code{returnRegularExpressionValues} is used to check that elements of a vector are all valid for an enumeration. \n")
RFile.write("#' It returns the vector if so, otherwise it terminates with an error message.\n")
RFile.write("#' \n")
            
RFile.write("#' @param RegularExpressionObject The R6 object defining the regular expression  (required).\n")
RFile.write("#' An regular expression object (not a class). All objects in the vector must have values conforming to this regular expression. (required).\n\n")

RFile.write("#' @param proposedCharacterVector A vector of the proposed values.\n")
RFile.write("#' A vector of the proposed character values of a length within the minimum and maximum cardinalities (required).\n\n")
            
RFile.write("#' @param minimumCardinality A number specifying the minimum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @param maximumCardinality A number specifying the maximum number of elements in the vector (required).\n\n")
RFile.write("#' An object of class numeric of length 1.\n")
            
RFile.write("#' @return Returns proposedCharacterVector if all members of the vector match the regular expression. The regular expression assumes that the whole string must match, althoughstart and end of string anchors are not included in the regular expression.\n")
RFile.write("#' \n")
            
RFile.write("#' @export\n")
            
            
RFile.write("#' @examples\n")
RFile.write("#' ch<-Char$new()")
RFile.write("#' returnRegularExpressionValues(RegularExpressionObject=ch,c(\"X\"),0,1) returns X\n")
RFile.write("\n")
RFile.write("returnRegularExpressionValues <- function(RegularExpressionObject, proposedCharacterVector, minimumCardinality, maximumCardinality) {\n")
RFile.write("  # check for constraints on the proposed vector \n")
RFile.write("\n")
RFile.write("  if(class(proposedCharacterVector)[[1]] != \"character\"){\n")
RFile.write("    stop(paste(\"the argument to proposedCharacterVector must be a character vector, not a \", paste(class(proposedCharacterVector),collapse=' '), \"\\n\"))\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  for (proposedString in proposedCharacterVector) {\n")
RFile.write("    if( !RegularExpressionObject$isValidValue(proposedString)) {\n")
RFile.write("      stop(paste(\"The value,\",proposedString,\", does not match the required regular expression: \" , RegularExpressionObject$get_RegularExpression() )) \n" )
RFile.write("    }\n")


RFile.write("    if (length(proposedCharacterVector) < minimumCardinality) {\n")
RFile.write("      stop(paste(length(proposedCharacterVector), \" values were entered. At least \", minimumCardinality, \" are required.\" ))\n")
RFile.write("    }\n")
RFile.write("    else if (length(proposedCharacterVector) > maximumCardinality) {\n")
RFile.write("      stop(paste(length(proposedCharacterVector), \" values were entered, No more than \", maximumCardinality, \" are allowed.\" ))\n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  # proposed vector follows constraints\n")
RFile.write("  proposedCharacterVector\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")



RFile.write("\n")
RFile.write("#' @title getActiveValues - Get a named vector of the active values of a DDI4 Object\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Formats the print layout for a DDI4 R6 object\n")
RFile.write("#' @param DdiObject a DDI4 R6 object (required).\n")
RFile.write("#'\n")
RFile.write("#' @param expandProperties include the content of all structured datatypes by recursion, using the top level value of expandRelations\n")
RFile.write("#'\n")
RFile.write("#' @param expandRelations include the content of all references by recursion, using the top level value of expandProperties\n")
RFile.write("#'\n")
RFile.write("#' @param numberOfLeadingBlanks the number of leading blanks to insert before each string in the vector\n")
RFile.write("#'\n")
RFile.write("#' @param level the depth level of the property or relation. A property of a property would be level 2.\n")
RFile.write("#'\n")
RFile.write("#' @param showNull if TRUE, show all properties with NULL values. If FALSE exclude them\n")
RFile.write("#'\n")
RFile.write("#' @param showId if TRUE show agency, id, and version. If FALSE only show DdiUrn\n")
RFile.write("#'\n")
RFile.write("#' \n")
RFile.write("#' \n")
RFile.write("#' @return a named character vector, one line per output line, indented as per the hierarchy of content. Some names may repeat\n")
RFile.write("#' \n")
RFile.write("#' @examples  getActiveValues(myCategory, expandRelations=TRUE, expandProperties=TRUE)\n")
RFile.write("#' @export\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("getActiveValues <- function(DdiObject, expandProperties=FALSE, expandRelations=FALSE, numberOfLeadingBlanks=2,  level=0, showNull=TRUE, showId=TRUE){\n")
RFile.write("  # return a list of all public fields for the object DdiObject (treated as an environment)\n")
RFile.write("\n")
RFile.write("  public_names <- ls(DdiObject, all.names = TRUE)\n")
RFile.write("  \n")
RFile.write("  newLineString <- \"\\n\"  \n")
RFile.write("  # return a list of the active bindings for the object DdiObject\n")
RFile.write("\n")
RFile.write("  active_names <- public_names[vapply(public_names,bindingIsActive, FUN.VALUE = logical(1), env=DdiObject)]\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  active_values_list <- NULL\n")
RFile.write("  for (ixName in seq_along(active_names)){\n")
RFile.write("\n")
RFile.write("    name <- active_names[ixName]\n")
RFile.write("      # the list of populated fields for this active binding class\n")
RFile.write("    fieldList <- .subset2(DdiObject,name)\n")
RFile.write("\n")
RFile.write("      # the structure (datatype) for this active binding class\n")
RFile.write("    structure <- DdiObject$describe_initializeArguments()[[name]]['structure']\n")
RFile.write("      # DdiUrn is special. It is not a formal (DDI property or relationship), just a convenience item for the user\n")
RFile.write("    if (name == \"DdiUrn\") structure = \"vector\" \n")
RFile.write("\n")

RFile.write("\n")
RFile.write("    if (length(fieldList)==0) {\n")
RFile.write("      # ********* This is a NULL VALUE            ************************\n")
RFile.write("      active_values_list <- append(active_values_list, paste0(newLineString, name, \": NULL\"))\n")
RFile.write("      names(active_values_list)[length(active_values_list)] <- name\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("    } else if (structure == \"DdiUrn vector\" ){\n")
RFile.write("      # ********* This is a REFERENCE            ************************\n")
RFile.write("      # one line for the whole list\n")
RFile.write("      active_values_list <- append(active_values_list,  paste0(newLineString, name, \n")
RFile.write("                      \" <\", structure, \"(\", length(fieldList), \")>:\"))  \n")
RFile.write("      names(active_values_list)[length(active_values_list)] <- name\n")
RFile.write("          \n")
RFile.write("      # a header line for each referenced object\n")
RFile.write("      for(ixReference in seq_along(fieldList)){   \n")
RFile.write("        referencedObject <- getObject(fieldList[ixReference])\n")
RFile.write("        if(is.null(referencedObject)){\n")
RFile.write("          referencedHeader <- paste0( strrep(\" \",numberOfLeadingBlanks),  \"(external object) \", fieldList[ixReference])\n")
RFile.write("          active_values_list <- append(active_values_list, referencedHeader)\n")
RFile.write("          names(active_values_list)[length(active_values_list)] <- class(referencedObject)[1]\n")
RFile.write("        } else {\n")
RFile.write("          referencedHeader <- paste0( strrep(\" \",numberOfLeadingBlanks), \"(\", class(referencedObject)[1], \") \", fieldList[ixReference])\n")
RFile.write("         \n")
RFile.write("          active_values_list <- append(active_values_list, referencedHeader)\n")
RFile.write("          names(active_values_list)[length(active_values_list)] <- class(referencedObject)[1]\n")
RFile.write("          \n")
RFile.write("          if(expandRelations){\n")
RFile.write("            #  find all of the properties of this object\n")
RFile.write("            nextLevel <- level+1 \n")            
RFile.write("            subProperties <- getActiveValues(referencedObject, \n")
RFile.write("                                             expandProperties=expandProperties, \n")
RFile.write("                                             expandRelations=expandRelations, \n")
RFile.write("                                             numberOfLeadingBlanks=numberOfLeadingBlanks, \n")
RFile.write("                                             level=nextLevel, \n")
RFile.write("                                             showNull=showNull, \n")
RFile.write("                                             showId=showId )\n")
RFile.write("            active_values_list <- append(active_values_list,  subProperties)\n")
RFile.write("          }      \n")
RFile.write("        }      \n")
RFile.write("      } \n")
RFile.write("\n")
RFile.write("\n")
RFile.write("   	  \n")
RFile.write("    } else if (R6::is.R6(fieldList[[1]]) &\n")
RFile.write("            !(\"Identifiable\" %in% class (fieldList[[1]]))){\n")
RFile.write("              # ********* this is a STRUCTURED DATATYPE            ************************\n")
RFile.write("              if(expandProperties){\n")

RFile.write("   	       if(level==0) structuredLineStart <- paste0(newLineString, newLineString, strrep(\" \",numberOfLeadingBlanks)) \n")
RFile.write("   	       else  structuredLineStart <- strrep(\" \",level*numberOfLeadingBlanks) \n")
RFile.write("                  active_values_list <- append(active_values_list, paste0( structuredLineStart,  name, \" <DDI R6 Datatype(\", length(fieldList), \")>: \"))\n")
RFile.write("                  names(active_values_list)[length(active_values_list)] <- name\n")
RFile.write("                for (ixField in seq_along(fieldList)){\n")
RFile.write("                  nextLevel <- level+1 \n")  
RFile.write("                  subProperties <- getActiveValues(fieldList[[ixField]], \n")
RFile.write("                                               expandProperties=expandProperties, \n")
RFile.write("                                               expandRelations=expandRelations, \n")
RFile.write("                                               numberOfLeadingBlanks=numberOfLeadingBlanks, \n")
RFile.write("                                               level=nextLevel, \n")
RFile.write("                                               showNull=showNull, \n")
RFile.write("                                               showId=showId )\n")
RFile.write("                  active_values_list <- append(active_values_list,  subProperties)      \n")
RFile.write("                }\n")
RFile.write("              } else {\n")
RFile.write("                active_values_list <- append(active_values_list, paste0(newLineString, name, \"<DDI R6 Datatype(\", length(fieldList), \")> \"))\n")
RFile.write("                names(active_values_list)[length(active_values_list)] <- name\n")
RFile.write("              }\n")
RFile.write("              \n")
RFile.write("              \n")
RFile.write("              \n")
RFile.write("    } else if (is.atomic(fieldList)){\n")

RFile.write("      # ********* this is an atomic (leaf) value             ************************\n")            
RFile.write("      #           indent every embedded line return and add one at the end \n")            
           
RFile.write("     active_values_list <- append(active_values_list, paste0(strrep(\" \",level*numberOfLeadingBlanks),  name, \n")
RFile.write("                  \" <\",typeof(fieldList), \"(\", length(fieldList), \")>:  \",\n")
RFile.write("          paste0(sapply(seq_along(fieldList),\n")
RFile.write("                                   function(ix){\n")
RFile.write("                                     fieldValue <- as.character(fieldList[ix]) \n")
RFile.write("                                     if(substr(fieldValue,1,1) != \"\\n\") fieldValue <- paste0(newLineString, fieldValue)\n")
RFile.write("                                     fieldValue <- gsub(\"\\n\", paste0(newLineString, strrep(\" \", (level+2)*numberOfLeadingBlanks)),  fieldValue)  \n")
RFile.write("                                     paste0( strrep(\" \",numberOfLeadingBlanks), fieldValue, newLineString )       \n")
RFile.write("                                   }\n")
RFile.write("                             ),\n")
RFile.write("                             collapse=\"\"\n")
RFile.write("                      )\n")
RFile.write("               )\n")
RFile.write("            ) \n")
RFile.write("    names(active_values_list)[length(active_values_list)] <- name	  \n")
RFile.write("    }             \n")
RFile.write("    #    paste(retVal, collapse=newLineString) \n")
RFile.write("  }\n")
RFile.write(" \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  if(! showNull)active_values_list <- active_values_list[ ! grepl(\"NULL$\",active_values_list)]\n")
RFile.write("  if(! showId)active_values_list <- active_values_list[!(names(active_values_list) %in% c(\"agency\", \"id\", \"version\"))]\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  # replace embedded newlines with a newline followed by indetation to a level below the current level\n")
RFile.write("  active_values <- sub(\"\\n\", paste0(newLineString, strrep(\" \", numberOfLeadingBlanks)), as.character(active_values_list)  )\n")
            
RFile.write("  #active_values <- paste(strrep(\" \",level*numberOfLeadingBlanks), valueString, sep=\"\") \n")
RFile.write("\n")
RFile.write("  names(active_values) <- names(active_values_list)\n")
RFile.write("  active_values\n")
RFile.write("\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")





RFile.write("\n")
RFile.write("#' @title ddiObjectFormat - Format a DDI4 R6 object for print.  (Not Exported)\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Formats the print layout for a DDI4 R6 object.  This function is not exported. It is only available to other functions within th package.\n")
RFile.write("#' @param DdiObject a DDI4 R6 object (required).\n")
RFile.write("#' An object of class R6 from the DDI4 package.\n")
RFile.write("#' \n")
RFile.write("#' @return a vector with embedded line endings.\n")
RFile.write("#' \n")
RFile.write("#' @examples\n")
RFile.write("#' ddiObjectFormat(myInstanceVariable)\n")
RFile.write("#'   where myInstanceVariable was created as myInstanceVariable <- InstanceVariable$new(....)\n")
            
RFile.write("ddiObjectFormat <- function(DdiObject) {\n")
RFile.write("  # return a list of all public fields for the object DdiObject\n")
RFile.write("\n")
RFile.write("  public_names <- ls(DdiObject, all.names = TRUE)\n")
RFile.write("  \n")
RFile.write("  # return a list of the active bindings for the object DdiObject\n")
RFile.write("\n")


RFile.write("  active_values <- getActiveValues(DdiObject, expandProperties=TRUE, expandRelations=FALSE, showNull=FALSE, showId=FALSE)\n")


RFile.write("    \n")
RFile.write("  # return a list of the functions for the object DdiObject\n")
RFile.write("  function_names <- public_names[vapply(public_names,\n")
RFile.write("                                        function(name){is.function(.subset2(DdiObject,name))},\n")
RFile.write("                                        FUN.VALUE = logical(1))]      \n")
RFile.write("\n")
RFile.write("  \n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("# Start building the return vector\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("# class name and DDI URN\n")
RFile.write("  retVal <- paste0(\"<\",\n")
RFile.write("                    class(DdiObject)[1], \n")
RFile.write("                    \">   DdiUrn=\", \n")
RFile.write("                    DdiObject$DdiUrn,\n")
RFile.write("	                \"\\n\")\n")
RFile.write("\n")
RFile.write("# Inheritance\n")
RFile.write("  NumberOfAncestors <- length(class(DdiObject))\n")
RFile.write("  if (NumberOfAncestors  > 2){ \n")
RFile.write("    retVal <- paste0(retVal, \"\\nInherits from: \", \n")
RFile.write("                as.character(NumberOfAncestors-1),\n")
RFile.write("                 \" classes\\n    \" , \n")
RFile.write("                paste0(sapply(class(DdiObject)[2:(NumberOfAncestors)],\n")
RFile.write("                        function(ancestor){\n")
RFile.write("                          paste0(ancestor, \"\\n\")\n")
RFile.write("                        }\n")
RFile.write("                      ),\n")
RFile.write("					  collapse=\"    \"\n")
RFile.write("               )\n")
RFile.write("			   \n")
RFile.write("    )\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  retVal <- paste0(retVal, \"\\nActive Bindings:\\n\", paste0(\" \", active_values, collapse=\"\\n\"))\n")
RFile.write("\n")
RFile.write("  retVal <- paste0(retVal, \n")
RFile.write("                 \"\\nFunctions:\\n    \",\n")
RFile.write("                  paste0(sapply(function_names,\n")
RFile.write("				             function(functionName){\n")
RFile.write("				               paste0(functionName, \"\\n\")\n")
RFile.write("                               }\n")
RFile.write("                        ),\n")
RFile.write("						collapse=\" \"\n")
RFile.write("                  )\n")
RFile.write("           )\n")
RFile.write("  \n")
RFile.write("  retVal                                        \n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")


RFile.write("\n")
RFile.write("\n")
RFile.write("#' @title printDdiObject - Prints a DDI4 Object, with various display options\n")
RFile.write("#'  \n")
RFile.write("#' @description\n")
RFile.write("#' Prints a DDI4 R6 object with the options to expand structured datatypes and relations. Also allows hiding NULL valued properties and agency, id and version\n")
RFile.write("#' @param DdiObject a DDI4 R6 object (required).\n")
RFile.write("#'\n")
RFile.write("#' @param expandProperties include the content of all structured datatypes by recursion, using the top level value of expandRelations\n")
RFile.write("#'\n")
RFile.write("#' @param expandRelations include the content of all references by recursion, using the top level value of expandProperties\n")
RFile.write("#'\n")
RFile.write("#' @param numberOfLeadingBlanks the number of leading blanks to insert before level in the vector\n")
RFile.write("#'\n")
RFile.write("#' @param showNull if TRUE, show all properties with NULL values. If FALSE exclude them\n")
RFile.write("#'\n")
RFile.write("#' @param showId if TRUE show agency, id, and version. If FALSE only show DdiUrn\n")
RFile.write("#'\n")
RFile.write("#' \n")
RFile.write("#' \n")
RFile.write("#' @return a named character vector, one line per output line, indented as per the hierarchy of content. Some names may repeat\n")
RFile.write("#' \n")
RFile.write("#' @examples  printDdiObject(myCategory, expandRelations=TRUE, expandProperties=TRUE)\n")
RFile.write("#' @export\n")
RFile.write("#'\n")
RFile.write("\n")
RFile.write("printDdiObject <- function(DdiObject, expandProperties=FALSE, expandRelations=FALSE, numberOfLeadingBlanks=2, showNull=TRUE, showId=TRUE){\n")
RFile.write("  active_values <- getActiveValues(DdiObject, expandProperties=expandProperties, expandRelations=expandRelations, numberOfLeadingBlanks=numberOfLeadingBlanks, showNull=showNull, showId=showId)\n")
RFile.write("  cat(paste0(active_values, collapse=\"\\n\"), \"\\n\")\n")
RFile.write("}\n")



RFile.write("\n\n\n")


RFile.write("#' @title is.DDIClassName - Is a name the name of a DDI4 R6 Class?\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Checks if a name is the name of a DDI4 R6 object generator (from the package DDI4R)\n")
RFile.write("#' \n")
RFile.write("#' @param name the name to check (required)\n")
RFile.write("#' A string.\n")
RFile.write("#' \n")
RFile.write("#' @return TRUE if name is the name of a DDI4 R6 class, FALSE if not\n")
RFile.write("#' \n")
RFile.write("#' @examples is.DDIClassName(xNodeName) returns TRUE if xNodeName is an R6 class generator in the package DDI4R\n")
RFile.write("#' \n")
RFile.write("#' @export\n\n")

RFile.write("is.DDIClassName <- function(name){\n")
RFile.write("  object <- mget(name, as.environment(\"package:DDI4R\"), ifnotfound=list(FALSE))\n")
RFile.write("  R6::is.R6Class(object[[1]]) \n")
RFile.write("}\n")


RFile.write("\n\n\n")


RFile.write("#' @title is.DDIClass - Is an object a DDI4 R6 Class Generator?\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Checks if an object is a DDI4 R6 object generator (from the package DDI4R)\n")
RFile.write("#' \n")
RFile.write("#' @param object the name to check (required)\n")
RFile.write("#' An R object.\n")
RFile.write("#' \n")
RFile.write("#' @return TRUE if teh object is a DDI4 R6 object generator, FALSE if not\n")
RFile.write("#' \n")
RFile.write("#' @examples is.DDIClass(Code) returns TRUE,  is.DDIClass(foo) returns FALSE\n")
RFile.write("#' \n")
RFile.write("#' @export\n\n")

RFile.write("is.DDIClass <- function(object){\n")
RFile.write("# explicitly check first for R6 objects, not everything has $parent_env\n")
RFile.write("  if (R6::is.R6Class(object)){\n")
RFile.write("   if (  identical(object$parent_env, asNamespace(\"DDI4R\")) ){\n")
RFile.write("      return(TRUE)\n")
RFile.write("   }\n")
RFile.write("  }\n")
RFile.write("  FALSE\n")
RFile.write("}\n")


RFile.write("\n\n\n")

RFile.write("#' @title is.DDIObject - Is an object a DDI4 R6 Object?\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' a function to check if an object is a DDI4 R6 object \n")
RFile.write("#' \n")
RFile.write("#' @param name the object to check (required)\n")
RFile.write("#' An object .\n")
RFile.write("#' \n")
RFile.write("#' @return TRUE if the object's class is a DDI4 R6 class, FALSE if not\n")
RFile.write("#' \n")
RFile.write("#' @examples is.DDIObject(myInstanceVariable) returns TRUE if myInstanceVariable was generated by a R6 class generator in the package DDI4R\n")
RFile.write(" \n")
RFile.write("#' \n")
RFile.write("#' @export\n\n")

RFile.write("is.DDIObject <- function(object){\n")
RFile.write("  R6::is.R6(object) & is.DDIClassName(class(object)[1])\n")
RFile.write("}\n")


RFile.write("\n\n\n")

RFile.write("#' @title parseDdiUrn - Parse a DDI URN into a named vector\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' parses a DDI URN into a named vector having agency, id, and version values \n")
RFile.write("#' \n")
RFile.write("#' @param DdiUrn DDI URN (required).\n")
RFile.write("#' A character vector of length 1 containing a DDI URN.\n")
RFile.write("#' \n")
RFile.write("#' @return A named character vector of length 3 containing agency, id, and version values.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' parseDdiUrn (\"urn:ddi:de.GESIS:gesis_ZA4265:1.0.0\" ) returns c(agency=\"GESIS\", id=\"gesis_ZA4265\", version=\"1.0.0\")\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("parseDdiUrn <- function(DdiUrn){\n")
RFile.write("  c(agency=strsplit(DdiUrn,\":\",fixed=TRUE)[[1]][3],\n")
RFile.write("    id=strsplit(DdiUrn,\":\",fixed=TRUE)[[1]][4],\n")
RFile.write("    version=strsplit(DdiUrn,\":\",fixed=TRUE)[[1]][5] \n")
RFile.write("  )\n")
RFile.write("}\n")

RFile.write("\n\n\n")

RFile.write("#' @title makeDdiUrn - Make a DDI URN from agency, id and version\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' builds a DDI URN from agency, id, and version values \n")
RFile.write("#' \n")
RFile.write("#' @param agency an agency identifier (required).\n")
RFile.write("#' A character vector of length 1 containing a agency identifier.\n")
RFile.write("#' @param id a DDI object id (required).\n")
RFile.write("#' A character vector of length 1 containing an id value, unique within the agency. This can be a globally unique value like a UUID as well.\n")
RFile.write("#' @param version (required).\n")
RFile.write("#' A character vector of length 1 containing version for the agency, id combination.\n")
RFile.write("#' \n")
RFile.write("#' @return A named character vector of length 1 containing a DDI URN.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' makeDdiUrn(agency=\"de.GESIS\", id=\"gesis_ZA4265\", version=\"1.0.0\") returns  \"urn:ddi:de.GESIS:gesis_ZA4265:1.0.0\"\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("makeDdiUrn <- function(agency=NULL, id=NULL, version=NULL){\n")
RFile.write("  if(!is.character(agency) | !is.character(id) | !is.character(version)){\n")
RFile.write("    stop(\"All values of agency, id and version must be character values \")\n")
RFile.write("  }\n")
RFile.write("  paste(\"ddi:urn\", agency, id, version,  sep=\":\", collapse=NULL)\n")

RFile.write("}\n\n\n")



RFile.write("#' @title ddiXmlNodeToObject - Create a DDI4 R6 object from a DDI4 XML node\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' From a DDI4 XML node, returns a DDI4 object using an R6 class generator in the package DDI4R\n")
RFile.write("\n")
RFile.write("#' \n")
RFile.write("#' @param xNode the XML node to import (required)\n")
RFile.write("#' An XML Node created using the XML package. Some DDI4 XML nodes represent clases. Others represent a property or relationship of the class and have a datatype a DDI4 R6 class (see xNodeClass) \n")
RFile.write("#' @param xNodeClass the DDI4 R6 class to be returned. If this parameter is not supplied xNode needs to represent a DDI4 class. Note that In DDI4R the class names all have the prefix \"DDI4_\" but the XML node names may not.  (optional)\n")
RFile.write("#' A DDI4 R6ClassGenerator. If the node represents a class this will be the same as that class. If the node represents a property or relationship This will be the datatype of that property or relationiship.\n")
RFile.write("#' @param silentState If TRUE some messages about relationship validation will be supressed (optional)\n")
RFile.write("#'  If FALSE, messages about missing references or references to external objects will be displayed.\n")

RFile.write("#' \n")
RFile.write("#' @return a DDI4 R6 object\n")
RFile.write("#' \n")
RFile.write("#' @examples ddiXmlNodeToObject(xNode=myNode, xNodeClass=paste(\"DDI4_\", myNode, collapse=\"\", sep=\"\"))  \n")
RFile.write(" \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("\n")
RFile.write("#   a function to create an R6 object from a DDI4 XML node\n")
RFile.write("#   recurses down to leaves\n")
RFile.write("\n")

RFile.write("ddiXmlNodeToObject <- function(xNode, xNodeClass, silentState=FALSE, verbose=FALSE, messageIndent=0){\n")
RFile.write("\n")
RFile.write("  xNodeName <- xmlName(xNode) \n\n")

RFile.write("  messageIndent <- messageIndent +2\n")
RFile.write("  if(verbose)message(\"\\n\", rep(\" \",messageIndent),  \"processing node: \",  xNodeName,  \" at line: \",  getLineNumber(xNode)  )\n")


RFile.write("\n")
RFile.write("  # the node name must match a DDI4 class name, with the prefix \"DDI4_\" if a class name is  not given\n")
RFile.write("  if (missing(xNodeClass)){\n")
RFile.write("    DDI4Name <- paste(\"DDI4_\", xNodeName, collapse=\"\", sep=\"\")\n")
RFile.write("    if (is.DDIClassName(DDI4Name )){\n")
RFile.write("      xNodeClass <- get(DDI4Name)\n")
RFile.write("    } else {\n")
RFile.write("      warning(paste0(xNodeName, \" is not a DDI4 R6 class name. An xNodeClass argument needs to be supplied in this case.\\n\"))\n")
RFile.write("      return()\n")
RFile.write("    }\n")
RFile.write("  } else {\n")
RFile.write("    # xNodeClass must be a DDI4 class generator\n")
RFile.write("    if (! is.DDIClass(xNodeClass) ){\n")
RFile.write("       stop(\"The xNodeClass parameter value is not a valid DDI4 R6 class. It is a  \", class(xNodeClass))\n")
RFile.write("    }\n")
RFile.write("  }\n")

RFile.write("\n")
RFile.write("  # retrieve the named character vector of attributes for this node\n")
RFile.write("  xNodeAttributes <- xmlAttrs(xNode)  \n")
RFile.write("  \n")
RFile.write("  if (is.null(xNodeAttributes)) {\n    attributeDisplay <- \"no attributes\"\n")
RFile.write("  } else {\n    attributeDisplay <- paste(sapply(names(xNodeAttributes), function(attName){paste(attName, xNodeAttributes[attName], sep=\"=\")}),  collapse=\", \")\n  }\n")
RFile.write("  if(verbose)message(rep(\" \",messageIndent+1),  \"  attributes: \", attributeDisplay)\n")

RFile.write("  \n")
RFile.write(" \n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  args <- sort(names(xNode))\n")
RFile.write("\n")
RFile.write("  # here we want to start from a named list of nodes, the children of xNode\n")
RFile.write("  #  there may be multiple child nodes with the same name\n")
RFile.write("  # the names are the property or relationship names from the DDI4 model\n")
RFile.write("\n")
RFile.write("  # these child nodes will be processed into a named list of lists,\n")
RFile.write("  # one list per child node name (property or relationship name), \n")
RFile.write("  #  with a corresponding list of R6 objects taken from a child node having that name\n")
RFile.write("  # example: \n")
RFile.write("    # A Study might have an \"overview\" property with a one element list of R6 InternationalStructuredString objects\n")
RFile.write("    # The Study might also have a \"hasInstanceVariable\" relationship with a multiple element list of R6 InstanceVariable objects\n")
RFile.write("\n")
RFile.write("  # a for loop is used here since the input list will possibly have more elements than the output list\n")
RFile.write("\n")
RFile.write("  # this is the list of lists that contains the arguments for the R6 object\n")
RFile.write("  # to be returned for xNode. In XML multiple elements may have the same node name.\n")
RFile.write("  # In R they will appear as a list of values for a single active binding.\n")
RFile.write("  argumentList = list()\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  # ChildNodes will always be properties or relationships. Their names all begin with a lower case letter but the XML may upcase them.\n")
RFile.write("  # the DataType of a ChildNode could be a DDI4 R6 class or an atomic value \n")
RFile.write("\n")
RFile.write("  # an R6 object of type xNode, \n")
RFile.write("  # it needs to exist here to be able to retrieve datatype and structure\n")
RFile.write("  # and attributes after all child nodes have been processed. It should not be registered.\n")
RFile.write("  r6Object <- xNodeClass$new(registerThisObject=FALSE)   \n")
RFile.write("  \n")
RFile.write("  for (childNode in xmlChildren(xNode)){\n")
RFile.write("     childNodeName <- xmlName(childNode)[1]\n")
RFile.write("     xmlChildNodeName <- childNodeName\n")
RFile.write("     substr(childNodeName, 1, 1) <- tolower(substr(childNodeName, 1, 1))\n")

RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"child node: \", xmlChildNodeName )\n")

RFile.write("\n")
RFile.write("            \n")
RFile.write("      # a special cases, when the content is just text (a leaf node)\n")
RFile.write("      # this may be  a relationship where \"typeOfClass\" is the attribute containing the class type\n")
RFile.write("      # sometimes the content property collapses and just text appears\n")
RFile.write("      # for now this may just be LanguageSpecificStringType and LanguageSpecificStructuredStringType\n")
RFile.write("      # and SummaryStatistic hasStatistic.\n")
RFile.write("      # For proper validation, one must look up the datatype of the collapsed \"content\" property\n")            
RFile.write("      \n")
RFile.write("     if (childNodeName == \"text\") {               \n")
RFile.write(" \n")
RFile.write("       # a collapsed content       \n")
RFile.write("         childDataType <-  r6Object$describe_initializeArguments()[[\"content\"]][\"datatype\"]\n")            
RFile.write("         argumentList[[\"content\"]] <- tryCatch(as(xmlValue(childNode),childDataType) , warning=function(w){message(\"Node \", xNodeName, \": \", childNodeName, \"=\", xmlValue(childNode), \" cannot be coerced to \",  childDataType)}) \n")
RFile.write("         next\n")
RFile.write("     }\n")
RFile.write(" \n")
RFile.write("\n")
RFile.write("      \n")
RFile.write("     childDataType <-  r6Object$describe_initializeArguments()[[childNodeName]][\"datatype\"]\n")
RFile.write("     childStructure <- r6Object$describe_initializeArguments()[[childNodeName]][\"structure\"]\n")
RFile.write("     if(verbose)message(rep(\" \",messageIndent+1), \" childDataType: \", childDataType, \" childStructure: \", childStructure )\n")
RFile.write("\n")
RFile.write("       # a structure of \"DdiUrn vector\" indicates a DDI relationship (association). \n")
RFile.write("       # The datatype is the target class of the relationship \n")
RFile.write("       # and the content of the active binding will be a vector of DdiUrns.\n")
RFile.write("     \n")
RFile.write("     if (is.null(childStructure)){\n")
RFile.write("          #  skip anything without a structure, but don't bother to warn about comments. \n")
RFile.write("       if (childNodeName == \"comment\")   next\n")          

RFile.write("       #   look here for substitution of an extension of an abstract class name for a property name where the datatype is the abstract class\n")            
RFile.write("       childDDI4Name <- paste(\"DDI4_\", childNodeName, sep=\"\", collapse=\"\")\n")
RFile.write("       substring(childDDI4Name,6,6) <- toupper(substring(childDDI4Name,6,6))\n")
RFile.write("       if (! is.DDIClassName(childDDI4Name)){\n")
RFile.write("         \n")
RFile.write("          message(\"missing structure for unknown element name \", childNodeName,\". \",  childStructure, \" ignoring \", childNodeName  )\n")     
RFile.write("            next\n")
RFile.write("       } else { \n")
RFile.write("       #   A DDI4 Class name here. Look for  aproperty which has the parent of this class as a datatype \n")            
RFile.write("            message(\"this is a DDI4 class name instead of a property name. there is probably a property with an abstract class as its datatype having an extension:  \", childDDI4Name )\n")     

RFile.write("            next\n")
RFile.write("       } \n")
RFile.write("     } \n")
RFile.write("      \n\n")
RFile.write("        # a reference, note that there is no validation \n")
RFile.write("     if(childStructure == \"DdiUrn vector\") {      \n\n")
RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"DdiUrn vector\" )\n")
RFile.write("       argumentList[[childNodeName]] <- c(argumentList[[childNodeName]], xmlValue(childNode))                \n")
RFile.write("       \n")

RFile.write("       # If a child node object is an R6 object: \n")
RFile.write("          # some datatypes like dates are structured as R6 classes with atomic content but constrained by enumeration or regex\n\n")

            
RFile.write("     } else if (childStructure == \"Enumeration vector\"){   \n\n")
RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"Enumeration vector\" )\n")
RFile.write("       # Enumerations and regular expressions are a special case. \n")
RFile.write("         argumentList[[childNodeName]] <- c(argumentList[[childNodeName]], as(xmlValue(childNode), \"character\") )\n\n")


            
RFile.write("     } else if (childStructure == \"Regex vector\"){   \n\n")
RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"Regex vector\" )\n")
RFile.write("       # Regular expressions are a special case. \n")
RFile.write("         argumentList[[childNodeName]] <- c(argumentList[[childNodeName]], as(xmlValue(childNode), \"character\") )\n\n")


RFile.write("       # If a child node object is a structured R6 object, use recursion to get down to the leaf value (atomic) \n")
RFile.write("     } else if (childStructure == \"StructuredDatatype list\"){   \n\n")            
RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"StructuredDatatype list\" )\n")
RFile.write("         argumentList[[childNodeName]] <- append(argumentList[[childNodeName]], \n")
RFile.write("                                               list(ddiXmlNodeToObject(childNode, \n")
RFile.write("                                                                          get(childDataType), \n")
RFile.write("                                                                          silentState=silentState,\n"),
RFile.write("                                                                          verbose=verbose,\n"),
RFile.write("                                                                          messageIndent=messageIndent)))\n\n")
RFile.write(" \n")

RFile.write("       # an atomic type \n")
RFile.write("     } else if (childStructure == \"vector\"){\n")
RFile.write("       if(verbose)message(rep(\" \",messageIndent+1), \"atomic vector\" )\n")
RFile.write("       # this is the terminal (leaf) case, vector types are atomic and have no (sub)-components \n")
RFile.write("       # the argument value for this is a simple atomic vector\n")
RFile.write("       argumentList[[childNodeName]] <- c(argumentList[[childNodeName]], tryCatch(as(xmlValue(childNode),childDataType) , warning=function(w){message(\"Node \", xNodeName, \": \",childNodeName,  \"=\",  xmlValue(childNode), \" cannot be coerced to \",  childDataType)}))\n")
RFile.write("                           \n")
RFile.write("     }  else {\n")
RFile.write("       stop(\"An unknown structure was encountered in parsing child node \", childNodeName)\n")
RFile.write("     }\n")
RFile.write("  }     \n")
RFile.write("\n")
RFile.write("\n")
RFile.write("  # Attributes always have a cardinality of 1, a single value  \n")
RFile.write("\n")
RFile.write("  r6Object <- xNodeClass$new(registerThisObject=FALSE) \n")
RFile.write("  for (attributeName in names(xNodeAttributes)){\n\n")
RFile.write("    if(verbose)message(rep(\" \",messageIndent+1), paste(\"attribute: \", attributeName) )\n\n")
RFile.write("    #  some attributes in the DDI4 XML are XML binding specific, ignore those\n")
RFile.write("    if (attributeName %in% names(r6Object$describe_initializeArguments())){\n")
RFile.write("    \n")
RFile.write("      attributeDataType <- r6Object$describe_initializeArguments()[[attributeName]][\"datatype\"]\n")
RFile.write("      attributeStructure <- r6Object$describe_initializeArguments()[[attributeName]][\"structure\"]\n")
RFile.write("        \n")

RFile.write("      if (attributeStructure == \"Enumeration vector\"){   \n")
RFile.write("         # Enumeration type \n")
RFile.write("        argumentList[[attributeName]] <- as(xNodeAttributes[attributeName],\"character\")\n\n") 
           
RFile.write("     } else if (attributeStructure == \"Regex vector\"){   \n")
RFile.write("         # regex type \n")
RFile.write("        argumentList[[attributeName]] <- as(xNodeAttributes[attributeName],\"character\")\n\n") 
  
RFile.write("     } else if (attributeStructure == \"StructuredDatatype list\"){   \n") 
RFile.write("       stop(\"The attribute \",  xNodeName, \"@\", attributeName,  \" had a datatype of StructuredDatatype,  \", attributeDataType)\n\n")  
           
RFile.write("     }  else {\n")
RFile.write("         # atomic type\n")
RFile.write("        argumentList[[attributeName]] <- tryCatch(as(xNodeAttributes[attributeName],attributeDataType), warning=function(w){message(\"Attribute  \", xNodeName, \": \",attributeName, \"=\", xNodeAttributes[attributeName], \" cannot be coerced to \",  attributeDataType)})\n")
RFile.write("     }\n")

RFile.write("    }  \n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  argumentList[[\"silentState\"]] <- silentState\n")
RFile.write("  returnR6Object <- do.call(xNodeClass$new,argumentList)\n")
RFile.write("  \n")
RFile.write("  returnR6Object \n")

RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")





RFile.write("\n")
RFile.write("#' @title quoteBackslashEscape - Quote the quote and backslash characters for inclusion if a quoted string in a script.  (Not Exported)\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' replace \" with \\\" and \\ with \\\\.    This function is not exported. It is only available to other functions within th package.   \n")
RFile.write("#' in a string that is to be printed inside of quotes\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("#' \n")
RFile.write("#' @param string the string to be modified \n")
RFile.write("#' \n")
RFile.write("#' @return a string with the quote and backslash character quoted\n")
RFile.write("#' \n")
RFile.write(" \n")
RFile.write("#' \n")
RFile.write("\n")
RFile.write("\n")
RFile.write("quoteBackslashEscape <- function(string) {\n")
RFile.write("  gsub( \"([\\\"\\\\])\",  \"\\\\\\\\\\\\1\",  string)\n")
RFile.write("}\n\n\n")




RFile.write("#' @title describeObject - Generate a script that recreates a DDI4 R6 object\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Generates a script that recreates a DDI4 R6 object.\n")
RFile.write("#' @param DdiObject a DDI4 R6 object (required).\n")
RFile.write("#' An object of class R6 from the DDI4 package.\n")
RFile.write("#' \n")
RFile.write("#' @return a vector (script) with embedded line endings.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples\n")
RFile.write("#' describeObject(myInstanceVariable)\n")
RFile.write("#'   where myInstanceVariable was created as myInstanceVariable <- InstanceVariable$new(....)\n")
RFile.write("\n")
RFile.write("describeObject <- function(DdiObject,objectNumber=1, lineStart=\"\"){\n")
RFile.write("\n")
RFile.write("  objectClass <- class(DdiObject)[1]\n")
RFile.write("\n")

RFile.write("# use the DdiObject$name property as the object name if it is populated\n")
RFile.write("  if (is.null(DdiObject$name)){\n")
RFile.write("    objectName <- paste(objectClass, as.character(objectNumber), sep=\"\", collapse=\"\")\n")
RFile.write("  } else {\n")
RFile.write("    objectName <- make.names(DdiObject$name[[1]]$content)\n")
RFile.write("  }\n")
RFile.write("\n")


RFile.write("    # return a list of all public fields for the object DdiObject\n")
RFile.write("  publicNames <- ls(DdiObject, all.names = TRUE)\n")
RFile.write("  \n")
RFile.write("    # return a list of the active bindings for the object DdiObject  \n")
RFile.write("  activeNames <- publicNames[vapply(publicNames,bindingIsActive, FUN.VALUE = logical(1), env=DdiObject)]\n")
RFile.write("  \n")
RFile.write("    #  a vector indicating whether the vector has a values\n")
RFile.write("  isParameterPresent <- sapply(activeNames, function(name){!is.null(DdiObject[[name]])})\n")
RFile.write("  \n")
RFile.write("    # the names of the populated active bindings\n")
RFile.write("      # DdiUrn is created automatically from id, agency, and version when they are entered\n")
RFile.write("      # never directly\n")
RFile.write("      # only process parameters with values\n")
RFile.write("  populatedNames <-  activeNames[isParameterPresent & activeNames != \"DdiUrn\"]\n")
RFile.write("  \n")
RFile.write("    # the structure of each populated active binding\n")
RFile.write("  structures <- sapply(populatedNames, function(name){\n")
RFile.write("    DdiObject$describe_initializeArguments()[[name]][['structure']]\n")
RFile.write("  })\n")
RFile.write("  \n")
RFile.write("    # the datatype of each populated active binding\n")
RFile.write("  datatypes <- sapply(populatedNames, function(name){\n")
RFile.write("    DdiObject$describe_initializeArguments()[[name]][['datatype']]\n")
RFile.write("  })  \n")
RFile.write("  \n")
RFile.write("    # the names of the atomic values, these will have vector parameters\n")
RFile.write("  vectorNames <-  populatedNames[grepl(\"vector\", structures[populatedNames])] \n")
RFile.write("  \n")
RFile.write("    # the names of the list structures, these are references to DDI R6 objects\n")
RFile.write("  listNames <-  populatedNames[grepl(\"list\", structures[populatedNames])]  \n")
RFile.write("\n")
RFile.write("# relationships and atomic parameters are vectors\n")
RFile.write("# a vector parameter to the new function will look like param1 <-  c(val1, val2)\n")
RFile.write("\n")
RFile.write("# StructuredDatatypes look like this(note that arguments are created first):\n")
RFile.write("# param2 < paramClass$new()\n")
RFile.write("# param3 < paramClass$new()\n")
RFile.write("# list(param1, param2 )\n")
RFile.write("\n")


RFile.write("  \n")
RFile.write("  # string to precede each line\n")
RFile.write("  lineStart <-  paste(lineStart,\" \")\n")
RFile.write("       \n")
RFile.write("  parameterNumber <- 0  \n")
RFile.write("  script <- NULL \n")
RFile.write("\n")
RFile.write("  \n")
RFile.write("# *** DDI4 StructuredDatatype\n")
RFile.write("# All of the list names need to be processed first. \n")
RFile.write("# These initiate a recusive chain (depth first)  \n")
RFile.write("\n")
RFile.write("  for (name in listNames){\n")
RFile.write("    parameterNumber <- parameterNumber +1\n")
RFile.write("    fieldList <- .subset2(DdiObject,name)\n")
RFile.write(" \n")
RFile.write("    if (R6::is.R6(fieldList[[1]]) &\n")
RFile.write("        !(\"Identifiable\" %in% class (fieldList[[1]]))){                 \n")
RFile.write("            \n")
RFile.write("      names(fieldList) <- paste0(class(fieldList[[1]])[1],1:length(fieldList))      \n")
RFile.write("         \n")
RFile.write("# recurse through all child classes\n")
RFile.write("         \n")
RFile.write("      childScript <- NULL\n")
RFile.write("      \n")
RFile.write("      for (ixChild in 1:length(fieldList)){\n")
RFile.write("        childScript <-   describeObject(fieldList[[ixChild]],\n")
RFile.write("                                           objectNumber=ixChild, \n")
RFile.write("                                           lineStart=lineStart)\n")
RFile.write("        script <- c(script, childScript)                                            \n")
RFile.write("      }\n")
RFile.write("             \n")
RFile.write("      listScript <- paste0(lineStart,\n")
RFile.write("                           paste(\"param_\",name, objectNumber, sep=\"\", collapse=\"\") ,\n")
RFile.write("                          ' <- list(', \n")
RFile.write("                          paste0(names(fieldList), \n")
RFile.write("                                 collapse=', '),\n")
RFile.write("                          ')'         \n")
RFile.write("                          )                          \n")
RFile.write("      script <- c(script, listScript)\n")
RFile.write("    }    \n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("# *** DDI4 PROPERTIES\n")
RFile.write("\n")
RFile.write("  for (name in vectorNames){\n")
RFile.write("    parameterNumber <- parameterNumber +1\n")
RFile.write("    fieldList <- .subset2(DdiObject,name)\n")
RFile.write("\n")

RFile.write("       #   character vector with quotes \n")
RFile.write("    if (class(fieldList) %in% c(\"character\", \"complex\", \"double\",  \"integer\", \"logical\", \"numeric\")){\n")
RFile.write("    paramScript <- paste0(lineStart,\n")
RFile.write("                           paste(\"param_\",name, objectNumber, sep=\"\", collapse=\"\") ,\n")
RFile.write("                          ' <- as(c(\"', \n")
RFile.write("                          paste0(quoteBackslashEscape(fieldList), \n")
RFile.write("                                 collapse='\", \"'),\n")
RFile.write("                          '\"), \"', \n")
RFile.write("                          class(fieldList),  \n")
RFile.write("                          '\")'         \n")
RFile.write("                          )        \n")

RFile.write("    } else if  (class(fieldList) == \"Date\" )  {\n")

RFile.write("       #   other atomic vector without quotes, e.g. logical \n")            
RFile.write("    paramScript <- paste0(lineStart,\n")
RFile.write("                           paste(\"param_\",name, objectNumber, sep=\"\", collapse=\"\") ,\n")
RFile.write("                          ' <- as.Date(c(', \n")
RFile.write("                          paste0(fieldList, \n")
RFile.write("                                 collapse=', '),\n")
RFile.write("                          '))'         \n")
RFile.write("                          )        \n")


RFile.write("    } else if  (class(fieldList) == \"POSIXlt\" )  {\n")

RFile.write("       #   other atomic vector without quotes, e.g. logical \n")            
RFile.write("    paramScript <- paste0(lineStart,\n")
RFile.write("                           paste(\"param_\",name, objectNumber, sep=\"\", collapse=\"\") ,\n")
RFile.write("                          ' <- as.POSIXlt(c(', \n")
RFile.write("                          paste0(fieldList, \n")
RFile.write("                                 collapse=', '),\n")
RFile.write("                          '))'         \n")
RFile.write("                          )        \n")
RFile.write("    } else { \n")
RFile.write("    message(\"In describeObject unknown datatype \", class(fieldList), \" for object\", objectName ) \n")

RFile.write("    }  \n\n")



RFile.write("    script <- c(script, paramScript)\n")
RFile.write("  }\n")
RFile.write("   \n")
RFile.write("\n")
RFile.write("  \n\n")
RFile.write("# *** Now create the new object, using the parameters generated above\n")
RFile.write("\n")
RFile.write("  script <- append(script, \n")
RFile.write("                   paste0(lineStart,\n")
RFile.write("                          objectName,\n")
RFile.write("                          \" <- \",\n")
RFile.write("                          objectClass,\n")
RFile.write("                          \"$new(\",\n")
RFile.write("                          paste0(sapply(\n")
RFile.write("                                  populatedNames,\n")
RFile.write("                                  function(name){\n")
RFile.write("                                    paste(name, \"=\", \"param_\", name, as.character(objectNumber), sep=\"\")\n")
RFile.write("                                  }                                  \n")
RFile.write("                                ),\n")
RFile.write("                              sep=\"\", \n")
RFile.write("                              collapse=\", \"   \n")
RFile.write("                          ),\n")
RFile.write("                          \")\\n\"\n")
RFile.write("                  )                  \n")
RFile.write("            )\n")
RFile.write("  script\n")
RFile.write("}\n")
RFile.write("\n")
            


RFile.write("\n")
RFile.write("#' @title validateObjectReferences - Validate a DDI4 Object\n")
RFile.write("#' \n")
RFile.write("#' @description \n")
RFile.write("#' This validates the relationships (associations) from a DDI4 Object to other DDI4 Objects.\n")
RFile.write("#' For each relationship it will check for the proper number of references and\n")
RFile.write("#' if a target object is in the local registry it will check for proper class.\n")
RFile.write("#' If an object is not in the local registry it will be checked against the XML catalog.\n")
RFile.write("#' If it cannot be found a message will report so.\n")
RFile.write("#' \n")
RFile.write("#' @param DDI R6 Object (required)\n")
RFile.write("#' An object of class R6.\n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("#' @examples validateObjectReferences(InstanceVariable37) \n")
RFile.write("#' \n")
            
            
            
RFile.write("validateObjectReferences <- function( DdiObject ) {\n")
RFile.write("  isValid <- TRUE\n")
RFile.write("  \n")
RFile.write("  # find the relationships. These will have a structure of DDiUrn vector\n")
RFile.write("  for (name in names(DdiObject$describe_initializeArguments())){\n")
RFile.write("    if ( DdiObject$describe_initializeArguments()[[name]][\"structure\"] == \"DdiUrn vector\" ){\n")
RFile.write("      min <- as.numeric(DdiObject$describe_initializeArguments()[[name]][\"min\"])\n")
RFile.write("      max <- as.numeric(DdiObject$describe_initializeArguments()[[name]][\"max\"])\n")
RFile.write("      datatype <- DdiObject$describe_initializeArguments()[[name]][\"datatype\"]\n")
RFile.write("      \n")
RFile.write("      referenceVector <- DdiObject[[name]]\n")
RFile.write("      \n")
RFile.write("      if (length(referenceVector) > max){\n")
RFile.write("        isValid <- FALSE\n")
RFile.write("        message(\"The number of references for relationship \", name, \", \", length(referenceVector), \", exceeds the maximum allowed\")\n")
RFile.write("      }\n")
RFile.write("      if (length(referenceVector) < min){\n")
RFile.write("        isValid <- FALSE\n")
RFile.write("        message(\"The number of references for relationship \", name, \", \", length(referenceVector), \", is less than the minimum allowed\")\n")
RFile.write("      }\n")
RFile.write("      \n")
RFile.write("      # check references\n")
RFile.write("      \n")
RFile.write("      for (referenceString in referenceVector){\n")
RFile.write("        referencedObject <- getObject(referenceString)\n")
RFile.write("        if (is.null(referencedObject)){\n")
RFile.write("        # could not find the referenced object in the local registry\n")
RFile.write("          URL <- resolveDdiUrn(referenceString, DefaultXmlCatalogFile)\n")
RFile.write("          if (is.null(URL)){\n")
RFile.write("            # could not find it through the XML catalog\n")
RFile.write("            isValid <- FALSE\n")
RFile.write("            message(\"For relationship \", name, \", the reference '\", referenceString, \", could not be found locally or through the XML catalog. It will have to be validated manually.\" )\n")
RFile.write("          } else {\n")
RFile.write("            isValid <- NULL\n")
RFile.write("            message(\"Note: for relationship \", name, \" must be validated manually. It can be found at : \", URL) \n")
RFile.write("          }\n")
RFile.write("        } else {\n")
RFile.write("        # in the local registry, is it the right class?\n")
RFile.write("          if(! datatype %in% class(referencedObject)){\n")
RFile.write("            \n")
RFile.write("            isValid <- NULL\n")
RFile.write("            message(\"Note: for relationship \", name, \" \", datatype, \" must appear in the class hierarchy of \", referenceString, \": \", paste(class(referencedObject), sep=\"\", collapse=\", \"))\n")
RFile.write("          }\n")
RFile.write("        }\n")
RFile.write("      }\n")
RFile.write("    }\n")
RFile.write("  }\n")
RFile.write("  isValid\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")


RFile.write("\n")
RFile.write("#' @title validateRegistry - Validates the references of all objects in the Registry\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Calls listDdiUrn to get the URNs of all objects in the registry.\n")
RFile.write("#' Then makse a list of the corresponding objects \n")
RFile.write("#' and calls validateObjectReferences on each one. Prints messages for any problems.\n")
RFile.write("#'  No parameters.\n")
RFile.write("\n")
RFile.write("#' \n")
RFile.write("#' @return a logical vector of the validity of each object in the DDI4 R6 object registry\n")
RFile.write("#' \n")
RFile.write("#' @examples validateRegistry()\n")
RFile.write(" \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("validateRegistry <- function(){\n")
RFile.write("  objectList <- lapply(listDdiUrn(), getObject)\n")
RFile.write("  validVector <- sapply(objectList , validateObjectReferences)\n")
RFile.write("  invisible(validVector)\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")




RFile.write("\n")
RFile.write("#' @title importDdi4XmlFile - Import A DDI4 XML file into R\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' From a DDI4 XML instance, import everything into DDI4 R6 objects\n")
RFile.write("#' A DDI4 XML instance may contain a single DDI4 object or\n")
RFile.write("#' a DDI element containing a number of objects \n")
RFile.write("\n")
RFile.write("#' \n")
RFile.write("#' @param xmlFile the name of the file from which to read the DDI XML \n")
RFile.write("#' This parameter is passed to xmlInternalTreeParse which has the following constraints:\n")
RFile.write("#' \"The name of the file containing the XML contents. \n")
RFile.write("#' This can contain \\~ which is expanded to the user's home directory. \n")
RFile.write("#' It can also be a URL.  See isURL. \n")
RFile.write("#' Additionally, the file can be compressed (gzip) and is read directly without the user having to de-compress (gunzip) it.\"\n")
RFile.write("#' \n")
RFile.write("#' @param rScriptFile  the name of the file to receive an R script that reproduces the imported R6 objects. \n")
RFile.write("#' \n")
RFile.write("#' @param verbose  display messages about the objects as they are imported. Potentially useful with problematic XML. \n")
RFile.write("#' \n")
RFile.write("#' @return a list of DDI4 R6 objects\n")
RFile.write("#' \n")
RFile.write("#' @examples importDdi4XmlFile(\"C:\\\\ddrive\\\\projects\\\\various\\\\DDI\\\\DDI4\\\\modelingAndTC\\\\AESuseCase\\\\ANES2017_2018_09_12A.xml\")  \n")
RFile.write(" \n")
RFile.write("#' \n")
RFile.write("#' @export\n")
RFile.write("\n")
RFile.write("importDdi4XmlFile <- function(xmlFile, rScriptFile=NULL, verbose=FALSE){\n")
RFile.write("  DDIdoc <- xmlInternalTreeParse(xmlFile)\n")
RFile.write("  root <- xmlRoot(DDIdoc)\n")
RFile.write("  \n")
RFile.write("    # when verbose display all messages\n")
RFile.write("    silentState <- ! verbose\n")
RFile.write("\n")

RFile.write("\n")
RFile.write("  if (! is.null(rScriptFile)){\n")
RFile.write("    writeScript <- TRUE\n")
RFile.write("    scriptConnection <- file(rScriptFile, open=\"w\")\n")
RFile.write("  } else {\n")
RFile.write("    writeScript <- FALSE\n")
RFile.write("  }\n")
RFile.write("  \n")

RFile.write("    # if writing a script make sure the appropriate libraries are loaded. \n")

RFile.write("  if(writeScript) {\n")
RFile.write("    script <- c(\"library(R6)\", \n")
RFile.write("                \"library(uuid)\", \n")
RFile.write("                \"library(XML)\", \n")
RFile.write("                \"library(RCurl)\", \n")
RFile.write("                \"library(stringr)\", \n")
RFile.write("                \"library(DDI4R)\") \n")

RFile.write("    writeLines(script,  scriptConnection, sep = \"\\n\") \n")

RFile.write("    script <- c(\"\\n#R Script imported from:  \", rScriptFile  )\n#")
RFile.write("    writeLines(script,  scriptConnection, sep = \"\\n#\") \n")

RFile.write("  }\n")

RFile.write("  \n")
RFile.write("  rootName <- xmlName(root)\n")
RFile.write("  returnList <- list()  \n")
RFile.write("\n")
RFile.write("  if(verbose) message(\"processing root \", rootName  )      \n")
RFile.write("\n")
RFile.write("    messageIndent <- 0\n")
RFile.write("\n")
RFile.write("# ***  a DDI \"document\" with a root element of DDI and immediate child elements of DDI4 classes  \n")
RFile.write("  if (rootName == \"DDI\"){\n")
RFile.write("    # this is a DDI \"document\" containing several DDI4 elements\n")
RFile.write("    # the first child element should be a DocumentInformation element \n")
RFile.write("    \n")
RFile.write("    nodeNumber <- 0\n")

RFile.write("\n")

RFile.write("  if(writeScript) {\n")
RFile.write("    script <- c(\"\\n# A DDI \\\"document\\\" containing several DDI4 elements\" ,\"  the first child element should be a DocumentInformation element\"  )\n")
RFile.write("    writeLines(script,  scriptConnection, sep = \"\\n#\") \n")
RFile.write("  }\n")

RFile.write("\n")
RFile.write("    for (childNode in xmlChildren(root)){\n")
RFile.write("      nodeNumber <- nodeNumber + 1\n")
RFile.write("      childNodeName <- xmlName(childNode)\n")
RFile.write("\n")

RFile.write("      if(writeScript) {\n")
RFile.write("        script <- paste(\"\\n\\n#  vvvvv ------ Beginning elment:  \", childNodeName, \"------ vvvvv\\n\", sep=\" \", collapse=\" \" )\n")
RFile.write("        writeLines(script,  scriptConnection) \n")
RFile.write("      }\n")

RFile.write("      if(verbose) message(\" child node \", nodeNumber, \" line \", getLineNumber(childNode), \" '\", childNodeName, \"'\" )      \n")
RFile.write("\n")
RFile.write("        # skip the comments\n")
RFile.write("      if (childNodeName == \"comment\") next\n")
RFile.write("      r6Object <- ddiXmlNodeToObject(childNode, silentState=silentState, verbose=verbose, messageIndent=messageIndent)\n")
RFile.write("      returnList[[paste(childNodeName, as.character(nodeNumber), sep=\"\")]] <- r6Object\n")

RFile.write("      if(writeScript) {\n")
RFile.write("        writeLines(describeObject(r6Object, objectNumber=nodeNumber),  scriptConnection, sep = \"\\n\") \n")

RFile.write("        script <- paste(\"#  ^^^^^ ------ Ending elment:  \", childNodeName, \"------ ^^^^^\\n\\n\", sep=\" \", collapse=\" \" )\n")
RFile.write("        writeLines(script,  scriptConnection) \n")
RFile.write("      }\n")

RFile.write("    }\n")
RFile.write("    \n")

RFile.write("  } else {\n")
RFile.write("        \n")
RFile.write("# ***  a DDI4 class is the root \n")
RFile.write("    # this must be a single DDI4 element\n")
            
RFile.write("  if(writeScript) {\n")
RFile.write("    script <- c(\"\\n# This must be a single DDI4 element\"  )\n")
RFile.write("    writeLines(script,  scriptConnection, sep = \"\\n#\") \n")
RFile.write("  }\n")
            
            
RFile.write("    singletonNode <- root\n")
RFile.write("    singletonNodeName <- xmlName(singletonNode)\n")
RFile.write("    returnList <- list()\n")
RFile.write("    r6Object <- ddiXmlNodeToObject(root, silentState=silentState, verbose=verbose, messageIndent=messageIndent)\n")
RFile.write("    returnList[[singletonNodeName]] <- r6Object\n")
RFile.write("    if(writeScript) writeLines(describeObject(r6Object, objectNumber=1), scriptConnection, sep = \"\\n\")\n")
RFile.write("  }\n")

RFile.write("  if(writeScript){\n")
RFile.write("    writeLines(\"#____________________________ Validating registered objects ______________________\\n\", scriptConnection)\n")
RFile.write("    writeLines(\"validateRegistry()\", scriptConnection)\n")
RFile.write("    close(scriptConnection)\n")
RFile.write("  }\n")

RFile.write("  if(!verbose){\n")
RFile.write("    message(\"__________________ validating imported classes _______________\")\n")
RFile.write("    sapply(returnList, validateObjectReferences)\n")
RFile.write("  }\n")
RFile.write("\n")
RFile.write("  returnList\n")
RFile.write("}\n")
RFile.write("\n")








RFile.write("#' @title insertDdiXmlNode - Insert a Ddi4 object into an XML Document\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Inserts a DDI4 R6 object into an XML document file\n")
RFile.write("#' @param DdiObject a DDI4 R6 object (required).\n")
RFile.write("#' @param ObjectNodeName - the node name of the object in XML\n")
RFile.write("#' @param XmlParent an XMLInternalDocument. Can be a sub-node in a larger XML document (required).\n")
RFile.write("#' \n")
RFile.write("#' @return \"an R object that points to the C-Level structure instance\" (see newXMLDoc in the XML package).\n")
RFile.write("#' \n")
RFile.write("#' @examples\n")
RFile.write("#' insertDdiXmlNode(DdiObject, ObjectNodeName XmlDoc)\n")
RFile.write("#'\n")
RFile.write("#' @export\n\n") 
RFile.write("#' @examples insertDdiXmlNode(DdiObject=InstVar$unitOfMeasurement, ObjectNodeName=\"UnitOfMeawsureMent\", XmlParent=xDoc ) \n")
RFile.write("#' \n")
            
            
            
RFile.write("insertDdiXmlNode <- function(DdiObject, ObjectNodeName, XmlParent){\n")
RFile.write("		  \n")
RFile.write("  if(!is.null(DdiObject)){\n")
RFile.write("  \n")
RFile.write("    # flag identifiable objects \n")
RFile.write("    ObjectIsIdentifiable <- DdiObject$get_ClassGroup() == \"identifiable\"\n")
RFile.write("  \n")
RFile.write("    DdiObjectNode <- XML::newXMLNode(name=ObjectNodeName,\n")
RFile.write("                                    parent=XmlParent)\n")
RFile.write("\n")
RFile.write("    # return a list of all public fields for the object DdiObject\n")
RFile.write("\n")
RFile.write("    public_names <- ls(DdiObject, all.names = TRUE)\n")
RFile.write("  \n")
RFile.write("    # return a list of the active bindings for the object DdiObject\n")
RFile.write("\n")
RFile.write("    active_names <- public_names[vapply(public_names,bindingIsActive, FUN.VALUE = logical(1), env=DdiObject)]\n")
RFile.write("    #  drop DdiUrn from the list of active names to process\n")
RFile.write("    active_names <- active_names[! active_names==\"DdiUrn\"]\n")
RFile.write("\n")
RFile.write("    #DDI4 XML requires that the first elements of an identifiable element be\n")
RFile.write("    #  agency, id, and version in that order\n")
RFile.write("    # identifiable objects will have these properties\n")
RFile.write("    \n")
RFile.write("    if (ObjectIsIdentifiable){\n")
RFile.write("      XML::newXMLNode(name=\"Agency\",\n")
RFile.write("                  text=.subset2(DdiObject,\"agency\"),\n")
RFile.write("                  parent=DdiObjectNode)\n")
RFile.write("      XML::newXMLNode(name=\"Id\",\n")
RFile.write("                  text=.subset2(DdiObject,\"id\"),\n")
RFile.write("                  parent=DdiObjectNode)            \n")
RFile.write("      XML::newXMLNode(name=\"Version\",\n")
RFile.write("                  text=.subset2(DdiObject,\"version\"),\n")
RFile.write("                  parent=DdiObjectNode)\n")
RFile.write("    }\n")
RFile.write("\n")
RFile.write("    # check if this object has a collapsed content field, with all properties as attributes\n")
RFile.write("    collapseContent <- TRUE\n")
RFile.write("    # If any property has a cardinality greater than 1, then cannot collapse\n")
RFile.write("    # a special case is translationSourceLanguage which has a cardinality of 0..n but is alloed to be an attribute. \n")
RFile.write("    # If there are relationships then cannot collapse \n")
RFile.write("    for (fieldName in active_names){\n")
RFile.write("      if ( fieldName != \"translationSourceLanguage\" & \n")
RFile.write("         (as.numeric(DdiObject$describe_initializeArguments()[[fieldName]]['max']) > 1) ) collapseContent <- FALSE\n")
RFile.write("      if ( ! (DdiObject$describe_initializeArguments()[[fieldName]]['structure'] %in% c(\"vector\", \"Regex vector\", \"Enumeration vector\"))) collapseContent <- FALSE \n")
RFile.write("    }   \n")
RFile.write("    # there must be a content field to allow compression\n")
RFile.write("    if ( ! \"content\" %in%  active_names) collapseContent <- FALSE\n")

RFile.write("    if(collapseContent){   \n")
RFile.write("      #message( \" Note - The Content property for   \", ObjectNodeName, \" has been collapsed. All other properties become attributes. \"  )\n")
RFile.write("      XML::newXMLCommentNode(text=   paste0(\" The content property for  \", ObjectNodeName, \" has been collapsed into a text node. All other properties become attributes. \" ) , \n")
RFile.write("         parent=XML::xmlParent(DdiObjectNode))\n")
RFile.write("    }   \n")

RFile.write("\n")
RFile.write("    \n")
RFile.write("    #process each of the fields (properties and associations) of this object           \n")
RFile.write("    for (fieldName in active_names){\n")
RFile.write("    \n")
RFile.write("      # The DDI4 XML has all tags upper case\n")
RFile.write("      fieldNodeName <- fieldName\n")
RFile.write("      substring(fieldNodeName,1,1) <- toupper(substring(fieldNodeName,1,1))\n")
RFile.write("    \n")
RFile.write("      # Check the structure of each field (property or relationship) of the object \n")
RFile.write("      # There are three subcases listed below\n")
RFile.write("      # For Structured Datatypes use recursion to fill in the subtree\n")
RFile.write("\n")
RFile.write("      # the structure (datatype) for this active binding class\n")
RFile.write("       fieldStructure <- DdiObject$describe_initializeArguments()[[fieldName]]['structure']\n")
RFile.write("\n")
RFile.write("       \n")
RFile.write("      # the values for this field\n")
RFile.write("       fieldValues <- .subset2(DdiObject,fieldName)\n")
RFile.write("       \n")
RFile.write("      # ignore empty fields agency, id version and DdiUrn\n")
RFile.write("       if ( (length(fieldValues) == 0) |\n")
RFile.write("            (fieldName == \"agency\") |\n")
RFile.write("            (fieldName == \"id\") |\n")
RFile.write("            (fieldName == \"version\") |\n")
RFile.write("            (fieldName == \"DdiUrn\")             \n")
RFile.write("          ){\n")
RFile.write("       }\n")
RFile.write("\n")
RFile.write("       \n")
RFile.write("      # associations (references by DdiUrn) \"DdiUrn vector\"\n")
RFile.write("      # DDI4 XML requires a typeofClass attribute\n")
RFile.write("      # this can be found by looking up the object from the URN and capturing its class\n")
RFile.write("      #  In DDI4 XML there is no \"DDI4_\" prefix. Delete it.\n")            
RFile.write("       else if(fieldStructure == \"DdiUrn vector\"){   \n")
RFile.write("         sapply(fieldValues, \n")
RFile.write("                function(val){\n")
RFile.write("                  referencedClass <- gsub(\"^DDI4_\", \"\", class(DDI4R::getObject(val))[1])\n")
RFile.write("                  XML::newXMLNode(name=fieldNodeName,\n")
RFile.write("                    text=val,\n")
RFile.write("                    attrs=c(typeOfClass=referencedClass),\n")
RFile.write("                    parent=DdiObjectNode)\n")
RFile.write("                }\n")
RFile.write("         )          \n")
RFile.write("    \n")
RFile.write("       }\n")
RFile.write("       \n")
RFile.write("          \n")
RFile.write("      # atomic structures: \"vector\", \"Regex vector\", and \"Enumeration vector\"\n")
RFile.write("      # logical values need to be lower case in XML\n")
RFile.write("      # there is also a special case for collapsing content fields \n")            
RFile.write("       else if(is.atomic(fieldValues)){\n")
RFile.write("         if(! collapseContent) sapply(fieldValues, \n")
RFile.write("                function(val){\n")
RFile.write("                  if(is.logical(val))textVal <- tolower(as.character(val))\n")
RFile.write("                  else textVal <- as.character(val)                   \n")
RFile.write("                  XML::newXMLNode(name=fieldNodeName,\n")
RFile.write("                  text=textVal,\n")
RFile.write("                  parent=DdiObjectNode)\n")
RFile.write("                }\n")
RFile.write("         )       \n")
RFile.write("         else {\n")
RFile.write("                # a single content field value becomes the the text of the object \n")   
RFile.write("           if(is.logical(fieldValues[1])) textVal <- tolower(as.character(fieldValues[1]))\n")
RFile.write("           else textVal <- as.character(fieldValues[1])                   \n\n")            
RFile.write("           if (fieldName == \"content\") addChildren(node=DdiObjectNode, textVal)\n")
RFile.write("                # All other properties become attributes \n")   
RFile.write("           else { \n")
RFile.write("             # message(\"attribute \", fieldNodeName,  \" = \", textVal  )\n")
RFile.write("             # the first character of each attribute name must be lower case \n")
RFile.write("             lowerFieldNodeName <- fieldNodeName \n")  
RFile.write("             substring(lowerFieldNodeName,1,1) <- tolower(substring(lowerFieldNodeName,1,1)) \n")  
RFile.write("             names(textVal) <- lowerFieldNodeName \n")   
RFile.write("             XML::addAttributes(node=DdiObjectNode, .attrs=textVal, append=TRUE)\n")
RFile.write("           }\n")
RFile.write("         }\n")
RFile.write("       }\n")
RFile.write("    \n")
RFile.write("       \n")
RFile.write("      # Non-identifiable DDI4 classes  \"StructuredDatatype list\" - travel down the subtree\n")
RFile.write("        else if(fieldStructure == \"StructuredDatatype list\"){\n")
RFile.write("          # Insert each of the objects in fieldValues as complex elements\n")
RFile.write("          sapply(seq_along(fieldValues),\n")
RFile.write("                function(ixValue){\n")
RFile.write("                    # Check for a reference to an abstract StructuredDatatype, if so the XML Node Name is the class name of the extension\n")
RFile.write("                  if (DdiObject$describe_initializeArguments()[[fieldName]]['isAbstractDatatype']){   \n")
RFile.write("                     fieldClassName = class(.subset2(DdiObject, fieldName)[[ixValue]])[1]\n")
RFile.write("                     fieldClassAncestry = paste(class(.subset2(DdiObject, fieldName)[[ixValue]]), sep=\", \", collapse=\" \" )\n")
RFile.write("                     fieldNodeName = substring(fieldClassName, 6, nchar(fieldClassName))\n")
RFile.write(" #message(xmlName(DdiObjectNode), \" Note - Abstract extension used for  \", fieldName, \": \", fieldNodeName  )\n")
RFile.write("                     XML::newXMLCommentNode(text=   paste0(\" Note - field  \", fieldName, \" has been replaced by the class name of an extension of its datatype. The hierarchy for the extension class is: \", fieldClassAncestry ) , \n")
RFile.write("                       parent=DdiObjectNode)\n")

RFile.write("                  }\n")
RFile.write("                  insertDdiXmlNode(.subset2(DdiObject, fieldName)[[ixValue]], \n")
RFile.write("                                   fieldNodeName,\n")
RFile.write("                                   XmlParent=DdiObjectNode )\n")
RFile.write("                }\n")
RFile.write("          )      \n")
RFile.write("       }     \n")
RFile.write("       \n")
RFile.write("       else{\n")
RFile.write("         message(\"unknown field type for object \",DdiObject$DdiUrn, \",  field \", fieldName )\n")
RFile.write("       }\n")
RFile.write("    }\n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  } else {\n")
RFile.write("    message(\"DDI4 object not found from DDI URN\")\n")
RFile.write("    DdiObjectNode <- NULL\n")
RFile.write("  }\n")
RFile.write("  \n")
RFile.write("  \n")
RFile.write("  invisible(DdiObjectNode)\n")
RFile.write("}\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("\n")





RFile.write("#' @title exportObjectXml - Export a list of Ddi4 objects to XML\n")
RFile.write("#' \n")
RFile.write("#' @description\n")
RFile.write("#' Exports a list of DDI4 R6 objects to an XML document file\n")
RFile.write("#' @param DdiObjects a list of DDI4 R6 objects (required).\n")
RFile.write("#' a vector of URNs such as returned by DDI4R::listDdiUrn()\n")
RFile.write("#' @param XmlFile the qualified name of the XML file (required).\n")
RFile.write("#' @param  docAgency the agency for the document DDI identifier.\n")
RFile.write("#' @param docId the id for the document DDI identifier, defaults to a randm UUID.\n")
RFile.write("#' @param docVersion the version for the document DDI identifier.\n")
RFile.write("#' \n")
RFile.write("#' @return an XML document.\n")
RFile.write("#' \n")
RFile.write("#' @examples\n")
RFile.write("#' exportObjectXml(DdiObjects, XmlFile, docAgency=\"example.org\", docVersion=\"1\")\n")
RFile.write("#'\n")
RFile.write("#'\n")
RFile.write("#' @export\n\n")            
RFile.write("exportObjectXml <- function(DdiObjects, XmlFile, docAgency=\"example.org\", docId=NA, docVersion=\"example.org\"){\n")
RFile.write("\n")
RFile.write("\n")
RFile.write("# if ID is not specified generate a random UUID\n")
RFile.write("  if(missing(docId)) docId <- uuid::UUIDgenerate(TRUE)\n")
RFile.write("\n")
RFile.write("# note: encoding can't be specified here. Specify it in saveXML\n")
RFile.write("  doc <- XML::newXMLDoc()\n")
RFile.write("  topNode <- XML::newXMLNode(name=\"DDI\",namespace=c(\"urn:ddi.org:4\", xsi=\"http://www.w3.org/2001/XMLSchema-instance\"), parent=doc)\n")
RFile.write("\n")
RFile.write("  documentInfo <- XML::newXMLNode(name=\"DocumentInformation\",  parent=topNode)\n")
RFile.write("  XML::newXMLNode(name=\"Agency\", text=docAgency, parent=documentInfo)\n")
RFile.write("  XML::newXMLNode(name=\"Id\", text=docId, parent=documentInfo)\n")
RFile.write("  XML::newXMLNode(name=\"Version\", text=docVersion, parent=documentInfo)\n")
RFile.write("  XML::newXMLNode(name=\"OfType\", text=\"DR0.2\", parent=documentInfo)\n")
RFile.write(" \n")
RFile.write("#   Remove the prefix of DDI4_ from the DDI4R class name  to make the proper DDI4 class name (XML Node Name in this case).\n")
RFile.write("  sapply(DdiObjects,\n")
RFile.write("         function(DdiObject){\n")
RFile.write("           insertDdiXmlNode(DdiObject=DdiObject,\n")
RFile.write("                            ObjectNodeName=gsub(\"^DDI4_\",\"\", class(DdiObject)[1]), \n")
RFile.write("                            XmlParent=topNode  \n")
RFile.write("           )                            \n")
RFile.write("         }\n")
RFile.write("        )\n")
RFile.write("   \n")
RFile.write(" \n")
RFile.write("  XML::saveXML(doc, encoding=\"UTF-8\", file=XmlFile, indent=TRUE)\n")
RFile.write(" \n")
RFile.write("}\n")
RFile.write("\n")






# ------------------
#Enumeration Classes            
# ------------------          

RFile.write("\n")
            
RFile.write("#-------------------------------------------\n")
RFile.write("# Enumerations follow\n")
RFile.write("#-------------------------------------------\n\n\n")


            
            
            
for enumName in sorted(classEnumerators.keys()):
    
  enumDDI4Name = "DDI4_" + enumName
# enumeration ROxygen documentation

  RFile.write("#' @title " + enumDDI4Name + " an Enumeration class.\n")
  RFile.write("#' \n")

  RFile.write("#' @docType class \n")
  RFile.write("#' @import R6   \n#               R6Class is.R6Class  is.R6\n")             
  RFile.write("#' @keywords enumeration \n")
  RFile.write("#' @return Object of class \\code{\link{R6Class}} with methods for exploring values in the enumeration. \n")
  RFile.write("#' @format \\code{\\link{R6Class}} object. \n")
  RFile.write("#' @export  \n")

  RFile.write("#' @description\n")
  RFile.write("#' Contains a list of valid values. \n")
  RFile.write("#' This class has two functions and no initialization arguments: \n")
  RFile.write("#'    isValidValue returns true if the argument is in the list\n")
  RFile.write("#'    get_ValidValues returns the values in the list.\n")
  RFile.write("#' \n")
 
 


  RFile.write("#' @section Methods: \n")
  RFile.write("#'    An Enumeration class contains a list of valid values and functions to validata against that list \n")            
  RFile.write("#'   \\describe{\n") 
  RFile.write("#'     \\item{\\code{new()}}{The default initialize method. An object of this enumeration class will be needed to use the class's methods.}\n")
  RFile.write("#'     \\item{\\code{get_ValidValues()}}{Returns the enumerated list.}\n\n")
  RFile.write("#'     \\item{\\code{isValidValue(val)}}{Returns TRUE if the contents of val is in the enumerated list, otherwise FALSE.}\n")
  RFile.write("#'     \\item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}\n\n")            
  RFile.write("#'     \\item{\code{initialize()}}{This initializes an object of this class.}\n\n")            

  RFile.write("#'   }\n\n\n")


            
  RFile.write("#' @examples\n")
  RFile.write("#' enum <- " +  enumDDI4Name+ "$new() \n")
  RFile.write("#' enum$isValidValue(\"Bag\") \n")
  RFile.write("#' enum$get_ValidValues() \n")

  RFile.write("#' \n\n\n")
    
    
    
    
  enumList = sorted(classEnumerators[enumName])
  RFile.write("#" + enumDDI4Name + ":\n")
  RFile.write(enumDDI4Name + " <- R6Class(classname=\"" + enumDDI4Name + "\",\n")
  RFile.write("  public = list(\n\n")
  RFile.write("    isValidValue = function(val){\n")
  RFile.write("      val %in% private$allowedValues \n")
  RFile.write("    },\n")
  RFile.write("    get_ClassGroup = function(){\n")
  RFile.write("      private$classGroup \n")
  RFile.write("    },\n\n")
  
  RFile.write("    get_ValidValues = function(){\n")
  RFile.write("      private$allowedValues \n")
  RFile.write("    },\n\n")
  
  RFile.write("    initialize = function(...){\n")
  RFile.write("      if (private$isAbstract){\n")
  RFile.write("        stop(\"The  abstract class, " + enumDDI4Name +   "  may not be instantiated\")\n")
  RFile.write("      }\n")
  RFile.write("\n")
  if enumName in classParentName:
    RFile.write("      super$initialize(...)\n")
  RFile.write("    }\n")
  RFile.write("	\n")  
  RFile.write("  ),\n")

  RFile.write("  private = list(\n")
  RFile.write("    isAbstract = FALSE,\n")
  RFile.write("    classGroup = c(\"enumeration\"),\n")  
# write the list of allowed values from enumList
  RFile.write("    allowedValues = c(")
  RFile.write("\"" + enumList[0] + "\"")
  if len(enumList) > 1:
    for allowedValue in enumList[1:len(enumList)]:
       RFile.write(",\"" + allowedValue + "\"")                             
  RFile.write(")\n")
      
  RFile.write("  )\n")
  RFile.write(")\n")

         
            
  RFile.write("\n")
  RFile.write("\n")
  RFile.write("\n")       


            
# ------------------
#RegularExpression Classes            
# ------------------          

RFile.write("\n")
            
RFile.write("#-------------------------------------------\n")
RFile.write("# Regular Expression Classes follow\n")
RFile.write("#-------------------------------------------\n\n\n")


            
            
for regexName in sorted(umlDataTypeRegexes.keys()):
    
  regexDDI4Name = "DDI4_" + regexName
# regular expression ROxygen documentation

  RFile.write("#' @title " + regexDDI4Name + " a regular expression class.\n")
  RFile.write("#' \n")

  RFile.write("#' @docType class \n")
  RFile.write("#' @import R6    \n#               R6Class \n")             
  RFile.write("#' @keywords regular expression regex \n")
  RFile.write("#' @return Object of class \\code{\link{R6Class}} with methods checking a character value for fitting the regular expression. \n")
  RFile.write("#' @format \\code{\\link{R6Class}} object. \n")
  RFile.write("#' @export  \n")

  RFile.write("#' @description\n")
  RFile.write("#' Contains a regular expression defining valid values. \n")
  RFile.write("#' This class has two functions and no initialization arguments: \n")
  RFile.write("#'    isValidValue returns true if the argument is in the list\n")
  RFile.write("#'    get_RegularExpression returns the values of the regular expression.\n")
  RFile.write("#' \n")
 
 

  RFile.write("#' @section Methods:\n")
  RFile.write("#'    An regular Expression class contains a regular expression and functions to validata against that regular expression \n")            

  RFile.write("#'   \\describe{\n")              
  RFile.write("#'     \\item{\\code{new()}}{The default initialize method. An object of this enumeration class will be needed to use the class's methods.}\n")
  RFile.write("#'     \\item{\\code{get_RegularExpression()}}{Returns the regular expression.}\n\n")
  RFile.write("#'     \\item{\\code{isValidValue(val)}}{Returns TRUE if the contents of val match the regular expression, otherwise FALSE.}\n")
  RFile.write("#'     \\item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}\n\n")            
  RFile.write("#'     \\item{\code{initialize()}}{This initializes an object of this class.}\n\n")            


  RFile.write("#'   }\n\n\n")


            
  RFile.write("#' @examples\n")
  RFile.write("#' rex <- " +  regexDDI4Name+ "$new() \n")
  RFile.write("#' rex$isValidValue(\"Bag\") \n")
  RFile.write("#' rex$get_RegularExpression() \n")

  RFile.write("#' \n\n\n")
    
    
    
    
  RegularExpressionString = umlDataTypeRegexes[regexName]
  RFile.write("#" + regexDDI4Name + ":\n")
  RFile.write(regexDDI4Name + " <- R6Class(classname=\"" + regexDDI4Name + "\",\n")
  RFile.write("  public = list(\n\n")
  RFile.write("    isValidValue = function(val){\n")
  RFile.write("      if (is.na(private$RegularExpressionString)) {\n")
  RFile.write("         TRUE\n")
  RFile.write("      } else {\n")
  RFile.write("        grepl(private$RegularExpressionString,val) \n")
  RFile.write("      }\n\n")
  RFile.write("    },\n")
  RFile.write("    get_ClassGroup = function(){\n")
  RFile.write("      private$classGroup \n")
  RFile.write("    },\n\n")

  RFile.write("    get_RegularExpression = function(){\n")
  RFile.write("      private$RegularExpressionString \n")
  RFile.write("    },\n\n")
  
  RFile.write("    initialize = function(...){\n")
  RFile.write("      if (private$isAbstract){\n")
  RFile.write("        stop(\"The  abstract class, " +  regexDDI4Name +    "  may not be instantiated\")\n")
  RFile.write("      }\n")
  RFile.write("\n")
  if regexName in classParentName:
      RFile.write("      super$initialize(...)\n")
  RFile.write("    }\n")
  RFile.write("	\n")  
  RFile.write("  ),\n")

  RFile.write("  private = list(\n")
  RFile.write("    isAbstract = FALSE,\n")
  RFile.write("    classGroup = c(\"regex\"),\n") 
  if RegularExpressionString == None:
    RFile.write("    RegularExpressionString = NA\n")
  else:
    RFile.write("    RegularExpressionString = \"" + RegularExpressionString + "\"\n")


      
  RFile.write("  )\n")
  RFile.write(")\n")

         
            
  RFile.write("\n")
  RFile.write("\n")
  RFile.write("\n")       















            
# -------------------------
#   Classes, Documentation 
# -------------------------      

RFile.write("#---------------------------------------------\n")
RFile.write("# class definitions follow for Content Classes\n")
RFile.write("#---------------------------------------------\n\n\n")


RFile.write("# \n")

RFile.write("# These classes carry the metadata content.\n")
RFile.write("# \n")
            
            
regex = re.compile('\#')            
for className in sorted(classIsAbstract.keys()):
    
    classDDI4Name = "DDI4_" + className
  # Content classes ROxygen documentation
  
                                    # use the definition field from the UML model as the title phrase
                                    # This could be a subtitle
    if className in classComments:
      titlePhrase = classComments[className].split('\n')[3]
      titlePhrase = titlePhrase[1:].split(".")[0]
    else:
      titlePhrase = "A DDI4 R6 Content Class "  
    
    RFile.write("\n\n#' @title " + classDDI4Name + "  " + titlePhrase + "\n")
    RFile.write("#' \n")

    RFile.write("#' @docType class \n")
    RFile.write("#' @import R6    \n#                R6Class \n")             
    RFile.write("#' @keywords content \n")
    RFile.write("#' @return Content carrying object of class \\code{\link{R6Class}}. \n")
    RFile.write("#' @format \\code{\\link{R6Class}} object. \n")

  
    if className in classComments:
      RFile.write("#' @description\n")
      RFile.write("#'   Implementation of the DDI4 class \href{https://lion.ddialliance.org/ddiobjects/" + className + "}{" + className + "} \n")            
      RFile.write(regex.sub("#'\n#'   ",classComments[className]) + "\n")
                          
    RFile.write("\n\n")
    
    # list the ancestry of the class
    RFile.write("#' @section Ancestry:\n")     
    RFile.write("#' Inherits from (direct parent listed first):\n")     
    RFile.write("#'  \itemize{\n")     
    
    if className in  classAncestorList.keys():           
      for ancestor in classAncestorList[className]:
        RFile.write("#'    \item{\code{\link{" + "DDI4_" + ancestor  +  "}}}\n")            
    
    RFile.write("#'  }\n#'\n")
                
# write a description for each property 
    slashChar = r'\\'   # a single backslash
    slashRegex = re.compile(slashChar)
    
    if className in classProperties.keys():
        
      RFile.write("#' @section Active Bindings:\n")    
      RFile.write("#'   Active Bindings to get or set properties or associations. Only values of the proper type and number are allowed.\n\n") 


      RFile.write("#' \\subsection{Common Active Bindings:}{\n")     
      RFile.write("#'   \\describe{\n")
      RFile.write("#'     \\item{\code{DdiUrn}}{ read only - \code{my" +  classDDI4Name + "$DdiUrn} returns the DDI URN of my" +  classDDI4Name + ".} \n")                    
      RFile.write("#'   }\n")            
                    
      RFile.write("#' }\n")  


                  
      RFile.write("\n#' \\subsection{Active Bindings for DDI4 Properties:}{\n")  
      for objectProperty in classProperties[className].keys():


        RFile.write("#'   \\describe{\n")
        RFile.write("#'     \\item{\code{" + objectProperty + "}}{ A read/write active binding.\n")                    
        
         # some property desctiptions have \r\n and other backslashed characters double up the backslash          
        propertyDescription = slashRegex.sub(slashChar + slashChar + slashChar + slashChar,propertyDescriptions[className][objectProperty])            
        RFile.write("#'        " +  propertyDescription + "\n")  
        
            
                    
        if classProperties[className][objectProperty]["dataType"] in primitiveDictionary.keys():
           propertyDatatype =  primitiveDictionary[classProperties[className][objectProperty]["dataType"]] 
           generalType = "primitive"
        else:
           propertyDatatype =  classProperties[className][objectProperty]["dataType"]
           generalType = "R6"
        if generalType == "primitive":
            RFile.write("#'\n#'                              \\code{my" +  classDDI4Name + "$" + objectProperty + " <- my" + propertyDatatype + "} sets the " + objectProperty  + " property of 'my" +  classDDI4Name + "' to the vector of datatype:" + propertyDatatype +  "\n")            
        else:
            RFile.write("#'\n#'                              \\code{my" +  classDDI4Name + "$" + objectProperty + " <- list(my" + propertyDatatype + ")} sets the '" + objectProperty  + "' property of 'my" +  classDDI4Name + "' to a list of R6 " + propertyDatatype +  " class instances\n")            
            
        if generalType == "primitive":
          RFile.write("#'\n#'                              \\code{my" +  classDDI4Name + "$" + objectProperty + "} returns a vector of datatype: " + propertyDatatype +  "}\n")            
        else:
          RFile.write("#'\n#'                              \\code{my" +  classDDI4Name + "$" + objectProperty + "} returns a list of of R6 : " + propertyDatatype +  " class instances}\n")            
              
        RFile.write("#'   }\n")            
                    
      RFile.write("#' }\n")  

# write a description for each association
    if className in   classRelations.keys():  
      RFile.write("\n#' \\subsection{Active Bindings for DDI4 Relationships:}{\n")    
      for objectRelation in classRelations[className].keys():
        if  objectRelation !=  "realizes":
          if associationDescriptions[className].get(objectRelation," ") == None:
              relationDescription = " "
          else:            
            relationDescription =   associationDescriptions[className].get(objectRelation," ")
          relationTargetClass =  classRelations[className][objectRelation] ["relationTarget"]
          RFile.write("#'   \describe{\n")
          RFile.write("#'     \item{\code{" + objectRelation + "}}{ A read/write active binding.\n")                         
          RFile.write("#'          "  + relationDescription + "\n")  
          RFile.write("#'\n#'                              \code{my" +  classDDI4Name + "$" + objectRelation + " <- c(my" + relationTargetClass + "URN)} sets the association '" + objectRelation  + "' from 'my" +  classDDI4Name + "' to a character vector of DdiUrns for R6 " + relationTargetClass +  " class instances\n")            
          RFile.write("#'\n#'                              \code{my" +  classDDI4Name + "$" + objectRelation + "} returns a character vector of of DdiUrns for R6  " + relationTargetClass +  " class instances}\n")            

          RFile.write("#'   }\n")            
                      
               
      RFile.write("#' }\n\n\n")  

    
    
    RFile.write("#' \\subsection{Other methods:}{\n")
    RFile.write("#'   \describe{\n")                
    RFile.write("#'       \item{\code{describe_initializeArguments()}}{This returns a named list of character vectors. Each describes one argument of the initialize function (i.e. an argument of " + classDDI4Name +"$new())." + 
                " A DDI 4 property or association is the name of a description vector." + 
                " The description vector contains two elements, the cardinality of the initialize argument, and the datatype of that argument." + 
                " An argument value is a vector or a list. The cardinality shows the minimum and maximum number of values in that vector or list. }\n\n")            

             
    RFile.write("#'       \item{\code{setSilent()}}{This sets the object to silent mode. Validation notes will not be displayed" + 
                " These include messages about the object not being registered, or not found through the XML Catalog." + 
                " Silent mode is useful when importing a set of objects that may not be in any particular order }\n\n")            

                
    RFile.write("#'       \item{\code{setNotSilent()}}{This sets the object to verbose mode. Validation notes will be displayed" + 
                " These include messages about the object not being registered, or not found through the XML Catalog." + 
                " Verbose mode might be useful when writing scripts to create objects where the order may be controlled. }\n\n")            

              
    RFile.write("#'       \item{\code{getIsSilent()}}{This returns the current silent or verbose mode (silent is TRUE or FALSE.}\n\n")            

    RFile.write("#'       \item{\code{get_ClassGroup()}}{This returns the type of class: enumeration, regex, identifiable, or structuredDatatype.}\n\n")            
                 

 
    #  only Identifiable will have the get_DdiUrn method
    if className == "Identifiable":
        RFile.write("#'       \item{\code{DdiUrn()}}{returns the DDI URN for the object. The DDI URN is a globally unique identifier for the object. }\n")            


    RFile.write("#'       \item{\code{print()}}{prints the object with a layout specific to a DDI 4 R6 object. }\n\n")            


    RFile.write("#'       \item{\code{new(silentState=FALSE, ")
    if className in classProperties.keys():     
      RFile.write("=NA, ".join(classProperties[className].keys()) + "=NA" )             
    RFile.write(", ...)}}{The \code{new()} function creates an object of this class. All classes that inherit from the \"Identifiable\" class also have arguments of \"agency\", \"id\", and \"version\", that together comprise a globally unique identifier. Defaults for these will be generated if they are not entered. The resulting global identifier can be retrieved with the \code{get_DdiUrn()} function.}\n")
                
    RFile.write("#'    }\n")                   
    RFile.write("#' }\n")     
    RFile.write("\n\n")   
    RFile.write("#' @export  \n")

    

# --------------------------
#   Classes, Definition code 
# --------------------------  

    
    

    RFile.write(classDDI4Name + " <- R6Class(classname=\"" + classDDI4Name + "\",\n")
    
    if className in classParentName:
        RFile.write("  inherit = " + "DDI4_" + classParentName[className] + ",\n")
		
# ----------------------------------------------------------------    
#public fields or functions		
    RFile.write("  public = list(\n")
  



#  specific print function for DDI4 R6 classes    
    RFile.write("    print = function(...) {\n")
    RFile.write("      cat(ddiObjectFormat(self), sep=\"\\n\") \n")
    RFile.write("    },	\n")
    RFile.write("\n")    
  
  
  #  This function returns details on all aossible fields for the initialize function called as  new()
    RFile.write("    describe_initializeArguments = function(){\n")
    RFile.write("      private$.initializeFields\n")  
    RFile.write("    },\n")  
    
    
  # set the object to silent
    RFile.write("\n    setSilent = function(){\n")
    RFile.write("      private$isSilent <- TRUE\n")  
    RFile.write("    },\n")  
    
  # set the object to not silent
    RFile.write("\n    setNotSilent = function(){\n")
    RFile.write("      private$isSilent <- FALSE\n")  
    RFile.write("    },\n")  

  # retrieve silent status
    RFile.write("\n   getIsSilent = function(){\n")
    RFile.write("      private$isSilent\n")  
    RFile.write("    },\n")  

   # retrieve the class group
    RFile.write("    get_ClassGroup = function(){\n")
    RFile.write("      private$classGroup \n")
    RFile.write("    },\n\n")

    
    
  # initialize is the last in the public list, no trailing comma
  
  # initialize function. The initialize function calls the setter function for each argument supplied
    RFile.write("\n    initialize = function(registerThisObject=TRUE, silentState=FALSE, ")
    if className in classProperties.keys():     
        RFile.write("=NA, ".join(classProperties[className].keys()) + "=NA" )   
    
    relationsList=[]  
    if className in   classRelations.keys():
      relationsList=list(classRelations[className].keys())
      if 'realizes' in relationsList:
          relationsList.remove('realizes')
  
      if len(relationsList) >0:
        if className in classProperties.keys() :    
          RFile.write(",    ") 
        RFile.write("=NA,".join(relationsList) + "=NA" ) 
    if  (className in classProperties.keys()) or (len(relationsList) >0): 
      RFile.write(", ...){\n") 
    else:
      RFile.write("...){\n")  
        
  # check for abstract classes          
    RFile.write("      if (private$isAbstract){\n")    
    RFile.write("        stop(\"The abstract class " +   classDDI4Name  + " may not be instantiated\")\n")
    RFile.write("      }\n\n")             
        
  # set the silent property
    RFile.write("      if(!missing(silentState)){\n")
    RFile.write("        private$isSilent=silentState\n")      
    RFile.write("      }\n\n") 
        
    if className in classParentName:
      RFile.write("        #  pass through the register and silent parameters\n")   
      RFile.write("      super$initialize(registerThisObject=registerThisObject, silentState=silentState, ...)\n\n")      



# initialize properties via the appropriate validation function, dependent on the datatype of the property
      
    if className in classProperties.keys(): 
      for objectProperty in  classProperties[className].keys():
       minimumCardinality = classProperties[className][objectProperty]["minimumCardinality"] 
       if minimumCardinality == None:
            minimumCardinality = '0'
        
       maximumCardinality = classProperties[className][objectProperty]["maximumCardinality"]
       if maximumCardinality == "-1":
            maximumCardinality = "Inf"
          
       RFile.write("      if(!missing(" +  objectProperty + ")){\n")
       

       if classProperties[className][objectProperty]["dataType"] in primitiveDictionary.keys():
            #  primitive value(s) will be in a vector not a list
            RFile.write("        private$." + objectProperty + " <- returnVectorIfValid(validVectorClass=\"" + primitiveDictionary[classProperties[className][objectProperty]["dataType"]] + "\", proposedVector=" + objectProperty + " , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")
            
       elif classProperties[className][objectProperty]["dataType"] in classEnumerators:
            RFile.write("        private$." + objectProperty + " <- returnEnumeratedValues(enumerationObject=DDI4_" + classProperties[className][objectProperty]["dataType"] + "$new(registerThisObject=FALSE), proposedCharacterVector=" + objectProperty + " , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")

       elif classProperties[className][objectProperty]["dataType"] in umlDataTypeRegexes:
            RFile.write("        private$." + objectProperty + " <- returnRegularExpressionValues(RegularExpressionObject=DDI4_" + classProperties[className][objectProperty]["dataType"] + "$new(registerThisObject=FALSE), proposedCharacterVector=" + objectProperty + " , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")

            
       else:                        
            RFile.write("        private$." + objectProperty + " <- returnR6ObjectIfValid(validClass=\"DDI4_" + classProperties[className][objectProperty]["dataType"] + "\", proposedObjectList=" + objectProperty + " , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")
                    
                     
       if(objectProperty not in ['agency', 'version', 'id', 'isPersistent', 'isUniversallyUnique']):
          RFile.write("      }\n")  
  
  # for identifiable classes, defaults will be supplied for missing agency, version, and id properties
  # id defaults to a random UUID
  # the user can retrieve this with the get_DdiUrn function             
       if objectProperty == 'agency':
         RFile.write("      } else {\n        private$.agency <- \"example.org\"\n") 
         RFile.write("###          warning('Agency was missing. It has been set to \"example.org\" as a temporary value')\n")
         RFile.write("      }\n") 
       if objectProperty == 'version':
         RFile.write("      } else {\n         private$.version <- \"1\"\n")   
         RFile.write("###           warning('Version was missing. It has been set to \"1\" as a temporary value')\n")
         RFile.write("      }\n") 
       if objectProperty == 'id':
         RFile.write("      } else {\n         private$.id <- uuid::UUIDgenerate(TRUE)\n")
         RFile.write("###           warning(paste(\"Id was missing. It has been set to the unique random UUID: \",private$id, \"You can change this with the set_id method.\"))\n")
         RFile.write("      }\n") 

# the following have default values
       if objectProperty == 'isPersistent':
         RFile.write("      } else {\n           private$.isPersistent <- FALSE\n")
         RFile.write("      }\n") 

       if objectProperty == 'isUniversallyUnique':
         RFile.write("      } else {\n           private$.isUniversallyUnique <- FALSE\n")
         RFile.write("      }\n") 

  
  
      RFile.write("\n      private$.DdiUrn <- paste(\"urn\",\"ddi\",private$.agency,private$.id,private$.version,sep=\":\")\n") 
      
      # this is a new object, register it, but only in the initialize for Identifiable
      if className == "Identifiable":
          RFile.write("\n      if(registerThisObject) registerObject(DdiUrn=private$.DdiUrn, self)  \n\n") 

      RFile.write("\n\n ")
  # initialize relationships with a vector of URNs of the related class      
    if len(relationsList) >0:
        RFile.write("\n\n ")
        for objectRelation in relationsList:
          targetCardinality = classRelations[className][objectRelation]["targetCardinality"] 
          minimumTargetCardinality = targetCardinality[0]
          maximumTargetCardinality = targetCardinality[3]
          if  maximumTargetCardinality == 'n' or maximumTargetCardinality == '-1':
             maximumTargetCardinality = 'Inf' 
  
          RFile.write("      if(!missing(" + objectRelation + ")){\n")
                      
          RFile.write("        private$." + objectRelation + " <- returnUrnVectorIfValid(validClass=\"DDI4_" + classRelations[className][objectRelation]["relationTarget"] + "\", proposedUrnVector=" + objectRelation + " , minimumCardinality=" + minimumTargetCardinality + ", maximumCardinality=" + maximumTargetCardinality + ", silent=silentState)  \n")            
          RFile.write("        thisUrn <- paste(\"urn\",\"ddi\",private$.agency,private$.id,private$.version,sep=\":\")\n") 
          RFile.write("        result <- addToReferencedUrn(referencedUrn=private$."+ objectRelation +  ", referencingAssociationName=\""   + objectRelation +  "\",  referencingUrn=thisUrn)  \n")            

          RFile.write("      }\n") 
    RFile.write("    }\n")    
            
          
          

    RFile.write("  ),\n") # end of public list
   



# ----------------------------------------------------------------    
# active bindings for all properties and relationships of the class  

    RFile.write("\n  active = list(\n")
 
# primitive properties, e.g. xs:string, are validated with the function returnVectorIfValid
# complex datatypes and associations are validated with returnR6ObjectIfValid    

    numberOfProperties = 0
    if className in classProperties.keys():
      numberOfProperties = len(classProperties[className].keys())  
      propertyNumber = 0  
      
      if numberOfProperties >0:
        RFile.write("\n# There are " + str(numberOfProperties) + " properties: \n") 
      
      for objectProperty in sorted(classProperties[className].keys()): 
        propertyNumber = propertyNumber +1  
        RFile.write("    " + objectProperty + " = function(val){\n" )  
        RFile.write("      if (!missing(val)){\n" )  
        minimumCardinality = classProperties[className][objectProperty]["minimumCardinality"] 
        if minimumCardinality == None:
            minimumCardinality = '0'
        
        maximumCardinality = classProperties[className][objectProperty]["maximumCardinality"]
        if maximumCardinality == "-1":
            maximumCardinality = "Inf"
            
        if  objectProperty in ['agency','version','id']:
          RFile.write("        if (private$.isPersistent) stop(\" isPersistent is true for this object. The agency, id or version cannot be changed for a persistent object.\") \n")
          RFile.write("# this object exists deRegister it before changing its DdiUrn \n")
          RFile.write("        deRegisterObject(private$.DdiUrn)  \n")            
          
        if classProperties[className][objectProperty]["dataType"] in primitiveDictionary.keys():
            #  primitive value(s) will be in a vector not a list
            RFile.write("        private$." + objectProperty + " <- returnVectorIfValid(validVectorClass=\"" + primitiveDictionary[classProperties[className][objectProperty]["dataType"]] + "\", proposedVector=val, minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")
            
        elif classProperties[className][objectProperty]["dataType"] in classEnumerators:
            RFile.write("        private$." + objectProperty + " <- returnEnumeratedValues(enumerationObject=DDI4_" + classProperties[className][objectProperty]["dataType"] + "$new(registerThisObject=FALSE), proposedCharacterVector=val , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")

        elif classProperties[className][objectProperty]["dataType"] in umlDataTypeRegexes:
            RFile.write("        private$." + objectProperty + " <- returnRegularExpressionValues(RegularExpressionObject=DDI4_" + classProperties[className][objectProperty]["dataType"] + "$new(registerThisObject=FALSE), proposedCharacterVector=val , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")

            
        else:                        
            RFile.write("        private$." + objectProperty + " <- returnR6ObjectIfValid(validClass=\"DDI4_" + classProperties[className][objectProperty]["dataType"] + "\", proposedObjectList=val , minimumCardinality=" + minimumCardinality + ", maximumCardinality=" + maximumCardinality + ")  \n")

# if the DDI global identifier changes, update the DDI URN
# NOTE !!!!!!   checking for allowed changes and reregistering will be needed here !!!!!!            
        if  objectProperty in ['agency','version','id']:
          RFile.write("        private$.DdiUrn <- paste(\"urn\",\"ddi\",private$.agency,private$.id,private$.version,sep=\":\")\n")
          RFile.write("        registerObject(DdiUrn=private$.DdiUrn, self)\n")

        RFile.write("      } else {\n")   		          
        RFile.write("        private$." + objectProperty + "\n")
        RFile.write("      }\n")  
			
        RFile.write("    }")     
        if className in   classRelations.keys():  
          numberOfRelations = len(classRelations[className].keys())  
          if "realizes" in classRelations[className].keys():
            numberOfRelations = numberOfRelations - 1
        else:
            numberOfRelations = 0

        
         
        if (propertyNumber < numberOfProperties) or numberOfRelations > 0 or (className in  classAncestorList.keys() and "Identifiable" in classAncestorList[className]):
          RFile.write(",\n\n") 
        else:
          RFile.write("\n\n") 
		  
		  
# set and gets for all relationships of the class   
    numberOfRelations = 0
    if className in   classRelations.keys():  
      numberOfRelations = len(classRelations[className].keys())  
      if "realizes" in classRelations[className].keys():
          numberOfRelations = numberOfRelations - 1
          
      RFile.write("\n# There are " + str(numberOfRelations) + " class association(s):\n") 

      relationNumber = 0        
      for objectRelation in sorted(classRelations[className].keys()):                
    
        if objectRelation != "realizes":  
          relationNumber = relationNumber +1    
          RFile.write("    " + objectRelation + " = function(val){\n" )  
          RFile.write("      if (!missing(val)){\n" )  
          targetCardinality = classRelations[className][objectRelation]["targetCardinality"] 
          minimumTargetCardinality = targetCardinality[0]
          maximumTargetCardinality = targetCardinality[3]
          if  maximumTargetCardinality == 'n' or maximumTargetCardinality == '-1':
              maximumTargetCardinality = 'Inf' 
  
         # RFile.write("#NOTE: maximumTargetCardinality is:" + maximumTargetCardinality + "\n")
         
         # if classRelations[className][objectRelation]["relationTarget"] in classEnumerators, 
         # the value must be text from the enumerated list
          
          if classRelations[className][objectRelation]["relationTarget"] in classEnumerators:
               RFile.write(" stop(\" Cannot have a relationship to an enumeration. \")")
          else:           
            RFile.write("        private$." + objectRelation + " <- returnUrnVectorIfValid(validClass=\"DDI4_" + classRelations[className][objectRelation]["relationTarget"] + "\", proposedUrnVector=val , minimumCardinality=" + minimumTargetCardinality + ", maximumCardinality=" + maximumTargetCardinality + ", silent= private$isSilent)  \n")
            RFile.write("        thisUrn <- paste(\"urn\",\"ddi\",private$.agency,private$.id,private$.version,sep=\":\")\n") 
            RFile.write("        result <- addToReferencedUrn(referencedUrn=private$."+ objectRelation +  ", referencingAssociationName=\""   + objectRelation +  "\",  referencingUrn=thisUrn)  \n")            
            RFile.write("       # invisible( private$." + objectRelation + ")\n")  

          RFile.write("      } else {\n") 			
		  
          RFile.write("          private$." + objectRelation + "\n")
          RFile.write("      }\n")  
        
          RFile.write("    }")  
              
       
          if (relationNumber < numberOfRelations) or (className in  classAncestorList.keys() and "Identifiable" in classAncestorList[className]):
            RFile.write(","  + "\n\n") 
          else:
            RFile.write("\n")                    
            
  # DdiUrn is read only   and is only there for Identifiables

    if className in  classAncestorList.keys() and "Identifiable" in classAncestorList[className]:
          RFile.write("    DdiUrn = function(val){\n")
          RFile.write("      if (missing(val)){\n" )            
          RFile.write("        private$.DdiUrn  \n")
          RFile.write("      } else {\n")  
          RFile.write("          stop(paste('DdiUrn is read only, to change a DDI identifier, assign the value of agency, id, or version independently.'))\n")      
          RFile.write("      }\n")
          RFile.write("    }\n\n")
    else:    
          RFile.write("\n") 	             
            
		  
    RFile.write("  ),\n") # end of active list		  



# ----------------------------------------------------------------  
# private fields
   
    RFile.write("\n  private = list(\n")
    if className in classProperties.keys():
      #objectPropertyList = list(classProperties[className].keys())  
      for objectProperty in classProperties[className].keys():
          RFile.write("    ." + objectProperty + " = NULL,\n") 

    if className in classRelations.keys():
      for relation in classRelations[className].keys():
          if relation != "realizes":
            RFile.write("    ." + relation + " = NULL,\n") 
            
    # DdiUrn should be a property of Identifiable only  and inherited, but that causes problems with update                
   #      if className == "Identifiable":
    RFile.write("\n    .DdiUrn = NULL,\n")
    
    
    # a structure for all arguments for initialize, uncluding their constraints
    RFile.write("\n    .initializeFields = list(")  
    nKeys = len(allFields[className].keys())
    key = 0
    for fieldName in sorted(allFields[className].keys()):
      key = key + 1  
      RFile.write("\n        " + fieldName + " = c(" )

      RFile.write("\n          min=" + '"' + allFields[className][fieldName][0].split('..')[0] + '", ')   
      maxCardinality = allFields[className][fieldName][0].split('..')[1]
      if maxCardinality == "n":
          maxCardinality = "Inf"
      RFile.write("\n          max=" + '"' + maxCardinality + '", ')   

# DDI4 class names have the prefix "DDI4_", but atomic fields do not.
      Ddi4Datatype = allFields[className][fieldName][1]
      if (Ddi4Datatype  in classIsAbstract.keys()) or (Ddi4Datatype in classEnumerators.keys())  or (Ddi4Datatype in umlDataTypeRegexes.keys()):
          Ddi4Datatype = "DDI4_" + Ddi4Datatype 
      
      RFile.write("\n          datatype = " + '"' + Ddi4Datatype + '", ' ) 

      if allFields[className][fieldName][2] == 'DdiClass':
        if allFields[className][fieldName][1] in IdentifiableClassList:
           RFile.write('\n          structure = "DdiUrn vector"' ) 
        elif allFields[className][fieldName][1] in classEnumerators:    
          RFile.write('\n          structure = "Enumeration vector"' )  
        elif allFields[className][fieldName][1] in umlDataTypeRegexes:    
          RFile.write('\n          structure = "Regex vector"' )  
        else:    
          if classIsAbstract[allFields[className][fieldName][1]]:
              RFile.write('\n          isAbstractDatatype = TRUE,' )
          else:
              RFile.write('\n          isAbstractDatatype = FALSE,' )
          RFile.write('\n          structure = "StructuredDatatype list"' )  

      else:
        RFile.write('\n          structure = "vector"' ) 
          
   

      RFile.write("\n        )"  )
      if key < nKeys:
        RFile.write("," )
    RFile.write("\n    ),\n")      
    
# by default all classes are not silent
    RFile.write("\n     isSilent = FALSE,\n")
    if className in IdentifiableClassList:
        RFile.write("     classGroup = c(\"identifiable\"),\n") 
    else:
        RFile.write("     classGroup = c(\"structuredDatatype\"),\n")
    
    if classIsAbstract[className]:
        RFile.write("\n     isAbstract = TRUE\n")
    else:
        RFile.write("\n     isAbstract = FALSE\n")
   
    RFile.write("  )\n") # end of private list
   
   
   
   
    RFile.write(")  # ends class definition for " + classDDI4Name + "\n")   # ends class definition

            
RFile.close()
  

###############################################################################
#  Now write the package documentation file
# look for all title lines, excluding DDI4_ names

RFile = open(DataFolder + "DDI4RClasses.R", 'r', encoding="utf-8")

PackageFile = open(DataFolder + "DDI4R.R", 'w', encoding="utf-8")

# write the package description

PackageFile.write("#' DDI4R: A package for working with DDI4 in R.\n")
PackageFile.write("#'\n")
PackageFile.write("#' The package provides one R6 class for each class in the DDI4 model.\n")
PackageFile.write("#' It also contains functions for working with these classes.\n")
PackageFile.write("#' Some DDI4 classes are intended to be reusable and therefore have a mandatory identifier - a DDI URN.\n")
PackageFile.write("#' These are referred to here as \"identifiable\" objects.\n")
PackageFile.write("#' The DDI Urn is a composite of an Id, a Version, and an Acency. The agency is responsible for ensuring the uniqueness of the Id.\n")
PackageFile.write("#' DDI4R identifiable objects have a read only active binding \"DdiUrn\" that returns the DDI URN.\n")
PackageFile.write("#' When identifiable DDI4R objects are created, a reference from the object's DdiUrn to the object is put in the DDI4R local registry.\n")
PackageFile.write("#' The R6 object can be found by lookup from this registry. \n")
PackageFile.write("#' A second registry allows reverse lookup to find the objects that reference an object. \n")                  
PackageFile.write("#' @section Details:\n")
PackageFile.write("#' Each R6 class representing a DDI4 class has an active binding for each property and association in the represented DDI4 class.\n")
PackageFile.write("#' Active bindings act like properties of the object, but can run validation code on assignment.\n")
PackageFile.write("#' Values for the property or relationship may be set, or retrieved. \n")
PackageFile.write("#' Relationships (associations) are represented by a DdiUrn.\n")
PackageFile.write("#' DDI4R object properties can be categorized into five groups:\n")
PackageFile.write("#' \\itemize{\n")
PackageFile.write("#'   \\item References - A list of DDI URNs pointing to referenced DDI4R objects\n")
PackageFile.write("#'   \\item Simple atomic values - A vector of atomic values (numeric, character, logical, etc)\n")
PackageFile.write("#'   \\item RegularExpression controlled values - A vector of simple atomic values with an associated regular expression for validation\n")
PackageFile.write("#'   \\item Enumeration controlled values - A vector of simple atomic values that must come from a predefined enumeration\n")
PackageFile.write("#'   \\item Structured Datatypes - a list of DDI4R objects. These are not identifiable. They may have multiple properties, including Structured Datatypes.\n")
PackageFile.write("#' }\n")
PackageFile.write("#'  \n")
PackageFile.write("#' @section DDI4R functions:\n")
PackageFile.write("#' The DDI4R package includes multiple functions for working with DDI4R objects.\n")
PackageFile.write("#' \\itemize{\n")

#Write documentation for each function

lines = RFile.readlines()

# a dictionary of the titles
titleDict = {}

# a dictionary of the classes
classDict = {}

titleRegex = re.compile(r'''(
       ^\#'\s*@title\s*   # title
       (\S+)[\s-]+       # name
       (.*)$              # description
       )''', re.VERBOSE)

# 

for line in lines:
    title = re.match(titleRegex, line)
    if title:
        name = title.group(2)
        nameStart = name[:4]
        if nameStart != "DDI4":
            titleDict[name] = title.group(3)
        elif  name[:5] == "DDI4_":   
            classDict[name] = title.group(3)
            
for name in sorted(titleDict.keys()):            
     PackageFile.write("#'   \item {\code{\link{" + name + "}}} 		" + titleDict[name] + "\n")
    
# close out the package description

PackageFile.write("#' }\n")
PackageFile.write("#' \n")
PackageFile.write("#'\n")
PackageFile.write("#' @section DDI4 Class List:\n")
PackageFile.write("#' Here is a list of DDI4R classes:\n")
PackageFile.write("#' \\itemize{\n")
                  
for name in sorted(classDict.keys()):            
     PackageFile.write("#'   \item {\code{\link{" + name + "}}} 		" + classDict[name] + "\n")                  

PackageFile.write("#' }\n")                  
PackageFile.write("#' @docType package\n")
PackageFile.write("#' @name DDI4R\n")
PackageFile.write("NULL\n")
PackageFile.write("\n")

RFile.close()
PackageFile.close()

DebugFile.close()

# which classes extend AnnotatedIdentifiable
#AnnotatedFile = open(DataFolder +  "AnnotatedClasses", 'w', encoding="utf-8")
#for childClass, parentClass in classParentName.items():
#    if parentClass=='AnnotatedIdentifiable':
#        AnnotatedFile.write(childClass + " \n")
#AnnotatedFile.close()
 
