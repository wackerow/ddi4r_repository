library(R6)
library(uuid)
library(XML)
library(RCurl)
library(stringr)
library(DDI4R)

# A DDI "document" containing several DDI4 elements
#  the first child element should be a DocumentInformation element
#

#  vvvvv ------ Beginning elment:   DocumentInformation ------ vvvvv

    param_basedOn1 <- as(c("https://www.ada.edu.au/ada/01259"), "character")
    BasedOnObjectInformation1 <- BasedOnObjectInformation$new(basedOn=param_basedOn1)

  param_basedOnObject1 <- list(BasedOnObjectInformation1)
          param_content1 <- as(c("Larry Hoyle"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName1 <- list(BibliographicName1)
          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("DDI4 XML entry"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent2 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("variable selection"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term2 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry2 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent2, term=param_term2)

      param_role1 <- list(PairedExternalControlledVocabularyEntry1, PairedExternalControlledVocabularyEntry2)
      AgentAssociation1 <- AgentAssociation$new(agentName=param_agentName1, role=param_role1)

          param_content1 <- as(c("Dan Gillman"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName2 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("variable selection"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

      param_role2 <- list(PairedExternalControlledVocabularyEntry1)
      AgentAssociation2 <- AgentAssociation$new(agentName=param_agentName2, role=param_role2)

          param_content1 <- as(c("Knut Wenzig"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName3 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("variable selection"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

      param_role3 <- list(PairedExternalControlledVocabularyEntry1)
      AgentAssociation3 <- AgentAssociation$new(agentName=param_agentName3, role=param_role3)

          param_content1 <- as(c("Arofan Gregory"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName4 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("variable selection"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

      param_role4 <- list(PairedExternalControlledVocabularyEntry1)
      AgentAssociation4 <- AgentAssociation$new(agentName=param_agentName4, role=param_role4)

    param_contributor1 <- list(AgentAssociation1, AgentAssociation2, AgentAssociation3, AgentAssociation4)
        param_content1 <- as(c("Original Study Copyright � 2014, The Australian National University. All rights reserved"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_copyright1 <- list(InternationalString1)
        param_content1 <- as(c("Original codebook, with highlighting to show information elements covered by DDI4 subset"), "character")
        ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

      param_typeOfRelatedResource1 <- list(ExternalControlledVocabularyEntry1)
      param_identifierContent1 <- as(c("https://ddi-alliance.atlassian.net/wiki/download/attachments/112427055/ADA.CODEBOOK.01259Highlighted2017_11_08.pdf?api=v2"), "character")
      param_isURI1 <- as(c("TRUE"), "logical")
      ResourceIdentifier1 <- ResourceIdentifier$new(identifierContent=param_identifierContent1, isURI=param_isURI1, typeOfRelatedResource=param_typeOfRelatedResource1)

        param_content1 <- as(c("Twelve variable extract of original data, in CSV format"), "character")
        ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

      param_typeOfRelatedResource2 <- list(ExternalControlledVocabularyEntry1)
      param_identifierContent2 <- as(c("https://ddi-alliance.atlassian.net/wiki/download/attachments/112427055/aes_2013_01259Subset.csv?api=v2"), "character")
      param_isURI2 <- as(c("TRUE"), "logical")
      ResourceIdentifier2 <- ResourceIdentifier$new(identifierContent=param_identifierContent2, isURI=param_isURI2, typeOfRelatedResource=param_typeOfRelatedResource2)

    param_relatedResource1 <- list(ResourceIdentifier1, ResourceIdentifier2)
        param_content1 <- as(c(" 
                 This is an example of documentation of a subset of the Australian Election Study 2013, first developed 
                 at the DDI4 Dagstuhl week 2 Sprint 2016 as an example of physical formatting of data, and further 
                 developed at the DDI4 Dagstuhl week 1 Sprint 2017 as a test case for the simple codebook view.
                 Twelve variables were selected from the original set of 354.
                 The study is documented with information copied from the pdf of the codebook from the 
                 Australian Data Archive restructured into DDI4 XML.
         "), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_summary1 <- list(InternationalString1)
        param_content1 <- as(c("DDI4 Subset of the Australian Election Study, 2013"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_title1 <- list(InternationalString1)
    Annotation1 <- Annotation$new(contributor=param_contributor1, copyright=param_copyright1, relatedResource=param_relatedResource1, summary=param_summary1, title=param_title1)

  param_hasAnnotation1 <- list(Annotation1)
    param_localIdValue1 <- as(c("au.edu.anu.ada.ddi.01259"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId1 <- list(LocalIdFormat1)
  param_agency1 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasPrimaryContent1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdStudy:1"), "character")
  param_id1 <- as(c("IdDDIMetadata"), "character")
  param_isPersistent1 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique1 <- as(c("FALSE"), "logical")
  param_ofType1 <- as(c("DR0.2"), "character")
  param_version1 <- as(c("1"), "character")
  param_versionDate1 <- as(c("2017-10-20"), "character")
  DocumentInformation1 <- DocumentInformation$new(agency=param_agency1, basedOnObject=param_basedOnObject1, hasAnnotation=param_hasAnnotation1, hasPrimaryContent=param_hasPrimaryContent1, id=param_id1, isPersistent=param_isPersistent1, isUniversallyUnique=param_isUniversallyUnique1, localId=param_localId1, ofType=param_ofType1, version=param_version1, versionDate=param_versionDate1)

#  ^^^^^ ------ Ending elment:   DocumentInformation ------ ^^^^^




#  vvvvv ------ Beginning elment:   Study ------ vvvvv

        param_content1 <- as(c("This study metadata describesa subset of the complete Australian Election Study, 2013. 
                    The subset was created at Dagstuhl event 16433, October 23 � 28 , 2016. 
                    At that sprint the subset was used to validate the ability of DDI4 to describe physical layout in both CSV and fixed column format.
                    A a sprint the next year; Dagstuhl event 17423, October 16 - 20, 2017; the subset was chosen for documentation as a  complete codebook. 
            "), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_basedOnRationaleDescription1 <- list(InternationalString1)
    param_basedOn1 <- as(c("https://www.ada.edu.au/ada/01259"), "character")
    BasedOnObjectInformation1 <- BasedOnObjectInformation$new(basedOn=param_basedOn1, basedOnRationaleDescription=param_basedOnRationaleDescription1)

  param_basedOnObject2 <- list(BasedOnObjectInformation1)
          param_content1 <- as(c("Bean, Clive"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName1 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Author"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent2 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Study Investigator Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term2 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry2 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent2, term=param_term2)

      param_role1 <- list(PairedExternalControlledVocabularyEntry1, PairedExternalControlledVocabularyEntry2)
      param_associatedAgent1 <- as(c("urn:ddi:dagstuhl17423.ddialliance.org:IdBean:1"), "character")
      AgentAssociation1 <- AgentAssociation$new(agentName=param_agentName1, associatedAgent=param_associatedAgent1, role=param_role1)

          param_content1 <- as(c("McAllister, Ian"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName2 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Author"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent2 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Study Investigator Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term2 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry2 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent2, term=param_term2)

          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent3 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Digital Preservation Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term3 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry3 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent3, term=param_term3)

          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent4 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("Depositor"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_term4 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry4 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent4, term=param_term4)

      param_role2 <- list(PairedExternalControlledVocabularyEntry1, PairedExternalControlledVocabularyEntry2, PairedExternalControlledVocabularyEntry3, PairedExternalControlledVocabularyEntry4)
      param_associatedAgent2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdMcAllister:1"), "character")
      AgentAssociation2 <- AgentAssociation$new(agentName=param_agentName2, associatedAgent=param_associatedAgent2, role=param_role2)

          param_content1 <- as(c("Pietsch, Juliet"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName3 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Author"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent2 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Study Investigator Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term2 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry2 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent2, term=param_term2)

      param_role3 <- list(PairedExternalControlledVocabularyEntry1, PairedExternalControlledVocabularyEntry2)
      param_associatedAgent3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdPietsch:1"), "character")
      AgentAssociation3 <- AgentAssociation$new(agentName=param_agentName3, associatedAgent=param_associatedAgent3, role=param_role3)

          param_content1 <- as(c("Gibson, Rachel Kay"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName4 <- list(BibliographicName1)
          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Author"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

          param_content1 <- as(c("Equal"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent2 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Study Investigator Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term2 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry2 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent2, term=param_term2)

      param_role4 <- list(PairedExternalControlledVocabularyEntry1, PairedExternalControlledVocabularyEntry2)
      param_associatedAgent4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdGibson:1"), "character")
      AgentAssociation4 <- AgentAssociation$new(agentName=param_agentName4, associatedAgent=param_associatedAgent4, role=param_role4)

          param_content1 <- as(c("The Social Research Centre (SRC)"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName5 <- list(BibliographicName1)
          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Data Collection Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

      param_role5 <- list(PairedExternalControlledVocabularyEntry1)
      param_associatedAgent5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdOrgSRC:1"), "character")
      AgentAssociation5 <- AgentAssociation$new(agentName=param_agentName5, associatedAgent=param_associatedAgent5, role=param_role5)

    param_contributor1 <- list(AgentAssociation1, AgentAssociation2, AgentAssociation3, AgentAssociation4, AgentAssociation5)
        param_content1 <- as(c("Copyright � 2014, The Australian National University. All rights reserved"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_copyright1 <- list(InternationalString1)
          param_content1 <- as(c("Australian Data Archive"), "character")
          LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

        param_languageSpecificString1 <- list(LanguageSpecificStringType1)
        BibliographicName1 <- BibliographicName$new(languageSpecificString=param_languageSpecificString1)

      param_agentName1 <- list(BibliographicName1)
          param_content1 <- as(c("Lead"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

        param_extent1 <- list(ExternalControlledVocabularyEntry1)
          param_content1 <- as(c("cro:Archivist Role"), "character")
          param_uri1 <- as(c("https://github.com/openrif/contribution-ontology/blob/master/diagrams/ContributionRoleHierarchy.png"), "character")
          ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

        param_term1 <- list(ExternalControlledVocabularyEntry1)
        PairedExternalControlledVocabularyEntry1 <- PairedExternalControlledVocabularyEntry$new(extent=param_extent1, term=param_term1)

      param_role1 <- list(PairedExternalControlledVocabularyEntry1)
      param_associatedAgent1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdOrgADA:1"), "character")
      AgentAssociation1 <- AgentAssociation$new(agentName=param_agentName1, associatedAgent=param_associatedAgent1, role=param_role1)

    param_publisher1 <- list(AgentAssociation1)
        param_content1 <- as(c("Study Documentation"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_subTitle1 <- list(InternationalString1)
        param_content1 <- as(c("The 2013 Australian Election Study (au.edu.anu.ada.ddi.01259) is the tenth in a series of surveys beginning in 1987 that
                have been timed to coincide with Australian Federal Elections. The series also builds on the 1967, 1969 and 1979 Australian
                Political Attitudes Surveys. The Australian Election Studies aim to provide a long-term perspective on stability and change
                in the political attitudes and behaviour of the Australian electorate, and investigate the changing social bases of Australian
                politics as the economy and society modernise and change character. In addition to these long-term goals, they examine the
                political issues prevalent in the current election and assess their importance for the election result.
                The 2013 survey replicates many questions from the previous Australian Election Studies, but also introduces new questions
                regarding immigrants to Australia. Other sections cover the respondent's interest in the election campaign and politics, their
                past and present political affiliation, evaluation of parties and candidates, alignment with parties on various election issues,
                evaluation of the current economic situation, and attitudes to a range of election issues including global warming, taxation,
                education, unemployment, health and Medicare, refugees and asylum seekers, and population policy. Opinions on social
                policy issues including abortion, equal opportunities, same sex marriages, and Australia's security were also covered in the
                2013 Australian Election Study.
                Background variables include level of education, employment status, occupation, type of employer, position at workplace,
                trade union membership, sex, age, own and parents' country of birth, parents' political preferences, religion, marital status,
                income, and where applicable, the occupation, trade union membership and political preference of the respondent's partner.
                The 2013 Australian Election Study is the second in the AES series to provide the option of completing the questionnaire
                online via a unique password, or via hardcopy. 576 of the 3,955 responses (14.6 percent) were completed online.
                This data file is also supplemented with division level election results from the Australian Electoral Commission election
                results website (http://www.aec.gov.au/Elections/Federal_Elections/2013/index.htm). These variables are found under the
                variable group \"AEC Election Results\". Information on the source of these election results and the method by which they
                were created is available in a Stata *.do syntax file located in the other study materials section.results and the method by
                which they were created is available in a Stata *.do syntax file located in the other study materials section."), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_summary1 <- list(InternationalString1)
        param_content1 <- as(c("Australian Election Study, 2013"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_title1 <- list(InternationalString1)
      param_content1 <- as(c("Australian Election Study"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfResource1 <- list(ExternalControlledVocabularyEntry1)
    Annotation1 <- Annotation$new(contributor=param_contributor1, copyright=param_copyright1, publisher=param_publisher1, subTitle=param_subTitle1, summary=param_summary1, title=param_title1, typeOfResource=param_typeOfResource1)

  param_hasAnnotation2 <- list(Annotation1)
    param_content1 <- as(c("survey data"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_kindOfData2 <- list(ExternalControlledVocabularyEntry1)
    param_localIdValue1 <- as(c("au.edu.anu.ada.ddi.01259"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId2 <- list(LocalIdFormat1)
        param_content1 <- as(c("Version 1.0 First release"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_rationaleDescription1 <- list(InternationalString1)
    RationaleDefinition1 <- RationaleDefinition$new(rationaleDescription=param_rationaleDescription1)

  param_versionRationale2 <- list(RationaleDefinition1)
  param_agency2 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasAnalysisUnit2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdUnitType:1"), "character")
  param_hasCoverage2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCoverage:1"), "character")
  param_hasFundingInformation2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdFunding:1"), "character")
  param_hasInstanceVariable2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivisNum:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarUniqueId:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarMode:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarDateComp:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarState:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarA4:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarG1Age:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarXg5:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarPartyAby:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdVarSwingn:1"), "character")
  param_hasMethodology2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdWeighting:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdDataCollection:1", "URN:DDI:dagstuhl17423.ddialliance.org:IdDataProcessing:1"), "character")
  param_hasSamplingProcedure2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSampling:1"), "character")
  param_id2 <- as(c("IdStudy"), "character")
  param_isPersistent2 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique2 <- as(c("FALSE"), "logical")
  param_version2 <- as(c("1"), "character")
  param_versionDate2 <- as(c("2014-02-28"), "character")
  Study2 <- Study$new(agency=param_agency2, basedOnObject=param_basedOnObject2, hasAnalysisUnit=param_hasAnalysisUnit2, hasAnnotation=param_hasAnnotation2, hasCoverage=param_hasCoverage2, hasFundingInformation=param_hasFundingInformation2, hasInstanceVariable=param_hasInstanceVariable2, hasMethodology=param_hasMethodology2, hasSamplingProcedure=param_hasSamplingProcedure2, id=param_id2, isPersistent=param_isPersistent2, isUniversallyUnique=param_isUniversallyUnique2, kindOfData=param_kindOfData2, localId=param_localId2, version=param_version2, versionDate=param_versionDate2, versionRationale=param_versionRationale2)

#  ^^^^^ ------ Ending elment:   Study ------ ^^^^^




#  vvvvv ------ Beginning elment:   Access ------ vvvvv

      param_content1 <- as(c("All manuscripts based in whole or in part on these data should 
                (i) identify the data and original investigators by using the recommended bibliographic reference to the data file;
                (ii) acknowledge the Australian Data Archive and, where the data are made available through the Australian Data Archive by another archive, acknowledge that archive;
                (iii) declare that those who carried out the original analysis and collection of the data bear no responsibility for the further analysis or interpretation of them."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_citationRequirement3 <- list(InternationalStructuredString1)
      param_content1 <- as(c("
                A copy of the User Undertaking Form must be signed before the data can be accessed.
            "), "character")
      param_isPlainText1 <- as(c("TRUE"), "logical")
      param_isTranslated1 <- as(c("FALSE"), "logical")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1, isPlainText=param_isPlainText1, isTranslated=param_isTranslated1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_confidentialityStatement3 <- list(InternationalStructuredString1)
    param_associatedAgent1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdMcAllister:1"), "character")
    AgentAssociation1 <- AgentAssociation$new(associatedAgent=param_associatedAgent1)

  param_contactAgent3 <- list(AgentAssociation1)
      param_content1 <- as(c("Disclaimer: Use of the material is solely at the user's risk. The depositor, The Australian National University and the Australian Data
                Archive shall not be held responsible for the accuracy and completeness of the material supplied."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_restrictions3 <- list(InternationalStructuredString1)
  param_agency3 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id3 <- as(c("IdStudyAccess"), "character")
  param_isPersistent3 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique3 <- as(c("FALSE"), "logical")
  param_version3 <- as(c("1"), "character")
  Access3 <- Access$new(agency=param_agency3, citationRequirement=param_citationRequirement3, confidentialityStatement=param_confidentialityStatement3, contactAgent=param_contactAgent3, id=param_id3, isPersistent=param_isPersistent3, isUniversallyUnique=param_isUniversallyUnique3, restrictions=param_restrictions3, version=param_version3)

#  ^^^^^ ------ Ending elment:   Access ------ ^^^^^




#  vvvvv ------ Beginning elment:   Coverage ------ vvvvv

  param_agency4 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasSpatialCoverage4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSpatialCoverage:1"), "character")
  param_hasTemporalCoverage4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdTemporalCoverage:1"), "character")
  param_hasTopicalCoverage4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdTopicalCoverage:1"), "character")
  param_id4 <- as(c("IdCoverage"), "character")
  param_isPersistent4 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique4 <- as(c("FALSE"), "logical")
  param_version4 <- as(c("1"), "character")
  Coverage4 <- Coverage$new(agency=param_agency4, hasSpatialCoverage=param_hasSpatialCoverage4, hasTemporalCoverage=param_hasTemporalCoverage4, hasTopicalCoverage=param_hasTopicalCoverage4, id=param_id4, isPersistent=param_isPersistent4, isUniversallyUnique=param_isUniversallyUnique4, version=param_version4)

#  ^^^^^ ------ Ending elment:   Coverage ------ ^^^^^




#  vvvvv ------ Beginning elment:   SpatialCoverage ------ vvvvv

    param_content1 <- as(c("AU"), "character")
    param_controlledVocabularyName1 <- as(c("ISO3166-1"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_spatialAreaCode5 <- list(ExternalControlledVocabularyEntry1)
  param_agency5 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id5 <- as(c("IdSpatialCoverage"), "character")
  param_includesGeographicUnit5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:GeoUnit_1:1"), "character")
  param_isPersistent5 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique5 <- as(c("FALSE"), "logical")
  param_lowestGeographicUnitTypeCovered5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdNationUnit:1"), "character")
  param_version5 <- as(c("1"), "character")
  SpatialCoverage5 <- SpatialCoverage$new(agency=param_agency5, id=param_id5, includesGeographicUnit=param_includesGeographicUnit5, isPersistent=param_isPersistent5, isUniversallyUnique=param_isUniversallyUnique5, lowestGeographicUnitTypeCovered=param_lowestGeographicUnitTypeCovered5, spatialAreaCode=param_spatialAreaCode5, version=param_version5)

#  ^^^^^ ------ Ending elment:   SpatialCoverage ------ ^^^^^




#  vvvvv ------ Beginning elment:   UnitType ------ vvvvv

    param_content1 <- as(c("Nation"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name6 <- list(ObjectName1)
  param_agency6 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id6 <- as(c("IdNationUnit"), "character")
  param_isPersistent6 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique6 <- as(c("FALSE"), "logical")
  param_version6 <- as(c("1"), "character")
  Nation <- UnitType$new(agency=param_agency6, id=param_id6, isPersistent=param_isPersistent6, isUniversallyUnique=param_isUniversallyUnique6, name=param_name6, version=param_version6)

#  ^^^^^ ------ Ending elment:   UnitType ------ ^^^^^




#  vvvvv ------ Beginning elment:   GeographicUnit ------ vvvvv

    param_content1 <- as(c("Australia"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name7 <- list(ObjectName1)
  param_agency7 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasUnitType7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdNationUnit:1"), "character")
  param_id7 <- as(c("GeoUnit_1"), "character")
  param_isPersistent7 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique7 <- as(c("FALSE"), "logical")
  param_version7 <- as(c("1"), "character")
  Australia <- GeographicUnit$new(agency=param_agency7, hasUnitType=param_hasUnitType7, id=param_id7, isPersistent=param_isPersistent7, isUniversallyUnique=param_isUniversallyUnique7, name=param_name7, version=param_version7)

#  ^^^^^ ------ Ending elment:   GeographicUnit ------ ^^^^^




#  vvvvv ------ Beginning elment:   TemporalCoverage ------ vvvvv

      param_content1 <- as(c("Time Period(s)"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject1 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate1 <- as(c("2013/2014"), "character")
    ReferenceDate1 <- ReferenceDate$new(isoDate=param_isoDate1, subject=param_subject1)

      param_content1 <- as(c("Data Collection Time Period(s)"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject2 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate2 <- as(c("2013-09-01/2013-09-06"), "character")
    ReferenceDate2 <- ReferenceDate$new(isoDate=param_isoDate2, subject=param_subject2)

      param_content1 <- as(c("Codebook Publication"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject3 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate3 <- as(c("2016_10_05"), "character")
    ReferenceDate3 <- ReferenceDate$new(isoDate=param_isoDate3, subject=param_subject3)

      param_content1 <- as(c("Metadata Production"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject4 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate4 <- as(c("2010_10_13"), "character")
    ReferenceDate4 <- ReferenceDate$new(isoDate=param_isoDate4, subject=param_subject4)

      param_content1 <- as(c("Title"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject5 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate5 <- as(c("2013"), "character")
    ReferenceDate5 <- ReferenceDate$new(isoDate=param_isoDate5, subject=param_subject5)

      param_content1 <- as(c("Production Date"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject6 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate6 <- as(c("2014-02-28"), "character")
    ReferenceDate6 <- ReferenceDate$new(isoDate=param_isoDate6, subject=param_subject6)

      param_content1 <- as(c("Data Collection"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_subject7 <- list(ExternalControlledVocabularyEntry1)
    param_isoDate7 <- as(c("2013-09-06/2014-01-06"), "character")
    ReferenceDate7 <- ReferenceDate$new(isoDate=param_isoDate7, subject=param_subject7)

  param_coverageDate8 <- list(ReferenceDate1, ReferenceDate2, ReferenceDate3, ReferenceDate4, ReferenceDate5, ReferenceDate6, ReferenceDate7)
  param_agency8 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id8 <- as(c("IdTemporalCoverage"), "character")
  param_isPersistent8 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique8 <- as(c("FALSE"), "logical")
  param_version8 <- as(c("1"), "character")
  TemporalCoverage8 <- TemporalCoverage$new(agency=param_agency8, coverageDate=param_coverageDate8, id=param_id8, isPersistent=param_isPersistent8, isUniversallyUnique=param_isUniversallyUnique8, version=param_version8)

#  ^^^^^ ------ Ending elment:   TemporalCoverage ------ ^^^^^




#  vvvvv ------ Beginning elment:   TopicalCoverage ------ vvvvv

    param_content1 <- as(c("Attitudes"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_content2 <- as(c("Defence"), "character")
    ExternalControlledVocabularyEntry2 <- ExternalControlledVocabularyEntry$new(content=param_content2)

    param_content3 <- as(c("Economic policy"), "character")
    ExternalControlledVocabularyEntry3 <- ExternalControlledVocabularyEntry$new(content=param_content3)

    param_content4 <- as(c("Elections"), "character")
    ExternalControlledVocabularyEntry4 <- ExternalControlledVocabularyEntry$new(content=param_content4)

    param_content5 <- as(c("Environment"), "character")
    ExternalControlledVocabularyEntry5 <- ExternalControlledVocabularyEntry$new(content=param_content5)

    param_content6 <- as(c("Ethnic groups"), "character")
    ExternalControlledVocabularyEntry6 <- ExternalControlledVocabularyEntry$new(content=param_content6)

    param_content7 <- as(c("Immigration"), "character")
    ExternalControlledVocabularyEntry7 <- ExternalControlledVocabularyEntry$new(content=param_content7)

    param_content8 <- as(c("International relations"), "character")
    ExternalControlledVocabularyEntry8 <- ExternalControlledVocabularyEntry$new(content=param_content8)

    param_content9 <- as(c("Internet"), "character")
    ExternalControlledVocabularyEntry9 <- ExternalControlledVocabularyEntry$new(content=param_content9)

    param_content10 <- as(c("Political parties"), "character")
    ExternalControlledVocabularyEntry10 <- ExternalControlledVocabularyEntry$new(content=param_content10)

    param_content11 <- as(c("Politicians"), "character")
    ExternalControlledVocabularyEntry11 <- ExternalControlledVocabularyEntry$new(content=param_content11)

    param_content12 <- as(c("Politics"), "character")
    ExternalControlledVocabularyEntry12 <- ExternalControlledVocabularyEntry$new(content=param_content12)

    param_content13 <- as(c("Republicanism"), "character")
    ExternalControlledVocabularyEntry13 <- ExternalControlledVocabularyEntry$new(content=param_content13)

    param_content14 <- as(c("Shares"), "character")
    ExternalControlledVocabularyEntry14 <- ExternalControlledVocabularyEntry$new(content=param_content14)

    param_content15 <- as(c("Social policy"), "character")
    ExternalControlledVocabularyEntry15 <- ExternalControlledVocabularyEntry$new(content=param_content15)

    param_content16 <- as(c("Taxation"), "character")
    ExternalControlledVocabularyEntry16 <- ExternalControlledVocabularyEntry$new(content=param_content16)

  param_keyword9 <- list(ExternalControlledVocabularyEntry1, ExternalControlledVocabularyEntry2, ExternalControlledVocabularyEntry3, ExternalControlledVocabularyEntry4, ExternalControlledVocabularyEntry5, ExternalControlledVocabularyEntry6, ExternalControlledVocabularyEntry7, ExternalControlledVocabularyEntry8, ExternalControlledVocabularyEntry9, ExternalControlledVocabularyEntry10, ExternalControlledVocabularyEntry11, ExternalControlledVocabularyEntry12, ExternalControlledVocabularyEntry13, ExternalControlledVocabularyEntry14, ExternalControlledVocabularyEntry15, ExternalControlledVocabularyEntry16)
    param_content1 <- as(c("Politics"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_content2 <- as(c("Election and Campaign Studies"), "character")
    ExternalControlledVocabularyEntry2 <- ExternalControlledVocabularyEntry$new(content=param_content2)

  param_subject9 <- list(ExternalControlledVocabularyEntry1, ExternalControlledVocabularyEntry2)
  param_agency9 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id9 <- as(c("IdTopicalCoverage"), "character")
  param_isPersistent9 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique9 <- as(c("FALSE"), "logical")
  param_version9 <- as(c("1"), "character")
  TopicalCoverage9 <- TopicalCoverage$new(agency=param_agency9, id=param_id9, isPersistent=param_isPersistent9, isUniversallyUnique=param_isUniversallyUnique9, keyword=param_keyword9, subject=param_subject9, version=param_version9)

#  ^^^^^ ------ Ending elment:   TopicalCoverage ------ ^^^^^




#  vvvvv ------ Beginning elment:   Individual ------ vvvvv

        param_content1 <- as(c("Queensland University of Technology (QUT)"), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation10 <- list(ContactInformation1)
        param_content1 <- as(c("Clive Bean"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_fullName1 <- list(InternationalString1)
    param_firstGiven1 <- as(c("Clive"), "character")
    param_lastFamily1 <- as(c("Bean"), "character")
    IndividualName1 <- IndividualName$new(firstGiven=param_firstGiven1, fullName=param_fullName1, lastFamily=param_lastFamily1)

  param_hasIndividualName10 <- list(IndividualName1)
  param_agency10 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id10 <- as(c("IdBean"), "character")
  param_isPersistent10 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique10 <- as(c("FALSE"), "logical")
  param_version10 <- as(c("1"), "character")
  Individual10 <- Individual$new(agency=param_agency10, hasContactInformation=param_hasContactInformation10, hasIndividualName=param_hasIndividualName10, id=param_id10, isPersistent=param_isPersistent10, isUniversallyUnique=param_isUniversallyUnique10, version=param_version10)

#  ^^^^^ ------ Ending elment:   Individual ------ ^^^^^




#  vvvvv ------ Beginning elment:   Individual ------ vvvvv

        param_content1 <- as(c("The Australian National University"), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      param_line1 <- as(c("Research School of Social Sciences,", "The Australian National University", "ACTON, ACT, 0200"), "character")
      Address1 <- Address$new(line=param_line1, locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
      param_uri1 <- as(c("http://aes.anu.edu.au"), "character")
      WebLink1 <- WebLink$new(uri=param_uri1)

    param_website1 <- list(WebLink1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1, website=param_website1)

  param_hasContactInformation11 <- list(ContactInformation1)
        param_content1 <- as(c("Ian McAllister"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_fullName1 <- list(InternationalString1)
    param_firstGiven1 <- as(c("Ian"), "character")
    param_lastFamily1 <- as(c("McAllister"), "character")
    IndividualName1 <- IndividualName$new(firstGiven=param_firstGiven1, fullName=param_fullName1, lastFamily=param_lastFamily1)

  param_hasIndividualName11 <- list(IndividualName1)
  param_agency11 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id11 <- as(c("IdMcAllister"), "character")
  param_isPersistent11 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique11 <- as(c("FALSE"), "logical")
  param_version11 <- as(c("1"), "character")
  Individual11 <- Individual$new(agency=param_agency11, hasContactInformation=param_hasContactInformation11, hasIndividualName=param_hasIndividualName11, id=param_id11, isPersistent=param_isPersistent11, isUniversallyUnique=param_isUniversallyUnique11, version=param_version11)

#  ^^^^^ ------ Ending elment:   Individual ------ ^^^^^




#  vvvvv ------ Beginning elment:   Individual ------ vvvvv

        param_content1 <- as(c("The Australian National University"), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation12 <- list(ContactInformation1)
        param_content1 <- as(c("Juliet Pietsch"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_fullName1 <- list(InternationalString1)
    param_firstGiven1 <- as(c("Juliet"), "character")
    param_lastFamily1 <- as(c("Pietsch"), "character")
    IndividualName1 <- IndividualName$new(firstGiven=param_firstGiven1, fullName=param_fullName1, lastFamily=param_lastFamily1)

  param_hasIndividualName12 <- list(IndividualName1)
  param_agency12 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id12 <- as(c("IdPietsch"), "character")
  param_isPersistent12 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique12 <- as(c("FALSE"), "logical")
  param_version12 <- as(c("1"), "character")
  Individual12 <- Individual$new(agency=param_agency12, hasContactInformation=param_hasContactInformation12, hasIndividualName=param_hasIndividualName12, id=param_id12, isPersistent=param_isPersistent12, isUniversallyUnique=param_isUniversallyUnique12, version=param_version12)

#  ^^^^^ ------ Ending elment:   Individual ------ ^^^^^




#  vvvvv ------ Beginning elment:   Individual ------ vvvvv

        param_content1 <- as(c("University of Manchester"), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation13 <- list(ContactInformation1)
        param_content1 <- as(c("Rachel Kay Gibson"), "character")
        LanguageSpecificStringType1 <- LanguageSpecificStringType$new(content=param_content1)

      param_languageSpecificString1 <- list(LanguageSpecificStringType1)
      InternationalString1 <- InternationalString$new(languageSpecificString=param_languageSpecificString1)

    param_fullName1 <- list(InternationalString1)
    param_firstGiven1 <- as(c("Rachel"), "character")
    param_lastFamily1 <- as(c("Gibson"), "character")
    param_middle1 <- as(c("Kay"), "character")
    IndividualName1 <- IndividualName$new(firstGiven=param_firstGiven1, fullName=param_fullName1, lastFamily=param_lastFamily1, middle=param_middle1)

  param_hasIndividualName13 <- list(IndividualName1)
  param_agency13 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id13 <- as(c("IdGibson"), "character")
  param_isPersistent13 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique13 <- as(c("FALSE"), "logical")
  param_version13 <- as(c("1"), "character")
  Individual13 <- Individual$new(agency=param_agency13, hasContactInformation=param_hasContactInformation13, hasIndividualName=param_hasIndividualName13, id=param_id13, isPersistent=param_isPersistent13, isUniversallyUnique=param_isUniversallyUnique13, version=param_version13)

#  ^^^^^ ------ Ending elment:   Individual ------ ^^^^^




#  vvvvv ------ Beginning elment:   Organization ------ vvvvv

        param_content1 <- as(c(""), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation14 <- list(ContactInformation1)
    param_content1 <- as(c("Australian Research Council (ARC)"), "character")
    OrganizationName1 <- OrganizationName$new(content=param_content1)

  param_hasOrganizationName14 <- list(OrganizationName1)
  param_agency14 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id14 <- as(c("IdOrgARC"), "character")
  param_isPersistent14 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique14 <- as(c("FALSE"), "logical")
  param_version14 <- as(c("1"), "character")
  Organization14 <- Organization$new(agency=param_agency14, hasContactInformation=param_hasContactInformation14, hasOrganizationName=param_hasOrganizationName14, id=param_id14, isPersistent=param_isPersistent14, isUniversallyUnique=param_isUniversallyUnique14, version=param_version14)

#  ^^^^^ ------ Ending elment:   Organization ------ ^^^^^




#  vvvvv ------ Beginning elment:   Organization ------ vvvvv

        param_content1 <- as(c(""), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation15 <- list(ContactInformation1)
    param_content1 <- as(c("The Social Research Centre (SRC)"), "character")
    OrganizationName1 <- OrganizationName$new(content=param_content1)

  param_hasOrganizationName15 <- list(OrganizationName1)
  param_agency15 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id15 <- as(c("IdOrgSRC"), "character")
  param_isPersistent15 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique15 <- as(c("FALSE"), "logical")
  param_version15 <- as(c("1"), "character")
  Organization15 <- Organization$new(agency=param_agency15, hasContactInformation=param_hasContactInformation15, hasOrganizationName=param_hasOrganizationName15, id=param_id15, isPersistent=param_isPersistent15, isUniversallyUnique=param_isUniversallyUnique15, version=param_version15)

#  ^^^^^ ------ Ending elment:   Organization ------ ^^^^^




#  vvvvv ------ Beginning elment:   Organization ------ vvvvv

        param_content1 <- as(c("The Australian National University"), "character")
        ObjectName1 <- ObjectName$new(content=param_content1)

      param_locationName1 <- list(ObjectName1)
      Address1 <- Address$new(locationName=param_locationName1)

    param_hasAddress1 <- list(Address1)
    ContactInformation1 <- ContactInformation$new(hasAddress=param_hasAddress1)

  param_hasContactInformation16 <- list(ContactInformation1)
    param_content1 <- as(c("Australian Data Archive (ADA)"), "character")
    OrganizationName1 <- OrganizationName$new(content=param_content1)

  param_hasOrganizationName16 <- list(OrganizationName1)
  param_agency16 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id16 <- as(c("IdOrgADA"), "character")
  param_isPersistent16 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique16 <- as(c("FALSE"), "logical")
  param_version16 <- as(c("1"), "character")
  Organization16 <- Organization$new(agency=param_agency16, hasContactInformation=param_hasContactInformation16, hasOrganizationName=param_hasOrganizationName16, id=param_id16, isPersistent=param_isPersistent16, isUniversallyUnique=param_isUniversallyUnique16, version=param_version16)

#  ^^^^^ ------ Ending elment:   Organization ------ ^^^^^




#  vvvvv ------ Beginning elment:   FundingInformation ------ vvvvv

  param_agency17 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasFunder17 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdOrgARC:1"), "character")
  param_id17 <- as(c("IdFunding"), "character")
  param_isPersistent17 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique17 <- as(c("FALSE"), "logical")
  param_version17 <- as(c("1"), "character")
  FundingInformation17 <- FundingInformation$new(agency=param_agency17, hasFunder=param_hasFunder17, id=param_id17, isPersistent=param_isPersistent17, isUniversallyUnique=param_isUniversallyUnique17, version=param_version17)

#  ^^^^^ ------ Ending elment:   FundingInformation ------ ^^^^^




#  vvvvv ------ Beginning elment:   MethodologyOverview ------ vvvvv

      param_content1 <- as(c("Sex, Age and State (based on AEC enrolment data for the 2013 election) and party vote (based on
                AEC final election vote tallies). See Appendix A4 of the Technical Report for further details."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_overview18 <- list(InternationalStructuredString1)
    param_content1 <- as(c("Weighting"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_subjectOfMethodology18 <- list(ExternalControlledVocabularyEntry1)
  param_agency18 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id18 <- as(c("IdWeighting"), "character")
  param_isPersistent18 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique18 <- as(c("FALSE"), "logical")
  param_version18 <- as(c("1"), "character")
  MethodologyOverview18 <- MethodologyOverview$new(agency=param_agency18, id=param_id18, isPersistent=param_isPersistent18, isUniversallyUnique=param_isUniversallyUnique18, overview=param_overview18, subjectOfMethodology=param_subjectOfMethodology18, version=param_version18)

#  ^^^^^ ------ Ending elment:   MethodologyOverview ------ ^^^^^




#  vvvvv ------ Beginning elment:   MethodologyOverview ------ vvvvv

      param_content1 <- as(c("Data Collection Mode: Postal survey with online completion option
                Data Collection Notes: An analysis of response patterns and sample performance is included in Sections 3 and 4 of the Technical Report.
                Questionnaires: Structured
                Supervision: 1. Follow-up mail-outs
                One week after the initial mail out, a reminder / thank you card was sent to all respondents, excluding those that had opted
                out of the survey (n=12,145). Approximately two weeks after this, the Social Research Centre downloaded a list of completed
                surveys from the online survey provider, and along with Datatime Services, determined all records that remained incomplete.
                A reminder survey booklet was then mailed on 4th October to 9,057 respondents. A reminder letter was then sent on 11th
                October to participants who had yet to respond (n=8,728), followed by a second and final reminder letter to all remaining
                non-respondents on 1 November (n=7,276).
                2. Survey Helpline
                A survey helpline, including an 1800 number and email address was printed on all survey materials to give sample members
                the opportunity to seek clarification or ask for assistance regarding any survey matters. This included, but was not limited
                help establishing survey bona fides, encourage response and record opt-outs to the survey.
                The 1800 number and email address were operational from September 6 until the end of the fieldwork period. All calls to the
                1800 number not answered in real time (all operators currently busy or the call was received outside hours of operation) were
                routed to a messaging service and returned within 24 hours. All emails were also responded to within a 24 hour period, unless
                received over the weekend.
                The 1800 number operators were fully briefed on survey background and procedures, the administration of the survey, and
                response maximisation techniques.
                3. Telephone follow-up
                Approximately one-third of the way through fieldwork, it was agreed that a telephone follow up would be conducted to try
                and maximise response to the Candidates survey. A total of 394 records were included as part of the follow up activity.
            "), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_overview19 <- list(InternationalStructuredString1)
    param_content1 <- as(c("Data Collection"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_subjectOfMethodology19 <- list(ExternalControlledVocabularyEntry1)
  param_agency19 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id19 <- as(c("IdDataCollection"), "character")
  param_isPersistent19 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique19 <- as(c("FALSE"), "logical")
  param_version19 <- as(c("1"), "character")
  MethodologyOverview19 <- MethodologyOverview$new(agency=param_agency19, id=param_id19, isPersistent=param_isPersistent19, isUniversallyUnique=param_isUniversallyUnique19, overview=param_overview19, subjectOfMethodology=param_subjectOfMethodology19, version=param_version19)

#  ^^^^^ ------ Ending elment:   MethodologyOverview ------ ^^^^^




#  vvvvv ------ Beginning elment:   SamplingProcedure ------ vvvvv

      param_content1 <- as(c("One-stage stratified or systematic random sample
                See Technical Report for further details
                No deviations from Sampling design.
                Response Rate: Of a total mailing of 12,200, there were 3,955 completed returns - 
                with 3,379 mail returns and 576 online returns - giving a
                raw response rate of 32.4 percent. An adjusted response rate of 34.2 percent was calculated by removing the Out of Scope
                sample (deceased, incapable, return to sender, n=530).
            "), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_overview20 <- list(InternationalStructuredString1)
    param_content1 <- as(c("Sampling"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_subjectOfMethodology20 <- list(ExternalControlledVocabularyEntry1)
  param_agency20 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id20 <- as(c("IdSampling"), "character")
  param_isPersistent20 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique20 <- as(c("FALSE"), "logical")
  param_version20 <- as(c("1"), "character")
  SamplingProcedure20 <- SamplingProcedure$new(agency=param_agency20, id=param_id20, isPersistent=param_isPersistent20, isUniversallyUnique=param_isUniversallyUnique20, overview=param_overview20, subjectOfMethodology=param_subjectOfMethodology20, version=param_version20)

#  ^^^^^ ------ Ending elment:   SamplingProcedure ------ ^^^^^




#  vvvvv ------ Beginning elment:   MethodologyOverview ------ vvvvv

      param_content1 <- as(c("Data Editing: The data was checked for out of range codes and that the skip patterns were followed. In consultation with the principal
                investigators, cleaning rules were developed for interpreting multiple responses for single response questions. Coding rules
                were also developed in consultation with the Principal Investigator where required. See Technical Report for further details."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_overview21 <- list(InternationalStructuredString1)
    param_content1 <- as(c("Data Processing"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_subjectOfMethodology21 <- list(ExternalControlledVocabularyEntry1)
  param_agency21 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id21 <- as(c("IdDataProcessing"), "character")
  param_isPersistent21 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique21 <- as(c("FALSE"), "logical")
  param_version21 <- as(c("1"), "character")
  MethodologyOverview21 <- MethodologyOverview$new(agency=param_agency21, id=param_id21, isPersistent=param_isPersistent21, isUniversallyUnique=param_isUniversallyUnique21, overview=param_overview21, subjectOfMethodology=param_subjectOfMethodology21, version=param_version21)

#  ^^^^^ ------ Ending elment:   MethodologyOverview ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("Electoral Division � AES Numeric Code"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel23 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType23 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("DivisNum"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name23 <- list(ObjectName1)
  param_agency23 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id23 <- as(c("IdVarDivisNum"), "character")
  param_isPersistent23 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique23 <- as(c("FALSE"), "logical")
  param_takesPlatformSpecificSentinelValues23 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSenMinus1:1"), "character")
  param_takesSubstantiveValuesFrom23 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubVDElectoralDivisions:1"), "character")
  param_version23 <- as(c("1"), "character")
  DivisNum <- InstanceVariable$new(agency=param_agency23, displayLabel=param_displayLabel23, hasIntendedDataType=param_hasIntendedDataType23, id=param_id23, isPersistent=param_isPersistent23, isUniversallyUnique=param_isUniversallyUnique23, name=param_name23, takesPlatformSpecificSentinelValues=param_takesPlatformSpecificSentinelValues23, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom23, version=param_version23)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("uniqueid: Unique Identifier"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel24 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType24 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("uniqueid"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name24 <- list(ObjectName1)
  param_agency24 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id24 <- as(c("IdVarUniqueId"), "character")
  param_isPersistent24 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique24 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom24 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDSubUniqueID:1"), "character")
  param_version24 <- as(c("1"), "character")
  uniqueid <- InstanceVariable$new(agency=param_agency24, displayLabel=param_displayLabel24, hasIntendedDataType=param_hasIntendedDataType24, id=param_id24, isPersistent=param_isPersistent24, isUniversallyUnique=param_isUniversallyUnique24, name=param_name24, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom24, version=param_version24)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("Mode: Mode of completion"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel25 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType25 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("Mode"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name25 <- list(ObjectName1)
  param_agency25 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id25 <- as(c("IdVarMode"), "character")
  param_isPersistent25 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique25 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom25 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubMode:1"), "character")
  param_version25 <- as(c("1"), "character")
  Mode <- InstanceVariable$new(agency=param_agency25, displayLabel=param_displayLabel25, hasIntendedDataType=param_hasIntendedDataType25, id=param_id25, isPersistent=param_isPersistent25, isUniversallyUnique=param_isUniversallyUnique25, name=param_name25, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom25, version=param_version25)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("DATECOMP: Date of return (Responses received)"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel26 <- list(LabelForDisplay1)
    param_content1 <- as(c("string"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType26 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("DATECOMP"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name26 <- list(ObjectName1)
  param_agency26 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id26 <- as(c("IdVarDateComp"), "character")
  param_isPersistent26 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique26 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom26 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubDateComp:1"), "character")
  param_version26 <- as(c("1"), "character")
  DATECOMP <- InstanceVariable$new(agency=param_agency26, displayLabel=param_displayLabel26, hasIntendedDataType=param_hasIntendedDataType26, id=param_id26, isPersistent=param_isPersistent26, isUniversallyUnique=param_isUniversallyUnique26, name=param_name26, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom26, version=param_version26)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("State (from sample)r"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel27 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType27 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("State"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name27 <- list(ObjectName1)
  param_agency27 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id27 <- as(c("IdVarState"), "character")
  param_isPersistent27 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique27 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom27 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubState:1"), "character")
  param_version27 <- as(c("1"), "character")
  State <- InstanceVariable$new(agency=param_agency27, displayLabel=param_displayLabel27, hasIntendedDataType=param_hasIntendedDataType27, id=param_id27, isPersistent=param_isPersistent27, isUniversallyUnique=param_isUniversallyUnique27, name=param_name27, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom27, version=param_version27)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("Division: Division (from sample)"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel28 <- list(LabelForDisplay1)
    param_content1 <- as(c("string"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType28 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("Division"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name28 <- list(ObjectName1)
  param_agency28 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id28 <- as(c("IdVarDivision"), "character")
  param_isPersistent28 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique28 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom28 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubDivision:1"), "character")
  param_version28 <- as(c("1"), "character")
  Division <- InstanceVariable$new(agency=param_agency28, displayLabel=param_displayLabel28, hasIntendedDataType=param_hasIntendedDataType28, id=param_id28, isPersistent=param_isPersistent28, isUniversallyUnique=param_isUniversallyUnique28, name=param_name28, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom28, version=param_version28)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("A4. Interest in election campaign"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel29 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType29 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("a4"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name29 <- list(ObjectName1)
  param_agency29 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id29 <- as(c("IdVarA4"), "character")
  param_isPersistent29 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique29 <- as(c("FALSE"), "logical")
  param_sourceCapture29 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdQuestA4:1"), "character")
  param_takesPlatformSpecificSentinelValues29 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSenMinus1:1"), "character")
  param_takesSubstantiveValuesFrom29 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubA4:1"), "character")
  param_version29 <- as(c("1"), "character")
  a4 <- InstanceVariable$new(agency=param_agency29, displayLabel=param_displayLabel29, hasIntendedDataType=param_hasIntendedDataType29, id=param_id29, isPersistent=param_isPersistent29, isUniversallyUnique=param_isUniversallyUnique29, name=param_name29, sourceCapture=param_sourceCapture29, takesPlatformSpecificSentinelValues=param_takesPlatformSpecificSentinelValues29, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom29, version=param_version29)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("G1. Age given (Numeric) (BASE: Age given)"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel30 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType30 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("g1age"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name30 <- list(ObjectName1)
  param_agency30 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id30 <- as(c("IdVarG1Age"), "character")
  param_isPersistent30 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique30 <- as(c("FALSE"), "logical")
  param_sourceCapture30 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdQuestAge:1"), "character")
  param_takesPlatformSpecificSentinelValues30 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSenMinus1:1"), "character")
  param_takesSubstantiveValuesFrom30 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDSubG1Age:1"), "character")
  param_unitOfMeasurement30 <- as(c("year"), "character")
  param_version30 <- as(c("1"), "character")
  g1age <- InstanceVariable$new(agency=param_agency30, displayLabel=param_displayLabel30, hasIntendedDataType=param_hasIntendedDataType30, id=param_id30, isPersistent=param_isPersistent30, isUniversallyUnique=param_isUniversallyUnique30, name=param_name30, sourceCapture=param_sourceCapture30, takesPlatformSpecificSentinelValues=param_takesPlatformSpecificSentinelValues30, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom30, unitOfMeasurement=param_unitOfMeasurement30, version=param_version30)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("G5. Extended-Occupation (ANZSCO 4 digit)"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel31 <- list(LabelForDisplay1)
    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType31 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("xg5"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name31 <- list(ObjectName1)
  param_agency31 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id31 <- as(c("IdVarXg5"), "character")
  param_isPersistent31 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique31 <- as(c("FALSE"), "logical")
  param_sourceCapture31 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdQuestXg5:1"), "character")
  param_takesSubstantiveValuesFrom31 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubXg5:1"), "character")
  param_version31 <- as(c("1"), "character")
  xg5 <- InstanceVariable$new(agency=param_agency31, displayLabel=param_displayLabel31, hasIntendedDataType=param_hasIntendedDataType31, id=param_id31, isPersistent=param_isPersistent31, isUniversallyUnique=param_isUniversallyUnique31, name=param_name31, sourceCapture=param_sourceCapture31, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom31, version=param_version31)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("Weight"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel32 <- list(LabelForDisplay1)
    param_content1 <- as(c("double"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#double"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType32 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("weight"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name32 <- list(ObjectName1)
    param_content1 <- as(c("weight"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_variableFunction32 <- list(ExternalControlledVocabularyEntry1)
  param_agency32 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id32 <- as(c("IdVarWeight"), "character")
  param_isPersistent32 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique32 <- as(c("FALSE"), "logical")
  param_takesSubstantiveValuesFrom32 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubWeight:1"), "character")
  param_version32 <- as(c("1"), "character")
  weight <- InstanceVariable$new(agency=param_agency32, displayLabel=param_displayLabel32, hasIntendedDataType=param_hasIntendedDataType32, id=param_id32, isPersistent=param_isPersistent32, isUniversallyUnique=param_isUniversallyUnique32, name=param_name32, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom32, variableFunction=param_variableFunction32, version=param_version32)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("Party of 2CP elected candidate"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel33 <- list(LabelForDisplay1)
    param_content1 <- as(c("string"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType33 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("partyaby"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name33 <- list(ObjectName1)
  param_agency33 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id33 <- as(c("IdVarPartyAby"), "character")
  param_isPersistent33 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique33 <- as(c("FALSE"), "logical")
  param_sourceCapture33 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdMeasPartyAby:1"), "character")
  param_takesSubstantiveValuesFrom33 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubPartyAby:1"), "character")
  param_version33 <- as(c("1"), "character")
  partyaby <- InstanceVariable$new(agency=param_agency33, displayLabel=param_displayLabel33, hasIntendedDataType=param_hasIntendedDataType33, id=param_id33, isPersistent=param_isPersistent33, isUniversallyUnique=param_isUniversallyUnique33, name=param_name33, sourceCapture=param_sourceCapture33, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom33, version=param_version33)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   InstanceVariable ------ vvvvv

      param_content1 <- as(c("N swing"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel34 <- list(LabelForDisplay1)
    param_content1 <- as(c("double"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#double"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_hasIntendedDataType34 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("swingn"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name34 <- list(ObjectName1)
  param_agency34 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id34 <- as(c("IdVarSwingn"), "character")
  param_isPersistent34 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique34 <- as(c("FALSE"), "logical")
  param_sourceCapture34 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDMeasSwingn:1"), "character")
  param_takesSubstantiveValuesFrom34 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdSubSwingn:1"), "character")
  param_version34 <- as(c("1"), "character")
  swingn <- InstanceVariable$new(agency=param_agency34, displayLabel=param_displayLabel34, hasIntendedDataType=param_hasIntendedDataType34, id=param_id34, isPersistent=param_isPersistent34, isUniversallyUnique=param_isUniversallyUnique34, name=param_name34, sourceCapture=param_sourceCapture34, takesSubstantiveValuesFrom=param_takesSubstantiveValuesFrom34, version=param_version34)

#  ^^^^^ ------ Ending elment:   InstanceVariable ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   VariableCollection ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivisNum:1"), "character")
    VariableIndicator1 <- VariableIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarUniqueID:1"), "character")
    VariableIndicator2 <- VariableIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarMode:1"), "character")
    VariableIndicator3 <- VariableIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDateComp:1"), "character")
    VariableIndicator4 <- VariableIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("6"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarState:1"), "character")
    VariableIndicator5 <- VariableIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("8"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1"), "character")
    VariableIndicator6 <- VariableIndicator$new(index=param_index6, member=param_member6)

  param_contains36 <- list(VariableIndicator1, VariableIndicator2, VariableIndicator3, VariableIndicator4, VariableIndicator5, VariableIndicator6)
    param_content1 <- as(c("Group Administration Variables"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name36 <- list(ObjectName1)
  param_agency36 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id36 <- as(c("IdVarGrpAdmim"), "character")
  param_isPersistent36 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique36 <- as(c("FALSE"), "logical")
  param_version36 <- as(c("1"), "character")
  Group.Administration.Variables <- VariableCollection$new(agency=param_agency36, contains=param_contains36, id=param_id36, isPersistent=param_isPersistent36, isUniversallyUnique=param_isUniversallyUnique36, name=param_name36, version=param_version36)

#  ^^^^^ ------ Ending elment:   VariableCollection ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableCollection ------ vvvvv

    param_index1 <- as(c("6"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarA4:1"), "character")
    VariableIndicator1 <- VariableIndicator$new(index=param_index1, member=param_member1)

  param_contains37 <- list(VariableIndicator1)
    param_content1 <- as(c("Group Section A: The Election Campaign"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name37 <- list(ObjectName1)
  param_agency37 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id37 <- as(c("IdVarGrpCampaign"), "character")
  param_isPersistent37 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique37 <- as(c("FALSE"), "logical")
  param_version37 <- as(c("1"), "character")
  Group.Section.A..The.Election.Campaign <- VariableCollection$new(agency=param_agency37, contains=param_contains37, id=param_id37, isPersistent=param_isPersistent37, isUniversallyUnique=param_isUniversallyUnique37, name=param_name37, version=param_version37)

#  ^^^^^ ------ Ending elment:   VariableCollection ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableCollection ------ vvvvv

    param_index1 <- as(c("2"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarG1Age:1"), "character")
    VariableIndicator1 <- VariableIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("7"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarXg5:1"), "character")
    VariableIndicator2 <- VariableIndicator$new(index=param_index2, member=param_member2)

  param_contains38 <- list(VariableIndicator1, VariableIndicator2)
    param_content1 <- as(c("Group Section G: Education and Work"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name38 <- list(ObjectName1)
  param_agency38 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id38 <- as(c("IdVarGrpEduc"), "character")
  param_isPersistent38 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique38 <- as(c("FALSE"), "logical")
  param_version38 <- as(c("1"), "character")
  Group.Section.G..Education.and.Work <- VariableCollection$new(agency=param_agency38, contains=param_contains38, id=param_id38, isPersistent=param_isPersistent38, isUniversallyUnique=param_isUniversallyUnique38, name=param_name38, version=param_version38)

#  ^^^^^ ------ Ending elment:   VariableCollection ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableCollection ------ vvvvv

    param_index1 <- as(c("6"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
    VariableIndicator1 <- VariableIndicator$new(index=param_index1, member=param_member1)

  param_contains39 <- list(VariableIndicator1)
    param_content1 <- as(c("Group Weight Variables"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name39 <- list(ObjectName1)
  param_agency39 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id39 <- as(c("IdVarGrpWeight"), "character")
  param_isPersistent39 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique39 <- as(c("FALSE"), "logical")
  param_version39 <- as(c("1"), "character")
  Group.Weight.Variables <- VariableCollection$new(agency=param_agency39, contains=param_contains39, id=param_id39, isPersistent=param_isPersistent39, isUniversallyUnique=param_isUniversallyUnique39, name=param_name39, version=param_version39)

#  ^^^^^ ------ Ending elment:   VariableCollection ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableCollection ------ vvvvv

    param_index1 <- as(c("30"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarPartyAby:1"), "character")
    VariableIndicator1 <- VariableIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("28"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarSwingn:1"), "character")
    VariableIndicator2 <- VariableIndicator$new(index=param_index2, member=param_member2)

  param_contains40 <- list(VariableIndicator1, VariableIndicator2)
    param_content1 <- as(c("Group AEC Election Results"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name40 <- list(ObjectName1)
  param_agency40 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id40 <- as(c("IdVarGrpResults"), "character")
  param_isPersistent40 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique40 <- as(c("FALSE"), "logical")
  param_version40 <- as(c("1"), "character")
  Group.AEC.Election.Results <- VariableCollection$new(agency=param_agency40, contains=param_contains40, id=param_id40, isPersistent=param_isPersistent40, isUniversallyUnique=param_isUniversallyUnique40, name=param_name40, version=param_version40)

#  ^^^^^ ------ Ending elment:   VariableCollection ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   SentinelValueDomain ------ vvvvv

  param_agency42 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain42 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCodesMinus1:1"), "character")
  param_id42 <- as(c("IdSenMinus1"), "character")
  param_isPersistent42 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique42 <- as(c("FALSE"), "logical")
  param_version42 <- as(c("1"), "character")
  SentinelValueDomain42 <- SentinelValueDomain$new(agency=param_agency42, enumeratedValueDomain=param_enumeratedValueDomain42, id=param_id42, isPersistent=param_isPersistent42, isUniversallyUnique=param_isUniversallyUnique42, version=param_version42)

#  ^^^^^ ------ Ending elment:   SentinelValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeMinus1:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

  param_contains43 <- list(CodeIndicator1)
  param_agency43 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id43 <- as(c("IdCodesMinus1"), "character")
  param_isPersistent43 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique43 <- as(c("FALSE"), "logical")
  param_version43 <- as(c("1"), "character")
  CodeList43 <- CodeList$new(agency=param_agency43, contains=param_contains43, id=param_id43, isPersistent=param_isPersistent43, isUniversallyUnique=param_isUniversallyUnique43, version=param_version43)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("-1"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation44 <- list(ValueString1)
  param_agency44 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes44 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcatMinus1:1"), "character")
  param_id44 <- as(c("IDcodeMinus1"), "character")
  param_isPersistent44 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique44 <- as(c("FALSE"), "logical")
  param_version44 <- as(c("1"), "character")
  Code44 <- Code$new(agency=param_agency44, denotes=param_denotes44, id=param_id44, isPersistent=param_isPersistent44, isUniversallyUnique=param_isUniversallyUnique44, representation=param_representation44, version=param_version44)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("missing value"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_definition45 <- list(InternationalStructuredString1)
      param_content1 <- as(c("Missing"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel45 <- list(LabelForDisplay1)
  param_agency45 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id45 <- as(c("IDcatMinus1"), "character")
  param_isPersistent45 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique45 <- as(c("FALSE"), "logical")
  param_version45 <- as(c("1"), "character")
  Category45 <- Category$new(agency=param_agency45, definition=param_definition45, displayLabel=param_displayLabel45, id=param_id45, isPersistent=param_isPersistent45, isUniversallyUnique=param_isUniversallyUnique45, version=param_version45)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType46 <- list(ExternalControlledVocabularyEntry1)
  param_agency46 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain46 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodesElectoralDivis:1"), "character")
  param_id46 <- as(c("IdSubVDElectoralDivisions"), "character")
  param_isPersistent46 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique46 <- as(c("FALSE"), "logical")
  param_version46 <- as(c("1"), "character")
  SubstantiveValueDomain46 <- SubstantiveValueDomain$new(agency=param_agency46, enumeratedValueDomain=param_enumeratedValueDomain46, id=param_id46, isPersistent=param_isPersistent46, isUniversallyUnique=param_isUniversallyUnique46, recommendedDataType=param_recommendedDataType46, version=param_version46)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode101EDC:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode102EDC:1"), "character")
    CodeIndicator2 <- CodeIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode103EDC:1"), "character")
    CodeIndicator3 <- CodeIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode104EDC:1"), "character")
    CodeIndicator4 <- CodeIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("5"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode105EDC:1"), "character")
    CodeIndicator5 <- CodeIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("6"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode106EDC:1"), "character")
    CodeIndicator6 <- CodeIndicator$new(index=param_index6, member=param_member6)

  param_contains47 <- list(CodeIndicator1, CodeIndicator2, CodeIndicator3, CodeIndicator4, CodeIndicator5, CodeIndicator6)
      param_content1 <- as(c("For demonstration. Only 6 of a much larger list documneted here"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_purpose47 <- list(InternationalStructuredString1)
  param_agency47 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id47 <- as(c("IDcodesElectoralDivis"), "character")
  param_isOrdered47 <- as(c("TRUE"), "logical")
  param_isPersistent47 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique47 <- as(c("FALSE"), "logical")
  param_version47 <- as(c("1"), "character")
  CodeList47 <- CodeList$new(agency=param_agency47, contains=param_contains47, id=param_id47, isOrdered=param_isOrdered47, isPersistent=param_isPersistent47, isUniversallyUnique=param_isUniversallyUnique47, purpose=param_purpose47, version=param_version47)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("101"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation48 <- list(ValueString1)
  param_agency48 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes48 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatCanberra:1"), "character")
  param_id48 <- as(c("IDcode101EDC"), "character")
  param_isPersistent48 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique48 <- as(c("FALSE"), "logical")
  param_version48 <- as(c("1"), "character")
  Code48 <- Code$new(agency=param_agency48, denotes=param_denotes48, id=param_id48, isPersistent=param_isPersistent48, isUniversallyUnique=param_isUniversallyUnique48, representation=param_representation48, version=param_version48)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("102"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation49 <- list(ValueString1)
  param_agency49 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes49 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatFraser:1"), "character")
  param_id49 <- as(c("IDcode102EDC"), "character")
  param_isPersistent49 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique49 <- as(c("FALSE"), "logical")
  param_version49 <- as(c("1"), "character")
  Code49 <- Code$new(agency=param_agency49, denotes=param_denotes49, id=param_id49, isPersistent=param_isPersistent49, isUniversallyUnique=param_isUniversallyUnique49, representation=param_representation49, version=param_version49)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("103"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation50 <- list(ValueString1)
  param_agency50 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes50 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatBanks:1"), "character")
  param_id50 <- as(c("IDcode103EDC"), "character")
  param_isPersistent50 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique50 <- as(c("FALSE"), "logical")
  param_version50 <- as(c("1"), "character")
  Code50 <- Code$new(agency=param_agency50, denotes=param_denotes50, id=param_id50, isPersistent=param_isPersistent50, isUniversallyUnique=param_isUniversallyUnique50, representation=param_representation50, version=param_version50)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("104"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation51 <- list(ValueString1)
  param_agency51 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes51 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatBarton:1"), "character")
  param_id51 <- as(c("IDcode104EDC"), "character")
  param_isPersistent51 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique51 <- as(c("FALSE"), "logical")
  param_version51 <- as(c("1"), "character")
  Code51 <- Code$new(agency=param_agency51, denotes=param_denotes51, id=param_id51, isPersistent=param_isPersistent51, isUniversallyUnique=param_isUniversallyUnique51, representation=param_representation51, version=param_version51)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("105"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation52 <- list(ValueString1)
  param_agency52 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes52 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatBennelong:1"), "character")
  param_id52 <- as(c("IDcode105EDC"), "character")
  param_isPersistent52 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique52 <- as(c("FALSE"), "logical")
  param_version52 <- as(c("1"), "character")
  Code52 <- Code$new(agency=param_agency52, denotes=param_denotes52, id=param_id52, isPersistent=param_isPersistent52, isUniversallyUnique=param_isUniversallyUnique52, representation=param_representation52, version=param_version52)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("106"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation53 <- list(ValueString1)
  param_agency53 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes53 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatBerowra:1"), "character")
  param_id53 <- as(c("IDcode106EDC"), "character")
  param_isPersistent53 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique53 <- as(c("FALSE"), "logical")
  param_version53 <- as(c("1"), "character")
  Code53 <- Code$new(agency=param_agency53, denotes=param_denotes53, id=param_id53, isPersistent=param_isPersistent53, isUniversallyUnique=param_isUniversallyUnique53, representation=param_representation53, version=param_version53)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Canberra"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel54 <- list(LabelForDisplay1)
    param_localIdValue1 <- as(c("http://www.aec.gov.au/profiles/act/canberra.htm"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId54 <- list(LocalIdFormat1)
  param_agency54 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id54 <- as(c("IdCatCanberra"), "character")
  param_isPersistent54 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique54 <- as(c("FALSE"), "logical")
  param_version54 <- as(c("1"), "character")
  Category54 <- Category$new(agency=param_agency54, displayLabel=param_displayLabel54, id=param_id54, isPersistent=param_isPersistent54, isUniversallyUnique=param_isUniversallyUnique54, localId=param_localId54, version=param_version54)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

    param_localIdValue1 <- as(c(""), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId55 <- list(LocalIdFormat1)
  param_agency55 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id55 <- as(c("IdCatFraser"), "character")
  param_isPersistent55 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique55 <- as(c("FALSE"), "logical")
  param_version55 <- as(c("1"), "character")
  Category55 <- Category$new(agency=param_agency55, id=param_id55, isPersistent=param_isPersistent55, isUniversallyUnique=param_isUniversallyUnique55, localId=param_localId55, version=param_version55)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Banks"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel56 <- list(LabelForDisplay1)
    param_localIdValue1 <- as(c("http://www.aec.gov.au/profiles/nsw/banks.htm"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId56 <- list(LocalIdFormat1)
  param_agency56 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id56 <- as(c("IdCatBanks"), "character")
  param_isPersistent56 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique56 <- as(c("FALSE"), "logical")
  param_version56 <- as(c("1"), "character")
  Category56 <- Category$new(agency=param_agency56, displayLabel=param_displayLabel56, id=param_id56, isPersistent=param_isPersistent56, isUniversallyUnique=param_isUniversallyUnique56, localId=param_localId56, version=param_version56)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Barton"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel57 <- list(LabelForDisplay1)
    param_localIdValue1 <- as(c("http://www.aec.gov.au/profiles/nsw/barton.htm"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId57 <- list(LocalIdFormat1)
  param_agency57 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id57 <- as(c("IdCatBarton"), "character")
  param_isPersistent57 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique57 <- as(c("FALSE"), "logical")
  param_version57 <- as(c("1"), "character")
  Category57 <- Category$new(agency=param_agency57, displayLabel=param_displayLabel57, id=param_id57, isPersistent=param_isPersistent57, isUniversallyUnique=param_isUniversallyUnique57, localId=param_localId57, version=param_version57)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Bennelong"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel58 <- list(LabelForDisplay1)
    param_localIdValue1 <- as(c("http://www.aec.gov.au/profiles/nsw/bennelong.htm"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId58 <- list(LocalIdFormat1)
  param_agency58 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id58 <- as(c("IdCatBennelong"), "character")
  param_isPersistent58 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique58 <- as(c("FALSE"), "logical")
  param_version58 <- as(c("1"), "character")
  Category58 <- Category$new(agency=param_agency58, displayLabel=param_displayLabel58, id=param_id58, isPersistent=param_isPersistent58, isUniversallyUnique=param_isUniversallyUnique58, localId=param_localId58, version=param_version58)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Berowra"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel59 <- list(LabelForDisplay1)
    param_localIdValue1 <- as(c("http://www.aec.gov.au/profiles/nsw/berowra.htm"), "character")
    LocalIdFormat1 <- LocalIdFormat$new(localIdValue=param_localIdValue1)

  param_localId59 <- list(LocalIdFormat1)
  param_agency59 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id59 <- as(c("IdCatBerowra"), "character")
  param_isPersistent59 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique59 <- as(c("FALSE"), "logical")
  param_version59 <- as(c("1"), "character")
  Category59 <- Category$new(agency=param_agency59, displayLabel=param_displayLabel59, id=param_id59, isPersistent=param_isPersistent59, isUniversallyUnique=param_isUniversallyUnique59, localId=param_localId59, version=param_version59)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType60 <- list(ExternalControlledVocabularyEntry1)
  param_agency60 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain60 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDRangeUniqueID:1"), "character")
  param_id60 <- as(c("IDSubUniqueID"), "character")
  param_isPersistent60 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique60 <- as(c("FALSE"), "logical")
  param_version60 <- as(c("1"), "character")
  SubstantiveValueDomain60 <- SubstantiveValueDomain$new(agency=param_agency60, describedValueDomain=param_describedValueDomain60, id=param_id60, isPersistent=param_isPersistent60, isUniversallyUnique=param_isUniversallyUnique60, recommendedDataType=param_recommendedDataType60, version=param_version60)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("integers between 1000794 and 9872391"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description61 <- list(InternationalStructuredString1)
    param_content1 <- as(c("(1000794 <= x <= 9872391) and mod(x,1)=0"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_logicalExpression61 <- list(ExternalControlledVocabularyEntry1)
  param_agency61 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id61 <- as(c("IDRangeUniqueID"), "character")
  param_isPersistent61 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique61 <- as(c("FALSE"), "logical")
  param_maximumValueInclusive61 <- as(c("9872391"), "character")
  param_minimumValueInclusive61 <- as(c("1000794"), "character")
  param_version61 <- as(c("1"), "character")
  ValueAndConceptDescription61 <- ValueAndConceptDescription$new(agency=param_agency61, description=param_description61, id=param_id61, isPersistent=param_isPersistent61, isUniversallyUnique=param_isUniversallyUnique61, logicalExpression=param_logicalExpression61, maximumValueInclusive=param_maximumValueInclusive61, minimumValueInclusive=param_minimumValueInclusive61, version=param_version61)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType62 <- list(ExternalControlledVocabularyEntry1)
  param_agency62 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain62 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCodesMode:1"), "character")
  param_id62 <- as(c("IdSubMode"), "character")
  param_isPersistent62 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique62 <- as(c("FALSE"), "logical")
  param_version62 <- as(c("1"), "character")
  SubstantiveValueDomain62 <- SubstantiveValueDomain$new(agency=param_agency62, enumeratedValueDomain=param_enumeratedValueDomain62, id=param_id62, isPersistent=param_isPersistent62, isUniversallyUnique=param_isUniversallyUnique62, recommendedDataType=param_recommendedDataType62, version=param_version62)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1HardCopy:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2Online:1"), "character")
    CodeIndicator2 <- CodeIndicator$new(index=param_index2, member=param_member2)

  param_contains63 <- list(CodeIndicator1, CodeIndicator2)
  param_agency63 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id63 <- as(c("IdCodesMode"), "character")
  param_isOrdered63 <- as(c("TRUE"), "logical")
  param_isPersistent63 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique63 <- as(c("FALSE"), "logical")
  param_version63 <- as(c("1"), "character")
  CodeList63 <- CodeList$new(agency=param_agency63, contains=param_contains63, id=param_id63, isOrdered=param_isOrdered63, isPersistent=param_isPersistent63, isUniversallyUnique=param_isUniversallyUnique63, version=param_version63)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("1"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation64 <- list(ValueString1)
  param_agency64 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes64 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatHardCopy:1"), "character")
  param_id64 <- as(c("IDcode1HardCopy"), "character")
  param_isPersistent64 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique64 <- as(c("FALSE"), "logical")
  param_version64 <- as(c("1"), "character")
  Code64 <- Code$new(agency=param_agency64, denotes=param_denotes64, id=param_id64, isPersistent=param_isPersistent64, isUniversallyUnique=param_isUniversallyUnique64, representation=param_representation64, version=param_version64)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("8"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation65 <- list(ValueString1)
  param_agency65 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes65 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCatOnline:1"), "character")
  param_id65 <- as(c("IDcode2Online"), "character")
  param_isPersistent65 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique65 <- as(c("FALSE"), "logical")
  param_version65 <- as(c("1"), "character")
  Code65 <- Code$new(agency=param_agency65, denotes=param_denotes65, id=param_id65, isPersistent=param_isPersistent65, isUniversallyUnique=param_isUniversallyUnique65, representation=param_representation65, version=param_version65)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Hard Copy"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel66 <- list(LabelForDisplay1)
  param_agency66 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id66 <- as(c("IdCatHardCopy"), "character")
  param_isPersistent66 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique66 <- as(c("FALSE"), "logical")
  param_version66 <- as(c("1"), "character")
  Category66 <- Category$new(agency=param_agency66, displayLabel=param_displayLabel66, id=param_id66, isPersistent=param_isPersistent66, isUniversallyUnique=param_isUniversallyUnique66, version=param_version66)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Online"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel67 <- list(LabelForDisplay1)
  param_agency67 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id67 <- as(c("IdCatOnline"), "character")
  param_isPersistent67 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique67 <- as(c("FALSE"), "logical")
  param_version67 <- as(c("1"), "character")
  Category67 <- Category$new(agency=param_agency67, displayLabel=param_displayLabel67, id=param_id67, isPersistent=param_isPersistent67, isUniversallyUnique=param_isUniversallyUnique67, version=param_version67)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("string"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType68 <- list(ExternalControlledVocabularyEntry1)
  param_agency68 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain68 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdValCharDate:1"), "character")
  param_id68 <- as(c("IdSubDateComp"), "character")
  param_isPersistent68 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique68 <- as(c("FALSE"), "logical")
  param_version68 <- as(c("1"), "character")
  SubstantiveValueDomain68 <- SubstantiveValueDomain$new(agency=param_agency68, describedValueDomain=param_describedValueDomain68, id=param_id68, isPersistent=param_isPersistent68, isUniversallyUnique=param_isUniversallyUnique68, recommendedDataType=param_recommendedDataType68, version=param_version68)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("Date as an 11 character string MM-dd-yyyy"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description69 <- list(InternationalStructuredString1)
    param_content1 <- as(c("MM-dd-yyyy"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_formatPattern69 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("/\\d\\d\\d\\d-\\d\\d-\\d\\d/"), "character")
    TypedString1 <- TypedString$new(content=param_content1)

  param_regularExpression69 <- list(TypedString1)
  param_agency69 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id69 <- as(c("IdValCharDate"), "character")
  param_isPersistent69 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique69 <- as(c("FALSE"), "logical")
  param_version69 <- as(c("1"), "character")
  ValueAndConceptDescription69 <- ValueAndConceptDescription$new(agency=param_agency69, description=param_description69, formatPattern=param_formatPattern69, id=param_id69, isPersistent=param_isPersistent69, isUniversallyUnique=param_isUniversallyUnique69, regularExpression=param_regularExpression69, version=param_version69)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType70 <- list(ExternalControlledVocabularyEntry1)
  param_agency70 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain70 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCodesState:1"), "character")
  param_id70 <- as(c("IdSubState"), "character")
  param_isPersistent70 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique70 <- as(c("FALSE"), "logical")
  param_version70 <- as(c("1"), "character")
  SubstantiveValueDomain70 <- SubstantiveValueDomain$new(agency=param_agency70, enumeratedValueDomain=param_enumeratedValueDomain70, id=param_id70, isPersistent=param_isPersistent70, isUniversallyUnique=param_isUniversallyUnique70, recommendedDataType=param_recommendedDataType70, version=param_version70)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1State:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2State:1"), "character")
    CodeIndicator2 <- CodeIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3State:1"), "character")
    CodeIndicator3 <- CodeIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4State:1"), "character")
    CodeIndicator4 <- CodeIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("5"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5State:1"), "character")
    CodeIndicator5 <- CodeIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("6"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6State:1"), "character")
    CodeIndicator6 <- CodeIndicator$new(index=param_index6, member=param_member6)

    param_index7 <- as(c("7"), "integer")
    param_member7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7State:1"), "character")
    CodeIndicator7 <- CodeIndicator$new(index=param_index7, member=param_member7)

    param_index8 <- as(c("8"), "integer")
    param_member8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8State:1"), "character")
    CodeIndicator8 <- CodeIndicator$new(index=param_index8, member=param_member8)

    param_index9 <- as(c("9"), "integer")
    param_member9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9State:1"), "character")
    CodeIndicator9 <- CodeIndicator$new(index=param_index9, member=param_member9)

  param_contains71 <- list(CodeIndicator1, CodeIndicator2, CodeIndicator3, CodeIndicator4, CodeIndicator5, CodeIndicator6, CodeIndicator7, CodeIndicator8, CodeIndicator9)
  param_agency71 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id71 <- as(c("IdCodesState"), "character")
  param_isPersistent71 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique71 <- as(c("FALSE"), "logical")
  param_version71 <- as(c("1"), "character")
  CodeList71 <- CodeList$new(agency=param_agency71, contains=param_contains71, id=param_id71, isPersistent=param_isPersistent71, isUniversallyUnique=param_isUniversallyUnique71, version=param_version71)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("1"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation72 <- list(ValueString1)
  param_agency72 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes72 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat1State:1"), "character")
  param_id72 <- as(c("IDcode1State"), "character")
  param_isPersistent72 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique72 <- as(c("FALSE"), "logical")
  param_version72 <- as(c("1"), "character")
  Code72 <- Code$new(agency=param_agency72, denotes=param_denotes72, id=param_id72, isPersistent=param_isPersistent72, isUniversallyUnique=param_isUniversallyUnique72, representation=param_representation72, version=param_version72)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("2"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation73 <- list(ValueString1)
  param_agency73 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes73 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat2State:1"), "character")
  param_id73 <- as(c("IDcode2State"), "character")
  param_isPersistent73 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique73 <- as(c("FALSE"), "logical")
  param_version73 <- as(c("1"), "character")
  Code73 <- Code$new(agency=param_agency73, denotes=param_denotes73, id=param_id73, isPersistent=param_isPersistent73, isUniversallyUnique=param_isUniversallyUnique73, representation=param_representation73, version=param_version73)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("3"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation74 <- list(ValueString1)
  param_agency74 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes74 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat3State:1"), "character")
  param_id74 <- as(c("IDcode3State"), "character")
  param_isPersistent74 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique74 <- as(c("FALSE"), "logical")
  param_version74 <- as(c("1"), "character")
  Code74 <- Code$new(agency=param_agency74, denotes=param_denotes74, id=param_id74, isPersistent=param_isPersistent74, isUniversallyUnique=param_isUniversallyUnique74, representation=param_representation74, version=param_version74)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("4"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation75 <- list(ValueString1)
  param_agency75 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes75 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat4State:1"), "character")
  param_id75 <- as(c("IDcode4State"), "character")
  param_isPersistent75 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique75 <- as(c("FALSE"), "logical")
  param_version75 <- as(c("1"), "character")
  Code75 <- Code$new(agency=param_agency75, denotes=param_denotes75, id=param_id75, isPersistent=param_isPersistent75, isUniversallyUnique=param_isUniversallyUnique75, representation=param_representation75, version=param_version75)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("5"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation76 <- list(ValueString1)
  param_agency76 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes76 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat5State:1"), "character")
  param_id76 <- as(c("IDcode5State"), "character")
  param_isPersistent76 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique76 <- as(c("FALSE"), "logical")
  param_version76 <- as(c("1"), "character")
  Code76 <- Code$new(agency=param_agency76, denotes=param_denotes76, id=param_id76, isPersistent=param_isPersistent76, isUniversallyUnique=param_isUniversallyUnique76, representation=param_representation76, version=param_version76)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("6"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation77 <- list(ValueString1)
  param_agency77 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes77 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat6State:1"), "character")
  param_id77 <- as(c("IDcode6State"), "character")
  param_isPersistent77 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique77 <- as(c("FALSE"), "logical")
  param_version77 <- as(c("1"), "character")
  Code77 <- Code$new(agency=param_agency77, denotes=param_denotes77, id=param_id77, isPersistent=param_isPersistent77, isUniversallyUnique=param_isUniversallyUnique77, representation=param_representation77, version=param_version77)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("7"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation78 <- list(ValueString1)
  param_agency78 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes78 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat7State:1"), "character")
  param_id78 <- as(c("IDcode7State"), "character")
  param_isPersistent78 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique78 <- as(c("FALSE"), "logical")
  param_version78 <- as(c("1"), "character")
  Code78 <- Code$new(agency=param_agency78, denotes=param_denotes78, id=param_id78, isPersistent=param_isPersistent78, isUniversallyUnique=param_isUniversallyUnique78, representation=param_representation78, version=param_version78)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("8"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation79 <- list(ValueString1)
  param_agency79 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes79 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat8State:1"), "character")
  param_id79 <- as(c("IDcode8State"), "character")
  param_isPersistent79 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique79 <- as(c("FALSE"), "logical")
  param_version79 <- as(c("1"), "character")
  Code79 <- Code$new(agency=param_agency79, denotes=param_denotes79, id=param_id79, isPersistent=param_isPersistent79, isUniversallyUnique=param_isUniversallyUnique79, representation=param_representation79, version=param_version79)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("9"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation80 <- list(ValueString1)
  param_agency80 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes80 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat9State:1"), "character")
  param_id80 <- as(c("IDcode9State"), "character")
  param_isPersistent80 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique80 <- as(c("FALSE"), "logical")
  param_version80 <- as(c("1"), "character")
  Code80 <- Code$new(agency=param_agency80, denotes=param_denotes80, id=param_id80, isPersistent=param_isPersistent80, isUniversallyUnique=param_isUniversallyUnique80, representation=param_representation80, version=param_version80)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("NSW"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel81 <- list(LabelForDisplay1)
  param_agency81 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id81 <- as(c("IdCat1State"), "character")
  param_isPersistent81 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique81 <- as(c("FALSE"), "logical")
  param_version81 <- as(c("1"), "character")
  Category81 <- Category$new(agency=param_agency81, displayLabel=param_displayLabel81, id=param_id81, isPersistent=param_isPersistent81, isUniversallyUnique=param_isUniversallyUnique81, version=param_version81)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("VIC"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel82 <- list(LabelForDisplay1)
  param_agency82 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id82 <- as(c("IdCat2State"), "character")
  param_isPersistent82 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique82 <- as(c("FALSE"), "logical")
  param_version82 <- as(c("1"), "character")
  Category82 <- Category$new(agency=param_agency82, displayLabel=param_displayLabel82, id=param_id82, isPersistent=param_isPersistent82, isUniversallyUnique=param_isUniversallyUnique82, version=param_version82)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("QLD"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel83 <- list(LabelForDisplay1)
  param_agency83 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id83 <- as(c("IdCat3State"), "character")
  param_isPersistent83 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique83 <- as(c("FALSE"), "logical")
  param_version83 <- as(c("1"), "character")
  Category83 <- Category$new(agency=param_agency83, displayLabel=param_displayLabel83, id=param_id83, isPersistent=param_isPersistent83, isUniversallyUnique=param_isUniversallyUnique83, version=param_version83)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("SA"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel84 <- list(LabelForDisplay1)
  param_agency84 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id84 <- as(c("IdCat4State"), "character")
  param_isPersistent84 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique84 <- as(c("FALSE"), "logical")
  param_version84 <- as(c("1"), "character")
  Category84 <- Category$new(agency=param_agency84, displayLabel=param_displayLabel84, id=param_id84, isPersistent=param_isPersistent84, isUniversallyUnique=param_isUniversallyUnique84, version=param_version84)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("WA"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel85 <- list(LabelForDisplay1)
  param_agency85 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id85 <- as(c("IdCat5State"), "character")
  param_isPersistent85 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique85 <- as(c("FALSE"), "logical")
  param_version85 <- as(c("1"), "character")
  Category85 <- Category$new(agency=param_agency85, displayLabel=param_displayLabel85, id=param_id85, isPersistent=param_isPersistent85, isUniversallyUnique=param_isUniversallyUnique85, version=param_version85)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("TAS"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel86 <- list(LabelForDisplay1)
  param_agency86 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id86 <- as(c("IdCat6State"), "character")
  param_isPersistent86 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique86 <- as(c("FALSE"), "logical")
  param_version86 <- as(c("1"), "character")
  Category86 <- Category$new(agency=param_agency86, displayLabel=param_displayLabel86, id=param_id86, isPersistent=param_isPersistent86, isUniversallyUnique=param_isUniversallyUnique86, version=param_version86)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("NT"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel87 <- list(LabelForDisplay1)
  param_agency87 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id87 <- as(c("IdCat7State"), "character")
  param_isPersistent87 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique87 <- as(c("FALSE"), "logical")
  param_version87 <- as(c("1"), "character")
  Category87 <- Category$new(agency=param_agency87, displayLabel=param_displayLabel87, id=param_id87, isPersistent=param_isPersistent87, isUniversallyUnique=param_isUniversallyUnique87, version=param_version87)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("ACT"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel88 <- list(LabelForDisplay1)
  param_agency88 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id88 <- as(c("IdCat8State"), "character")
  param_isPersistent88 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique88 <- as(c("FALSE"), "logical")
  param_version88 <- as(c("1"), "character")
  Category88 <- Category$new(agency=param_agency88, displayLabel=param_displayLabel88, id=param_id88, isPersistent=param_isPersistent88, isUniversallyUnique=param_isUniversallyUnique88, version=param_version88)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Other Territories"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel89 <- list(LabelForDisplay1)
  param_agency89 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id89 <- as(c("IdCat9State"), "character")
  param_isPersistent89 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique89 <- as(c("FALSE"), "logical")
  param_version89 <- as(c("1"), "character")
  Category89 <- Category$new(agency=param_agency89, displayLabel=param_displayLabel89, id=param_id89, isPersistent=param_isPersistent89, isUniversallyUnique=param_isUniversallyUnique89, version=param_version89)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("string"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType91 <- list(ExternalControlledVocabularyEntry1)
  param_agency91 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain91 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdValDivision:1"), "character")
  param_id91 <- as(c("IdSubDivision"), "character")
  param_isPersistent91 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique91 <- as(c("FALSE"), "logical")
  param_version91 <- as(c("1"), "character")
  SubstantiveValueDomain91 <- SubstantiveValueDomain$new(agency=param_agency91, describedValueDomain=param_describedValueDomain91, id=param_id91, isPersistent=param_isPersistent91, isUniversallyUnique=param_isUniversallyUnique91, recommendedDataType=param_recommendedDataType91, version=param_version91)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("uppercase characters up to 255 characters long"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description92 <- list(InternationalStructuredString1)
    param_content1 <- as(c("/[ A-Z]{1,255}/"), "character")
    TypedString1 <- TypedString$new(content=param_content1)

  param_regularExpression92 <- list(TypedString1)
  param_agency92 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id92 <- as(c("IdValDivision"), "character")
  param_isPersistent92 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique92 <- as(c("FALSE"), "logical")
  param_version92 <- as(c("1"), "character")
  ValueAndConceptDescription92 <- ValueAndConceptDescription$new(agency=param_agency92, description=param_description92, id=param_id92, isPersistent=param_isPersistent92, isUniversallyUnique=param_isUniversallyUnique92, regularExpression=param_regularExpression92, version=param_version92)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType93 <- list(ExternalControlledVocabularyEntry1)
  param_agency93 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain93 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodesNotMuch:1"), "character")
  param_id93 <- as(c("IdSubA4"), "character")
  param_isPersistent93 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique93 <- as(c("FALSE"), "logical")
  param_version93 <- as(c("1"), "character")
  SubstantiveValueDomain93 <- SubstantiveValueDomain$new(agency=param_agency93, enumeratedValueDomain=param_enumeratedValueDomain93, id=param_id93, isPersistent=param_isPersistent93, isUniversallyUnique=param_isUniversallyUnique93, recommendedDataType=param_recommendedDataType93, version=param_version93)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch1:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch2:1"), "character")
    CodeIndicator2 <- CodeIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch3:1"), "character")
    CodeIndicator3 <- CodeIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch4:1"), "character")
    CodeIndicator4 <- CodeIndicator$new(index=param_index4, member=param_member4)

  param_contains94 <- list(CodeIndicator1, CodeIndicator2, CodeIndicator3, CodeIndicator4)
  param_agency94 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id94 <- as(c("IDcodesNotMuch"), "character")
  param_isOrdered94 <- as(c("TRUE"), "logical")
  param_isPersistent94 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique94 <- as(c("FALSE"), "logical")
  param_version94 <- as(c("1"), "character")
  CodeList94 <- CodeList$new(agency=param_agency94, contains=param_contains94, id=param_id94, isOrdered=param_isOrdered94, isPersistent=param_isPersistent94, isUniversallyUnique=param_isUniversallyUnique94, version=param_version94)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("1"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation95 <- list(ValueString1)
  param_agency95 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes95 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcatNotMuch1:1"), "character")
  param_id95 <- as(c("IDcodeNotMuch1"), "character")
  param_isPersistent95 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique95 <- as(c("FALSE"), "logical")
  param_version95 <- as(c("1"), "character")
  Code95 <- Code$new(agency=param_agency95, denotes=param_denotes95, id=param_id95, isPersistent=param_isPersistent95, isUniversallyUnique=param_isUniversallyUnique95, representation=param_representation95, version=param_version95)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("2"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation96 <- list(ValueString1)
  param_agency96 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes96 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcatNotMuch2:1"), "character")
  param_id96 <- as(c("IDcodeNotMuch2"), "character")
  param_isPersistent96 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique96 <- as(c("FALSE"), "logical")
  param_version96 <- as(c("1"), "character")
  Code96 <- Code$new(agency=param_agency96, denotes=param_denotes96, id=param_id96, isPersistent=param_isPersistent96, isUniversallyUnique=param_isUniversallyUnique96, representation=param_representation96, version=param_version96)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("3"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation97 <- list(ValueString1)
  param_agency97 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes97 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcatNotMuch3:1"), "character")
  param_id97 <- as(c("IDcodeNotMuch3"), "character")
  param_isPersistent97 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique97 <- as(c("FALSE"), "logical")
  param_version97 <- as(c("1"), "character")
  Code97 <- Code$new(agency=param_agency97, denotes=param_denotes97, id=param_id97, isPersistent=param_isPersistent97, isUniversallyUnique=param_isUniversallyUnique97, representation=param_representation97, version=param_version97)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("4"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation98 <- list(ValueString1)
  param_agency98 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes98 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcatNotMuch4:1"), "character")
  param_id98 <- as(c("IDcodeNotMuch4"), "character")
  param_isPersistent98 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique98 <- as(c("FALSE"), "logical")
  param_version98 <- as(c("1"), "character")
  Code98 <- Code$new(agency=param_agency98, denotes=param_denotes98, id=param_id98, isPersistent=param_isPersistent98, isUniversallyUnique=param_isUniversallyUnique98, representation=param_representation98, version=param_version98)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("A good deal"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel99 <- list(LabelForDisplay1)
  param_agency99 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id99 <- as(c("IDcatNotMuch1"), "character")
  param_isPersistent99 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique99 <- as(c("FALSE"), "logical")
  param_version99 <- as(c("1"), "character")
  Category99 <- Category$new(agency=param_agency99, displayLabel=param_displayLabel99, id=param_id99, isPersistent=param_isPersistent99, isUniversallyUnique=param_isUniversallyUnique99, version=param_version99)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Some"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel100 <- list(LabelForDisplay1)
  param_agency100 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id100 <- as(c("IDcatNotMuch2"), "character")
  param_isPersistent100 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique100 <- as(c("FALSE"), "logical")
  param_version100 <- as(c("1"), "character")
  Category100 <- Category$new(agency=param_agency100, displayLabel=param_displayLabel100, id=param_id100, isPersistent=param_isPersistent100, isUniversallyUnique=param_isUniversallyUnique100, version=param_version100)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("Not much"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel101 <- list(LabelForDisplay1)
  param_agency101 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id101 <- as(c("IDcatNotMuch3"), "character")
  param_isPersistent101 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique101 <- as(c("FALSE"), "logical")
  param_version101 <- as(c("1"), "character")
  Category101 <- Category$new(agency=param_agency101, displayLabel=param_displayLabel101, id=param_id101, isPersistent=param_isPersistent101, isUniversallyUnique=param_isUniversallyUnique101, version=param_version101)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("None at all"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel102 <- list(LabelForDisplay1)
  param_agency102 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id102 <- as(c("IDcatNotMuch4"), "character")
  param_isPersistent102 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique102 <- as(c("FALSE"), "logical")
  param_version102 <- as(c("1"), "character")
  Category102 <- Category$new(agency=param_agency102, displayLabel=param_displayLabel102, id=param_id102, isPersistent=param_isPersistent102, isUniversallyUnique=param_isUniversallyUnique102, version=param_version102)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#integer"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType103 <- list(ExternalControlledVocabularyEntry1)
  param_agency103 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain103 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDValsG1Age:1"), "character")
  param_id103 <- as(c("IDSubG1Age"), "character")
  param_isPersistent103 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique103 <- as(c("FALSE"), "logical")
  param_version103 <- as(c("1"), "character")
  SubstantiveValueDomain103 <- SubstantiveValueDomain$new(agency=param_agency103, describedValueDomain=param_describedValueDomain103, id=param_id103, isPersistent=param_isPersistent103, isUniversallyUnique=param_isUniversallyUnique103, recommendedDataType=param_recommendedDataType103, version=param_version103)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("integers"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description104 <- list(InternationalStructuredString1)
    param_content1 <- as(c("###"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_formatPattern104 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("mod(x,1)=0"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_logicalExpression104 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("/\\d{1,3}/"), "character")
    TypedString1 <- TypedString$new(content=param_content1)

  param_regularExpression104 <- list(TypedString1)
  param_agency104 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id104 <- as(c("IDValsG1Age"), "character")
  param_isPersistent104 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique104 <- as(c("FALSE"), "logical")
  param_version104 <- as(c("1"), "character")
  ValueAndConceptDescription104 <- ValueAndConceptDescription$new(agency=param_agency104, description=param_description104, formatPattern=param_formatPattern104, id=param_id104, isPersistent=param_isPersistent104, isUniversallyUnique=param_isUniversallyUnique104, logicalExpression=param_logicalExpression104, regularExpression=param_regularExpression104, version=param_version104)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType105 <- list(ExternalControlledVocabularyEntry1)
  param_agency105 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain105 <- as(c("http://www.abs.gov.au/ANZSCO"), "character")
  param_id105 <- as(c("IdSubXg5"), "character")
  param_isPersistent105 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique105 <- as(c("FALSE"), "logical")
  param_version105 <- as(c("1"), "character")
  SubstantiveValueDomain105 <- SubstantiveValueDomain$new(agency=param_agency105, enumeratedValueDomain=param_enumeratedValueDomain105, id=param_id105, isPersistent=param_isPersistent105, isUniversallyUnique=param_isUniversallyUnique105, recommendedDataType=param_recommendedDataType105, version=param_version105)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("double"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/#double"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType106 <- list(ExternalControlledVocabularyEntry1)
  param_agency106 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain106 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDValsWeight:1"), "character")
  param_id106 <- as(c("IdSubWeight"), "character")
  param_isPersistent106 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique106 <- as(c("FALSE"), "logical")
  param_version106 <- as(c("1"), "character")
  SubstantiveValueDomain106 <- SubstantiveValueDomain$new(agency=param_agency106, describedValueDomain=param_describedValueDomain106, id=param_id106, isPersistent=param_isPersistent106, isUniversallyUnique=param_isUniversallyUnique106, recommendedDataType=param_recommendedDataType106, version=param_version106)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("doubles"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description107 <- list(InternationalStructuredString1)
    param_content1 <- as(c("#.###############"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_formatPattern107 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("/\\d.\\d{0,15}/"), "character")
    TypedString1 <- TypedString$new(content=param_content1)

  param_regularExpression107 <- list(TypedString1)
  param_agency107 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id107 <- as(c("IDValsWeight"), "character")
  param_isPersistent107 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique107 <- as(c("FALSE"), "logical")
  param_version107 <- as(c("1"), "character")
  ValueAndConceptDescription107 <- ValueAndConceptDescription$new(agency=param_agency107, description=param_description107, formatPattern=param_formatPattern107, id=param_id107, isPersistent=param_isPersistent107, isUniversallyUnique=param_isUniversallyUnique107, regularExpression=param_regularExpression107, version=param_version107)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType108 <- list(ExternalControlledVocabularyEntry1)
  param_agency108 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_enumeratedValueDomain108 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCodesPartyAby:1"), "character")
  param_id108 <- as(c("IdSubPartyAby"), "character")
  param_isPersistent108 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique108 <- as(c("FALSE"), "logical")
  param_version108 <- as(c("1"), "character")
  SubstantiveValueDomain108 <- SubstantiveValueDomain$new(agency=param_agency108, enumeratedValueDomain=param_enumeratedValueDomain108, id=param_id108, isPersistent=param_isPersistent108, isUniversallyUnique=param_isUniversallyUnique108, recommendedDataType=param_recommendedDataType108, version=param_version108)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   CodeList ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1PartyAby:1"), "character")
    CodeIndicator1 <- CodeIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2PartyAby:1"), "character")
    CodeIndicator2 <- CodeIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3PartyAby:1"), "character")
    CodeIndicator3 <- CodeIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4PartyAby:1"), "character")
    CodeIndicator4 <- CodeIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("5"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5PartyAby:1"), "character")
    CodeIndicator5 <- CodeIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("6"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6PartyAby:1"), "character")
    CodeIndicator6 <- CodeIndicator$new(index=param_index6, member=param_member6)

    param_index7 <- as(c("7"), "integer")
    param_member7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7PartyAby:1"), "character")
    CodeIndicator7 <- CodeIndicator$new(index=param_index7, member=param_member7)

    param_index8 <- as(c("8"), "integer")
    param_member8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8PartyAby:1"), "character")
    CodeIndicator8 <- CodeIndicator$new(index=param_index8, member=param_member8)

    param_index9 <- as(c("9"), "integer")
    param_member9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9PartyAby:1"), "character")
    CodeIndicator9 <- CodeIndicator$new(index=param_index9, member=param_member9)

  param_contains109 <- list(CodeIndicator1, CodeIndicator2, CodeIndicator3, CodeIndicator4, CodeIndicator5, CodeIndicator6, CodeIndicator7, CodeIndicator8, CodeIndicator9)
  param_agency109 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id109 <- as(c("IdCodesPartyAby"), "character")
  param_isPersistent109 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique109 <- as(c("FALSE"), "logical")
  param_version109 <- as(c("1"), "character")
  CodeList109 <- CodeList$new(agency=param_agency109, contains=param_contains109, id=param_id109, isPersistent=param_isPersistent109, isUniversallyUnique=param_isUniversallyUnique109, version=param_version109)

#  ^^^^^ ------ Ending elment:   CodeList ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("ALP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation110 <- list(ValueString1)
  param_agency110 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes110 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat1PartyAby:1"), "character")
  param_id110 <- as(c("IDcode1PartyAby"), "character")
  param_isPersistent110 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique110 <- as(c("FALSE"), "logical")
  param_version110 <- as(c("1"), "character")
  Code110 <- Code$new(agency=param_agency110, denotes=param_denotes110, id=param_id110, isPersistent=param_isPersistent110, isUniversallyUnique=param_isUniversallyUnique110, representation=param_representation110, version=param_version110)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("CLP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation111 <- list(ValueString1)
  param_agency111 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes111 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat2PartyAby:1"), "character")
  param_id111 <- as(c("IDcode2PartyAby"), "character")
  param_isPersistent111 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique111 <- as(c("FALSE"), "logical")
  param_version111 <- as(c("1"), "character")
  Code111 <- Code$new(agency=param_agency111, denotes=param_denotes111, id=param_id111, isPersistent=param_isPersistent111, isUniversallyUnique=param_isUniversallyUnique111, representation=param_representation111, version=param_version111)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("GRN"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation112 <- list(ValueString1)
  param_agency112 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes112 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat3PartyAby:1"), "character")
  param_id112 <- as(c("IDcode3PartyAby"), "character")
  param_isPersistent112 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique112 <- as(c("FALSE"), "logical")
  param_version112 <- as(c("1"), "character")
  Code112 <- Code$new(agency=param_agency112, denotes=param_denotes112, id=param_id112, isPersistent=param_isPersistent112, isUniversallyUnique=param_isUniversallyUnique112, representation=param_representation112, version=param_version112)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("IND"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation113 <- list(ValueString1)
  param_agency113 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes113 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat4PartyAby:1"), "character")
  param_id113 <- as(c("IDcode4PartyAby"), "character")
  param_isPersistent113 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique113 <- as(c("FALSE"), "logical")
  param_version113 <- as(c("1"), "character")
  Code113 <- Code$new(agency=param_agency113, denotes=param_denotes113, id=param_id113, isPersistent=param_isPersistent113, isUniversallyUnique=param_isUniversallyUnique113, representation=param_representation113, version=param_version113)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("KAP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation114 <- list(ValueString1)
  param_agency114 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes114 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat5PartyAby:1"), "character")
  param_id114 <- as(c("IDcode5PartyAby"), "character")
  param_isPersistent114 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique114 <- as(c("FALSE"), "logical")
  param_version114 <- as(c("1"), "character")
  Code114 <- Code$new(agency=param_agency114, denotes=param_denotes114, id=param_id114, isPersistent=param_isPersistent114, isUniversallyUnique=param_isUniversallyUnique114, representation=param_representation114, version=param_version114)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("LNP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation115 <- list(ValueString1)
  param_agency115 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes115 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat6PartyAby:1"), "character")
  param_id115 <- as(c("IDcode6PartyAby"), "character")
  param_isPersistent115 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique115 <- as(c("FALSE"), "logical")
  param_version115 <- as(c("1"), "character")
  Code115 <- Code$new(agency=param_agency115, denotes=param_denotes115, id=param_id115, isPersistent=param_isPersistent115, isUniversallyUnique=param_isUniversallyUnique115, representation=param_representation115, version=param_version115)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("LP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation116 <- list(ValueString1)
  param_agency116 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes116 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat7PartyAby:1"), "character")
  param_id116 <- as(c("IDcode7PartyAby"), "character")
  param_isPersistent116 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique116 <- as(c("FALSE"), "logical")
  param_version116 <- as(c("1"), "character")
  Code116 <- Code$new(agency=param_agency116, denotes=param_denotes116, id=param_id116, isPersistent=param_isPersistent116, isUniversallyUnique=param_isUniversallyUnique116, representation=param_representation116, version=param_version116)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("NP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation117 <- list(ValueString1)
  param_agency117 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes117 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat8PartyAby:1"), "character")
  param_id117 <- as(c("IDcode8PartyAby"), "character")
  param_isPersistent117 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique117 <- as(c("FALSE"), "logical")
  param_version117 <- as(c("1"), "character")
  Code117 <- Code$new(agency=param_agency117, denotes=param_denotes117, id=param_id117, isPersistent=param_isPersistent117, isUniversallyUnique=param_isUniversallyUnique117, representation=param_representation117, version=param_version117)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Code ------ vvvvv

    param_content1 <- as(c("PUP"), "character")
    ValueString1 <- ValueString$new(content=param_content1)

  param_representation118 <- list(ValueString1)
  param_agency118 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_denotes118 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdCat9PartyAby:1"), "character")
  param_id118 <- as(c("IDcode9PartyAby"), "character")
  param_isPersistent118 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique118 <- as(c("FALSE"), "logical")
  param_version118 <- as(c("1"), "character")
  Code118 <- Code$new(agency=param_agency118, denotes=param_denotes118, id=param_id118, isPersistent=param_isPersistent118, isUniversallyUnique=param_isUniversallyUnique118, representation=param_representation118, version=param_version118)

#  ^^^^^ ------ Ending elment:   Code ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("ALP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel119 <- list(LabelForDisplay1)
  param_agency119 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id119 <- as(c("IdCat1PartyAby"), "character")
  param_isPersistent119 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique119 <- as(c("FALSE"), "logical")
  param_version119 <- as(c("1"), "character")
  Category119 <- Category$new(agency=param_agency119, displayLabel=param_displayLabel119, id=param_id119, isPersistent=param_isPersistent119, isUniversallyUnique=param_isUniversallyUnique119, version=param_version119)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("CLP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel120 <- list(LabelForDisplay1)
  param_agency120 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id120 <- as(c("IdCat2PartyAby"), "character")
  param_isPersistent120 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique120 <- as(c("FALSE"), "logical")
  param_version120 <- as(c("1"), "character")
  Category120 <- Category$new(agency=param_agency120, displayLabel=param_displayLabel120, id=param_id120, isPersistent=param_isPersistent120, isUniversallyUnique=param_isUniversallyUnique120, version=param_version120)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("GRN"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel121 <- list(LabelForDisplay1)
  param_agency121 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id121 <- as(c("IdCat3PartyAby"), "character")
  param_isPersistent121 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique121 <- as(c("FALSE"), "logical")
  param_version121 <- as(c("1"), "character")
  Category121 <- Category$new(agency=param_agency121, displayLabel=param_displayLabel121, id=param_id121, isPersistent=param_isPersistent121, isUniversallyUnique=param_isUniversallyUnique121, version=param_version121)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("IND"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel122 <- list(LabelForDisplay1)
  param_agency122 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id122 <- as(c("IdCat4PartyAby"), "character")
  param_isPersistent122 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique122 <- as(c("FALSE"), "logical")
  param_version122 <- as(c("1"), "character")
  Category122 <- Category$new(agency=param_agency122, displayLabel=param_displayLabel122, id=param_id122, isPersistent=param_isPersistent122, isUniversallyUnique=param_isUniversallyUnique122, version=param_version122)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("KAP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel123 <- list(LabelForDisplay1)
  param_agency123 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id123 <- as(c("IdCat5PartyAby"), "character")
  param_isPersistent123 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique123 <- as(c("FALSE"), "logical")
  param_version123 <- as(c("1"), "character")
  Category123 <- Category$new(agency=param_agency123, displayLabel=param_displayLabel123, id=param_id123, isPersistent=param_isPersistent123, isUniversallyUnique=param_isUniversallyUnique123, version=param_version123)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("LNP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel124 <- list(LabelForDisplay1)
  param_agency124 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id124 <- as(c("IdCat6PartyAby"), "character")
  param_isPersistent124 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique124 <- as(c("FALSE"), "logical")
  param_version124 <- as(c("1"), "character")
  Category124 <- Category$new(agency=param_agency124, displayLabel=param_displayLabel124, id=param_id124, isPersistent=param_isPersistent124, isUniversallyUnique=param_isUniversallyUnique124, version=param_version124)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("LP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel125 <- list(LabelForDisplay1)
  param_agency125 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id125 <- as(c("IdCat7PartyAby"), "character")
  param_isPersistent125 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique125 <- as(c("FALSE"), "logical")
  param_version125 <- as(c("1"), "character")
  Category125 <- Category$new(agency=param_agency125, displayLabel=param_displayLabel125, id=param_id125, isPersistent=param_isPersistent125, isUniversallyUnique=param_isUniversallyUnique125, version=param_version125)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("NP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel126 <- list(LabelForDisplay1)
  param_agency126 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id126 <- as(c("IdCat8PartyAby"), "character")
  param_isPersistent126 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique126 <- as(c("FALSE"), "logical")
  param_version126 <- as(c("1"), "character")
  Category126 <- Category$new(agency=param_agency126, displayLabel=param_displayLabel126, id=param_id126, isPersistent=param_isPersistent126, isUniversallyUnique=param_isUniversallyUnique126, version=param_version126)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   Category ------ vvvvv

      param_content1 <- as(c("PUP"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    LabelForDisplay1 <- LabelForDisplay$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_displayLabel127 <- list(LabelForDisplay1)
  param_agency127 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id127 <- as(c("IdCat9PartyAby"), "character")
  param_isPersistent127 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique127 <- as(c("FALSE"), "logical")
  param_version127 <- as(c("1"), "character")
  Category127 <- Category$new(agency=param_agency127, displayLabel=param_displayLabel127, id=param_id127, isPersistent=param_isPersistent127, isUniversallyUnique=param_isUniversallyUnique127, version=param_version127)

#  ^^^^^ ------ Ending elment:   Category ------ ^^^^^




#  vvvvv ------ Beginning elment:   SubstantiveValueDomain ------ vvvvv

    param_content1 <- as(c("integer"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/xmlschema-2/"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_recommendedDataType128 <- list(ExternalControlledVocabularyEntry1)
  param_agency128 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_describedValueDomain128 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdValsSwingn:1"), "character")
  param_id128 <- as(c("IdSubSwingn"), "character")
  param_isPersistent128 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique128 <- as(c("FALSE"), "logical")
  param_version128 <- as(c("1"), "character")
  SubstantiveValueDomain128 <- SubstantiveValueDomain$new(agency=param_agency128, describedValueDomain=param_describedValueDomain128, id=param_id128, isPersistent=param_isPersistent128, isUniversallyUnique=param_isUniversallyUnique128, recommendedDataType=param_recommendedDataType128, version=param_version128)

#  ^^^^^ ------ Ending elment:   SubstantiveValueDomain ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueAndConceptDescription ------ vvvvv

      param_content1 <- as(c("real numbers between -14.3000001907349 and 49.9700012207031 "), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_description129 <- list(InternationalStructuredString1)
    param_content1 <- as(c("(-14.3000001907349 <= x <= 49.9700012207031)"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_logicalExpression129 <- list(ExternalControlledVocabularyEntry1)
  param_agency129 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id129 <- as(c("IdValsSwingn"), "character")
  param_isPersistent129 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique129 <- as(c("FALSE"), "logical")
  param_maximumValueInclusive129 <- as(c("-14.3000001907349"), "character")
  param_minimumValueInclusive129 <- as(c("49.9700012207031"), "character")
  param_version129 <- as(c("1"), "character")
  ValueAndConceptDescription129 <- ValueAndConceptDescription$new(agency=param_agency129, description=param_description129, id=param_id129, isPersistent=param_isPersistent129, isUniversallyUnique=param_isUniversallyUnique129, logicalExpression=param_logicalExpression129, maximumValueInclusive=param_maximumValueInclusive129, minimumValueInclusive=param_minimumValueInclusive129, version=param_version129)

#  ^^^^^ ------ Ending elment:   ValueAndConceptDescription ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   RepresentedQuestion ------ vvvvv

    DynamicText1 <- DynamicText$new()

  param_questionText131 <- list(DynamicText1)
  param_agency131 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id131 <- as(c("IdQuestA4"), "character")
  param_isPersistent131 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique131 <- as(c("FALSE"), "logical")
  param_version131 <- as(c("1"), "character")
  RepresentedQuestion131 <- RepresentedQuestion$new(agency=param_agency131, id=param_id131, isPersistent=param_isPersistent131, isUniversallyUnique=param_isUniversallyUnique131, questionText=param_questionText131, version=param_version131)

#  ^^^^^ ------ Ending elment:   RepresentedQuestion ------ ^^^^^




#  vvvvv ------ Beginning elment:   RepresentedQuestion ------ vvvvv

    DynamicText1 <- DynamicText$new()

  param_questionText132 <- list(DynamicText1)
  param_agency132 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id132 <- as(c("IdQuestAge"), "character")
  param_isPersistent132 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique132 <- as(c("FALSE"), "logical")
  param_version132 <- as(c("1"), "character")
  RepresentedQuestion132 <- RepresentedQuestion$new(agency=param_agency132, id=param_id132, isPersistent=param_isPersistent132, isUniversallyUnique=param_isUniversallyUnique132, questionText=param_questionText132, version=param_version132)

#  ^^^^^ ------ Ending elment:   RepresentedQuestion ------ ^^^^^




#  vvvvv ------ Beginning elment:   RepresentedQuestion ------ vvvvv

    DynamicText1 <- DynamicText$new()

  param_questionText133 <- list(DynamicText1)
  param_agency133 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id133 <- as(c("IdQuestXg5"), "character")
  param_isPersistent133 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique133 <- as(c("FALSE"), "logical")
  param_version133 <- as(c("1"), "character")
  RepresentedQuestion133 <- RepresentedQuestion$new(agency=param_agency133, id=param_id133, isPersistent=param_isPersistent133, isUniversallyUnique=param_isUniversallyUnique133, questionText=param_questionText133, version=param_version133)

#  ^^^^^ ------ Ending elment:   RepresentedQuestion ------ ^^^^^




#  vvvvv ------ Beginning elment:   RepresentedMeasurement ------ vvvvv

    param_uri1 <- as(c("http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(uri=param_uri1)

  param_captureSource134 <- list(ExternalControlledVocabularyEntry1)
  param_agency134 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id134 <- as(c("IdMeasPartyAby"), "character")
  param_isPersistent134 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique134 <- as(c("FALSE"), "logical")
  param_relatedMaterial134 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdExternalPartyAby:1"), "character")
  param_version134 <- as(c("1"), "character")
  RepresentedMeasurement134 <- RepresentedMeasurement$new(agency=param_agency134, captureSource=param_captureSource134, id=param_id134, isPersistent=param_isPersistent134, isUniversallyUnique=param_isUniversallyUnique134, relatedMaterial=param_relatedMaterial134, version=param_version134)

#  ^^^^^ ------ Ending elment:   RepresentedMeasurement ------ ^^^^^




#  vvvvv ------ Beginning elment:   ExternalMaterial ------ vvvvv

      param_content1 <- as(c("AEC election result information for respondent's electoral division - sourced from
                http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm
                See Note in Related Materials for further information on derivation of variables."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_descriptiveText135 <- list(InternationalStructuredString1)
  param_agency135 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id135 <- as(c("IdExternalPartyAby"), "character")
  param_isPersistent135 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique135 <- as(c("FALSE"), "logical")
  param_uri135 <- as(c("http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm"), "character")
  param_version135 <- as(c("1"), "character")
  ExternalMaterial135 <- ExternalMaterial$new(agency=param_agency135, descriptiveText=param_descriptiveText135, id=param_id135, isPersistent=param_isPersistent135, isUniversallyUnique=param_isUniversallyUnique135, uri=param_uri135, version=param_version135)

#  ^^^^^ ------ Ending elment:   ExternalMaterial ------ ^^^^^




#  vvvvv ------ Beginning elment:   RepresentedMeasurement ------ vvvvv

    param_uri1 <- as(c("http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(uri=param_uri1)

  param_captureSource136 <- list(ExternalControlledVocabularyEntry1)
  param_agency136 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id136 <- as(c("IDMeasSwingn"), "character")
  param_isPersistent136 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique136 <- as(c("FALSE"), "logical")
  param_relatedMaterial136 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdExternalSwingn:1"), "character")
  param_version136 <- as(c("1"), "character")
  RepresentedMeasurement136 <- RepresentedMeasurement$new(agency=param_agency136, captureSource=param_captureSource136, id=param_id136, isPersistent=param_isPersistent136, isUniversallyUnique=param_isUniversallyUnique136, relatedMaterial=param_relatedMaterial136, version=param_version136)

#  ^^^^^ ------ Ending elment:   RepresentedMeasurement ------ ^^^^^




#  vvvvv ------ Beginning elment:   ExternalMaterial ------ vvvvv

      param_content1 <- as(c("AEC election result information for respondent's electoral division - sourced from
                http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm
                See Note in Related Materials for further information on derivation of variables."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_descriptiveText137 <- list(InternationalStructuredString1)
  param_agency137 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id137 <- as(c("IdExternalSwingn"), "character")
  param_isPersistent137 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique137 <- as(c("FALSE"), "logical")
  param_uri137 <- as(c("http://results.aec.gov.au/17496/Website/HouseDownloadsMenu-17496-csv.htm"), "character")
  param_version137 <- as(c("1"), "character")
  ExternalMaterial137 <- ExternalMaterial$new(agency=param_agency137, descriptiveText=param_descriptiveText137, id=param_id137, isPersistent=param_isPersistent137, isUniversallyUnique=param_isUniversallyUnique137, uri=param_uri137, version=param_version137)

#  ^^^^^ ------ Ending elment:   ExternalMaterial ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   UnitType ------ vvvvv

      param_content1 <- as(c("Individual"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_definition139 <- list(InternationalStructuredString1)
  param_agency139 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id139 <- as(c("IdUnitType"), "character")
  param_isPersistent139 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique139 <- as(c("FALSE"), "logical")
  param_version139 <- as(c("1"), "character")
  UnitType139 <- UnitType$new(agency=param_agency139, definition=param_definition139, id=param_id139, isPersistent=param_isPersistent139, isUniversallyUnique=param_isUniversallyUnique139, version=param_version139)

#  ^^^^^ ------ Ending elment:   UnitType ------ ^^^^^




#  vvvvv ------ Beginning elment:   Universe ------ vvvvv

      param_content1 <- as(c("Australian adults aged 18 years or over, enrolled and eligible to vote in Australian elections"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_definition140 <- list(InternationalStructuredString1)
  param_agency140 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id140 <- as(c("IdUniverse"), "character")
  param_isPersistent140 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique140 <- as(c("FALSE"), "logical")
  param_version140 <- as(c("1"), "character")
  Universe140 <- Universe$new(agency=param_agency140, definition=param_definition140, id=param_id140, isPersistent=param_isPersistent140, isUniversallyUnique=param_isUniversallyUnique140, version=param_version140)

#  ^^^^^ ------ Ending elment:   Universe ------ ^^^^^




#  vvvvv ------ Beginning elment:   Population ------ vvvvv

      param_content1 <- as(c("Australian adults aged 18 years or over, enrolled and eligible to vote in Australian elections in 2013/2014"), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_definition141 <- list(InternationalStructuredString1)
  param_agency141 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id141 <- as(c("IdPopulation"), "character")
  param_isPersistent141 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique141 <- as(c("FALSE"), "logical")
  param_version141 <- as(c("1"), "character")
  Population141 <- Population$new(agency=param_agency141, definition=param_definition141, id=param_id141, isPersistent=param_isPersistent141, isUniversallyUnique=param_isUniversallyUnique141, version=param_version141)

#  ^^^^^ ------ Ending elment:   Population ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   DataStore ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("IdLogRec"), "character")
    LogicalRecordIndicator1 <- LogicalRecordIndicator$new(index=param_index1, member=param_member1)

  param_contains143 <- list(LogicalRecordIndicator1)
    param_content1 <- as(c("Rectangular Unit Records"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_dataStoreType143 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("DDI4 example, a subset of the Australian Election Study 2013"), "character")
    ObjectName1 <- ObjectName$new(content=param_content1)

  param_name143 <- list(ObjectName1)
  param_agency143 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id143 <- as(c("IdDataStore"), "character")
  param_isPersistent143 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique143 <- as(c("FALSE"), "logical")
  param_recordCount143 <- as(c("3955"), "integer")
  param_version143 <- as(c("1"), "character")
  DDI4.example..a.subset.of.the.Australian.Election.Study.2013 <- DataStore$new(agency=param_agency143, contains=param_contains143, dataStoreType=param_dataStoreType143, id=param_id143, isPersistent=param_isPersistent143, isUniversallyUnique=param_isUniversallyUnique143, name=param_name143, recordCount=param_recordCount143, version=param_version143)

#  ^^^^^ ------ Ending elment:   DataStore ------ ^^^^^




#  vvvvv ------ Beginning elment:   UnitDataRecord ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivisNum:1"), "character")
    InstanceVariableIndicator1 <- InstanceVariableIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarUniqueID:1"), "character")
    InstanceVariableIndicator2 <- InstanceVariableIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarMode:1"), "character")
    InstanceVariableIndicator3 <- InstanceVariableIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDateComp:1"), "character")
    InstanceVariableIndicator4 <- InstanceVariableIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("5"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarState:1"), "character")
    InstanceVariableIndicator5 <- InstanceVariableIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("6"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1"), "character")
    InstanceVariableIndicator6 <- InstanceVariableIndicator$new(index=param_index6, member=param_member6)

    param_index7 <- as(c("7"), "integer")
    param_member7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarA4:1"), "character")
    InstanceVariableIndicator7 <- InstanceVariableIndicator$new(index=param_index7, member=param_member7)

    param_index8 <- as(c("8"), "integer")
    param_member8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarG1Age:1"), "character")
    InstanceVariableIndicator8 <- InstanceVariableIndicator$new(index=param_index8, member=param_member8)

    param_index9 <- as(c("9"), "integer")
    param_member9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarXg5:1"), "character")
    InstanceVariableIndicator9 <- InstanceVariableIndicator$new(index=param_index9, member=param_member9)

    param_index10 <- as(c("10"), "integer")
    param_member10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
    InstanceVariableIndicator10 <- InstanceVariableIndicator$new(index=param_index10, member=param_member10)

    param_index11 <- as(c("11"), "integer")
    param_member11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarPartyAby:1"), "character")
    InstanceVariableIndicator11 <- InstanceVariableIndicator$new(index=param_index11, member=param_member11)

    param_index12 <- as(c("12"), "integer")
    param_member12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarSwingn:1"), "character")
    InstanceVariableIndicator12 <- InstanceVariableIndicator$new(index=param_index12, member=param_member12)

  param_contains144 <- list(InstanceVariableIndicator1, InstanceVariableIndicator2, InstanceVariableIndicator3, InstanceVariableIndicator4, InstanceVariableIndicator5, InstanceVariableIndicator6, InstanceVariableIndicator7, InstanceVariableIndicator8, InstanceVariableIndicator9, InstanceVariableIndicator10, InstanceVariableIndicator11, InstanceVariableIndicator12)
  param_agency144 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id144 <- as(c("IdLogRec"), "character")
  param_isPersistent144 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique144 <- as(c("FALSE"), "logical")
  param_version144 <- as(c("1"), "character")
  UnitDataRecord144 <- UnitDataRecord$new(agency=param_agency144, contains=param_contains144, id=param_id144, isPersistent=param_isPersistent144, isUniversallyUnique=param_isUniversallyUnique144, version=param_version144)

#  ^^^^^ ------ Ending elment:   UnitDataRecord ------ ^^^^^




#  vvvvv ------ Beginning elment:   PhysicalDataSet ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdPhysrecSeg:1"), "character")
    PhysicalRecordSegmentIndicator1 <- PhysicalRecordSegmentIndicator$new(index=param_index1, member=param_member1)

  param_contains145 <- list(PhysicalRecordSegmentIndicator1)
  param_agency145 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_formatsDataStore145 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdDataStore:1"), "character")
  param_id145 <- as(c("IdPhysicalDataProduct"), "character")
  param_isPersistent145 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique145 <- as(c("FALSE"), "logical")
  param_physicalFileName145 <- as(c("aes_2013_01259Subset.csv"), "character")
  param_relatedMaterial145 <- as(c("https://ddi-alliance.atlassian.net/wiki/download/attachments/112427055/aes_2013_01259Subset.csv?api=v2", "https://ddi-alliance.atlassian.net/wiki/download/attachments/112427055/ADA.CODEBOOK.01259Highlighted2017_11_08.pdf?api=v2"), "character")
  param_version145 <- as(c("1"), "character")
  PhysicalDataSet145 <- PhysicalDataSet$new(agency=param_agency145, contains=param_contains145, formatsDataStore=param_formatsDataStore145, id=param_id145, isPersistent=param_isPersistent145, isUniversallyUnique=param_isUniversallyUnique145, physicalFileName=param_physicalFileName145, relatedMaterial=param_relatedMaterial145, version=param_version145)

#  ^^^^^ ------ Ending elment:   PhysicalDataSet ------ ^^^^^




#  vvvvv ------ Beginning elment:   PhysicalRecordSegment ------ vvvvv

  param_agency146 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_hasPhysicalLayout146 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdRecLay:1"), "character")
  param_id146 <- as(c("IdPhysrecSeg"), "character")
  param_isPersistent146 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique146 <- as(c("FALSE"), "logical")
  param_version146 <- as(c("1"), "character")
  PhysicalRecordSegment146 <- PhysicalRecordSegment$new(agency=param_agency146, hasPhysicalLayout=param_hasPhysicalLayout146, id=param_id146, isPersistent=param_isPersistent146, isUniversallyUnique=param_isUniversallyUnique146, version=param_version146)

#  ^^^^^ ------ Ending elment:   PhysicalRecordSegment ------ ^^^^^




#  vvvvv ------ Beginning elment:   UnitSegmentLayout ------ vvvvv

    param_index1 <- as(c("1"), "integer")
    param_member1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarDivisNum:1"), "character")
    ValueMappingIndicator1 <- ValueMappingIndicator$new(index=param_index1, member=param_member1)

    param_index2 <- as(c("2"), "integer")
    param_member2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarUniqueID:1"), "character")
    ValueMappingIndicator2 <- ValueMappingIndicator$new(index=param_index2, member=param_member2)

    param_index3 <- as(c("3"), "integer")
    param_member3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarMode:1"), "character")
    ValueMappingIndicator3 <- ValueMappingIndicator$new(index=param_index3, member=param_member3)

    param_index4 <- as(c("4"), "integer")
    param_member4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarDateComp:1"), "character")
    ValueMappingIndicator4 <- ValueMappingIndicator$new(index=param_index4, member=param_member4)

    param_index5 <- as(c("5"), "integer")
    param_member5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarState:1"), "character")
    ValueMappingIndicator5 <- ValueMappingIndicator$new(index=param_index5, member=param_member5)

    param_index6 <- as(c("6"), "integer")
    param_member6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarDivision:1"), "character")
    ValueMappingIndicator6 <- ValueMappingIndicator$new(index=param_index6, member=param_member6)

    param_index7 <- as(c("7"), "integer")
    param_member7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarA4:1"), "character")
    ValueMappingIndicator7 <- ValueMappingIndicator$new(index=param_index7, member=param_member7)

    param_index8 <- as(c("8"), "integer")
    param_member8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarG1Age:1"), "character")
    ValueMappingIndicator8 <- ValueMappingIndicator$new(index=param_index8, member=param_member8)

    param_index9 <- as(c("9"), "integer")
    param_member9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarXg5:1"), "character")
    ValueMappingIndicator9 <- ValueMappingIndicator$new(index=param_index9, member=param_member9)

    param_index10 <- as(c("10"), "integer")
    param_member10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarWeight:1"), "character")
    ValueMappingIndicator10 <- ValueMappingIndicator$new(index=param_index10, member=param_member10)

    param_index11 <- as(c("11"), "integer")
    param_member11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarPartyAby:1"), "character")
    ValueMappingIndicator11 <- ValueMappingIndicator$new(index=param_index11, member=param_member11)

    param_index12 <- as(c("12"), "integer")
    param_member12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDVmVarSwingn:1"), "character")
    ValueMappingIndicator12 <- ValueMappingIndicator$new(index=param_index12, member=param_member12)

  param_contains147 <- list(ValueMappingIndicator1, ValueMappingIndicator2, ValueMappingIndicator3, ValueMappingIndicator4, ValueMappingIndicator5, ValueMappingIndicator6, ValueMappingIndicator7, ValueMappingIndicator8, ValueMappingIndicator9, ValueMappingIndicator10, ValueMappingIndicator11, ValueMappingIndicator12)
      param_content1 <- as(c("A simple comma delimited layout with one header line containing variable names. No text values contain commas so quotes are not used."), "character")
      LanguageSpecificStructuredStringType1 <- LanguageSpecificStructuredStringType$new(content=param_content1)

    param_languageSpecificStructuredString1 <- list(LanguageSpecificStructuredStringType1)
    InternationalStructuredString1 <- InternationalStructuredString$new(languageSpecificStructuredString=param_languageSpecificStructuredString1)

  param_overview147 <- list(InternationalStructuredString1)
  param_agency147 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_delimiter147 <- as(c(","), "character")
  param_hasHeader147 <- as(c("TRUE"), "logical")
  param_headerRowCount147 <- as(c("1"), "integer")
  param_id147 <- as(c("IdRecLay"), "character")
  param_isDelimited147 <- as(c("TRUE"), "logical")
  param_isOrdered147 <- as(c("TRUE"), "logical")
  param_isPersistent147 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique147 <- as(c("FALSE"), "logical")
  param_treatConsecutiveDelimitersAsOne147 <- as(c("FALSE"), "logical")
  param_type147 <- as(c("Set"), "character")
  param_version147 <- as(c("1"), "character")
  UnitSegmentLayout147 <- UnitSegmentLayout$new(agency=param_agency147, contains=param_contains147, delimiter=param_delimiter147, hasHeader=param_hasHeader147, headerRowCount=param_headerRowCount147, id=param_id147, isDelimited=param_isDelimited147, isOrdered=param_isOrdered147, isPersistent=param_isPersistent147, isUniversallyUnique=param_isUniversallyUnique147, overview=param_overview147, treatConsecutiveDelimitersAsOne=param_treatConsecutiveDelimitersAsOne147, type=param_type147, version=param_version147)

#  ^^^^^ ------ Ending elment:   UnitSegmentLayout ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("3.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format149 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType149 <- list(ExternalControlledVocabularyEntry1)
  param_agency149 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions149 <- as(c("0"), "integer")
  param_defaultDecimalSeparator149 <- as(c("."), "character")
  param_formatsDataPoint149 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptVarDivisNum:1"), "character")
  param_id149 <- as(c("IDVmVarDivisNum"), "character")
  param_isPersistent149 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique149 <- as(c("FALSE"), "logical")
  param_version149 <- as(c("1"), "character")
  ValueMapping149 <- ValueMapping$new(agency=param_agency149, decimalPositions=param_decimalPositions149, defaultDecimalSeparator=param_defaultDecimalSeparator149, format=param_format149, formatsDataPoint=param_formatsDataPoint149, id=param_id149, isPersistent=param_isPersistent149, isUniversallyUnique=param_isUniversallyUnique149, physicalDataType=param_physicalDataType149, version=param_version149)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency150 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id150 <- as(c("IDDptVarDivisNum"), "character")
  param_isDescribedBy150 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1"), "character")
  param_isPersistent150 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique150 <- as(c("FALSE"), "logical")
  param_version150 <- as(c("1"), "character")
  DataPoint150 <- DataPoint$new(agency=param_agency150, id=param_id150, isDescribedBy=param_isDescribedBy150, isPersistent=param_isPersistent150, isUniversallyUnique=param_isUniversallyUnique150, version=param_version150)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("8.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format151 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType151 <- list(ExternalControlledVocabularyEntry1)
  param_agency151 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions151 <- as(c("0"), "integer")
  param_defaultDecimalSeparator151 <- as(c("."), "character")
  param_formatsDataPoint151 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdDptVarUniqueId:1"), "character")
  param_id151 <- as(c("IDVmVarUniqueID"), "character")
  param_isPersistent151 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique151 <- as(c("FALSE"), "logical")
  param_version151 <- as(c("1"), "character")
  ValueMapping151 <- ValueMapping$new(agency=param_agency151, decimalPositions=param_decimalPositions151, defaultDecimalSeparator=param_defaultDecimalSeparator151, format=param_format151, formatsDataPoint=param_formatsDataPoint151, id=param_id151, isPersistent=param_isPersistent151, isUniversallyUnique=param_isUniversallyUnique151, physicalDataType=param_physicalDataType151, version=param_version151)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency152 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id152 <- as(c("IdDptVarUniqueId"), "character")
  param_isDescribedBy152 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarUniqueId:1"), "character")
  param_isPersistent152 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique152 <- as(c("FALSE"), "logical")
  param_version152 <- as(c("1"), "character")
  DataPoint152 <- DataPoint$new(agency=param_agency152, id=param_id152, isDescribedBy=param_isDescribedBy152, isPersistent=param_isPersistent152, isUniversallyUnique=param_isUniversallyUnique152, version=param_version152)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("1.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format153 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType153 <- list(ExternalControlledVocabularyEntry1)
  param_agency153 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions153 <- as(c("0"), "integer")
  param_defaultDecimalSeparator153 <- as(c("."), "character")
  param_formatsDataPoint153 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptMode:1"), "character")
  param_id153 <- as(c("IDVmVarMode"), "character")
  param_isPersistent153 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique153 <- as(c("FALSE"), "logical")
  param_version153 <- as(c("1"), "character")
  ValueMapping153 <- ValueMapping$new(agency=param_agency153, decimalPositions=param_decimalPositions153, defaultDecimalSeparator=param_defaultDecimalSeparator153, format=param_format153, formatsDataPoint=param_formatsDataPoint153, id=param_id153, isPersistent=param_isPersistent153, isUniversallyUnique=param_isUniversallyUnique153, physicalDataType=param_physicalDataType153, version=param_version153)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency154 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id154 <- as(c("IDDptMode"), "character")
  param_isDescribedBy154 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarMode:1"), "character")
  param_isPersistent154 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique154 <- as(c("FALSE"), "logical")
  param_version154 <- as(c("1"), "character")
  DataPoint154 <- DataPoint$new(agency=param_agency154, id=param_id154, isDescribedBy=param_isDescribedBy154, isPersistent=param_isPersistent154, isUniversallyUnique=param_isUniversallyUnique154, version=param_version154)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("MM-dd-yyyy"), "character")
    param_uri1 <- as(c("https://www.w3.org/TR/2015/REC-tabular-data-model-20151217/#formats-for-dates-and-times"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, uri=param_uri1)

  param_format155 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("date string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType155 <- list(ExternalControlledVocabularyEntry1)
  param_agency155 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_formatsDataPoint155 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptDateComp:1"), "character")
  param_id155 <- as(c("IDVmVarDateComp"), "character")
  param_isPersistent155 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique155 <- as(c("FALSE"), "logical")
  param_version155 <- as(c("1"), "character")
  ValueMapping155 <- ValueMapping$new(agency=param_agency155, format=param_format155, formatsDataPoint=param_formatsDataPoint155, id=param_id155, isPersistent=param_isPersistent155, isUniversallyUnique=param_isUniversallyUnique155, physicalDataType=param_physicalDataType155, version=param_version155)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency156 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id156 <- as(c("IDDptDateComp"), "character")
  param_isDescribedBy156 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDateComp:1"), "character")
  param_isPersistent156 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique156 <- as(c("FALSE"), "logical")
  param_version156 <- as(c("1"), "character")
  DataPoint156 <- DataPoint$new(agency=param_agency156, id=param_id156, isDescribedBy=param_isDescribedBy156, isPersistent=param_isPersistent156, isUniversallyUnique=param_isUniversallyUnique156, version=param_version156)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("1.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format157 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType157 <- list(ExternalControlledVocabularyEntry1)
  param_agency157 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions157 <- as(c("0"), "integer")
  param_defaultDecimalSeparator157 <- as(c("."), "character")
  param_formatsDataPoint157 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptState:1"), "character")
  param_id157 <- as(c("IDVmVarState"), "character")
  param_isPersistent157 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique157 <- as(c("FALSE"), "logical")
  param_version157 <- as(c("1"), "character")
  ValueMapping157 <- ValueMapping$new(agency=param_agency157, decimalPositions=param_decimalPositions157, defaultDecimalSeparator=param_defaultDecimalSeparator157, format=param_format157, formatsDataPoint=param_formatsDataPoint157, id=param_id157, isPersistent=param_isPersistent157, isUniversallyUnique=param_isUniversallyUnique157, physicalDataType=param_physicalDataType157, version=param_version157)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency158 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id158 <- as(c("IDDptState"), "character")
  param_isDescribedBy158 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarState:1"), "character")
  param_isPersistent158 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique158 <- as(c("FALSE"), "logical")
  param_version158 <- as(c("1"), "character")
  DataPoint158 <- DataPoint$new(agency=param_agency158, id=param_id158, isDescribedBy=param_isDescribedBy158, isPersistent=param_isPersistent158, isUniversallyUnique=param_isUniversallyUnique158, version=param_version158)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("xs:string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType159 <- list(ExternalControlledVocabularyEntry1)
  param_agency159 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_formatsDataPoint159 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptDivision:1"), "character")
  param_id159 <- as(c("IDVmVarDivision"), "character")
  param_isPersistent159 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique159 <- as(c("FALSE"), "logical")
  param_maximumLength159 <- as(c("255"), "integer")
  param_version159 <- as(c("1"), "character")
  ValueMapping159 <- ValueMapping$new(agency=param_agency159, formatsDataPoint=param_formatsDataPoint159, id=param_id159, isPersistent=param_isPersistent159, isUniversallyUnique=param_isUniversallyUnique159, maximumLength=param_maximumLength159, physicalDataType=param_physicalDataType159, version=param_version159)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency160 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id160 <- as(c("IDDptDivision"), "character")
  param_isDescribedBy160 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1"), "character")
  param_isPersistent160 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique160 <- as(c("FALSE"), "logical")
  param_version160 <- as(c("1"), "character")
  DataPoint160 <- DataPoint$new(agency=param_agency160, id=param_id160, isDescribedBy=param_isDescribedBy160, isPersistent=param_isPersistent160, isUniversallyUnique=param_isUniversallyUnique160, version=param_version160)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("2.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format161 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType161 <- list(ExternalControlledVocabularyEntry1)
  param_agency161 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions161 <- as(c("0"), "integer")
  param_defaultDecimalSeparator161 <- as(c("."), "character")
  param_formatsDataPoint161 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptA4:1"), "character")
  param_id161 <- as(c("IDVmVarA4"), "character")
  param_isPersistent161 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique161 <- as(c("FALSE"), "logical")
  param_version161 <- as(c("1"), "character")
  ValueMapping161 <- ValueMapping$new(agency=param_agency161, decimalPositions=param_decimalPositions161, defaultDecimalSeparator=param_defaultDecimalSeparator161, format=param_format161, formatsDataPoint=param_formatsDataPoint161, id=param_id161, isPersistent=param_isPersistent161, isUniversallyUnique=param_isUniversallyUnique161, physicalDataType=param_physicalDataType161, version=param_version161)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency162 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id162 <- as(c("IDDptA4"), "character")
  param_isDescribedBy162 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarA4:1"), "character")
  param_isPersistent162 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique162 <- as(c("FALSE"), "logical")
  param_version162 <- as(c("1"), "character")
  DataPoint162 <- DataPoint$new(agency=param_agency162, id=param_id162, isDescribedBy=param_isDescribedBy162, isPersistent=param_isPersistent162, isUniversallyUnique=param_isUniversallyUnique162, version=param_version162)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("2.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format163 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType163 <- list(ExternalControlledVocabularyEntry1)
  param_agency163 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions163 <- as(c("0"), "integer")
  param_defaultDecimalSeparator163 <- as(c("."), "character")
  param_formatsDataPoint163 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptG1Age:1"), "character")
  param_id163 <- as(c("IDVmVarG1Age"), "character")
  param_isPersistent163 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique163 <- as(c("FALSE"), "logical")
  param_version163 <- as(c("1"), "character")
  ValueMapping163 <- ValueMapping$new(agency=param_agency163, decimalPositions=param_decimalPositions163, defaultDecimalSeparator=param_defaultDecimalSeparator163, format=param_format163, formatsDataPoint=param_formatsDataPoint163, id=param_id163, isPersistent=param_isPersistent163, isUniversallyUnique=param_isUniversallyUnique163, physicalDataType=param_physicalDataType163, version=param_version163)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency164 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id164 <- as(c("IDDptG1Age"), "character")
  param_isDescribedBy164 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarG1Age:1"), "character")
  param_isPersistent164 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique164 <- as(c("FALSE"), "logical")
  param_version164 <- as(c("1"), "character")
  DataPoint164 <- DataPoint$new(agency=param_agency164, id=param_id164, isDescribedBy=param_isDescribedBy164, isPersistent=param_isPersistent164, isUniversallyUnique=param_isUniversallyUnique164, version=param_version164)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("4.0"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format165 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType165 <- list(ExternalControlledVocabularyEntry1)
  param_agency165 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions165 <- as(c("0"), "integer")
  param_defaultDecimalSeparator165 <- as(c("."), "character")
  param_formatsDataPoint165 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptXg5:1"), "character")
  param_id165 <- as(c("IDVmVarXg5"), "character")
  param_isPersistent165 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique165 <- as(c("FALSE"), "logical")
  param_version165 <- as(c("1"), "character")
  ValueMapping165 <- ValueMapping$new(agency=param_agency165, decimalPositions=param_decimalPositions165, defaultDecimalSeparator=param_defaultDecimalSeparator165, format=param_format165, formatsDataPoint=param_formatsDataPoint165, id=param_id165, isPersistent=param_isPersistent165, isUniversallyUnique=param_isUniversallyUnique165, physicalDataType=param_physicalDataType165, version=param_version165)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency166 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id166 <- as(c("IDDptXg5"), "character")
  param_isDescribedBy166 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarXg5:1"), "character")
  param_isPersistent166 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique166 <- as(c("FALSE"), "logical")
  param_version166 <- as(c("1"), "character")
  DataPoint166 <- DataPoint$new(agency=param_agency166, id=param_id166, isDescribedBy=param_isDescribedBy166, isPersistent=param_isPersistent166, isUniversallyUnique=param_isUniversallyUnique166, version=param_version166)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("17.15"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format167 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType167 <- list(ExternalControlledVocabularyEntry1)
  param_agency167 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions167 <- as(c("15"), "integer")
  param_defaultDecimalSeparator167 <- as(c("."), "character")
  param_formatsDataPoint167 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptWeight:1"), "character")
  param_id167 <- as(c("IDVmVarWeight"), "character")
  param_isPersistent167 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique167 <- as(c("FALSE"), "logical")
  param_version167 <- as(c("1"), "character")
  ValueMapping167 <- ValueMapping$new(agency=param_agency167, decimalPositions=param_decimalPositions167, defaultDecimalSeparator=param_defaultDecimalSeparator167, format=param_format167, formatsDataPoint=param_formatsDataPoint167, id=param_id167, isPersistent=param_isPersistent167, isUniversallyUnique=param_isUniversallyUnique167, physicalDataType=param_physicalDataType167, version=param_version167)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency168 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id168 <- as(c("IDDptWeight"), "character")
  param_isDescribedBy168 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
  param_isPersistent168 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique168 <- as(c("FALSE"), "logical")
  param_version168 <- as(c("1"), "character")
  DataPoint168 <- DataPoint$new(agency=param_agency168, id=param_id168, isDescribedBy=param_isDescribedBy168, isPersistent=param_isPersistent168, isUniversallyUnique=param_isUniversallyUnique168, version=param_version168)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("xs:string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType169 <- list(ExternalControlledVocabularyEntry1)
  param_agency169 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_formatsDataPoint169 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptPartyAby:1"), "character")
  param_id169 <- as(c("IDVmVarPartyAby"), "character")
  param_isPersistent169 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique169 <- as(c("FALSE"), "logical")
  param_maximumLength169 <- as(c("3"), "integer")
  param_version169 <- as(c("1"), "character")
  ValueMapping169 <- ValueMapping$new(agency=param_agency169, formatsDataPoint=param_formatsDataPoint169, id=param_id169, isPersistent=param_isPersistent169, isUniversallyUnique=param_isUniversallyUnique169, maximumLength=param_maximumLength169, physicalDataType=param_physicalDataType169, version=param_version169)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency170 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id170 <- as(c("IDDptPartyAby"), "character")
  param_isDescribedBy170 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarPartyAby:1"), "character")
  param_isPersistent170 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique170 <- as(c("FALSE"), "logical")
  param_version170 <- as(c("1"), "character")
  DataPoint170 <- DataPoint$new(agency=param_agency170, id=param_id170, isDescribedBy=param_isDescribedBy170, isPersistent=param_isPersistent170, isUniversallyUnique=param_isUniversallyUnique170, version=param_version170)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   ValueMapping ------ vvvvv

    param_content1 <- as(c("17.13"), "character")
    param_controlledVocabularyName1 <- as(c("SPSS Numeric"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1, controlledVocabularyName=param_controlledVocabularyName1)

  param_format171 <- list(ExternalControlledVocabularyEntry1)
    param_content1 <- as(c("numeric string"), "character")
    ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

  param_physicalDataType171 <- list(ExternalControlledVocabularyEntry1)
  param_agency171 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_decimalPositions171 <- as(c("13"), "integer")
  param_defaultDecimalSeparator171 <- as(c("."), "character")
  param_formatsDataPoint171 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDDptSwingn:1"), "character")
  param_id171 <- as(c("IDVmVarSwingn"), "character")
  param_isPersistent171 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique171 <- as(c("FALSE"), "logical")
  param_version171 <- as(c("1"), "character")
  ValueMapping171 <- ValueMapping$new(agency=param_agency171, decimalPositions=param_decimalPositions171, defaultDecimalSeparator=param_defaultDecimalSeparator171, format=param_format171, formatsDataPoint=param_formatsDataPoint171, id=param_id171, isPersistent=param_isPersistent171, isUniversallyUnique=param_isUniversallyUnique171, physicalDataType=param_physicalDataType171, version=param_version171)

#  ^^^^^ ------ Ending elment:   ValueMapping ------ ^^^^^




#  vvvvv ------ Beginning elment:   DataPoint ------ vvvvv

  param_agency172 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_id172 <- as(c("IDDptSwingn"), "character")
  param_isDescribedBy172 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarSwingn:1"), "character")
  param_isPersistent172 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique172 <- as(c("FALSE"), "logical")
  param_version172 <- as(c("1"), "character")
  DataPoint172 <- DataPoint$new(agency=param_agency172, id=param_id172, isDescribedBy=param_isDescribedBy172, isPersistent=param_isPersistent172, isUniversallyUnique=param_isUniversallyUnique172, version=param_version172)

#  ^^^^^ ------ Ending elment:   DataPoint ------ ^^^^^




#  vvvvv ------ Beginning elment:   comment ------ vvvvv



#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("Canberra"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("28"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode101EDC:1"), "character")
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, forCodeItem=param_forCodeItem1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("Fraser"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("30"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode102EDC:1"), "character")
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, forCodeItem=param_forCodeItem2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("Banks"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("25"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode103EDC:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("Barton"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("23"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode104EDC:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

      param_content1 <- as(c("Bennelong"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue5 <- list(ValueString1)
      param_content1 <- as(c("29"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode105EDC:1"), "character")
    CategoryStatistic5 <- CategoryStatistic$new(categoryValue=param_categoryValue5, forCodeItem=param_forCodeItem5, hasStatistic=param_hasStatistic5, typeOfCategoryStatistic=param_typeOfCategoryStatistic5)

      param_content1 <- as(c("Berowra"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue6 <- list(ValueString1)
      param_content1 <- as(c("35"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode106EDC:1"), "character")
    CategoryStatistic6 <- CategoryStatistic$new(categoryValue=param_categoryValue6, forCodeItem=param_forCodeItem6, hasStatistic=param_hasStatistic6, typeOfCategoryStatistic=param_typeOfCategoryStatistic6)

      param_content1 <- as(c("Canberra"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue7 <- list(ValueString1)
      param_content1 <- as(c("0.7"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode101EDC:1"), "character")
    CategoryStatistic7 <- CategoryStatistic$new(categoryValue=param_categoryValue7, forCodeItem=param_forCodeItem7, hasStatistic=param_hasStatistic7, typeOfCategoryStatistic=param_typeOfCategoryStatistic7)

      param_content1 <- as(c("Fraser"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue8 <- list(ValueString1)
      param_content1 <- as(c("0.8"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode102EDC:1"), "character")
    CategoryStatistic8 <- CategoryStatistic$new(categoryValue=param_categoryValue8, forCodeItem=param_forCodeItem8, hasStatistic=param_hasStatistic8, typeOfCategoryStatistic=param_typeOfCategoryStatistic8)

      param_content1 <- as(c("Banks"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue9 <- list(ValueString1)
      param_content1 <- as(c("0.6"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode103EDC:1"), "character")
    CategoryStatistic9 <- CategoryStatistic$new(categoryValue=param_categoryValue9, forCodeItem=param_forCodeItem9, hasStatistic=param_hasStatistic9, typeOfCategoryStatistic=param_typeOfCategoryStatistic9)

      param_content1 <- as(c("Barton"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue10 <- list(ValueString1)
      param_content1 <- as(c("0.6"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode104EDC:1"), "character")
    CategoryStatistic10 <- CategoryStatistic$new(categoryValue=param_categoryValue10, forCodeItem=param_forCodeItem10, hasStatistic=param_hasStatistic10, typeOfCategoryStatistic=param_typeOfCategoryStatistic10)

      param_content1 <- as(c("Bennelong"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue11 <- list(ValueString1)
      param_content1 <- as(c("0.7"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic11 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic11 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode105EDC:1"), "character")
    CategoryStatistic11 <- CategoryStatistic$new(categoryValue=param_categoryValue11, forCodeItem=param_forCodeItem11, hasStatistic=param_hasStatistic11, typeOfCategoryStatistic=param_typeOfCategoryStatistic11)

      param_content1 <- as(c("Berowra"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue12 <- list(ValueString1)
      param_content1 <- as(c("0.9"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic12 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic12 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode106EDC:1"), "character")
    CategoryStatistic12 <- CategoryStatistic$new(categoryValue=param_categoryValue12, forCodeItem=param_forCodeItem12, hasStatistic=param_hasStatistic12, typeOfCategoryStatistic=param_typeOfCategoryStatistic12)

  param_hasCategoryStatistic174 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4, CategoryStatistic5, CategoryStatistic6, CategoryStatistic7, CategoryStatistic8, CategoryStatistic9, CategoryStatistic10, CategoryStatistic11, CategoryStatistic12)
      param_content1 <- as(c("101"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("316"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

  param_hasSummaryStatistic174 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4)
  param_agency174 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable174 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivisNum:1"), "character")
  param_id174 <- as(c("IdVarStatsDivisNum"), "character")
  param_isPersistent174 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique174 <- as(c("FALSE"), "logical")
  param_version174 <- as(c("1"), "character")
  VariableStatistics174 <- VariableStatistics$new(agency=param_agency174, forInstanceVariable=param_forInstanceVariable174, hasCategoryStatistic=param_hasCategoryStatistic174, hasSummaryStatistic=param_hasSummaryStatistic174, id=param_id174, isPersistent=param_isPersistent174, isUniversallyUnique=param_isUniversallyUnique174, version=param_version174)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("1000794"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("9872391"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("5349839.27"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Mean"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("2546301.564"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("StdDev"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

  param_hasSummaryStatistic175 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6)
  param_agency175 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable175 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarUniqueId:1"), "character")
  param_id175 <- as(c("IdVarStatsUniqueID"), "character")
  param_isPersistent175 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique175 <- as(c("FALSE"), "logical")
  param_version175 <- as(c("1"), "character")
  VariableStatistics175 <- VariableStatistics$new(agency=param_agency175, forInstanceVariable=param_forInstanceVariable175, hasSummaryStatistic=param_hasSummaryStatistic175, id=param_id175, isPersistent=param_isPersistent175, isUniversallyUnique=param_isUniversallyUnique175, version=param_version175)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("3379"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1HardCopy:1"), "character")
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, forCodeItem=param_forCodeItem1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("2"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("576"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2Online:1"), "character")
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, forCodeItem=param_forCodeItem2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("85.4"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1HardCopy:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("2"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("14.6"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2Online:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

  param_hasCategoryStatistic176 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4)
      param_content1 <- as(c("1"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("2"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

  param_hasSummaryStatistic176 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4)
  param_agency176 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable176 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarMode:1"), "character")
  param_id176 <- as(c("IdVarStatsMode"), "character")
  param_isPersistent176 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique176 <- as(c("FALSE"), "logical")
  param_version176 <- as(c("1"), "character")
  VariableStatistics176 <- VariableStatistics$new(agency=param_agency176, forInstanceVariable=param_forInstanceVariable176, hasCategoryStatistic=param_hasCategoryStatistic176, hasSummaryStatistic=param_hasSummaryStatistic176, id=param_id176, isPersistent=param_isPersistent176, isUniversallyUnique=param_isUniversallyUnique176, version=param_version176)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("2013-09-09"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("16"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("2013-09-10"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("58"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("2013-09-09"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("0.4"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("2013-09-10"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("1.5"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

  param_hasCategoryStatistic177 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4)
      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

  param_hasSummaryStatistic177 <- list(SummaryStatistic1, SummaryStatistic2)
  param_agency177 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable177 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDateComp:1"), "character")
  param_id177 <- as(c("IdVarStatsDateComp"), "character")
  param_isPersistent177 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique177 <- as(c("FALSE"), "logical")
  param_version177 <- as(c("1"), "character")
  VariableStatistics177 <- VariableStatistics$new(agency=param_agency177, forInstanceVariable=param_forInstanceVariable177, hasCategoryStatistic=param_hasCategoryStatistic177, hasSummaryStatistic=param_hasSummaryStatistic177, id=param_id177, isPersistent=param_isPersistent177, isUniversallyUnique=param_isUniversallyUnique177, version=param_version177)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("1223"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1State:1"), "character")
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, forCodeItem=param_forCodeItem1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("2"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("967"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2State:1"), "character")
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, forCodeItem=param_forCodeItem2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("3"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("794"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3State:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("4"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("347"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4State:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

      param_content1 <- as(c("5"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue5 <- list(ValueString1)
      param_content1 <- as(c("378"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5State:1"), "character")
    CategoryStatistic5 <- CategoryStatistic$new(categoryValue=param_categoryValue5, forCodeItem=param_forCodeItem5, hasStatistic=param_hasStatistic5, typeOfCategoryStatistic=param_typeOfCategoryStatistic5)

      param_content1 <- as(c("6"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue6 <- list(ValueString1)
      param_content1 <- as(c("159"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6State:1"), "character")
    CategoryStatistic6 <- CategoryStatistic$new(categoryValue=param_categoryValue6, forCodeItem=param_forCodeItem6, hasStatistic=param_hasStatistic6, typeOfCategoryStatistic=param_typeOfCategoryStatistic6)

      param_content1 <- as(c("7"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue7 <- list(ValueString1)
      param_content1 <- as(c("31"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7State:1"), "character")
    CategoryStatistic7 <- CategoryStatistic$new(categoryValue=param_categoryValue7, forCodeItem=param_forCodeItem7, hasStatistic=param_hasStatistic7, typeOfCategoryStatistic=param_typeOfCategoryStatistic7)

      param_content1 <- as(c("8"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue8 <- list(ValueString1)
      param_content1 <- as(c("56"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8State:1"), "character")
    CategoryStatistic8 <- CategoryStatistic$new(categoryValue=param_categoryValue8, forCodeItem=param_forCodeItem8, hasStatistic=param_hasStatistic8, typeOfCategoryStatistic=param_typeOfCategoryStatistic8)

      param_content1 <- as(c("9"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue9 <- list(ValueString1)
      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9State:1"), "character")
    CategoryStatistic9 <- CategoryStatistic$new(categoryValue=param_categoryValue9, forCodeItem=param_forCodeItem9, hasStatistic=param_hasStatistic9, typeOfCategoryStatistic=param_typeOfCategoryStatistic9)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue10 <- list(ValueString1)
      param_content1 <- as(c("30.9"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1State:1"), "character")
    CategoryStatistic10 <- CategoryStatistic$new(categoryValue=param_categoryValue10, forCodeItem=param_forCodeItem10, hasStatistic=param_hasStatistic10, typeOfCategoryStatistic=param_typeOfCategoryStatistic10)

      param_content1 <- as(c("2"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue11 <- list(ValueString1)
      param_content1 <- as(c("24.5"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic11 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic11 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2State:1"), "character")
    CategoryStatistic11 <- CategoryStatistic$new(categoryValue=param_categoryValue11, forCodeItem=param_forCodeItem11, hasStatistic=param_hasStatistic11, typeOfCategoryStatistic=param_typeOfCategoryStatistic11)

      param_content1 <- as(c("3"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue12 <- list(ValueString1)
      param_content1 <- as(c("20.1"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic12 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic12 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3State:1"), "character")
    CategoryStatistic12 <- CategoryStatistic$new(categoryValue=param_categoryValue12, forCodeItem=param_forCodeItem12, hasStatistic=param_hasStatistic12, typeOfCategoryStatistic=param_typeOfCategoryStatistic12)

      param_content1 <- as(c("4"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue13 <- list(ValueString1)
      param_content1 <- as(c("8.8"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic13 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic13 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem13 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4State:1"), "character")
    CategoryStatistic13 <- CategoryStatistic$new(categoryValue=param_categoryValue13, forCodeItem=param_forCodeItem13, hasStatistic=param_hasStatistic13, typeOfCategoryStatistic=param_typeOfCategoryStatistic13)

      param_content1 <- as(c("5"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue14 <- list(ValueString1)
      param_content1 <- as(c("9.6"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic14 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic14 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem14 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5State:1"), "character")
    CategoryStatistic14 <- CategoryStatistic$new(categoryValue=param_categoryValue14, forCodeItem=param_forCodeItem14, hasStatistic=param_hasStatistic14, typeOfCategoryStatistic=param_typeOfCategoryStatistic14)

      param_content1 <- as(c("6"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue15 <- list(ValueString1)
      param_content1 <- as(c("4"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic15 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic15 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem15 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6State:1"), "character")
    CategoryStatistic15 <- CategoryStatistic$new(categoryValue=param_categoryValue15, forCodeItem=param_forCodeItem15, hasStatistic=param_hasStatistic15, typeOfCategoryStatistic=param_typeOfCategoryStatistic15)

      param_content1 <- as(c("7"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue16 <- list(ValueString1)
      param_content1 <- as(c("0.8"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic16 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic16 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem16 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7State:1"), "character")
    CategoryStatistic16 <- CategoryStatistic$new(categoryValue=param_categoryValue16, forCodeItem=param_forCodeItem16, hasStatistic=param_hasStatistic16, typeOfCategoryStatistic=param_typeOfCategoryStatistic16)

      param_content1 <- as(c("8"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue17 <- list(ValueString1)
      param_content1 <- as(c("1.4"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic17 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic17 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem17 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8State:1"), "character")
    CategoryStatistic17 <- CategoryStatistic$new(categoryValue=param_categoryValue17, forCodeItem=param_forCodeItem17, hasStatistic=param_hasStatistic17, typeOfCategoryStatistic=param_typeOfCategoryStatistic17)

      param_content1 <- as(c("9"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue18 <- list(ValueString1)
      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic18 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic18 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem18 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9State:1"), "character")
    CategoryStatistic18 <- CategoryStatistic$new(categoryValue=param_categoryValue18, forCodeItem=param_forCodeItem18, hasStatistic=param_hasStatistic18, typeOfCategoryStatistic=param_typeOfCategoryStatistic18)

  param_hasCategoryStatistic178 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4, CategoryStatistic5, CategoryStatistic6, CategoryStatistic7, CategoryStatistic8, CategoryStatistic9, CategoryStatistic10, CategoryStatistic11, CategoryStatistic12, CategoryStatistic13, CategoryStatistic14, CategoryStatistic15, CategoryStatistic16, CategoryStatistic17, CategoryStatistic18)
      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

  param_hasSummaryStatistic178 <- list(SummaryStatistic1, SummaryStatistic2)
  param_agency178 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable178 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarState:1"), "character")
  param_id178 <- as(c("IdVarStatsState"), "character")
  param_isPersistent178 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique178 <- as(c("FALSE"), "logical")
  param_version178 <- as(c("1"), "character")
  VariableStatistics178 <- VariableStatistics$new(agency=param_agency178, forInstanceVariable=param_forInstanceVariable178, hasCategoryStatistic=param_hasCategoryStatistic178, hasSummaryStatistic=param_hasSummaryStatistic178, id=param_id178, isPersistent=param_isPersistent178, isUniversallyUnique=param_isUniversallyUnique178, version=param_version178)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("ADELAIDE"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("32"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("ASTON"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("25"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("ADELAIDE"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("0.8"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1HardCopy:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("ASTON"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("0.6"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2Online:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

  param_hasCategoryStatistic179 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4)
      param_content1 <- as(c("3955"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("0"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

  param_hasSummaryStatistic179 <- list(SummaryStatistic1, SummaryStatistic2)
  param_agency179 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable179 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarDivision:1"), "character")
  param_id179 <- as(c("IdVarStatsDivision"), "character")
  param_isPersistent179 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique179 <- as(c("FALSE"), "logical")
  param_version179 <- as(c("1"), "character")
  VariableStatistics179 <- VariableStatistics$new(agency=param_agency179, forInstanceVariable=param_forInstanceVariable179, hasCategoryStatistic=param_hasCategoryStatistic179, hasSummaryStatistic=param_hasSummaryStatistic179, id=param_id179, isPersistent=param_isPersistent179, isUniversallyUnique=param_isUniversallyUnique179, version=param_version179)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch1:1"), "character")
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, forCodeItem=param_forCodeItem1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("2"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch2:1"), "character")
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, forCodeItem=param_forCodeItem2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("3"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch3:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("4"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch4:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

      param_content1 <- as(c("-1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue5 <- list(ValueString1)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeMinus1:1"), "character")
    CategoryStatistic5 <- CategoryStatistic$new(categoryValue=param_categoryValue5, forCodeItem=param_forCodeItem5, hasStatistic=param_hasStatistic5, typeOfCategoryStatistic=param_typeOfCategoryStatistic5)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue6 <- list(ValueString1)
      param_content1 <- as(c("1283"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch1:1"), "character")
    CategoryStatistic6 <- CategoryStatistic$new(categoryValue=param_categoryValue6, forCodeItem=param_forCodeItem6, hasStatistic=param_hasStatistic6, typeOfCategoryStatistic=param_typeOfCategoryStatistic6)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue7 <- list(ValueString1)
      param_content1 <- as(c("1515.3"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch2:1"), "character")
    CategoryStatistic7 <- CategoryStatistic$new(categoryValue=param_categoryValue7, forCodeItem=param_forCodeItem7, hasStatistic=param_hasStatistic7, typeOfCategoryStatistic=param_typeOfCategoryStatistic7)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue8 <- list(ValueString1)
      param_content1 <- as(c("887.8"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch3:1"), "character")
    CategoryStatistic8 <- CategoryStatistic$new(categoryValue=param_categoryValue8, forCodeItem=param_forCodeItem8, hasStatistic=param_hasStatistic8, typeOfCategoryStatistic=param_typeOfCategoryStatistic8)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue9 <- list(ValueString1)
      param_content1 <- as(c("236.1"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch4:1"), "character")
    CategoryStatistic9 <- CategoryStatistic$new(categoryValue=param_categoryValue9, forCodeItem=param_forCodeItem9, hasStatistic=param_hasStatistic9, typeOfCategoryStatistic=param_typeOfCategoryStatistic9)

      param_content1 <- as(c("-1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue10 <- list(ValueString1)
      param_content1 <- as(c("32.7"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeMinus1:1"), "character")
    CategoryStatistic10 <- CategoryStatistic$new(categoryValue=param_categoryValue10, forCodeItem=param_forCodeItem10, hasStatistic=param_hasStatistic10, typeOfCategoryStatistic=param_typeOfCategoryStatistic10)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue11 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("32.7"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic11 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic11 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch1:1"), "character")
    CategoryStatistic11 <- CategoryStatistic$new(categoryValue=param_categoryValue11, forCodeItem=param_forCodeItem11, hasStatistic=param_hasStatistic11, typeOfCategoryStatistic=param_typeOfCategoryStatistic11)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue12 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("38.6"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic12 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic12 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch2:1"), "character")
    CategoryStatistic12 <- CategoryStatistic$new(categoryValue=param_categoryValue12, forCodeItem=param_forCodeItem12, hasStatistic=param_hasStatistic12, typeOfCategoryStatistic=param_typeOfCategoryStatistic12)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue13 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("22.6"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic13 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic13 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem13 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch3:1"), "character")
    CategoryStatistic13 <- CategoryStatistic$new(categoryValue=param_categoryValue13, forCodeItem=param_forCodeItem13, hasStatistic=param_hasStatistic13, typeOfCategoryStatistic=param_typeOfCategoryStatistic13)

      param_content1 <- as(c("1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue14 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("6"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic14 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic14 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem14 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeNotMuch4:1"), "character")
    CategoryStatistic14 <- CategoryStatistic$new(categoryValue=param_categoryValue14, forCodeItem=param_forCodeItem14, hasStatistic=param_hasStatistic14, typeOfCategoryStatistic=param_typeOfCategoryStatistic14)

  param_hasCategoryStatistic180 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4, CategoryStatistic5, CategoryStatistic6, CategoryStatistic7, CategoryStatistic8, CategoryStatistic9, CategoryStatistic10, CategoryStatistic11, CategoryStatistic12, CategoryStatistic13, CategoryStatistic14)
      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("4"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3915"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("40"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("3922.287"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("32.713"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

  param_hasSummaryStatistic180 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6)
  param_agency180 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_applicableWeightVariable180 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
  param_forInstanceVariable180 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarA4:1"), "character")
  param_id180 <- as(c("IdVarStatsA4"), "character")
  param_isPersistent180 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique180 <- as(c("FALSE"), "logical")
  param_version180 <- as(c("1"), "character")
  VariableStatistics180 <- VariableStatistics$new(agency=param_agency180, applicableWeightVariable=param_applicableWeightVariable180, forInstanceVariable=param_forInstanceVariable180, hasCategoryStatistic=param_hasCategoryStatistic180, hasSummaryStatistic=param_hasSummaryStatistic180, id=param_id180, isPersistent=param_isPersistent180, isUniversallyUnique=param_isUniversallyUnique180, version=param_version180)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("3"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("27"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3723"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("232"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("16.496"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Mean"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("1.376"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("StdDev"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

      param_content1 <- as(c("3735.042"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic7 <- SummaryStatistic$new(hasStatistic=param_hasStatistic7, typeOfSummaryStatistic=param_typeOfSummaryStatistic7)

      param_content1 <- as(c("219.958"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic8 <- SummaryStatistic$new(hasStatistic=param_hasStatistic8, typeOfSummaryStatistic=param_typeOfSummaryStatistic8)

      param_content1 <- as(c("16.664"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Mean"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic9 <- SummaryStatistic$new(hasStatistic=param_hasStatistic9, typeOfSummaryStatistic=param_typeOfSummaryStatistic9)

      param_content1 <- as(c("1.344"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("StdDev"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic10 <- SummaryStatistic$new(hasStatistic=param_hasStatistic10, typeOfSummaryStatistic=param_typeOfSummaryStatistic10)

  param_hasSummaryStatistic181 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6, SummaryStatistic7, SummaryStatistic8, SummaryStatistic9, SummaryStatistic10)
  param_agency181 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_applicableWeightVariable181 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
  param_forInstanceVariable181 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarG1Age:1"), "character")
  param_id181 <- as(c("IdVarStatsG1Age"), "character")
  param_isPersistent181 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique181 <- as(c("FALSE"), "logical")
  param_version181 <- as(c("1"), "character")
  VariableStatistics181 <- VariableStatistics$new(agency=param_agency181, applicableWeightVariable=param_applicableWeightVariable181, forInstanceVariable=param_forInstanceVariable181, hasSummaryStatistic=param_hasSummaryStatistic181, id=param_id181, isPersistent=param_isPersistent181, isUniversallyUnique=param_isUniversallyUnique181, version=param_version181)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("1000"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("65"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("1111"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("61"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("1112"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("27"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("1210"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("16"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

      param_content1 <- as(c("-1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue5 <- list(ValueString1)
      param_content1 <- as(c("577"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeMinus1:1"), "character")
    CategoryStatistic5 <- CategoryStatistic$new(categoryValue=param_categoryValue5, forCodeItem=param_forCodeItem5, hasStatistic=param_hasStatistic5, typeOfCategoryStatistic=param_typeOfCategoryStatistic5)

      param_content1 <- as(c("1000"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue6 <- list(ValueString1)
      param_content1 <- as(c("60.2"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic6 <- CategoryStatistic$new(categoryValue=param_categoryValue6, hasStatistic=param_hasStatistic6, typeOfCategoryStatistic=param_typeOfCategoryStatistic6)

      param_content1 <- as(c("1111"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue7 <- list(ValueString1)
      param_content1 <- as(c("49"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic7 <- CategoryStatistic$new(categoryValue=param_categoryValue7, hasStatistic=param_hasStatistic7, typeOfCategoryStatistic=param_typeOfCategoryStatistic7)

      param_content1 <- as(c("1112"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue8 <- list(ValueString1)
      param_content1 <- as(c("27.3"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic8 <- CategoryStatistic$new(categoryValue=param_categoryValue8, hasStatistic=param_hasStatistic8, typeOfCategoryStatistic=param_typeOfCategoryStatistic8)

      param_content1 <- as(c("1210"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue9 <- list(ValueString1)
      param_content1 <- as(c("12.9"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic9 <- CategoryStatistic$new(categoryValue=param_categoryValue9, hasStatistic=param_hasStatistic9, typeOfCategoryStatistic=param_typeOfCategoryStatistic9)

      param_content1 <- as(c("-1"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue10 <- list(ValueString1)
      param_content1 <- as(c("547.9"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcodeMinus1:1"), "character")
    CategoryStatistic10 <- CategoryStatistic$new(categoryValue=param_categoryValue10, forCodeItem=param_forCodeItem10, hasStatistic=param_hasStatistic10, typeOfCategoryStatistic=param_typeOfCategoryStatistic10)

      param_content1 <- as(c("1000"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue11 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("1.8"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic11 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic11 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic11 <- CategoryStatistic$new(categoryValue=param_categoryValue11, hasStatistic=param_hasStatistic11, typeOfCategoryStatistic=param_typeOfCategoryStatistic11)

      param_content1 <- as(c("1111"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue12 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("1.4"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic12 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic12 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic12 <- CategoryStatistic$new(categoryValue=param_categoryValue12, hasStatistic=param_hasStatistic12, typeOfCategoryStatistic=param_typeOfCategoryStatistic12)

      param_content1 <- as(c("1112"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue13 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("0.8"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic13 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic13 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic13 <- CategoryStatistic$new(categoryValue=param_categoryValue13, hasStatistic=param_hasStatistic13, typeOfCategoryStatistic=param_typeOfCategoryStatistic13)

      param_content1 <- as(c("1210"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue14 <- list(ValueString1)
      param_computationBase1 <- as(c("ValidOnly"), "character")
      param_content1 <- as(c("0.4"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(computationBase=param_computationBase1, content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic14 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic14 <- list(ExternalControlledVocabularyEntry1)
    CategoryStatistic14 <- CategoryStatistic$new(categoryValue=param_categoryValue14, hasStatistic=param_hasStatistic14, typeOfCategoryStatistic=param_typeOfCategoryStatistic14)

  param_hasCategoryStatistic182 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4, CategoryStatistic5, CategoryStatistic6, CategoryStatistic7, CategoryStatistic8, CategoryStatistic9, CategoryStatistic10, CategoryStatistic11, CategoryStatistic12, CategoryStatistic13, CategoryStatistic14)
      param_content1 <- as(c("1000"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("9999"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3378"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("577"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("3407.124"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("547.876"), "numeric")
      param_isWeighted1 <- as(c("TRUE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

  param_hasSummaryStatistic182 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6)
  param_agency182 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_applicableWeightVariable182 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
  param_forInstanceVariable182 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarXg5:1"), "character")
  param_id182 <- as(c("IdVarStatsXg5"), "character")
  param_isPersistent182 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique182 <- as(c("FALSE"), "logical")
  param_version182 <- as(c("1"), "character")
  VariableStatistics182 <- VariableStatistics$new(agency=param_agency182, applicableWeightVariable=param_applicableWeightVariable182, forInstanceVariable=param_forInstanceVariable182, hasCategoryStatistic=param_hasCategoryStatistic182, hasSummaryStatistic=param_hasSummaryStatistic182, id=param_id182, isPersistent=param_isPersistent182, isUniversallyUnique=param_isUniversallyUnique182, version=param_version182)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("0.360120221577392"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("6.65336083134877"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3955"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("0"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("1"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Mean"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("0.556"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("StdDev"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

  param_hasSummaryStatistic183 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6)
  param_agency183 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable183 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarWeight:1"), "character")
  param_id183 <- as(c("IdVarStatsWeight"), "character")
  param_isPersistent183 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique183 <- as(c("FALSE"), "logical")
  param_version183 <- as(c("1"), "character")
  VariableStatistics183 <- VariableStatistics$new(agency=param_agency183, forInstanceVariable=param_forInstanceVariable183, hasSummaryStatistic=param_hasSummaryStatistic183, id=param_id183, isPersistent=param_isPersistent183, isUniversallyUnique=param_isUniversallyUnique183, version=param_version183)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("ALP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue1 <- list(ValueString1)
      param_content1 <- as(c("1392"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem1 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1PartyAby:1"), "character")
    CategoryStatistic1 <- CategoryStatistic$new(categoryValue=param_categoryValue1, forCodeItem=param_forCodeItem1, hasStatistic=param_hasStatistic1, typeOfCategoryStatistic=param_typeOfCategoryStatistic1)

      param_content1 <- as(c("CLP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue2 <- list(ValueString1)
      param_content1 <- as(c("19"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem2 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2PartyAby:1"), "character")
    CategoryStatistic2 <- CategoryStatistic$new(categoryValue=param_categoryValue2, forCodeItem=param_forCodeItem2, hasStatistic=param_hasStatistic2, typeOfCategoryStatistic=param_typeOfCategoryStatistic2)

      param_content1 <- as(c("GRN"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue3 <- list(ValueString1)
      param_content1 <- as(c("25"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem3 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3PartyAby:1"), "character")
    CategoryStatistic3 <- CategoryStatistic$new(categoryValue=param_categoryValue3, forCodeItem=param_forCodeItem3, hasStatistic=param_hasStatistic3, typeOfCategoryStatistic=param_typeOfCategoryStatistic3)

      param_content1 <- as(c("IND"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue4 <- list(ValueString1)
      param_content1 <- as(c("57"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem4 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4PartyAby:1"), "character")
    CategoryStatistic4 <- CategoryStatistic$new(categoryValue=param_categoryValue4, forCodeItem=param_forCodeItem4, hasStatistic=param_hasStatistic4, typeOfCategoryStatistic=param_typeOfCategoryStatistic4)

      param_content1 <- as(c("KAP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue5 <- list(ValueString1)
      param_content1 <- as(c("32"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem5 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5PartyAby:1"), "character")
    CategoryStatistic5 <- CategoryStatistic$new(categoryValue=param_categoryValue5, forCodeItem=param_forCodeItem5, hasStatistic=param_hasStatistic5, typeOfCategoryStatistic=param_typeOfCategoryStatistic5)

      param_content1 <- as(c("LNP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue6 <- list(ValueString1)
      param_content1 <- as(c("574"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem6 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6PartyAby:1"), "character")
    CategoryStatistic6 <- CategoryStatistic$new(categoryValue=param_categoryValue6, forCodeItem=param_forCodeItem6, hasStatistic=param_hasStatistic6, typeOfCategoryStatistic=param_typeOfCategoryStatistic6)

      param_content1 <- as(c("LP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue7 <- list(ValueString1)
      param_content1 <- as(c("1578"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic7 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic7 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem7 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7PartyAby:1"), "character")
    CategoryStatistic7 <- CategoryStatistic$new(categoryValue=param_categoryValue7, forCodeItem=param_forCodeItem7, hasStatistic=param_hasStatistic7, typeOfCategoryStatistic=param_typeOfCategoryStatistic7)

      param_content1 <- as(c("NP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue8 <- list(ValueString1)
      param_content1 <- as(c("247"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic8 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic8 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem8 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8PartyAby:1"), "character")
    CategoryStatistic8 <- CategoryStatistic$new(categoryValue=param_categoryValue8, forCodeItem=param_forCodeItem8, hasStatistic=param_hasStatistic8, typeOfCategoryStatistic=param_typeOfCategoryStatistic8)

      param_content1 <- as(c("PUP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue9 <- list(ValueString1)
      param_content1 <- as(c("31"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic9 <- list(Statistic1)
      param_content1 <- as(c("Count"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic9 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem9 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9PartyAby:1"), "character")
    CategoryStatistic9 <- CategoryStatistic$new(categoryValue=param_categoryValue9, forCodeItem=param_forCodeItem9, hasStatistic=param_hasStatistic9, typeOfCategoryStatistic=param_typeOfCategoryStatistic9)

      param_content1 <- as(c("ALP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue10 <- list(ValueString1)
      param_content1 <- as(c("35.2"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic10 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic10 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem10 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode1PartyAby:1"), "character")
    CategoryStatistic10 <- CategoryStatistic$new(categoryValue=param_categoryValue10, forCodeItem=param_forCodeItem10, hasStatistic=param_hasStatistic10, typeOfCategoryStatistic=param_typeOfCategoryStatistic10)

      param_content1 <- as(c("CLP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue11 <- list(ValueString1)
      param_content1 <- as(c("0.5"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic11 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic11 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem11 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode2PartyAby:1"), "character")
    CategoryStatistic11 <- CategoryStatistic$new(categoryValue=param_categoryValue11, forCodeItem=param_forCodeItem11, hasStatistic=param_hasStatistic11, typeOfCategoryStatistic=param_typeOfCategoryStatistic11)

      param_content1 <- as(c("GRN"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue12 <- list(ValueString1)
      param_content1 <- as(c("0.6"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic12 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic12 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem12 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode3PartyAby:1"), "character")
    CategoryStatistic12 <- CategoryStatistic$new(categoryValue=param_categoryValue12, forCodeItem=param_forCodeItem12, hasStatistic=param_hasStatistic12, typeOfCategoryStatistic=param_typeOfCategoryStatistic12)

      param_content1 <- as(c("IND"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue13 <- list(ValueString1)
      param_content1 <- as(c("1.4"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic13 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic13 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem13 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode4PartyAby:1"), "character")
    CategoryStatistic13 <- CategoryStatistic$new(categoryValue=param_categoryValue13, forCodeItem=param_forCodeItem13, hasStatistic=param_hasStatistic13, typeOfCategoryStatistic=param_typeOfCategoryStatistic13)

      param_content1 <- as(c("KAP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue14 <- list(ValueString1)
      param_content1 <- as(c("0.8"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic14 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic14 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem14 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode5PartyAby:1"), "character")
    CategoryStatistic14 <- CategoryStatistic$new(categoryValue=param_categoryValue14, forCodeItem=param_forCodeItem14, hasStatistic=param_hasStatistic14, typeOfCategoryStatistic=param_typeOfCategoryStatistic14)

      param_content1 <- as(c("LNP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue15 <- list(ValueString1)
      param_content1 <- as(c("14.5"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic15 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic15 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem15 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode6PartyAby:1"), "character")
    CategoryStatistic15 <- CategoryStatistic$new(categoryValue=param_categoryValue15, forCodeItem=param_forCodeItem15, hasStatistic=param_hasStatistic15, typeOfCategoryStatistic=param_typeOfCategoryStatistic15)

      param_content1 <- as(c("LP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue16 <- list(ValueString1)
      param_content1 <- as(c("39.9"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic16 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic16 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem16 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode7PartyAby:1"), "character")
    CategoryStatistic16 <- CategoryStatistic$new(categoryValue=param_categoryValue16, forCodeItem=param_forCodeItem16, hasStatistic=param_hasStatistic16, typeOfCategoryStatistic=param_typeOfCategoryStatistic16)

      param_content1 <- as(c("NP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue17 <- list(ValueString1)
      param_content1 <- as(c("6.2"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic17 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic17 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem17 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode8PartyAby:1"), "character")
    CategoryStatistic17 <- CategoryStatistic$new(categoryValue=param_categoryValue17, forCodeItem=param_forCodeItem17, hasStatistic=param_hasStatistic17, typeOfCategoryStatistic=param_typeOfCategoryStatistic17)

      param_content1 <- as(c("PUP"), "character")
      ValueString1 <- ValueString$new(content=param_content1)

    param_categoryValue18 <- list(ValueString1)
      param_content1 <- as(c("0.8"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic18 <- list(Statistic1)
      param_content1 <- as(c("Percentage"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfCategoryStatistic18 <- list(ExternalControlledVocabularyEntry1)
    param_forCodeItem18 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IDcode9PartyAby:1"), "character")
    CategoryStatistic18 <- CategoryStatistic$new(categoryValue=param_categoryValue18, forCodeItem=param_forCodeItem18, hasStatistic=param_hasStatistic18, typeOfCategoryStatistic=param_typeOfCategoryStatistic18)

  param_hasCategoryStatistic184 <- list(CategoryStatistic1, CategoryStatistic2, CategoryStatistic3, CategoryStatistic4, CategoryStatistic5, CategoryStatistic6, CategoryStatistic7, CategoryStatistic8, CategoryStatistic9, CategoryStatistic10, CategoryStatistic11, CategoryStatistic12, CategoryStatistic13, CategoryStatistic14, CategoryStatistic15, CategoryStatistic16, CategoryStatistic17, CategoryStatistic18)
      param_content1 <- as(c("3955"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("0"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

  param_hasSummaryStatistic184 <- list(SummaryStatistic1, SummaryStatistic2)
  param_agency184 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable184 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarPartyAby:1"), "character")
  param_id184 <- as(c("IdVarStatsPartyAby"), "character")
  param_isPersistent184 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique184 <- as(c("FALSE"), "logical")
  param_version184 <- as(c("1"), "character")
  VariableStatistics184 <- VariableStatistics$new(agency=param_agency184, forInstanceVariable=param_forInstanceVariable184, hasCategoryStatistic=param_hasCategoryStatistic184, hasSummaryStatistic=param_hasSummaryStatistic184, id=param_id184, isPersistent=param_isPersistent184, isUniversallyUnique=param_isUniversallyUnique184, version=param_version184)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^




#  vvvvv ------ Beginning elment:   VariableStatistics ------ vvvvv

      param_content1 <- as(c("-14.3000001907349"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic1 <- list(Statistic1)
      param_content1 <- as(c("Minimum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic1 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic1 <- SummaryStatistic$new(hasStatistic=param_hasStatistic1, typeOfSummaryStatistic=param_typeOfSummaryStatistic1)

      param_content1 <- as(c("49.9700012207031"), "numeric")
      Statistic1 <- Statistic$new(content=param_content1)

    param_hasStatistic2 <- list(Statistic1)
      param_content1 <- as(c("Maximum"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic2 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic2 <- SummaryStatistic$new(hasStatistic=param_hasStatistic2, typeOfSummaryStatistic=param_typeOfSummaryStatistic2)

      param_content1 <- as(c("3955"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic3 <- list(Statistic1)
      param_content1 <- as(c("ValidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic3 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic3 <- SummaryStatistic$new(hasStatistic=param_hasStatistic3, typeOfSummaryStatistic=param_typeOfSummaryStatistic3)

      param_content1 <- as(c("0"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic4 <- list(Statistic1)
      param_content1 <- as(c("InvalidCount"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic4 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic4 <- SummaryStatistic$new(hasStatistic=param_hasStatistic4, typeOfSummaryStatistic=param_typeOfSummaryStatistic4)

      param_content1 <- as(c("0.93"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic5 <- list(Statistic1)
      param_content1 <- as(c("Mean"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic5 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic5 <- SummaryStatistic$new(hasStatistic=param_hasStatistic5, typeOfSummaryStatistic=param_typeOfSummaryStatistic5)

      param_content1 <- as(c("10.04"), "numeric")
      param_isWeighted1 <- as(c("FALSE"), "logical")
      Statistic1 <- Statistic$new(content=param_content1, isWeighted=param_isWeighted1)

    param_hasStatistic6 <- list(Statistic1)
      param_content1 <- as(c("StdDev"), "character")
      ExternalControlledVocabularyEntry1 <- ExternalControlledVocabularyEntry$new(content=param_content1)

    param_typeOfSummaryStatistic6 <- list(ExternalControlledVocabularyEntry1)
    SummaryStatistic6 <- SummaryStatistic$new(hasStatistic=param_hasStatistic6, typeOfSummaryStatistic=param_typeOfSummaryStatistic6)

  param_hasSummaryStatistic185 <- list(SummaryStatistic1, SummaryStatistic2, SummaryStatistic3, SummaryStatistic4, SummaryStatistic5, SummaryStatistic6)
  param_agency185 <- as(c("dagstuhl17423.ddialliance.org"), "character")
  param_forInstanceVariable185 <- as(c("URN:DDI:dagstuhl17423.ddialliance.org:IdVarSwingn:1"), "character")
  param_id185 <- as(c("IdVarStatsSwingn"), "character")
  param_isPersistent185 <- as(c("FALSE"), "logical")
  param_isUniversallyUnique185 <- as(c("FALSE"), "logical")
  param_version185 <- as(c("1"), "character")
  VariableStatistics185 <- VariableStatistics$new(agency=param_agency185, forInstanceVariable=param_forInstanceVariable185, hasSummaryStatistic=param_hasSummaryStatistic185, id=param_id185, isPersistent=param_isPersistent185, isUniversallyUnique=param_isUniversallyUnique185, version=param_version185)

#  ^^^^^ ------ Ending elment:   VariableStatistics ------ ^^^^^


#____________________________ Validating registered objects ______________________

validateRegistry()
